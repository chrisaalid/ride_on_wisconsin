<?php
App::uses('AppController', 'Controller');
App::uses('CakeSession', 'Model/Datasource');

class AjaxController extends AppController {
	//public $components = array(
	//	'Session',
	//	'Auth' => array(
	//		'loginAction' => array('controller' => 'ajax', 'action' => 'login'),
	//		'loginRedirect' => array('controller' => 'users', 'action' => 'index'),
	//		'logoutRedirect' => array('controller' => 'users', 'action' => 'login'),
	//		'authorize' => array('Controller') 
	//	)
	//);
	
	function beforeFilter() {
		$this->Auth->allow(array('status','login','logout'));
		parent::beforeFilter();
		$this->autoRedirect = false;
	}
	
	function beforeRender() {
		parent::beforeRender();
		
		$this->layout = 'ajax';
		$this->status = $this->status === false ? false : true;
		
		$this->set('message', $this->message);
		$this->set('status', $this->status);
		$this->set('request', $this->req);
		$this->set('payload', $this->payload);
	}
	
	function logout() {
		$this->Auth->logout();
		$this->payload->user = null;
		$this->payload->logged_in = false;
		$this->message = 'logged out';
		$this->render('call');
	}
	
	function status() {
		$this->payload->user = $this->Auth->user();
		$this->payload->logged_in = $this->payload->user !== null;
		$this->render('call');
	}
	
	function login() {
		
		//$this->payload->logged_in = false;
		$this->message = "Invalid username or password";
		
    if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				$liu = $this->Auth->user();
				$this->message = "Logged in";
				$this->payload->logged_in = true;
				$this->payload->user = $liu; //$liu['username'];
			} else {
				//$this->Session->setFlash(__('Invalid username or password, try again'));
			}
    }
		
		$this->render('call');
	}
	
	public function isAuthorized($user) {
		//return true;
		
		$ok_actions = array('login', 'logout', 'register', 'call');
		
    if (in_array($this->action, $ok_actions)) {
      return true;
    } else {
			if ($user !== null) {
				return true;
			} else {
				return false;
			}
		}
    //return parent::isAuthorized($user);
	}
	
}

?>