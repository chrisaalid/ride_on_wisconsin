<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Controller', 'Controller');
App::uses('UserContentController', 'Controller');
App::uses('UploadController', 'Controller');
App::uses('Model', 'User');
App::uses('Model', 'Upload');
App::uses('Model', 'Setting');
App::uses('Sanitize', 'Utility');

// For CakePdf HTML -> PDF


/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	public $components = array(
		'RequestHandler',
		'Session',
		'Uploader',
		'SchemaParser',
		'Auth' => array(
			'loginAction' => array('controller' => 'users', 'action' => 'login'),
			'loginRedirect' => array('controller' => 'users', 'action' => 'index'),
			'logoutRedirect' => array('controller' => 'users', 'action' => 'login'),
			'authorize' => array('Controller') 
		)
	);
	
	public $helpers = array(
	  'FieldFormatter',
	  'EmailImage',
	  'Link',
	  'Html'
	);
	
	public $layout_finalized = false;
	
	public $action_requires = array(
		'schema' => 'developer',
		'dump' => 'developer'
	);
	
	protected $model_name;
	protected $payload;
	protected $resp;
	protected $status;
	protected $req;
	protected $user;
	protected $authChecked = false;
	protected $authorized = false;
	protected $is_dialog = false;
	public $is_admin = false;
	public $is_developer = false;
	public $uploaded_files = array();
	public $current_id = false;
	
	public $add_mode = false;
	public $edit_mode = false;
	
	public $edit_resets = false;
	
	public function setActionRequirements($action = 'index', $needs = 'admin') {
	  if (is_array($action)) {
		foreach ($action as $a => $n) {
		  $this->setActionRequirements($a, $n);
		}
	  } else {
		$this->action_requires[$action] = $needs;
	  }
	}
	
	public function isAuthorized() {
	  return $this->authCheck();
	}
	
	public function authCheck() {
	  $user = $this->Auth->user();
	  
	  if (empty($user)) {
			$user = array();
	  }
	  
	  if (array_key_exists('User', $user)) {
			$user = $user['User'];
	  }
	  
	  if ($this->authChecked) {
			return $this->authorized;
	  }
	  $allowed = false;
	  
	  $action_requires_list = $this->action_requires;
	  
	  
	  
	  
	  $action = $this->action;
	  $id = count($this->request->params['pass']) > 0 ? $this->request->params['pass'][0] : false;
	  if ($id == 'me') {
			if ($this->Auth->loggedIn()) {
				$id = $this->user['id'];
			} else {
				$id = false;
			}
	  } 
	  
	  //pr($id);
	  //pr($this->request->params);
	  //exit;
	  
	  
	  
	  $requires = array_key_exists($action, $action_requires_list) ? $action_requires_list[$action] : 'admin';
	  $logged_in = !empty($user);
	  $admin = $logged_in ? ($user['admin'] ? true : false) : false;
	  
	  if (!$logged_in) {
			if ($requires == 'any') {
				$allowed = true;
			}
	  } else {
			$allowed = true;
			if ($requires === 'developer' || $this->is_developer) {
				$allowed = $this->is_developer;
			} elseif ($requires === 'admin' || $admin) {
				$allowed = $admin;
			} elseif ($requires === 'own') {
				$options = array('conditions' => array($this->model_name . '.id' => $id));
				$record =  $this->{$this->model_name}->find('first', $options);  
				if ($this->model_name === 'User') {
					if (array_key_exists('User', $record) && array_key_exists('id', $record['User'])) {
						$allowed = $record['User']['id'] == $user['id'];
					} else {
						$allowed = false;
					}
				} else {
					$allowed = $record[$this->model_name]['user_id'] == $user['id'];
				}
			}
	  }
	  
	  if (!$allowed) {
			if ($requires == 'logged') {
				$path = $this->is_dialog ? '/dialog/' : '/';
				$path .= 'users/login';
			} else {
				$this->error("That action is not allowed");
			}
	  } else {
			$this->Auth->allow($action);
	  }
	  
	  $this->authorized = $allowed;
	  $this->authChecked = true;
	  
	  return $allowed;
	}
	
	function manageOwnership() {
	  if ($this->Auth->loggedIn()) {
			if ($this->is_admin) {
				if (array_key_exists($this->modelClass, $this->data)) {
					if ($this->add_mode) {
						$this->request->data[$this->modelClass]['user_id'] = $this->user['id'];
					} 
				}
			} else {
				// user's can't change ownership
				if (array_key_exists($this->modelClass, $this->data)) {
					if ($this->add_mode) {
						$this->request->data[$this->modelClass]['user_id'] = $this->user['id'];
					} else {
						unset($this->request->data[$this->modelClass]['user_id']);
					}
				}
				
				// prevent users from approving their own content
				if (array_key_exists($this->modelClass, $this->data) && array_key_exists('approved', $this->data[$this->modelClass])) {
					unset($this->request->data[$this->modelClass]['approved']);
				}
			}
	  }
	}
	
	
	function fullDebug() {
	  if (Configure::read('debug') >= 2) {
			ini_set('display_errors',1);
			ini_set('display_startup_errors',1);
			error_reporting(-1);
	  }
	}
	
	function beforeFilter() {
		$this->fullDebug();
		$this->loadModel('User');
		$this->loadModel('Upload');
		$this->loadModel('Event');
		$this->loadModel('Setting');
	  
		$this->Event->regenIfSearchOld();
		
		$model_name = $this->modelClass;
		$this->model_name = $model_name;
		$this->edit_mode = $this->request->action == 'edit';
		$this->add_mode = $this->request->action == 'add' || $this->request->action == 'import';
		
		$this->user = $this->Auth->user();
		$this->is_admin = false;
		if (is_array($this->user)) {
		  if (array_key_exists('User', $this->user)) {
			$this->user = $this->user['User'];
		  }
		  
		  $this->is_admin = array_key_exists('admin', $this->user) && $this->user['admin'] ? true : false;
		  $this->is_developer = array_key_exists('developer', $this->user) && $this->user['developer'] ? true : false;
		} else {
		  $this->user = array();
		}
		
		$this->Session->write('Auth.User', $this->user);
		$this->Session->write('Action', $this->action);
		
		$this->is_dialog = true;
		$edit_resets = false;
		if ($this->Auth->loggedIn()) {
		  if ($this->user['admin']) {
			if ($this->Session->read('use_dialogs') != 1) {
				$this->is_dialog = false;
			}
			
		  } else {
			if ($this->edit_mode) {
				$this->edit_resets = true;
			}
		  }
		}
		
		$this->manageOwnership();
		
		if (array_key_exists('Upload', $this->{$this->modelClass}->hasMany)) {
			$this->Uploader->handle();
		}
		
		$this->$model_name->is_admin = $this->is_admin;
		
		$this->setViewVars();
		
		
		$this->set('perms', $this->action_requires);
		
		$this->set('error', false); 
		
		if ($this->authCheck()) {
		  //$this->Auth->allow($this->request->action);
		  //pr($this->Auth->allowedActions); exit(0);
		} else {
			if ($this->Auth->loggedIn()) {
				$this->redirect(array('controller'=>'users','action'=>'view', $this->user['id']));
			} else {
				$this->redirect(array('controller'=>'users','action'=>'login'));
			}
		}
	}
	
	private function setRecordID() {
	  $id = false;
	  $owner = false;
	  if ($this->request->action == 'view' || $this->request->action == 'edit') {
			$p = $this->request->params['pass'];
			
			if (count($p) > 0 && preg_match('/^\d+$/', $p[0])) {
				$id = $p[0] * 1;
			
				$ul = $this->{$this->modelClass}->find('first', array('conditions' => array('id' => $id), 'recursive' => -1));
				//pr($ul); exit(0);
				if ($this->Auth->loggedIn() && array_key_exists($this->modelClass, $ul) && array_key_exists('user_id', $ul[$this->modelClass]) && $ul[$this->modelClass]['user_id'] == $this->user['id']) {
					$owner = true;
				}
				
			}
	  }

	  $this->current_id = $id;
	  $this->set('CURRENT_ID', $id);
	  $this->set('CONTENT_OWNER', $owner);
	}
	
	private function getSettingsForView() {
	  $settings = $this->Setting->find('all');
	  $new_settings = array();
	  foreach ($settings as $idx => $s) {
			$new_settings[$s['Setting']['key']] = $s['Setting'];
	  }
	  $this->set('SETTINGS', $new_settings);
	}
	
	private function setViewActionVars() {
	  $obj =  $this->{$this->modelClass}->find('first', array('conditions' => array($this->modelClass . '.id' => $this->current_id)));
	  $content_title = '';
	  
	  
	  if (array_key_exists($this->modelClass, $obj)) {
			$disp_field = isset($this->{$this->modelClass}->displayField) ? $this->{$this->modelClass}->displayField : false;
			if ($disp_field !== false) {
				$content_title = $obj[$this->modelClass][$disp_field];
			} else {
				if (array_key_exists('title', $obj[$this->modelClass])) {
					$content_title = $obj[$this->modelClass]['title'];
				} elseif (array_key_exists('name', $obj[$this->modelClass])) {
					$content_title = $obj[$this->modelClass]['name'];
				}
			} 
	  }
	  
	  $this->set('content_title', $content_title);
	}
	
	private function setViewVars() {
	  
	  $this->setRecordID();
	  
	  $this->set('logged_in_user', $this->user);
	  $this->set('logged_in', $this->Auth->loggedIn());
	  $this->set('is_admin', $this->is_admin);
	  $this->set('is_developer', $this->is_developer);
	  $this->set('is_dialog', $this->is_dialog);
	  $this->set('referer', $this->referer());
	  $this->set('CURRENT_MODEL', $this->modelClass);
	  $this->set('CURRENT_CONTROLLER', strtolower(Inflector::pluralize($this->modelClass)));
	  $this->set('CURRENT_ACTION', $this->request->action);
	  $this->set('CURRENT_PARAMS', $this->request->pass);
	  $this->set('UPLOADS', $this->Upload);
	  $this->set('MODEL_OBJ', $this->{$this->modelClass});
	  $this->set('CONTROLLER_OBJ', $this);
	  $this->set('edit_mode', $this->edit_mode);
	  $this->set('add_mode', $this->add_mode);
	  $this->set('edit_resets', $this->edit_resets);
	  
	  if ($this->request->action == 'view') {
		$this->setViewActionVars();
	  }
	  
	  $this->getSettingsForView();
	  
	}
	
	private function changeApproval($id = null, $approved = true, $redirect = true) {
	  $model = $this->modelClass;
	  $model_lc = strtolower($model);
	
	  if (!$this->$model->exists($id)) {
			throw new NotFoundException(__("Invalid $model"));
	  }
	  
	  //$options = array('conditions' => array("$model." . $this->$model->primaryKey => $id), 'recursive' => -1);
	  //$this->request->data = $this->$model->find('first', $options);
	  
	  
	  //$this->request->data[$model]['approved'] = $approved === true ? 1 : 0;
	  
	  $data = array($model => array(
			'id' => $id,
			'approved' => $approved
	  ));
	  
	  $success = $this->$model->save($data);
	  
	//  if ($success) {
	//		//$this->Session->setFlash(__("The $model_lc has been " . ($approved === true ? 'approved' : 'disapproved')));
	//  } else {
	//		//$this->Session->setFlash(__("The $model_lc could not be saved. Please, try again."));
	//		//print json_encode($this->validationErrors); exit(0);
	//  }
	  
	//  if ($redirect) {
	//	$this->redirect(array('action' => 'index'));
	//  }
	  
	  return $success;
	}
	
	function searchableIndex() {
	  $query = $this->indexSearchTerms();
	  $show_search = count($query) > 0;
	  $this->set('show_search', $show_search);
	  $this->set('is_searchable', true);
	  return $query;
	}
	
	function indexSearchTerms($fields = array()) { 
	  if (count($fields) < 1 && isset($this->{$this->model_name}->indexSearchFields)) {
		$fields = $this->{$this->model_name}->indexSearchFields;
	  }
	  
	  $terms = array();
	  if (array_key_exists($this->model_name, $this->data)) {
		if (array_key_exists('Terms', $this->data[$this->model_name])) {
		  $search = strtolower($this->data[$this->model_name]['Terms']);
		  if (count($fields) > 0) {
			foreach ($fields as $f) {
			  $terms['Lower(' . $this->model_name . '.' . $f . ') LIKE'] = '%' . $search . '%';
			}
		  }
		}
	  }
	  
	  if (count($terms) > 0) {
		$terms = array('OR' => $terms);
	  }
	  
	  return $terms;
	}
	
	function batch($action = 'approve') {
	  $model = $this->modelClass;
	  $batch = array_key_exists('batch', $this->request->data) ? $this->request->data['batch'] : array();
	  
	  $resp = new stdClass();
	  $resp->good = array();
	  $resp->bad = array();
	  $resp->success = true;
	  $resp->message = '...';
	  
	  foreach ($batch as $rec) {
	
		
		if ($action == 'approve') {
		  if ($this->changeApproval($rec, true, false)) {
			$resp->good[] = $rec;
		  } else {
			$resp->bad[] = $rec;
		  }
		} elseif ($action == 'disapprove') {
		  if ($this->changeApproval($rec, false, false)) {
			$resp->good[] = $rec;
		  } else {
			$resp->bad[] = $rec;
		  }
		} else {
		  $msg = 'Unknown action  "' . $action . '"';
		  $resp->success = false;
		  break;
		}
		
		//$data[$model][] = $r_ar;
	  }
	  
	  //$this->Session->delete('Message.flash');
	  
	  $gc = count($resp->good);
	  $bc = count($resp->bad);
	  
	  $msg = "Successfully updated {$gc} record" . ($gc != 1 ? 's' : '') . '.';
	  if ($bc > 0) {
		$msg .= "  {$bc} record update" . ($bc != 1 ? 's' : '') . ' failed.';
	  }
	
	  $resp->errors = $this->validationErrors || false;
  
	  
	  $this->Session->setFlash($msg);
	  $resp->message = $msg;

	  
	  $this->set('resp', $resp);
	  $this->render('/Elements/ajax');
	  
	  //print json_encode($resp); exit(0);
	  
	  
	  //pr($this->request);
	  //exit(0);
	}
	
	function approve($id = null, $redirect = true) {
	  $model_lc = strtolower($this->modelClass);
	  
	  if ($this->changeApproval($id, true, $redirect)) {
			$this->Session->setFlash(__("The $model_lc has been approved"));
	  } else {
			$this->Session->setFlash(__("The $model_lc could not be saved. Please, try again."));
	  }
	  
	  if ($redirect) {
		$this->redirect(array('action' => 'index'));
	  }
	  
	}
	
	function disapprove($id = null, $redirect = true) {
	  $model_lc = strtolower($this->modelClass);
	  
	  if ($this->changeApproval($id, false, $redirect)) {
		$this->Session->setFlash(__("The $model_lc has been disapproved"));
	  } else {
		$this->Session->setFlash(__("The $model_lc could not be saved. Please, try again."));
	  }
	  
	  if ($redirect) {
		$this->redirect(array('action' => 'index'));
	  }
	  //$this->changeApproval($id, false, $redirect);
	}
	
	
	function content($id = null) {
	  $this->RequestHandler->respondAs('text/plain');
	  
	  $model = $this->modelClass;
	  $model_lc = strtolower($model);

	  $this->layout = 'ajax';
	  
	  $results = $this->$model->getContentForUser($id);
	  $this->set('json_obj', $results);
	  
	  $this->render('/json');
	}
	
	function useDialogs($use = true) {
	  $use = $use === false ? false : true;
	  $this->Session->write('use_dialogs', $use);
	}
	
	function beforeRender() {
	  $admin_view_path = APP . 'View' . DS . $this->viewPath . DS . $this->action . '.ctp';
	  
	  
	  
	  if (!$this->layout_finalized && $this->is_dialog) {
		if ($this->name == 'CakeError' || $this->name == 'Error') {
		  $this->redirect(array('controller'=>'contents', 'action' => 'dialog', 'error'));
		}
		
		$this->layout_finalized = true;
		$this->layout = 'dialog';
		$this->viewPath .= '/dialogs';
		$dialog_view_path = APP . 'View' . DS . $this->viewPath . DS . $this->action . '.ctp';
		if (!file_exists($dialog_view_path)) {

		  if ($this->Auth->loggedIn()) {
			$this->redirect(array('controller'=>'users', 'action'=>'view', $this->user['id']));
		  } else {
			$this->redirect(array('controller'=>'users','action'=>'login'));
		  }
		}
	  }/* else {
		if (!file_exists($admin_view_path)) {
		  $this->useDialogs(true);
		  $this->redirect($this->request->here);
		  $this->redirect('/#!/' . $this->request->here);
		}
	  }*/
	}
	
	function error($err) {
	  $this->set('error', $err);
	}
	
	protected function debug($msg, $obj = null, $fatal = false) {
	  print "<h1>$msg</h1>";
	  debug($obj);
	  if ($fatal) { exit(0); }
	}
	
	public function add() {
	  
	}
	
	public function dump() {
	  
	  $sql = array();
	  
	  $model = $this->modelClass;
	  
	  $table = ucfirst(strtolower(Inflector::pluralize($model)));
	  
	  $records = $this->$model->find('all', array('recursive' => -1));
	  
	  if (count($records) > 0) {
		
		$field_details = $this->$model->getColumnTypes();
		$field_list = array_keys($field_details);
		$fields = '  `' . join("`,\n  `", $field_list) . '`';
		$sql[] = "INSERT INTO `{$table}` (\n{$fields}\n) VALUES";
		
		$sql_records = array();
		
		foreach ($records as $idx => $rec) {
		  $r = $rec[$model];
		  
		  $values = array();
		  
		  foreach ($field_list as $fld) {
				$val = array_key_exists($fld, $r) && !is_array($r[$fld]) ? $r[$fld] : '"-------"';
				$q = "'";
				if ($field_details[$fld] == 'boolean') {
					$val = $val == 1 ? 1 : 0;
				}
				if (preg_match('/^\d+$/', $val)) {
					$q = '';
				}
				
				$esc_val = Sanitize::escape($val, 'default');
				$values[] = $q . $esc_val . $q;
		  }
		  
		  $sql_records[] = "(" . join(', ', $values) . ")";
		}
		$sql[] = join(",\n", $sql_records);
	  }
	  
	  //pr($this->$model->getColumnTypes());
	  
	  print "<pre style=\"font-size: 12px;\">";
	  print htmlentities(join("\n", $sql));
	  print "</pre>";
	  exit(0);
	}
	
	public function schema($update = false) {
	  //$this->set('schema', $this->{$this->modelClass}->schema());
	  $sql = file_get_contents(APP . DS . '..' . DS . 'mysql' . DS . 'schema.sql');
	  
	  $allModelNames = App::objects('Model');
	  //pr($allModelNames);
	  $issues = array();
	  
	  //foreach ($allModelNames as $modelName) {
	  $modelName = $this->modelClass;
		$this->loadModel($modelName);
		
		$table = Inflector::pluralize($modelName);
		$my_schema = false;
		foreach (split(';', $sql) as $sub_sql) {
		  $schema = $this->SchemaParser->parse($sub_sql);
		  if (is_array($schema)) {
			if (array_key_exists('tables', $schema)) {
			  if (array_key_exists($table, $schema['tables'])) {
				$my_schema = new stdClass();//$schema['tables'][$table];
				$my_schema->columns = $schema['tables'][$table]['columns'];
				if (array_key_exists('PRIMARY', $my_schema->columns)) {
								unset($my_schema->columns['PRIMARY']);
				}
				ksort($my_schema->columns);
				$my_schema->indices = $schema['tables'][$table]['indices'];
				break;
			  }
			}
		  }
		}
		
		
		if ($my_schema !== false) {												
		  $auth_schema = $my_schema->columns;
		  $db_schema = $this->{$modelName}->stdSchema()->columns;
		  $issues = $this->{$modelName}->schemaDiff($table, $auth_schema, $db_schema);
		} else {
		  //print "Couldn't find schema definition.";
		}
	  //}

	  if ($update !== false && count($issues) > 0) {
		$cnt = 0;
		$results = array();
		foreach ($issues as $issue) {
		  $sql = $issue->resolution;
		  $results[] = $this->$modelName->query($sql);
		  $cnt++;
		}
		
		$this->Session->setFlash($cnt . ' quer' . (count($cnt) == 1 ? 'y' : 'ies') . ' executed');
		$this->redirect(array('action'=>'schema'));
	  }
		
		
	  
	  $this->set('issues', $issues);
	  $this->set('table', $table);
	  $this->viewPath = '_Shared';
	  
	  //exit(0);
	}
	
	function req($name, $default = false) {
	  $p = $this->request->params['named'];
	  if (array_key_exists($name, $p)) {
		return $p[$name];
	  } else {
		return $default;
	  }
	}
	
	function expected($params = array()) {
	  $plist = array();
	  foreach ($params as $name => $def) {
		$plist[$name] = $this->req($name, $def);
	  }
	  return $plist;
	}
	
}
