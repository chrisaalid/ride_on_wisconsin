<?php
//App::uses('AppController', 'Controller');
App::uses('UserContentController', 'Controller');
/**
 * Billboards Controller
 *
 * @property Billboard $Billboard
 */
class BillboardsController extends UserContentController {


	public function beforeFilter() {
	  $req = array(
	  );
	  
	  $this->setActionRequirements($req);
	  parent::beforeFilter();
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {		
		$this->Billboard->recursive = 1;
		$this->set('billboards', $this->paginate($this->searchableIndex()));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Billboard->exists($id)) {
			throw new NotFoundException(__('Invalid billboard'));
		}
		$options = array('conditions' => array('Billboard.' . $this->Billboard->primaryKey => $id));
		$this->set('billboard', $this->Billboard->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Billboard->create();
			if ($this->Billboard->save($this->request->data)) {
				$this->Session->setFlash(__('The billboard has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The billboard could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Billboard->exists($id)) {
			throw new NotFoundException(__('Invalid billboard'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Billboard->save($this->request->data)) {
				$this->Session->setFlash(__('The billboard has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The billboard could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Billboard.' . $this->Billboard->primaryKey => $id));
			$this->request->data = $this->Billboard->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Billboard->id = $id;
		if (!$this->Billboard->exists()) {
			throw new NotFoundException(__('Invalid billboard'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Billboard->delete()) {
			$this->Session->setFlash(__('Billboard deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Billboard was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
