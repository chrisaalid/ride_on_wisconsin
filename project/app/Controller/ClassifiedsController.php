<?php
//App::uses('AppController', 'Controller');
App::uses('UserContentController', 'Controller');
/**
 * Classifieds Controller
 *
 * @property Classified $Classified
 */
class ClassifiedsController extends UserContentController {


	public function dynamicValidation() {
	  if (array_key_exists('Classified', $this->data) && array_key_exists('type', $this->data['Classified'])) {
		//$this->Classified->validator()->add('agree', 'notempty', array('rule' => 'notEmpty', 'message' => "Please enter the bike's make"));
		
		if ($this->data['Classified']['type'] == 'bike') {
		  $this->Classified->validator()->add('bikeMake', 'notempty', array('rule' => 'notEmpty', 'message' => "Please enter the bike's make"));
		  $this->Classified->validator()->add('bikeModel', 'notempty', array('rule' => 'notEmpty', 'message' => "Please enter the bike's model"));
		} else {
		  $this->Classified->validator()->add('partMake', 'notempty', array('rule' => 'notEmpty', 'message' => "Please enter the part's make"));
		  $this->Classified->validator()->add('partModel', 'notempty', array('rule' => 'notEmpty', 'message' => "Please enter the part's model"));
		  $this->Classified->validator()->add('partType', 'notempty', array('rule' => 'notEmpty', 'message' => "Please enter the type of part"));
		}
	  }
	}

	public function beforeFilter() {
	  parent::beforeFilter();
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Classified->recursive = 1;
		$this->set('classifieds', $this->paginate($this->searchableIndex()));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null, $search = false) {
		if (!$this->Classified->exists($id)) {
			throw new NotFoundException(__('Invalid classified'));
		}
		$options = array('conditions' => array('Classified.' . $this->Classified->primaryKey => $id));
		$this->set('classified', $this->Classified->find('first', $options));
		$this->set('search', $search);
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		parent::add();
		
		if ($this->request->is('post')) {
			$this->dynamicValidation();
			$this->Classified->create();
			if ($this->Classified->save($this->request->data)) {
				$this->Session->setFlash(__('The classified has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The classified could not be saved. Please, try again.'));
			}
		}
		$users = $this->Classified->User->find('list');
		$this->set(compact('users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Classified->exists($id)) {
			throw new NotFoundException(__('Invalid classified'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->dynamicValidation();
			if ($this->Classified->save($this->request->data)) {
				$this->Session->setFlash(__('The classified has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The classified could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Classified.' . $this->Classified->primaryKey => $id));
			$this->request->data = $this->Classified->find('first', $options);
		}
		$users = $this->Classified->User->find('list');
		$this->set(compact('users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Classified->id = $id;
		if (!$this->Classified->exists()) {
			throw new NotFoundException(__('Invalid classified'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Classified->delete()) {
			$this->Session->setFlash(__('Classified deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Classified was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	public function search($query = false) {
	  if ($query !== false) {
		$resp = new stdClass();
		
		
		if ($query === 'cache') {
		  $results = $this->Session->read('classified.search.cache');
		  if (isset($results)) {
			$this->set('results', $results);
		  }
		} else {
		  $search_opts = $this->request->data;
		  
		  if (array_key_exists('zip', $search_opts) && (!array_key_exists('lat', $search_opts) || !array_key_exists('lng', $search_opts))) {
			$result = $this->Classified->geocodeAddress($search_opts['zip']);
			if ($result !== false) {
			  $search_opts['lat'] = $result['lat'];
			  $search_opts['lng'] = $result['lng'];
			}
		  }
		  
		  $resp->results = $this->Classified->search($search_opts);
		  $resp->request = $search_opts;
		  $resp->success = true;
		  print json_encode($resp);
		  
		  $this->Session->write('classified.search.cache', $resp);
		  
		  exit(0);
		}
	  }
	}
	
}
