<?php

App::uses('Component', 'Controller');
App::uses('Component', 'Auth');
App::uses('Model', 'User');

class UploaderComponent extends Component {
  public $components = array(
	'Auth'
  );

  private $thumbs_to_generate = array(
	//array(1024, 768),
	array(340, 325),
	array(128, 128)
  );
  
  private $ctrl;
  private $Upload;
  private $errors = array();
  private $current_file = null;
  
  public function initialize(Controller $controller) {
		$this->ctrl = $controller;
		$this->Upload = ClassRegistry::init('Upload');
  }
  
  public function createSizes($path) {
		foreach ($this->thumbs_to_generate as $dimensions) {
			$this->Upload->croppedThumb($path, $dimensions[0], $dimensions[1]);
		}
  }
  
  public function invalidate($msg) {
		$mdl = $this->ctrl->modelClass;
		$this->errors[] = $msg;
		$this->ctrl->$mdl->validationErrors['uploads'] = $msg;
  }

  public function convertToJpeg($path, $type) {
		switch ($type) {
			case IMAGETYPE_GIF  : $img = imagecreatefromgif($path);  break;
			case IMAGETYPE_PNG  : $img = imagecreatefrompng($path);  break;
			default : return $path;
		}
		
		$new_path = preg_replace('/\.[A-Za-z0-9]+$/',  '.jpg', $path);
		imagejpeg($img, $new_path);
		
		return $new_path;
  }
  
  public function processImage($path) {
		if (!file_exists($path)) {
			$this->invalidate("Uploaded file {$path} does not exist.");
			return false;
		}
		if (($info = getimagesize($path)) === FALSE) {
			$this->invalidate("Uploaded file {$path} does not appear to be an image");
			return false;
		}
		
		if ($info !== false && count($info) > 2) {
			$type = $info[2];
			$w = $info[0];
			$h = $info[1];
		
			
		
			if ($type != IMAGETYPE_JPEG) {
				$path = $this->convertToJpeg($path, $type);
			}
			
			$max_dim = max($w, $h);
		
			if ($max_dim > 1280) { // scale down large image originals
				$orig = imagecreatefromjpeg($path);
				$nw = 0;
				$nh = 0;
				
				if ($w == $max_dim) {
					$nw = 1280;
					$nh = ($nw * $h) / $w;
				} else {
					$nh = 1280;
					$nw = ($nh * $w) / $h;
				}
				
				$scaled = ImageCreateTrueColor($nw, $nh);
				imagecopyresampled($scaled, $orig, 0, 0, 0, 0, $nw, $nh, $w, $h);
				imagejpeg($scaled, $path);
			}
			
			$this->createSizes($path, $info);
			
			return $path;
		} else {
			$this->invalidate("Could not extract metadata from {$path}");
			return false;
		}
  }
  
  private function monitor() {
		if ($this->ctrl->request->is('post') || $this->ctrl->request->is('put')) {
			$req_uploads = $this->ctrl->uploaded_files;
			$prev_uploads = array();
			if (array_key_exists($this->ctrl->modelClass, $this->ctrl->data) && array_key_exists('previous_uploads', $this->ctrl->data[$this->ctrl->modelClass])) {
				$prev_uploads = json_decode($this->ctrl->data[$this->ctrl->modelClass]['previous_uploads']);
			}
			
			$all_uploads = array_unique(array_merge($req_uploads, $prev_uploads));
			
			$mdl = $this->ctrl->modelClass;
			$insert_id = $this->ctrl->$mdl->getInsertID();
			if (preg_match('/^\d+$/', $insert_id)) {
				$this->associate($mdl, $insert_id, $all_uploads);
			} elseif (array_key_exists($mdl, $this->ctrl->data) && array_key_exists('id', $this->ctrl->data[$mdl])) {
				$record_id = $this->ctrl->data[$mdl]['id'];
				$this->associate($mdl, $record_id, $all_uploads);
			}
			
			
			
			$this->ctrl->set('prev_uploads', $all_uploads);
		}
	
  }
  
  public function beforeRedirect(Controller $controller, $url, $status = NULL, $exit = true) { //$controller, $url, $status = null, $exit = true) {
		$this->monitor();
  }
  
  public function beforeRender(Controller $controller) {
		$this->monitor();
  }
  
  private function associate($mdl, $id, $files) {
		$for_key = strtolower($mdl) . '_id';
		
		if (!$this->Upload->hasField($for_key)) { return; }
		
		$current_uploads = $this->Upload->find('all', array('conditions' => array($for_key => $id), 'recursive' => -1));
		
		$next_order = 0;
		
		if (count($current_uploads) > 0) {
			foreach ($current_uploads as $upload) {
				$next_order = max($next_order, $upload['Upload']['order']);
			}
		}
		
		
		foreach ($files as $f_id) {
			$this->Upload->id = $f_id;
			
			$ul = $this->Upload->read();

			$this->Upload->saveField($for_key, $id);
			
			if (array_key_exists('Upload', $ul) && array_key_exists('order', $ul['Upload']) && $ul['Upload']['order'] == 0) {
				$this->Upload->saveField('order', ++$next_order);
			}
		}
  }
  
  
	
  private function save($file, $owner = false) {
	
	
		if ($owner !== false) {
			$owner += 0;
			$ext = 'dat';
			
			if (preg_match('/\.([A-Za-z0-9]+)$/', $file['name'], $matches)) {
				$ext = $matches[1];
			}
			
			$upload_dir = WWW_ROOT . 'user-uploads/' . $owner . '/';
			
			if (!is_dir($upload_dir) || !file_exists($upload_dir)) {
				mkdir($upload_dir);
			}
			
			$key = md5(time() . $file['name']);
			
			$upload_dir .= $key . '/';
			
			if (!is_dir($upload_dir) || !file_exists($upload_dir)) {
				mkdir($upload_dir);
			}
			
			
			$dest = $upload_dir . 'orig.' . $ext;
			
			move_uploaded_file($file['tmp_name'], $dest);
			
			if (preg_match('/^image\//i', $file['type'])) {
				$new_dest = $this->processImage($dest);
				if ($new_dest === false) {
					return false;
				} elseif ($new_dest != $dest) {
					unlink($dest);
					$dest = $new_dest;
				}
			} else {
				$this->invalidate("Image uploads only.");
				unlink($dest);
				$dest = false;
			}

			$data = array(
				'Upload' => array(
					'filename' => $file['name'],
					'md5' => $key,
					'user_id' => $owner,
					'mime_type' => $file['type'],
					'size' => $file['size']
				)
			);
			
			if ($dest !== false && strlen($key) != 0 && $owner > 0 && $file['size'] != 0) {
				$this->Upload->create();
				if ($this->Upload->save($data)) {
					$this->ctrl->uploaded_files[] = $this->Upload->getInsertID();
					return true;
				} else {
					$this->invalidate("Unable to save {$file['name']} to the database.");
					return false;
				}
				return true;
			} else {
			  //$this->invalidate(print_r($data, true));
			  return false;
			}
		} else {
			$this->invalidate("No owner specified for files.");
			return false;
		}
  }
  
  function handle() {
	
	
		if (($this->ctrl->request->is('post') || $this->ctrl->request->is('put')) && array_key_exists($this->ctrl->modelClass, $this->ctrl->data)) {
			$data = $this->ctrl->data[$this->ctrl->modelClass];
			
			
			//$files = array();
			$i = 1;
			while (array_key_exists('file_upload_' . $i, $data) || $i < 5) {
				if (array_key_exists('file_upload_' . $i, $data) && file_exists($data['file_upload_' .$i]['tmp_name'])) {
					$file = $data['file_upload_' . $i];
					$this->current_file = $file;
					$this->save($file, $this->ctrl->Auth->user('id'));
					
				}
				$i++;
			}
		}
		
		$this->ctrl->set('uploader_component_errors', array_unique($this->errors));
	
  }
  
  
}

?>