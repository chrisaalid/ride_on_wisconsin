<?php
App::uses('AppController', 'Controller');
App::uses('CakePdf', 'CakePdf.Pdf');


/**
 * Contents Controller
 *
 * @property Content $Content
 */
class ContentsController extends AppController {

  public $CakePdf;

	public function beforeFilter() {
	  $req = array(
		'dialog' => 'any',
		'pdf' => 'any'
	  );
	  
	  $this->setActionRequirements($req);
	  //$this->PDFSetup();
	  
	  
	  parent::beforeFilter();
	}
	
	private function PDFSetup() {
	  //$this->CakePdf = new CakePdf();
	}

	
	public function pdf($name = null) {
	  if ($name == null) {
		throw new NotFoundException(__('Invalid content'));
	  }
	  
	  if (!$this->Content->validName($name)) {
		throw new NotFoundException(__('Invalid content'));
	  }
	  
	  $web_path = 'content' . DS . 'pdfs' . DS . $name . '.pdf';
	  $path = WWW_ROOT . $web_path;
	  
	  if (file_exists($path)) {
		$this->redirect('/' . $web_path);
	  } else {
		$this->Content->makePDF($name);
		if (file_exists($path)) {
		  $this->redirect('/' . $web_path);
		} else {
		  $this->Session->setFlash("Unable to generate PDF");
		  $this->redirect('/');
		}
	  }
	  
	}
	
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Content->recursive = 0;
		$this->set('contents', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Content->exists($id)) {
			throw new NotFoundException(__('Invalid content'));
		}
		$options = array('conditions' => array('Content.' . $this->Content->primaryKey => $id));
		$this->set('content', $this->Content->find('first', $options));
	}

	public function dialog($name = null) {
	  if ($name == null) {
		throw new NotFoundException(__('Invalid content'));
	  }
	  
	  $options = array('conditions' => array('Content.name' => 'dialog.' . $name));
	  $content = $this->Content->find('first', $options);
	  if (!array_key_exists('Content', $content)) {
		throw new NotFoundException(__('Invalid content'));
	  }
	  
	  $this->set('content', $content);
	  
	}
	
/**
 * add method
 *
 * @return void
 */
	public function add() {
	  if ($this->request->is('post')) {
		$this->Content->create();
		if ($this->Content->save($this->request->data)) {
		  $msg = 'The content has been saved';
		
		  if ($this->Content->output_error_occured) {
			$msg .= $this->Content->output_error_message;
		  }
		
		  $this->Session->setFlash(__($msg));
		  $this->redirect(array('action' => 'index'));
		} else {
			$this->Session->setFlash(__('The content could not be saved. Please, try again.'));
		}
	  }
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Content->exists($id)) {
			throw new NotFoundException(__('Invalid content'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Content->save($this->request->data)) {
				$msg = 'The content has been saved';
			  
				if ($this->Content->output_error_occured) {
				  $msg .= $this->Content->output_error_message;
				}
			  
				$this->Session->setFlash(__($msg));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The content could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Content.' . $this->Content->primaryKey => $id));
			$this->request->data = $this->Content->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
	  $this->Content->id = $id;
	  if (!$this->Content->exists()) {
		  throw new NotFoundException(__('Invalid content'));
	  }
	  $this->request->onlyAllow('post', 'delete');
	  if ($this->Content->delete()) {
		  $this->Session->setFlash(__('Content deleted'));
		  $this->redirect(array('action' => 'index'));
	  }
	  $this->Session->setFlash(__('Content was not deleted'));
	  $this->redirect(array('action' => 'index'));
	}
}
