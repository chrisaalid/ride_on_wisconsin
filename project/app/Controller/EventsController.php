<?php
//App::uses('AppController', 'Controller');
App::uses('UserContentController', 'Controller');
/**
 * Events Controller
 *
 * @property Event $Event
 */
class EventsController extends UserContentController {

	//public $action_requires = array(
	//  'add' => 'any'  
	//);

	public function beforeFilter() {
	  //$this->setActionRequirements($req);
	  parent::beforeFilter();
	  
	}

	public function xml($specified_date = false) {
		//Search:
		//
		//\s*<item>\s*<title>(.*)</title>\s*<link>(.*)</link>\s*<description>(.*) &#124; (.*) &#124; (.*) &#124; (.*) &#124; (.*)</description>\s*</item>
		//
		//
		//
		//Replace:
		//
		//<listing><title>\1</title><link>\2</link><date>\3</date><location>\4</location><address>\5</address><city>\6</city><extra>\7</extra></listing>
		
		//DateTime::ATOM
		//DATE_ATOM
		//Atom (example: 2005-08-15T15:52:01+00:00)
		
		$this->Event->recursive = 1;
		
		$conditions = array(
			'Event.approved' => 1,
			'Event.start > ' => date('Y-m-d H:i:s')
		);
		
		if ($specified_date !== false) {
			
		}
		
		$this->layout = 'xml/events';
		$this->response->type(array('xml' => 'text/plain'));
		$this->response->type('xml');
		$this->set('events', $this->Event->find('all', array('conditions' => $conditions, 'order' => 'Event.start ASC')));
	}
	
/**
 * index method
 *
 * @return void
 */
	public function index() {
	  $this->Event->recursive = 1;
	  $this->set('events', $this->paginate($this->searchableIndex()));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Event->exists($id)) {
			throw new NotFoundException(__('Invalid event'));
		}
		$options = array('conditions' => array('Event.' . $this->Event->primaryKey => $id));
		$this->set('event', $this->Event->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		parent::add();
		
		if ($this->request->is('post')) {
			$this->Event->create();
			
			if ($this->Event->save($this->request->data)) {
				$this->Session->setFlash(__('The event has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The event could not be saved. Please, try again.'));
			}
		}
		$users = $this->Event->User->find('list');
		$this->set(compact('users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Event->exists($id)) {
			throw new NotFoundException(__('Invalid event'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Event->save($this->request->data)) {
				$this->Session->setFlash(__('The event has been saved'));
				$this->redirect(array('action' => 'view', $id));
			} else {
				$this->Session->setFlash(__('The event could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Event.' . $this->Event->primaryKey => $id));
			$this->request->data = $this->Event->find('first', $options);
		}
		$users = $this->Event->User->find('list');
		$this->set(compact('users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Event->id = $id;
		if (!$this->Event->exists()) {
			throw new NotFoundException(__('Invalid event'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Event->delete()) {
			$this->Session->setFlash(__('Event deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Event was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	public function feature($id = null) {
	  if (!$this->Event->exists($id)) {
		throw new NotFoundException(__('Invalid event'));
	  }
	  
	  $this->Event->updateAll(array('Event.featured' => 0));
	  
	  $conditions = array('Event.' . $this->Event->primaryKey => $id);
	  $this->Event->updateAll(array('Event.featured' => 1), $conditions);
	  $this->Session->setFlash("New featured event set");
	  $this->Event->rebuildFrontend();
	  $this->redirect(array('controller' => 'events', 'action' => 'index'));
	  
	  //$this->set('event', $this->Event->find('first', $options));
	}
}
