<?php
App::uses('AppController', 'Controller');
/**
 * Merchandises Controller
 *
 * @property Merchandise $Merchandise
 */
class MerchandisesController extends AppController {

	function beforeFilter() {
	  //pr('RoutesController::beforeFilter()'); exit(0);
	  $req = array(
		'all' => 'any',
		'view' => 'any'
	  );
	  
	  $this->setActionRequirements($req);
	  
	  parent::beforeFilter();
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Merchandise->recursive = 1;
		$this->set('merchandises', $this->paginate());
	}
	
	public function all() {
		$this->Merchandise->recursive = 1;
		$merch = $this->Merchandise->find('all'); //, array('order' => '`Merchandise`.`order`'));
		
		$this->set('merchandises', $merch);
	}
	

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if ($this->is_dialog) {
		  $this->redirect(array('action' => 'all', '#' => 'item-' . $id));
		}
	  
	  
		if (!$this->Merchandise->exists($id)) {
			throw new NotFoundException(__('Invalid merchandise'));
		}
		$options = array('conditions' => array('Merchandise.' . $this->Merchandise->primaryKey => $id));
		$this->set('merchandise', $this->Merchandise->find('first', $options));
		
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Merchandise->create();
			if ($this->Merchandise->save($this->request->data)) {
				$this->Session->setFlash(__('The merchandise has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The merchandise could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Merchandise->exists($id)) {
			throw new NotFoundException(__('Invalid merchandise'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Merchandise->save($this->request->data)) {
				$this->Session->setFlash(__('The merchandise has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The merchandise could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Merchandise.' . $this->Merchandise->primaryKey => $id));
			$this->request->data = $this->Merchandise->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Merchandise->id = $id;
		if (!$this->Merchandise->exists()) {
			throw new NotFoundException(__('Invalid merchandise'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Merchandise->delete()) {
			$this->Session->setFlash(__('Merchandise deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Merchandise was not deleted'));
		$this->redirect(array('action' => 'index'));
	}	


	
	public function moveup($id = null) {
		$this->Merchandise->id = $id;
		if (!$this->Merchandise->exists()) {
			throw new NotFoundException(__('Invalid merchandise'));
		}
		if ($this->Merchandise->moveup($id)) {
			$this->Session->setFlash(__('Merchandise reordered'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Merchandise could not be reordered'));
		$this->redirect(array('action' => 'index'));
	}
	
	public function movedown($id = null) {
		$this->Merchandise->id = $id;
		if (!$this->Merchandise->exists()) {
			throw new NotFoundException(__('Invalid merchandise'));
		}
		if ($this->Merchandise->movedown($id)) {
			$this->Session->setFlash(__('Merchandise reordered'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Merchandise could not be reordered'));
		$this->redirect(array('action' => 'index'));
	}
	
}
