<?php

App::uses('AppController', 'Controller');

class MiscController extends AppController{
  var $name = "Misc";
  
  var $uses = array('Misc', 'Route', 'Event', 'Location', 'Classified', 'Group', 'Merchandise');
  
  function beforeFilter() {
	  $req = array(
		'sitemap' => 'any'
	  );
	  
	  $this->setActionRequirements($req);
	  
	  parent::beforeFilter();
	}
  
  public function index() {
	$this->redirect(array('action' => 'admin'));
  }
  
  public function loadForSitemap($type) {
	
	$results = $this->allOfType($type, true, array(), array('id', $this->{$type}->displayField, 'modified', 'created'));
	foreach ($results as $idx => $r) {
	  $results[$idx][$type]['label'] = $r[$type][$this->{$type}->displayField];
	  $results[$idx][$type]['modified'] = strtotime($r[$type]['modified']);
	  $results[$idx][$type]['created'] = strtotime($r[$type]['created']);
	}
	
	return $results;
  }
  
  public function allOfType($type = false, $only_approved = false, $conditions = array(), $fields = false) {
	$options = array('recursive' => -1, 'conditions' => $conditions);
	if ($only_approved && $this->{$type}->hasField('approved')) { $options['conditions'][$type . '.approved'] = true; }
	if (is_array($fields)) { $options['fields'] = $fields; }
	if (!is_object($this->{$type})) { $this->loadModel($type); }
	
	return $this->{$type}->find('all', $options);
  } 
  
  public function allRoutes($only_approved = false) {
	$conditions = array();
	if ($only_approved) { $conditions = array('approved' => true); }
	
	return $this->Route->find('all', array('fields' => array('Route.name', 'Route.id', 'User.username'), 'order' => array('Route.name')));
  }
  
  public function allEvents($only_approved = false) {
	$conditions = array();
	if ($only_approved) { $conditions = array('approved' => true); }
	
	return $this->Event->find('all', array('fields' => array('Event.name', 'Event.id', 'User.username'), 'order' => array('Event.name'), 'conditions' => $conditions));
  }

  public function admin() {
	$featured_routes = $this->Misc->loadData('featured-routes');
	
	
	$this->set('featured_routes', $featured_routes);
	$this->set('all_routes', $this->allRoutes());
	$this->set('all_events', $this->allEvents());
  }
  
  public function save() {
	$resp = new stdClass();
	$resp->success = false;
	$resp->message = 'Unknown error';
	
	$data = $this->request->data;
	if (array_key_exists('section', $data)) {
	  $section = $data['section'];
	  if ($this->Misc->validName($section)) {
		if (array_key_exists('payload', $data)) {
		  $payload = $data['payload'];
		  
		  
		  $this->Route->updateAll(array('Route.featured' => 0), array('NOT' => array('Route.id' => $payload)));
		  $this->Route->updateAll(array('Route.featured' => 1), array('Route.id' => $payload));
		  $this->Route->rebuildSlideShow();
		  
		  if ($this->Misc->saveData($section, $payload)) {
			$resp->message = "Success";
			$resp->success = true;
		  } else {
			$resp->message = "Unable to save data";
		  }
		} else {
		  $resp->message = "No payload provided";
		}
	  } else {
		$resp->message = "Invalid section name: $section";
	  }
	  
	} else {
	  $resp->message = 'No section specified';
	}
	
	
	print json_encode($resp); exit(0);
	//print json_encode($this->request); exit(0);
  }
  
  
  public function sitemap($type = 'html') {
	$routes = $this->loadForSitemap('Route');
	$locations = $this->loadForSitemap('Location');
	$events = $this->loadForSitemap('Event');
	$classifieds = $this->loadForSitemap('Classified');
	$groups = $this->loadForSitemap('Group');
	$merch = $this->loadForSitemap('Merchandise');
	
	$this->set(compact('routes', 'locations', 'events', 'classifieds', 'groups', 'merch')); //, 'content'));
	$this->set('map_type', $type);
	
	if ($type == 'xml') {
	  $this->layout_finalized = true;
	  $this->layout = 'sitemap-xml';
	  $this->response->type('xml');
	}
	
	//$this->render('/Elements/sitemap.ctp');
	
  }
}