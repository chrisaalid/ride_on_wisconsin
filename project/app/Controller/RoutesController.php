<?php
//App::uses('AppController', 'Controller');
App::uses('UserContentController', 'Controller');
/**
 * Routes Controller
 *
 * @property Route $Route
 */
class RoutesController extends UserContentController {


	function beforeFilter() {
	  //pr('RoutesController::beforeFilter()'); exit(0);
	  $req = array(
		'add' => 'logged',
		'index' => 'admin',
		'edit' => 'own', 
		'view' => 'any',
		'kml' => 'any',
		'gpx' => 'any',
		'json' => 'any'
	  );
	  
	  $this->setActionRequirements($req);
	  
	  parent::beforeFilter();
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Route->recursive = 1;
		$this->set('routes', $this->paginate($this->searchableIndex()));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Route->exists($id)) {
			throw new NotFoundException(__('Invalid route'));
		}
		$options = array('conditions' => array('Route.' . $this->Route->primaryKey => $id));
		$this->set('route', $this->Route->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
    private function gpxUploaded() {
	  $ret_val = false;
	  
	  if (array_key_exists('Route', $this->data) && array_key_exists('gpx', $this->data['Route'])) {
		$gpx_file = $this->data['Route']['gpx'];
		if (strlen($gpx_file['tmp_name']) > 0 && file_exists($gpx_file['tmp_name'])) {
		  $ret_val = true;
		}
	  }
	  
	  return $ret_val;
	}
 
	public function add($points = false) {
		parent::add();
		
		if ($this->request->is('post') && $points === false) {
		  //if (array_key_exists('Route', $this->data) && array_key_exists('gpx', $this->data['Route'])) {
		  if ($this->gpxUploaded()) {
			$points = $this->import();
			if (count($points) >= 2) {
			  $this->Session->setFlash(__('GPX data imported'));
			} else {
			  $this->Session->setFlash(__('Unable to parse GPX data'));
			  //$this->Session->setFlash(__('Unable to parse GPX data: ' . h($this->Route->output_error_message->message)));
			  //pr($this->Route->output_error_message); exit(0);
			}
		  } else {
			//debug($this->request); die;
			
			$this->Route->create();
			if ($this->Route->save($this->request->data)) {
				$this->Session->setFlash(__('The route has been saved'));
				$this->redirect(array('controller'=>'users','action' => 'view', $this->user['id']));
			} else {
				$this->Session->setFlash(__('The route could not be saved. Please, try again.'));
				$this->set('errors', $this->Route->validationErrors, true);
			}
		  }
		}
		
		if (is_array($points)) {
		  $this->request->data['Route']['points'] = json_encode($points);
		}
		
		$users = $this->Route->User->find('list');
		$this->set(compact('users'));
		$this->set('importing', is_array($points));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Route->exists($id)) {
			throw new NotFoundException(__('Invalid route'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Route->save($this->request->data)) {
				$this->Session->setFlash(__('The route has been saved'));
				if ($this->is_admin) {
					$this->redirect(array('action' => 'index'));
				} else {
					$this->redirect(array('controller' => 'users', 'action' => 'view', $this->user['id']));
				}
			} else {
				$this->Session->setFlash(__('The route could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Route.' . $this->Route->primaryKey => $id), 'recursive' => 1);
			$this->request->data = $this->Route->find('first', $options);
		}
		$users = $this->Route->User->find('list');
		$this->set(compact('users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Route->id = $id;
		if (!$this->Route->exists()) {
			throw new NotFoundException(__('Invalid route'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Route->delete()) {
			$this->Session->setFlash(__('Route deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Route was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	public function kml($id = null) {
	  if (!$this->Route->exists($id)) {
		  throw new NotFoundException(__('Invalid route'));
	  }
	  
	  $options = array('conditions' => array('Route.' . $this->Route->primaryKey => $id));
	  $route = $this->Route->find('first', $options);
	  
	  $this->set('route', $route);
	  
	  $kml_path = $this->Route->pathToKML($route['Route']['id']);
	  
	  
	  
	  if (!file_exists($kml_path)) {
		$this->Route->rebuildKML($id);
	  }
	  
	  if (file_exists($kml_path)) {
		$filename = preg_replace('/[^A-Za-z]+/', '-', $route['Route']['name']);
		header('Content-Disposition: attachment; filename=RideOnWisconsin_Route_' . $filename . '.kml');
		header('Content-Type: application/vnd.google-earth.kml+xml');
		
		readfile($kml_path);
	  }
	  
	  
	  exit;
	}
	
	public function gpx($id = null) {
	  if (!$this->Route->exists($id)) {
		  throw new NotFoundException(__('Invalid route'));
	  }
	  
	  $options = array('conditions' => array('Route.' . $this->Route->primaryKey => $id));
	  $route = $this->Route->find('first', $options);
	  
	  $this->set('route', $route);
	  
	  $gpx_path = $this->Route->pathToGPX($route['Route']['id']);
	  
	  if (!file_exists($gpx_path)) {
		$this->Route->rebuildGPX($id);
	  }
	  
	  if (file_exists($gpx_path)) {
		$filename = preg_replace('/[^A-Za-z]+/', '-', $route['Route']['name']);
		header('Content-Disposition: attachment; filename=RideOnWisconsin_Route_' . $filename . '.gpx');
		header('Content-Type: application/gpx+xml');
		readfile($gpx_path);
	  }
	  
	  
	  exit;
	}
	
	public function content($id = null) {
	  parent::content($id);
	}
	
	public function import() {
	  //$this->set('data', '...');
	  
	  if ($this->request->is('post')) {
		if (array_key_exists('Route', $this->data) && array_key_exists('gpx', $this->data['Route'])) {
		  $file = $this->data['Route']['gpx'];
		  
		  if (file_exists($file['tmp_name'])) {
			
			
			
			$gpx_xml = file_get_contents($file['tmp_name']);
			$pt_array = $this->Route->gpxToPointArray($gpx_xml);
			return $pt_array;
			
		  }
		}
	  }
	  return array();
	}
	
	

}
