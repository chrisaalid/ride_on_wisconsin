<?php

class StaticPagesController extends AppController {
  
  function beforeFilter() {
	$req = array(
	  'display' => 'any'
	);
	
	$this->setActionRequirements($req);
	
	parent::beforeFilter();
  }
  
  
  function display($page = null) {
	$missing = false;
	$path = false;
	
	if ($page == null) {
	  $missing = true;
	} else {
	  $path = $this->StaticPage->pagePath($page);
	  if ($path === false) {
		$missing = true;
	  }
	}
	
	if ($missing) {
	  //throw new NotFoundException(__('Page Not Found: ' . $page));
	  $path = $this->StaticPage->missing();
	  $missing = false;
	}
	
	$this->set('missing', $missing);
	$this->set('path', $path);
	
	$this->layout = false;
  }
  
  function beforeRender() {
	if (preg_match('/\.txt$/i', $this->here)) {
	  $this->response->type('text');
	}
  }
}