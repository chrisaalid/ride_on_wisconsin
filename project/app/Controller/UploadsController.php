<?php
App::uses('AppController', 'Controller');
/**
 * Uploads Controller
 *
 * @property Upload $Upload
 */
class UploadsController extends AppController {
	public function beforeFilter() {
	  
	  $req = array(
		'add' => 'admin',
		'index' => 'admin',
		'edit' => 'admin', 
		'view' => 'admin',
		'del' => 'any',
		'shift' => 'any',
		'swap' => 'any',
		'fetch' => 'any'
	  );
	  
	  $this->setActionRequirements($req);
	  
	  parent::beforeFilter();
	}


	public function fetch($id = null, $format = 'orig') {
	  $path = $this->Upload->placeholder(true);
		
		if ($this->Upload->exists($id)) {
			$path = $this->Upload->getPath($id, 'orig', true); //$format, true);
		}
		
		if (preg_match('/^(\d{1,5}x\d{1,5}|orig)$/', $format)) {			
			if (file_exists($path)) {
				if (strpos($format, 'x') > 1) {
					list ($w, $h) = split('x', $format);
					
					$w += 0; $h += 0;
					
					
					if ($w > 1024) { $w = 1024; }
					if ($h > 1024) { $h = 1024; }
					
					$path = $this->Upload->croppedThumb($path, $w, $h);
					
				} else {
					//$path = $this->Upload->placeholder(true);
					//header("Content-Type: text/plain");
					//print "Missing image";
					//exit(0);
				}
			}
		}
	  
	  $size = filesize($path);
	  header("Content-Type: image/jpeg");
	  header("Content-Length: " . $size);
	  readfile($path);
	  exit(0);
		
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Upload->recursive = 0;
		$this->set('uploads', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Upload->exists($id)) {
			throw new NotFoundException(__('Invalid upload'));
		}
		$options = array('conditions' => array('Upload.' . $this->Upload->primaryKey => $id));
		$this->set('upload', $this->Upload->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Upload->create();
			if ($this->Upload->save($this->request->data)) {
				$this->Session->setFlash(__('The upload has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The upload could not be saved. Please, try again.'));
			}
		}
		$users = $this->Upload->User->find('list');
		$events = $this->Upload->Event->find('list');
		$classifieds = $this->Upload->Classified->find('list');
		$routes = $this->Upload->Route->find('list');
		$groups = $this->Upload->Group->find('list');
		$this->set(compact('users', 'events', 'classifieds', 'routes', 'groups'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
	  
		if (!$this->Upload->exists($id)) {
			throw new NotFoundException(__('Invalid upload'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Upload->save($this->request->data)) {
				$this->Session->setFlash(__('The upload has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The upload could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Upload.' . $this->Upload->primaryKey => $id));
			$this->request->data = $this->Upload->find('first', $options);
		}
		$users = $this->Upload->User->find('list');
		$events = $this->Upload->Event->find('list');
		$classifieds = $this->Upload->Classified->find('list');
		$routes = $this->Upload->Route->find('list');
		$groups = $this->Upload->Group->find('list');
		$this->set(compact('users', 'events', 'classifieds', 'routes', 'groups'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Upload->id = $id;
		if (!$this->Upload->exists()) {
			throw new NotFoundException(__('Invalid upload'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Upload->delete()) {
			$this->Session->setFlash(__('Upload deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Upload was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	public function del($id = null) {
		$this->Upload->id = $id;
		
		$resp = new stdClass();
		$resp->success = false;
		$resp->message = 'Unknown failure';
		
		if (!$this->Upload->exists()) {
		  $resp->message = 'Does not exist';
		} else {
		  $ul = $this->Upload->read('user_id');
		  if (!$this->Auth->loggedIn()) {
			$resp->message = 'Not logged in';
		  } elseif ($ul['Upload']['user_id'] != $this->user['id']) {
			$resp->message = 'Not owner';
			$resp->meta = $ul;
		  } elseif ($this->Upload->delete($id)) {
			$resp->success = true;
			$resp->message = 'Deleted';
			$resp->id = $id;
		  }
		}
		
		$this->layout = false;
		echo json_encode($resp);
		exit(0);
	}
	
	public function swap($first = null, $second = null) {
	  $resp = new stdClass();
	  $resp->success = false;
	  $resp->message = 'Unknown failure';
	  if (!$this->Auth->loggedIn()) {
			$resp->message = "Not logged in";
	  } else {
			if (!$this->Upload->exists($first) || !$this->Upload->exists($second)) {
				$resp->message = "Image doesn't exist";
			} else {
				
				
				$img_1 = $this->Upload->find('first', array('conditions' => array('Upload.id' => $first), 'recursive' => -1));
				$img_2 = $this->Upload->find('first', array('conditions' => array('Upload.id' => $second), 'recursive' => -1));
				
				//$resp->meta = new stdClass();
				//$resp->meta->first = $img_1;
				//$resp->meta->second = $img_2;
				//
				if (!$this->user['admin'] || $img_1['Upload']['user_id'] != $this->user['id'] ||$img_1['Upload']['user_id'] != $this->user['id']) {
					$resp->message = "You are not the image owner";
				} else {
					$this->Upload->read(null, $first);
					$this->Upload->set('order', $img_2['Upload']['order']);
					$success1 = $this->Upload->save();
					
					$this->Upload->read(null, $second);
					$this->Upload->set('order', $img_1['Upload']['order']);
					$success2 = $this->Upload->save();
					
					
					$resp->success = $success1 && $success2;
					$resp->message = $resp->success ? 'Success!' : 'Unknown error';
				}
			}
	  }
	  
	  $this->layout = false;
	  echo json_encode($resp);
	  exit(0);
	}
	
	public function shift($id = null, $direction = false) {
		$this->Upload->id = $id;
		
		$resp = new stdClass();
		$resp->success = false;
		$resp->message = 'Unknown failure';
		
		
		if ($direction === false) {
		  $resp->message = "no direction specified";
		} elseif (!$this->Upload->exists()) {
		  $resp->message = 'Does not exist';
		} else {
		  $ul = $this->Upload->read('user_id');
		  if (!$this->Auth->loggedIn()) {
			$resp->message = 'Not logged in';
		  } elseif ($ul['Upload']['user_id'] != $this->user['id']) {
			$resp->message = 'Not owner';
			$resp->meta = $ul;
		  } elseif ($this->Upload->delete($id)) {
			$resp->success = true;
			$resp->message = 'Deleted';
		  }
		}
		
		$this->layout = false;
		echo json_encode($resp);
		exit(0);
	}
	
	
	
	
	
}
