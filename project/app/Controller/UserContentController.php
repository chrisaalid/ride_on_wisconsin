<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * UserContents Controller
 *
 * @property UserContent $UserContent
 */
class UserContentController extends AppController {
	
	

  function beforeFilter() {
	$req = array(
	  'add' => 'logged',
	  'edit' => 'own',
	  'view' => 'any',
	  'index' => 'admin',
	  'activate' => 'admin',
	  'json' => 'any',
	  'search' => 'any'
	);
	
	$this->setActionRequirements($req);
	
	parent::beforeFilter();
  }

  function json($id = null) {
	$model = $this->modelClass;
	
	$response = new stdClass();
	$response->success = true;
	
	if (!$this->$model->exists($id)) {
	  $response->success = false;
	} else {
	  $conditions = array($model . '.' . $this->$model->primaryKey => $id, $model . '.approved' => 1);
	  $options = array('conditions' => $conditions);
	  $record = $this->$model->find('first', $options);
	  $safe_record = $this->$model->cacheable($record);
	  $response->data = $safe_record;
	}
	
	print json_encode($response);
	
	exit(0);
  }
  
//  public function search($params = null) {
//	$resp = new stdClass();
//	$resp->success = true;
//	$resp->payload = $params;
//	$resp->request = $this->request->params;
//	
//
//	$resp->params = $this->expected(array(
//	  'from' => 53704,
//	  'distance' => 50,
//	  'type' => 'positional',
//	  'start' => time(),
//	  'end' => time() + (60 * 60 * 24 * 365),
//	  'keywords' => false
//	));
//	
//	pr($resp);
//	//print json_encode($resp);
//	exit(0);
//  }
	
}

?>