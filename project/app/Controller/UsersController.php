<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Users Controller
 *
 * @property User $User
 */
class UsersController extends AppController {
	//public $components = array(
	//	'Session',
	//	'Auth' => array(
	//		'allow' => array('add'),
	//		'loginAction' => array('controller' => 'users', 'action' => 'login'),
	//		'loginRedirect' => array('controller' => 'users', 'action' => 'index'),
	//		'logoutRedirect' => array('controller' => 'users', 'action' => 'login')/*,
	//		'authorize' => array('Controller') */
	//	)
	//);
	
	
	
	
	//function allowAccess() {
	//	$allowed = array('login','logout','add');
	//	$admin_allowed = array('edit', 'delete');
	//	$user = $this->Auth->user();
	//	if ($user['admin']) {
	//		$allowed = array_merge($allowed, $admin_allowed);
	//	}
	//	$this->Auth->allow($allowed);
	//}
	
	public function beforeFilter() {
		$req = array(
		  'close' => 'any',
		  'add' => 'any',
		  'login' => 'any',
		  'logout' => 'any',
		  'edit' => 'own',
		  'view' => 'own',
		  'index' => 'admin',
		  'status' => 'any',
		  'activate' => 'any',
		  'beginprr' => 'any',
		  'prr' => 'any',
		  'reset' => 'any',
		  'resendActivation' => 'any',
		  'who' => 'any',
		  'dialogs' => 'logged'
		);
		
		$this->setActionRequirements($req);
		
		parent::beforeFilter();
	}
	
	
	
	function dialogs($use = 'no', $redirect = false) {
	  if ($this->is_admin) {
		$this->useDialogs($use === 'yes' ? true : false);
	  }
	  
	  
	  $url = $this->referer();
	  
	  if ($redirect === false) {
		if ($use == 'yes') {
		  $path = parse_url($url, PHP_URL_PATH);
		  $url = '/#!' . $path;
		} else {
		  //$url = parse_url($url, PHP_URL_PATH);
		}
	  } else {
		$url = $redirect;
	  }
	  
	  //pr($url); exit(0);

	  $this->redirect($url);
	}
	
	public function login() {
	  if ($this->Auth->loggedIn()) {
		$this->redirect(array('controller' => 'users', 'action' => 'view', $this->user['id']));
	  }
	  
	  
	  if ($this->request->is('post')) {
		
		$submitted_name = $this->data['User']['username'];
		
		if (strpos($submitted_name, '@') > 0) {
		  $options = array('conditions' => array('User.email' => $submitted_name));
		} else {
		  $options = array('conditions' => array('User.username' => $submitted_name));
		}
		
		$options['recursive'] = 0;
		
		$user = $this->User->find('first', $options);
		
		if (!empty($user)) {
		  
		  if ($user['User']['approved']) {
			
			
			$test_pass = $this->Auth->password($this->data['User']['password']);
			$real_pass = $user['User']['password'];
			
			if ($test_pass === $real_pass) {
			  $this->Auth->login($user);
			  $this->user = $this->Auth->user();
			  $this->user = $this->user['User'];
			  $this->is_admin = $this->user['admin'];
			  $this->dialogs('yes', $this->Auth->redirect());
			  //$this->redirect($this->Auth->redirect());
		
			} else {
			  $this->Session->setFlash(__('Invalid username or password, try again'));
			}
		  } else {
			$this->Session->setFlash("Please confirm your email address before logging in");
		  }
		} else {
		  $this->Session->setFlash("Invalid username");
		}
	  }
	}
	
	public function logout() {
	  $redir = $this->Auth->logout();
	  //$this->redirect(array('controller' => 'users', 'action' => 'login'));
	  $this->redirect('/');
	//  if ($this->is_dialog) {
	//	$this->redirect('/dialog/users/close');
	//  } else {
	//	$this->redirect($redir);
	//  }
	}
	
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->User->recursive = 0;
		$this->set('users', $this->paginate($this->searchableIndex()));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if ($id == 'me' && $this->Auth->loggedIn()) {
		  $id = $this->user['id'];
		}
	  
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$user_record = $this->User->find('first', $options);
		
		
		if ($this->is_dialog) {
		  //pr($user_record); exit(0);  
		  $this->set('content', $this->sortContent($user_record));
		}
		$this->set('user', $user_record);
		
		
		
	}
	
	public function sortContent($user) {
	  function custSort($a, $b) {
		if (!array_key_exists('approved', $a) || !array_key_exists('approved', $b)) {
		  return 0;
		}
		
		
		if ($a['approved'] == $b['approved']) {
		  return 0;
		}
		
		return $a['approved'] ? -1 : 1;
	  }
	  
	  $content = array();
	  
	  foreach ($user as $key => $records) {
		if ($key != 'User') {
		  foreach ($records as $r) {
			$r['content_type'] = strtolower($key);
			$r['controller'] = Inflector::pluralize(strtolower($key));
			if (array_key_exists('approved', $r)) {
			  $content[] = $r;
			}
		  }
		}
	  }
	  usort($content, 'custSort');
	  //pr($content); exit(0);
	  return $content;
	}
	

/**
 * add method
 *
 * @return void
 */
	public function add() {
		parent::add();
		
		if ($this->request->is('post')) {
			$this->User->create();
			$this->User->Behaviors->attach('Tools.Passwordable');
			
			$this->request->data['User']['approved'] = false;
			$this->request->data['User']['admin'] = false;
			$this->request->data['User']['resetkey'] = $this->User->generateResetKey();
			
			if ($this->User->save($this->request->data)) {
				
				$user = $this->data['User'];
				
				$this->sendActivationLink($user);
				
				$this->Session->setFlash(__('Please check your email for your confirmation link.'));
				$this->redirect(array('action' => 'login'));
				//$this->redirect(array('controller'=>'users','action' => 'index')); exit(0);
				
				//$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		}
	}
	
	public function activate($key) {
	  $options = array('conditions' => array('User.resetkey' => $key));
	  $user_to_activate = $this->User->find('first', $options);
	  
	  if (!isset($user_to_activate) || !isset($user_to_activate['User'])) {
		$this->Session->setFlash("That key is not valid.");
	  } else {
		$sur = array('User' => array(
		  'id' => $user_to_activate['User']['id'],
		  'approved' => true,
		  'resetkey' => $this->User->generateResetKey()
		));
	  
		
		if ($this->User->save($sur)) {
		  $this->Session->setFlash(__('Your email address has been verified.'));
		  $this->Auth->logout();
		} else {
		  $this->Session->setFlash(__('The user could not be saved. Please, try again'));
		}
	  }
	  
	  $this->redirect(array('action' => 'login'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if (isset($this->request->data['User']['pwd'])) {
				$this->User->Behaviors->attach('Tools.Passwordable');
			}
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved'));
				$this->redirect(array('action' => 'view', $this->request->data['User']['id']));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('User deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('User was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	function beginprr() {
	  if ($this->request->is('post')) {
		$user = $this->User->find('first', array('conditions' => array('User.email' => $this->data['User']['email'])));
		
		if (array_key_exists('User', $user)) {
		  $user = $user['User'];
		  if ($user['approved']) {
			$this->prr($user['email']);
			$this->Session->setFlash('Please check your email for your reset link');
		  } else {
			$this->sendActivationLink($user);
			$this->Session->setFlash('Please check your email for your activation link');
		  }
		} else {
		  $this->Session->setFlash('That user could not be found');
		}
		$this->redirect(array('action' => 'login'));
	  }
	}
	
	function prr($email = null) {
	  $options = array('conditions' => array('User.email' => $email));
	  $user_to_reset = $this->User->find('first', $options);
	  
	  
	  
	  if (!isset($user_to_reset) || !isset($user_to_reset['User'])) {
		$this->Session->setFlash("That user could not be found.");
	  } else {
		$user_to_reset = $user_to_reset['User'];
		
		$this->sendPasswordReset($user_to_reset);
		
		$this->Session->setFlash("Password reset email sent to {$user_to_reset['email']}.");
	  }
	  
	  $this->redirect(array('action'=>'login'));
	}
	
	function reset($key = null) {
	  $options = array('conditions' => array('User.resetkey' => $key));
	  $user_to_reset = $this->User->find('first', $options);
	  
	  if (!isset($user_to_reset) || !isset($user_to_reset['User'])) {
		$this->Session->setFlash("That key is not valid.");
		$this->redirect(array('action' => 'login'));
	  } else {
		
		
		if ($this->request->is('post') || $this->request->is('put')) {
		  if (isset($this->request->data['User']['pwd'])) {
			$this->User->Behaviors->attach('Tools.Passwordable');
		  }
		  
		  unset($this->request->data['User']['admin']);
		  $this->request->data['User']['resetkey'] = $this->User->generateResetKey();
		  
		  if ($this->User->save($this->request->data)) {
			$this->Session->setFlash(__('Your password has been reset.'));
			$this->Auth->logout();
			$this->redirect(array('action' => 'login'));
		  } else {
			$this->Session->setFlash(__('The user could not be saved. Please, try again'));
		  }
		} else {
		  $this->request->data = $user_to_reset;
		}
	  }
	}
	
	private function sendPasswordReset($user) {
	  $Email = new CakeEmail();
		
	  $Email
		->viewVars(array(
		  'user' => $user,
		  'topMsg' => 'Reset your password'
		))
		->template('reset-password', 'standard')
		->emailFormat('html')
		->to($user['email'])
		->from('no-reply@rideonwisconsin.org')
		->subject('Ride On Wisconsin: Password Reset')
		->send();
	}
	
	private function sendActivationLink($user) {
	  $Email = new CakeEmail();
				
	  $Email
		->viewVars(array(
		  'user' => $user,
		  'topMsg' => 'Activate your account. <br>Welcome to Ride On Wisconsin!'
		))
		->template('new-account', 'standard')
		->emailFormat('html')
		->to($user['email'])
		->from('no-reply@rideonwisconsin.org')
		->subject('Ride On Wisconsin: Account Activation')
		->send();
	}
	
	function resendActivation($email) {
	  $options = array('conditions' => array('User.email' => $email));
	  $user = $this->User->find('first', $options);
	  
	  
	  
	  if (!isset($user) || !isset($user['User'])) {
		$this->Session->setFlash("That user could not be found.");
	  } else {
		$user = $user['User'];
		
		$this->sendActivationLink($user);
		
		
		$this->Session->setFlash("Password reset email sent to {$user['email']}.");
	  }
	  
	  $this->redirect(array('action'=>'index'));
	}
	
	function content($id = null) {
	  $this->redirect(array('action'=>'index'));
	}
	
	function status() {
	  
	}
	
	function who() {
	  $user = false;
	  if ($this->Auth->loggedIn()) {
		$user = $this->user;
		$user = $this->User->cacheable($user);
	  }
	  
	  
	  $this->response->type(array('plain' => 'text/plain'));
	  $this->response->type('plain');
	  
	  $this->autoLayout = false;
	  $this->set('user', $user);
	}
	
	

	
}
