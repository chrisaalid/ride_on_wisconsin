<?php

class Formatter {
  public static $date_formats = array(
    'longDateTime'   => 'M. j<\s\u\p>S</\s\u\p>, Y g:i a',
	'longDate'       => 'M. j<\s\u\p>S</\s\u\p>, Y',
	'shortDateTime'  => 'n/j/Y  g:i a',
	'shortDate'      => 'n/j/Y',
	'eventDate'      => 'D, M j, Y',
	'eventTime'      => 'g:i A'
  );
  
  public static $loc_types = array(
	'bike-shop' => 'Bike Shop',
	'lodging' => 'Lodging',
	'restaurant' => 'Restaurant',
	'mtb-trail-head' => 'MTB Trail Head'
  );
  
  public static $difficulty_types = array(
	'Easy <span class="small">(For all riders)</span><br/>',
	'Medium <span class="small">(For intermediate riders)</span><br/>',
	'Hard <span class="small">(For experienced riders)</span>'
  );
  
  public static $simple_difficulty_types = array(
	'Easy',
	'Medium',
	'Hard'
  );

  public static $classified_types = array(
	'part' => 'Bike Part',
	'bike' => 'Complete Bike'
  );
  
  public static $classified_condition = array(
	'new' => 'New / In Packaging',
	'like-new' => 'Like New',
	'gently-used' => 'Gently Used',
	'heavily-used' => 'Heavily Used',
	'broken' => 'Broken (for parts only)'
  );

  
  public static function bool($value, $options = array()) {
	return $value ? 'YES' : 'NO';
  }
  
  public static function date($value, $options = array()) {
	
	$value = is_string($value) ? strtotime($value) : $value;
	
	$format = array_key_exists('format', $options) ? $options['format'] : false;
	
	if ($format === false) {
	  
	  $format = Formatter::$date_formats['longDateTime'];
	} elseif (array_key_exists($format, Formatter::$date_formats)) {
	  $format = Formatter::$date_formats[$format];
	}

	return date($format, $value);
  }
  
  public static function locationType($type) {
	$ret_val = 'Unknown';
	
	if (array_key_exists($type, Formatter::$loc_types)) {
	  $ret_val = Formatter::$loc_types[$type];
	}
	
	return $ret_val;
  }
  
  public static function currency($value) {
	return '$' . Formatter::precision($value, 2, true);
  }
  
  public static function precision($value, $decimal_places = 2, $use_commas = false) {
	$ret_val = $value;
	
	if (is_numeric($value) && is_int($decimal_places)) {
	  $dec_pt = '.';
	  $comma = $use_commas ? ',' : '';
	  $ret_val = number_format($value, $decimal_places, $dec_pt, $comma);
	}
	
	return $ret_val;
  }
  
  public static function calculated($record, $func, $params = array()) {
    return $func($record, $params);
  }
  
  public static function difficulty($val, $type = 'simple') {
	if ($type == 'simple') {
	  return self::$simple_difficulty_types[$val];
	} else {
	  return self::$difficulty_types[$val];
	}
  }
  
  public static function cleanContent($content, $opts = array()) {
	$content = preg_replace('/<\s*\/?\s*br\s*\/?\s*>/i', "\n", $content);
	$content = preg_replace('/<\s*\/?\s*p\s*\/?\s*>/i', "\n\n", $content);
	$content = strip_tags($content);
	
	$content = preg_replace('/\n/i', '<br>', $content);
	return $content;
  }
	
	public static function escapeEntitiesPreserveMarkup($content) {
		$list = get_html_translation_table(HTML_ENTITIES);
		unset($list['"']);
		unset($list['<']);
		unset($list['>']);
		
		$search = array_keys($list);
		$values = array_values($list);
		$search = array_map('utf8_encode', $search);
		
		$str_out = str_replace($search, $values, $content);
		return $str_out;
	}
  
  
}
