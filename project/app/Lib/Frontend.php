<?php

if (array_key_exists('_escaped_fragment_', $_GET)) {
  header('Location: ' . $_GET['_escaped_fragment_'], true, 302);
  exit(0);
}


function outputContentBlock($name = false, $section = false, $default = false) {
  if ($name === false) {
	print '<!-- NO CONTENT BLOCK NAME SPECIFIED -->';
  } elseif (!preg_match('/^[A-Za-z-.]+$/', $name)) {
	print '<!-- INVALID CONTENT BLOCK NAME -->';
  } else {
	if ($section !== false) {
	  if (!preg_match('/^[A-Za-z-.]+$/', $section)) {
		print '<!-- INVALID CONTENT SECTION -->';
	  } else {
		$name = $section . DS . $name;
	  }
	}
	
	$file = WWW_ROOT . 'content' . DS . $name . '.html';
	if (file_exists($file)) {
	  readfile($file);
	} else {
		$default = $default != false ? $default : '<!-- CONTENT BLOCK DOES NOT EXIST: ' . $name . ' -->';
	  print $default;
	}
  }
}