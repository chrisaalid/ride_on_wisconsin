<?php
/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Model', 'Model');
App::uses('Template', 'Model');
App::uses('Setting', 'Model');
App::uses('CakeSession', 'Model/Datasource');
App::uses('Formatter', 'Data');
App::uses('GeocodeLib', 'Tools.Lib');

require_once(APP . 'Vendor' . DS . 'Mustache' . DS . 'Autoloader.php');

Mustache_Autoloader::register();

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {
  
  public $cached_fields = false;
  public $cache_includes_username = true;
  
  public $output_error_occured = false;
  public $output_error_message = '';
  
  public $Mustache = false;
  public $is_admin = false;
  
  public $Formatter = false;
  public $Template = null;
	public $Settings = false;
  
  public static $parent_inited = false;
  public $Geocode = null;
	
  
  public $printable = array(); // override printable action names in specficic models
	
	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);
		$this->Geocode = new GeocodeLib();
		$this->Geocode->setOptions(array('host' => 'us')); //optional - you can set it to your country's top level domain.	
  }
  
  public function outputError($msg) {
	$this->output_error_occured = true;
	$this->output_error_message = $msg;
  }
  
  public function mRender($template, $object) {
	if ($this->Mustache === false) {
	  $this->Mustache = new Mustache_Engine();
	}
	
	return $this->Mustache->render($template, $object);
  }
  
  public function cacheKey($user) {
	$key = md5($user . Configure::read('Security.salt'));
	return $key;
  }
  
  public function cachePath($user, $type = 'Route') {
	return WWW_ROOT . 'js/data/' . $type . '/' . $this->cacheKey($user) . '.json';
  }
  
  public function getContentForUser($user_id, $approved_only = true) {
	$options = array('conditions' => array($this->name . '.user_id' => $user_id), 'recursive'=>-1);
	
	if ($approved_only === true) {
	  $options['conditions'][$this->name . '.approved'] = 1;
	}
	
	$results = $this->find('all', $options);
	$user_content = array();
	foreach ($results as $idx => $rec) {
	  $user_content[] = $this->cacheable($rec);
	}
	
	$ret_obj = new stdClass();
	$ret_obj->_loaded = true;
	$ret_obj->content_type = $this->name;
	$ret_obj->records = $user_content;
	
	return $ret_obj;
  }
  
  public function cacheable($record) {
	$cache_obj = new stdClass();
	$cache_obj->_loaded = true;
	
	if ($this->cached_fields === false) {
	  $cache_obj->_message = 'No cache fields specified.  Override in model.';
	  $cache_obj->_loaded = false;
	} else {
	  
	  $rec = array_key_exists($this->name, $record) ? $record[$this->name] : $record;
	  if ($this->cached_fields === true) {
		foreach ($rec as $fld  => $val) {
		  $cache_obj->$fld = $val;
		}
	  } else {
		foreach ($this->cached_fields as $idx => $fld) {
		  if (isset($rec[$fld])) {
			$cache_obj->$fld = $rec[$fld];
		  } else {
		    $cache_obj->$fld = '[[MISSING DATA]]';
		  }
		}
	  }
	  
	  if ($this->cache_includes_username && isset($record['User']) && isset($record['User']['username'])) {
		$cache_obj->username = $record['User']['username'];
	  }
	}
	
	$cache_obj->dataType = ucfirst(strtolower(Inflector::singularize($this->name)));
	$cache_obj->ctrl = strtolower(Inflector::pluralize($this->name));
	
	return $cache_obj;
  }
  
  
  
  public function beforeSave($options = array()) {
    $user = CakeSession::read('Auth.User');
    $action = CakeSession::read('Action');
    
    if (array_key_exists('admin', $user) && !$user['admin'] && $action == 'edit') {
      $this->data['Route']['approved'] = false;
    }
  }
  
  private function normalizeField($field, $params) {
		switch (strtolower($params['type'])) {
			case 'string':
			$params['type'] = 'varchar';
			break;
			case 'boolean':
			$params['type'] = 'tinyint';
			$params['length'] = 1;
			break;
			case 'float':
			$params['type'] = 'decimal';
			break;
			case 'integer':
			$params['type'] = 'int';
			break;
		}
	
		$ent = array(
			'Field' => $field,
			'Type' => $params['type'] . (strlen($params['length']) > 0 ? '(' . $params['length'] . ')' : ''),
			'TypeName' => $params['type'],
			'TypeParams' => split(',', $params['length']),
			'Unsigned' => '',
			'Null' => $params['null'] ? 'YES' : 'NO',
			'Default' => $params['default'],
			'Extra' => ''
		);
		
		return $ent;
  }
  
  public function stdSchema() {
		$orig = $this->schema();
		$schema = new stdClass();
		$schema->columns = array();
		$schema->indices = array();
		foreach ($orig as $field => $params) {
			$ent = $this->normalizeField($field, $params);
			$schema->columns[$field] = $ent;
		}
		
		ksort($schema->columns);
		
		return $schema;
  }
  
  public function schemaDiff($table, $auth = array(), $db = array()) {
		$keys_auth = array_keys($auth);
		$keys_db = array_keys($db);
		
		$issues = array();
		
		$problems = array();
		
		$resolution = array();
		
		foreach ($auth as $fld => $params) {
			$a_fld = $auth[$fld];
			
			$issue = new stdClass();
			
			$issue->type = 'none';
			$issue->auth = $a_fld;
			$issue->db = false;
			$issue->field = false;
			$issue->resolution = '';
			
			
			if (!array_key_exists($fld, $db)) {
				$problems[] = "Missing Field: \"$fld\" of type \"{$a_fld['TypeName']}\"";
				$r_sql = "ALTER TABLE `{$table}` ADD COLUMN `{$fld}` " . strtoupper($a_fld['TypeName']) . " " . (count($a_fld['TypeParams']) > 0 ? ('(' . join(',', $a_fld['TypeParams']) . ') ') : '' ) . ($a_fld['Null'] == 'NO' ? 'NOT ' : '') . "NULL DEFAULT '{$a_fld['Default']}';";
				$resolution[] = "<code>$r_sql</code>";
				
				$issue->type = 'missing';
				$issue->field = $fld;
				$issue->resolution = $r_sql;
				$issues[] = $issue;
			
			} else {
				$d_fld = $db[$fld];
				
				if ($a_fld['TypeName'] != $d_fld['TypeName']) {
					if (strpos($a_fld['TypeName'], $d_fld['TypeName']) < 0 && strpos($d_fld['TypeName'], $a_fld['TypeName']) < 0) {
						$problems[] = "Type Conflict: \"{$fld}\" should be \"{$a_fld['TypeName']}\" not \"{$d_fld['TypeName']}\"";
						 
						$r_sql = "ALTER TABLE `{$table}` MODIFY COLUMN `{$fld}` {$a_fld['TypeName']};";
						 
						$issue->type = 'type';
						$issue->field = $fld;
						$issue->db = $d_fld;
						$issue->resolution = $r_sql;
						$issues[] = $issue;
					}
				} else {
					if ($a_fld['Default'] != $d_fld['Default']) {
						$problems[] = "Default Conflict: \"{$fld}\" should default to \"{$a_fld['Default']}\" not \"{$d_fld['Default']}\"";
						$r_sql = "ALTER TABLE `{$table}` ALTER COLUMN `{$fld}` SET DEFAULT '{$a_fld['Default']}';";
						$resolution[] = "<code>$r_sql</code>";
						
						$issue->type = 'default';
						$issue->field = $fld;
						$issue->db = $d_fld;
						$issue->resolution = $r_sql;
						$issues[] = $issue;
					}
				}
			}
			
			
		}
		return $issues;
  }
  
  public function cleanContent($content) {
	  $to_replace = array( 
		chr(145), 
		chr(146), 
		chr(147), 
		chr(148), 
		chr(151),
		'�'
	  );
	  
	  $replacements = array( 
		"'", 
		"'", 
		'&quot;', 
		'&quot;', 
		'&mdash;',
		'&mdash;'
	  ); 
	  return str_replace($to_replace, $replacements, $content); 
  }
  
  public function saveJSON($data, $section = false, $name = false) {
		if ($section === false) { $section = $this->name; }
		if ($name === false) { $name = 'all.json'; }
		
		$content_path = WWW_ROOT . 'js' . DS . 'data' . DS . $section . DS;
		
		$file_path = $content_path . $name;
		
		
		$writable = is_writable($content_path) && (!file_exists($file_path) || is_writable($file_path));
		
		
		
		//if (is_array($data)) {
		//  $new_data = array();
		//  for ($i = 0; $i < count($data); $i++) {
		//	$rec = $data[$i];
		//	$new_rec = new stdClass();
		//	foreach ($rec as $fld => $val) {
		//	  $new_rec->$fld = $val;
		//	  if (is_string($val)) {
		//		//$new_rec->$fld = $this->cleanContent($new_rec->$fld);
		//		//$new_rec->$fld = htmlspecialchars($new_rec->$fld, ENT_QUOTES);
		//	  }
		//	}
		//	$new_data[] = $new_rec;
		//  }
		//  $data = $new_data;
		//  pr($new_data);
		//}
		//exit(0);
		
		if ($writable) {
			file_put_contents($file_path, json_encode($data));
		}
  }
  
  public function geocodeOnSave() {
	$model = $this->name;
	if (array_key_exists($model, $this->data)) {
	  $addr_parts = array();
	  if (array_key_exists('address', $this->data[$model])) { $addr_parts[] = $this->data[$model]['address']; }
	  if (array_key_exists('city', $this->data[$model])) { $addr_parts[] = $this->data[$model]['city']; }
	  if (array_key_exists('state', $this->data[$model])) { $addr_parts[] = $this->data[$model]['state']; }
	  $addr = join(', ', $addr_parts);
	  
	  $result = $this->geocodeAddress($addr);
	  if ($result !== false) {
		$this->data[$model]['lat'] = $result['lat'];
		$this->data[$model]['lng'] = $result['lng'];
	  }
	}
	
	
  }

  public function geocodeAddress($address) {
	$result = false;
	if ($this->Geocode->geocode($address)) {
	  //debug($this->Geocode->getResult());
	  $result = $this->Geocode->getResult();
	  if (array_key_exists(0, $result)) {
		$result = $result[0];
	  }
	}
	
	return $result;
  }

  public function afterSave($created = false) {
	//pr('ack!'); exit(0);
	//$this->rebuildFrontend();
  }
  
  public function jsDataDir() {
		return WWW_ROOT . DS . 'js' . DS . 'data' . DS;
  }
  
  public function rebuildFormats() {
		exit(0);
  }
  
  public function rebuildIfStale($ref_file, $newer_file, $if_not) {
		$ref_mtime = file_exists($ref_file) ? filemtime($ref_file) : -1;
		$newer_mtime = file_exists($newer_file) ? filemtime($ref_file) : -1;
		
		if ($newer_mtime === -1 || $newer_mtime < $ref_mtime) {
			$this->$if_not();
		}
		
  }
  
  public function rebuildFrontend() {
	$src = APP . DS . 'Lib' . DS . 'Data' . DS . 'Formatter.php';
	$dest = $this->jsDataDir() . 'Misc' . 'formats.js';
	$action = 'rebuildFormats';
	
    $this->rebuildIfStale($src, $dest, $action);
  }
  
  public function saveContent($section, $name, $content, $extension = 'html') {
		$content_path = WWW_ROOT . 'content' . DS . $section . DS;
		
		$file_path = $content_path . $name . '.' . $extension;
		
		$writable = is_writable($content_path) && (!file_exists($file_path) || is_writable($file_path));
	
		if ($writable) {
			file_put_contents($file_path, $content);
		} else {
			$this->outputError(' but content file is not writable');
		}
  }
  
  public function coordDist($lat1, $lon1, $lat2, $lon2, $unit = 'M') {
	$theta = $lon1 - $lon2;
	$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	$dist = acos($dist);
	$dist = rad2deg($dist);
	$miles = $dist * 60 * 1.1515;
	$unit = strtoupper($unit);
	
	
	if (is_nan($miles)) {
	  $miles = 0;
	}
	
	
	if ($unit == "K") {
	  return ($miles * 1.609344);
	} else if ($unit == "N") {
	  return ($miles * 0.8684);
	} else {
	  return $miles;
	}
  }

  
  
  public function isPointInPoly($poly, $pt){
	for ($c = false, $i = -1, $l = count($poly), $j = $l - 1; ++$i < $l; $j = $i)
	  (($poly[$i]->y <= $pt->y && $pt->y < $poly[$j]->y) || ($poly[$j]->y <= $pt->y && $pt->y < $poly[$i]->y))
	  && ($pt->x < ($poly[$j]->x - $poly[$i]->x) * ($pt->y - $poly[$i]->y) / ($poly[$j]->y - $poly[$i]->y) + $poly[$i]->x)
	  && ($c = !$c);
	return $c;
  }
  
  public function hasField($name) {
	//pr($this->schema()); exit(0);
	$columns = $this->schema();
	
	return array_key_exists($name, $columns);
  }
  
}
