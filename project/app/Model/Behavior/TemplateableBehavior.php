<?php

App::uses('ModelBehavior', 'Model');

require_once(APP . 'Vendor' . DS . 'Mustache' . DS . 'Autoloader.php');

Mustache_Autoloader::register();

class TemplateableBehavior extends ModelBehavior {
  private $Template = null;
  private $Model = null;
  private $Mustache = null;
  public $settings = array();
  private $cache = array();
  
  public function setup(Model $Model, $settings = array()) {
	$this->Model = $Model;
	
	App::import('model','Template');
	$this->Template = new Template();
	
	$this->Mustache = new Mustache_Engine();
	
	if (!isset($this->settings[$Model->alias])) {
	  $this->settings[$Model->alias] = array(
		  'option1_key' => 'option1_default_value',
		  'option2_key' => 'option2_default_value',
		  'option3_key' => 'option3_default_value',
	  );
    }
    $this->settings[$Model->alias] = array_merge($this->settings[$Model->alias], (array)$settings);
	
	//$this->settings = $settings;
  }
  
  public function getTemplate($model, $name = false, $use_cache = true) {
	if ($name === false) { ''; }
	
	if (is_array($name)) {
	  $name = join('.', $name);
	}
	
	$tmpl = '';
	
	if ($use_cache && array_key_exists($name, $this->cache)) {
	  $tmpl = $this->cache[$name];
	} else {
	  $result = $this->Template->find('first', array('conditions' => array('Template.name' => $name)));
	  if (array_key_exists('Template', $result)) {
		$tmpl = $result['Template']['tmpl'];
		$this->cache[$name] = $tmpl;
	  }
	}
	
	return $tmpl;
  }
  
  public function render($model, $name = false, $data = array()) {
	if ($name === false) { return ''; }
	
	$tmpl = $name;
	if (preg_match('/^[A-Za-z\.]+$/', $name)) {
	  $tmpl = $this->getTemplate($model, $name);
	}
	
	
	return $this->Mustache->render($tmpl, $data);
  }
  
}
