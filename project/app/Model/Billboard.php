<?php
App::uses('AppModel', 'Model');
/**
 * Billboard Model
 *
 * @property Upload $Upload
 */
class Billboard extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'Billboards';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'key' => array(
			'CleanName' => array(
				'rule' => array('/^[A-Za-z0-9-]+$/'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'Unique' => 'isUnique'
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Upload' => array(
			'className' => 'Upload',
			'foreignKey' => 'billboard_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	
	public function afterSave($created = false) {
	  $this->rebuildFrontend();
	  parent::afterSave($created);
	}
	
	
	
	public function rebuildFrontend($id = false) {
		if (!isset($this->data['Billboard']['id']) && $id === false) { return; }
    if ($id === false) {
			$id = $this->data['Billboard']['id'];
	  }
		
		$billboard = $route = $this->find('first', array('conditions' => array('Billboard.id' => $id)));
		
		$this->saveContent('billboards', $billboard['Billboard']['key'], $billboard['Billboard']['markup']);
		//$this->saveContent('billboards', 'debug', print_r($this->data, true) . "\n\n" . print_r($billboard, true));
	}
	
	

}
