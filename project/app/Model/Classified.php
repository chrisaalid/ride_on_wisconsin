<?php
App::uses('AppModel', 'Model');
/**
 * Classified Model
 *
 * @property User $User
 */
class Classified extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'Classifieds';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';
	
	public $indexSearchFields = array('title','details');
	
  public $cached_fields = array(
	'id',
	'user_id',
	'title',
	'city',
	'state',
	'lat',
	'lng',
	'type',
	'bikeMake',
	'bikeModel',
	'partType',
	'partMake',
	'partModel',
	'price',
	'offers'
  );

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'type' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'title' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Please enter a title',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		//'bikeMake' => array(
		//	'notempty' => array(
		//		'rule' => array('notempty'),
		//		//'message' => 'Your custom message here',
		//		//'allowEmpty' => false,
		//		//'required' => false,
		//		//'last' => false, // Stop validation after this rule
		//		//'on' => 'create', // Limit validation to 'create' or 'update' operations
		//	),
		//),
		//'bikeModel' => array(
		//	'notempty' => array(
		//		'rule' => array('notempty'),
		//		//'message' => 'Your custom message here',
		//		//'allowEmpty' => false,
		//		//'required' => false,
		//		//'last' => false, // Stop validation after this rule
		//		//'on' => 'create', // Limit validation to 'create' or 'update' operations
		//	),
		//),
		//'partMake' => array(
		//	'notempty' => array(
		//		'rule' => array('notempty'),
		//		//'message' => 'Your custom message here',
		//		//'allowEmpty' => false,
		//		//'required' => false,
		//		//'last' => false, // Stop validation after this rule
		//		//'on' => 'create', // Limit validation to 'create' or 'update' operations
		//	),
		//),
		//'partModel' => array(
		//	'notempty' => array(
		//		'rule' => array('notempty'),
		//		//'message' => 'Your custom message here',
		//		//'allowEmpty' => false,
		//		//'required' => false,
		//		//'last' => false, // Stop validation after this rule
		//		//'on' => 'create', // Limit validation to 'create' or 'update' operations
		//	),
		//),
		'condition' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'price' => array(
			'money' => array(
				'rule' => array('money'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'offers' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'details' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Please provide a description of the item',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'approved' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	public $hasMany = array(
		'Upload' => array(
			'className' => 'Upload',
			'foreignKey' => 'classified_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)  
	);
	
	public function __construct($id = false, $table = null, $ds = null) {
	  parent::__construct($id, $table, $ds);
	  $this->Template = new Template();
		$this->Settings = new Setting();
	  $this->Upload = new Upload();
	}
	
	public function beforeSave($options = array()) {
	  parent::beforeSave($options);
	  
	  $agreed = false;
	  
	  $this->geocodeOnSave();
	  
	  if (array_key_exists('Classified', $this->data)) {// && array_key_exists('city', $this->data['Classified']) && array_key_exists('state', $this->data['Classified'])) {
		
		//$addr = $this->data['Classified']['city'] . ', ' . $this->data['Classified']['state'];
		//$result = $this->geocodeAddress($addr);
		//if ($result !== false) {
		//  $this->data['Classified']['lat'] = $result['lat'];
		//  $this->data['Classified']['lng'] = $result['lng'];
		//}
		
		
		if (array_key_exists('agree', $this->data['Classified'])) {
		  if ($this->data['Classified']['agree'] == 'yes') {
			$agreed = true;
		  }
		}
	  }
	  
	  if ($this->is_admin) {
		$agreed = true;
	  } else {
		if (!$agreed) {
		  $this->invalidate('agree',  '') ;//'You must agree to the terms');
		}
	  }
	  
	  return $agreed;
	}
	
	public function containsTerms($record, $terms, $parts = true, $bikes = true) {
	  
	  $fields = array('title', 'details');
	  
	  if ($bikes) {
			$fields[] = 'bikeMake';
			$fields[] = 'bikeModel';
	  }
	  
	  if ($parts) {
			$fields[] = 'partMake';
			$fields[] = 'partModel';
			$fields[] = 'partType';
	  }
	  
	  $terms = strtolower($terms);
	  foreach ($fields as $idx => $t) {
			$pos = strpos(strtolower($record[$t]), $terms);
			if ($pos !== false) {
				return true;
			}
	  }
	  
	  return false;
	}
	
	public function search($search_opts) {
	  
	  if (array_key_exists('parts', $search_opts)) { $search_opts['parts'] = $search_opts['parts'] === 'true'; }
	  if (array_key_exists('bikes', $search_opts)) { $search_opts['bikes'] = $search_opts['bikes'] === 'true'; }
	  if (array_key_exists('onlyImages', $search_opts)) { $search_opts['onlyImages'] = $search_opts['onlyImages'] === 'true'; }
	  if (array_key_exists('terms', $search_opts)) {
			if (strlen(trim($search_opts['terms'])) < 1) {
				$search_opts['terms'] = false;
			}
	  }
	  
	  $default_opts = array(
			'lat' => 36.090625,
			'lng' => -91.841536,
			'bikes' => true,
			'parts' => true,
			'onlyImages' => false,
			'range' => 100,
			'terms' => false
	  );
	  
	  if (array_key_exists('zip', $search_opts)) {
			$result = $this->geocodeAddress($search_opts['zip']);
			if ($result !== false) {
				$search_opts['lat'] = $result['lat'];
				$search_opts['lng'] = $result['lng'];
			}
	  }
	  
	  $params = array_merge($default_opts, $search_opts);
	  
	  $conditions = array(
			'Classified.approved' => 1
	  );
		
		$max_age = $this->Settings->getSetting('classified-expires-in');
		$max_age = array_key_exists('Setting', $max_age) ? $max_age['Setting']['content'] : 7;
		
		$conditions['TimeStampDiff(DAY, Classified.modified, Now()) <='] = $max_age;
		
	  // No results, guaranteed
	  if (!$params['bikes'] && !$params['parts']) {
			return array();
	  }
	  
	  if (!$params['bikes']) {
			$conditions['type'] = 'part';
	  } elseif (!$params['parts']) {
			$conditions['type'] = 'bike';
	  }
	  
	  $classifieds = $this->find('all', array('conditions' => $conditions, 'recursive' => 1));
	  
	  $matches = array();
	  
	  foreach ($classifieds as $idx => $classified) {
			$c = $classified['Classified'];
			$ul_count = count($classified['Upload']);
			
			if ($params['terms'] === false || $this->containsTerms($c, $params['terms'])) {
				if ($ul_count > 0 || $params['onlyImages'] === false) {
					$dist = $this->coordDist($c['lat'], $c['lng'], $params['lat'], $params['lng'], 'M');
					if ($dist <= $params['range']) {
						$matches[] = $c['id'];
					}
				}
			}
	  }
	  
		
	  return $matches;
	}
	
	
  public function rebuildJSON($approved) {
	$classifieds = array();

	foreach ($approved as $rec) {
	  $cro = $this->cacheable($rec);
	  
	  $nul = array();
	  foreach ($rec['Upload'] as $idx => $ul) {
		if ($this->Upload->fileExists($ul['id'])) {
		  $nul[] = $ul;
		}
	  }
	  $rec['Upload'] = $nul;
	  
	  $cro->firstImage = count($rec['Upload']) > 0 ? $rec['Upload'][0]['id'] : false;
	  $cro->formattedPrice = number_format($cro->price, 2, '.', ',');
	  $cro->isBike = $cro->type == 'bike';
	  $cro->isPart = $cro->type == 'part';
	  $classifieds[] = $cro;
	}
	
	//exit(0);
	$this->saveJSON($classifieds); 
  }

  public function rebuildFrontend() {
	$approved_classifieds = $this->find('all', array('conditions' => array('Classified.approved' => true)));
  
	$this->rebuildJSON($approved_classifieds);
  }
  
  public function afterSave($created = false) {
	$this->rebuildFrontend();
  }
}
