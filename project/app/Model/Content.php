<?php
App::uses('AppModel', 'Model');
App::uses('CakePdf', 'CakePdf.Pdf');
/**
 * Content Model
 *
 */
class Content extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'Contents';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Please enter a name',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'pattern' => array(
				'rule' => '/^[A-Za-z.-]+$/',
				'message' => 'Letters, dots, and dashes only',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'customUnique' => array(
			  'rule' => array('customUnique'),
			  'message' => 'This name is already used by another content block'
			),
			//'isUnique' => array(
			//	'rule' => 'isUnique',
			//	'required' => 'create',
			//	'message' => 'This name is already used by another content block'
			//),
		)
	);
	
	public function customUnique($check) {
	  $conditions = array(
		'name' => $check['name']
	  );
	  
	  if (isset($this->data['Content']['id'])) {
		$conditions['NOT'] = array('id' => $this->data['Content']['id']);
	  }
	  
	  $matches = $this->find('count', array('conditions' => $conditions));
	  
	  return $matches < 1;
	}
	
	
	public function beforeSave($options = array()) {
	  parent::beforeSave($options);
	  
	  $content_path = WWW_ROOT . 'content/';
	  
	  $file_path = $content_path . $this->data['Content']['name'] . '.html';
	  
	  $writable = is_writable($content_path) && (!file_exists($file_path) || is_writable($file_path));
  
	  if ($writable) {
      //file_put_contents($file_path, $this->data['Content']['content']);
	  } else {
      $this->outputError(' but content file is not writable');
	  }
	}
	
  public function rebuildFrontend() {
    $contents = $this->find('all');
    
    $content_path = WWW_ROOT . 'content/';
    
    foreach ($contents as $cont) {
      $c = $cont['Content'];
      $file_path = $content_path . $c['name'] . '.html';
	  
      $writable = is_writable($content_path) && (!file_exists($file_path) || is_writable($file_path));
    
      if ($writable) {
        file_put_contents($file_path, $c['content']);
      }
    }
  }
  
  public function afterSave($created = false) {
    $this->rebuildFrontend();
	//$this->makePDF($this->data['Content']['name']);
  }
  
  public function makePDF($name) {
	$options = array('conditions' => array('Content.name' => $name));
	$content = $this->find('first', $options);
	if (!array_key_exists('Content', $content)) {
	  return false;
	}
	
	if (!$this->validName($name)) {
	  return false;
	}
	
	
	$CakePdf = new CakePdf();
	
	$CakePdf->template('document', 'default');
	
	$web_path = 'content' . DS . 'pdfs' . DS . $name . '.pdf';
	$path = WWW_ROOT . $web_path;
	
	$CakePdf->viewVars(array(
	  'content' => $content['Content']['content']
	));
	
	if (file_exists($path) && is_writable($path)) {
	  unlink($path);
	}
	
	$pdf = @$CakePdf->write($path);
	return true;
  }
  
  public function validName($name) {
	return preg_match('/^[A-Za-z0-9\.-]+$/', $name);
  }
  
}
