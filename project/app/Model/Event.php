<?php
App::uses('AppModel', 'Model');
/**
 * Event Model
 *
 * @property User $User
 */
class Event extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'Events';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

	public $cached_fields = true;
	
	public $indexSearchFields = array('name','details');
	
	public $validTags = array(
	  'Partner Events','Ride','Road Race','Off-Road Race','Road Time Trial','Off-Road Time Trial','Multi-Sport Event','Weekly Club Road Ride','Meeting','Class','Special Event','Trail Building','Cyclocross Race','Fun Ride',  
	);
	
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Please provide a name for the event',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'start' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				'message' => 'Please choose a valid date/time',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'end' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				'message' => 'Please choose a valid date/time',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'venue' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Please enter the name of the event venue',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'address' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Please enter the address where the event takes place',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'state' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Please select the event state',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'maxlength' => array(
				'rule' => array('maxlength', 2),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'city' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Please enter the city where the event takes place',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'email' => array(
			'email' => array(
				'rule' => array('email'),
				'message' => 'Please provide a contact email address',
				'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'phone' => array(
			'phone' => array(
				'rule' => array('phone', '/^\d{3}-\d{3}-\d{4}(\s*x\d+)?$/'),
				'message' => 'Please enter a phone number in the format 000-000-0000 or 000-000-0000 x000',
				'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'url' => array(
			'url' => array(
				'rule' => array('url', true),
				'message' => 'Please enter a valid URL',
				'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'approved' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	public $hasMany = array(
		'Upload' => array(
			'className' => 'Upload',
			'foreignKey' => 'event_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)  
	);
	
	public function __construct($id = false, $table = null, $ds = null) {
	  parent::__construct($id, $table, $ds);
	  $this->Template = new Template();
	  $this->Upload = new Upload();
	}
	
	public function afterSave($created = false) {
	  $this->rebuildFrontend();
	  parent::afterSave($created);
	}
	
	public function cacheable($rec) {
	  $nrec = parent::cacheable($rec);
	  
	  //$nrec->difficultyLabel = Formatter::difficulty($rec->difficulty);
	  //$nrec->distance = Formatter::precision($rec->distance, 1);
	  
	  $start = strtotime($nrec->start);
	  $end = strtotime($nrec->end);
	  $nrec->startTS = $start;
	  $nrec->endTS = $end;
	  $nrec->startDate = date('D, M j, Y', $start);
	  $nrec->startTime = date('g:i a', $start);
	  $nrec->endDate = date('D, M j, Y', $end);
	  $nrec->endTime = date('g:i a', $end);
	  $nrec->featuredTime = date('F j, Y', $start); // June 21, 2013
	  
	  $nrec->uploads = array();
	  
	  $nul = array();
	  foreach ($rec['Upload'] as $idx => $ul) {
		if ($this->Upload->fileExists($ul['id'])) {
		  $nrec->uploads[] = $ul['id'];
		  $nul[] = $ul;
		}
	  }
	  $rec['Upload'] = $nul;
	  
	  $nrec->firstImage = count($rec['Upload']) > 0 ? $rec['Upload'][0]['id'] : false;
	  
	  return $nrec;
	}
	
	public function renderFeatured($event = array()) {
	  $featured_event_template = $this->Template->find('first', array('conditions' => array('name' => 'featured.Event')));
	  $featured_event = $this->mRender($featured_event_template['Template']['tmpl'], $event);
	  $this->saveContent('events', 'featured', $featured_event);
	}
	
	public function regenIfSearchOld() {
		$content_path = WWW_ROOT . 'content' . DS . 'events' . DS . 'rendered-search.html';
		$file_modified = filemtime($content_path);
		$file_modified_date = date('Ymd', $file_modified);
		$now_date = date('Ymd');
		$days_old = $now_date - $file_modified_date;
		if ($days_old > 0) {
			$this->rebuildFrontend();
		}
	}
	
	public function generateSearchResults($events = array()) {
	  $template = $this->Template->find('first', array('conditions' => array('name' => 'search.calendar.Event.list')));
	  $tmpl = $template['Template']['tmpl'];
	  $now = time();
	  $mkup = array();
	  $cnt = 0;
	  
	  if (!function_exists('generateEventSearchResults_SortFunction')) {
			function generateEventSearchResults_SortFunction($a, $b) {
				if ($a->startTS == $b->startTS) {
					return 0;
				}
				return ($a->startTS < $b->startTS) ? -1 : 1;
			}
	  }
	  usort($events, 'generateEventSearchResults_SortFunction');
	  
	  foreach ($events as $idx => $ev) {
			if ($ev->startTS >= $now) {
				$ev->oddEven = $cnt++ % 2 ? 'even' : 'odd';
				$mkup[] = $this->mRender($tmpl, $ev);
				
			}
	  }
	  
	  $this->saveContent('events', 'rendered-search', join("\n", $mkup));
	}
	
	public function rebuildFrontend() {
		$approved_events = $this->find('all', array('conditions' => array('Event.approved' => true), 'recursive' => 1));
	
		$events = array();
		
		foreach ($approved_events as $ev) {
			$cro = $this->cacheable($ev);
			$cro->regions = preg_split('/,\s*/', $cro->regions); //split(', ', $cro->regions);
			$cro->tags = preg_split('/,\s*/', $cro->tags); //split(',', $cro->tags);
			
			if ($cro->featured) {
			  $this->renderFeatured($cro);
			}
			
			if (strlen(trim($cro->venue)) < 1) { unset($cro->venue); }
			
			$events[] = $cro;
		}
		
		$this->saveJSON($events);
		
		$this->generateSearchResults($events);
	}
	
	public function beforeSave($options = array()) {
	  $lat = false;
	  $lng = false;
	  
	 
	  if (array_key_exists('address', $this->data['Event']) && array_key_exists('city', $this->data['Event']) && array_key_exists('state', $this->data['Event'])) {
		$this->geocodeOnSave();
		$lat = $this->data['Event']['lat'];
		$lng = $this->data['Event']['lng'];
	  }
	  
	  $region_path = WWW_ROOT . 'js' . DS . 'data' . DS . 'Region' . DS . 'all.json';
	  $region_data = file_get_contents($region_path);
	  $regions = json_decode($region_data);
	  
	  $in_regions = array();
	  
	  if (is_numeric($lat) && is_numeric($lng)) {
		$latLng = new stdClass();
		$latLng->x = $lng;
		$latLng->y = $lat;
		foreach ($regions as $idx => $reg) {
		  if ($this->isPointInPoly($reg->points, $latLng)) {
			$in_regions[] = $reg->name;
		  }
		}
		$this->data['Event']['regions'] = join(', ', $in_regions);
	  }
	  
	  
	  return true;
	}
	
}
