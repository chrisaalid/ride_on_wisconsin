<?php
App::uses('AppModel', 'Model');
/**
 * Group Model
 *
 * @property User $User
 */
class Group extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'Groups';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
	public $indexSearchFields = array('name','details');
	
	public $cached_fields = array(
	  'id',
	  'user_id',
	  'name',
	  'email',
	  'phone',
	  'url',
	  'city',
	  'state',
	  'lat',
	  'lng'
	);
	
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Please enter a name for the group',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'contact' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Please enter the name of the organizer/contact for this group',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'email' => array(
			'email' => array(
				'rule' => array('email'),
				'message' => 'Please enter a contact email address',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'url' => array(
			'notempty' => array(
				'rule' => array('url', true),
				'message' => 'Please enter a valid URL',
				'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'phone' => array(
			//'phone' => array(
				'rule' => '/^\d{3}-\d{3}-\d{4}$/', //array('phone'),
				'message' => 'Please enter enter the number using an XXX-XXX-XXXX format',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			//),
		),
		'venue' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Please enter the location rides depart from (e.g. bike shop name)',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'address' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Please enter the address rides depart from',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'state' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'city' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Please enter the city rides depart from',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'details' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Please enter group details here (i.e. mission, meeting frequency, etc)',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'approved' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	public $hasMany = array(
		'Upload' => array(
			'className' => 'Upload',
			'foreignKey' => 'group_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)  
	);
	
	public function rebuildJSON($approved) {
	  $groups = array();
  
	  foreach ($approved as $rec) {
		$cro = $this->cacheable($rec);
		$groups[] = $cro;
	  }
	  
	  $this->saveJSON($groups); 
	}
  
	public function rebuildFrontend() {
	  $approved_groups = $this->find('all', array('conditions' => array('Group.approved' => true)));
	
	  $this->rebuildJSON($approved_groups);
	}
	
	public function afterSave($created = false) {
	  $this->rebuildFrontend();
	}
	
	public function search($search_opts) {
	  
	  $default_opts = array(
		'lat' => 36.090625,
		'lng' => -91.841536,
		'range' => 100
	  );
	  
	  if (array_key_exists('zip', $search_opts)) {
		$result = $this->geocodeAddress($search_opts['zip']);
		if ($result !== false) {
		  $search_opts['lat'] = $result['lat'];
		  $search_opts['lng'] = $result['lng'];
		}
	  }
	  
	  $params = array_merge($default_opts, $search_opts);
	  
	  $conditions = array(
		'Group.approved' => 1
	  );
	  
	  $groups = $this->find('all', array('conditions' => $conditions, 'recursive' => 1));
	  
	  $matches = array();
	  
	  foreach ($groups as $idx => $group) {
		$g = $group['Group'];
		$ul_count = count($group['Upload']);
		
		$dist = $this->coordDist($g['lat'], $g['lng'], $params['lat'], $params['lng'], 'M');
		if ($dist <= $params['range']) {
		  $matches[] = $g['id'];
		}
	  }
	  
	  return $matches;
	}
	
}
