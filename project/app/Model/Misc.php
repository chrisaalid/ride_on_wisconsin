<?php

class Misc extends AppModel {
  // don't use a database for this model
  var $useTable = false;
  
  public function dataPath($name) {
	$path = WWW_ROOT . 'js' . DS . 'data' . DS . 'Misc' . DS . $name . '.json';
	return $path;
  }
  
  public function validName($name) {
	return preg_match('/^[A-Za-z0-9-]+$/', $name);
  }
  
  public function loadData($name) {
	if (!$this->validName($name)) {
	  return array();
	}
	
	$path = $this->dataPath($name);
	
	if (!file_exists($path)) {
	  return array();
	}
	
	$data = file_get_contents($path);
	return json_decode($data);
  }
  
  public function saveData($name, $data = array()) {
	if (!$this->validName($name)) {
	  return false;
	}
	
	$path = $this->dataPath($name);
	
	file_put_contents($path, json_encode($data));
	
	return true;
  }
  
}
