<?php
App::uses('AppModel', 'Model');
App::uses('User', 'Model');
/**
 * Route Model
 *
 * @property User $User
 */
class Route extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'Routes';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
	public $indexSearchFields = array('name','comment');
	private $gpx_error = false;
	
	public $actsAs = array('Templateable');

	public $printable = array('view');
	
	
	private $route_attributes = array(
	  'surfaces' => array(
		'surAsphalt' => 'Asphalt/Concrete',
		'surLimestone' => 'Crushed Limestone',
		'surGravel' => 'Gravel',
		'surWoodchips' => 'Woodchips',
		'surDirt' => 'Dirt/Turf',
		'surSand' => 'Sand'
	  ),
	  'topography' => array(
		'topFlat' => 'Flat',
		'topRolling' => 'Rolling',
		'topClimbs' => 'Long Climbs'
	  ),
	  'surroundings' => array(
		'surUrban' => 'Urban',
		'surRural' => 'Rural/Agricultural',
		'surResidential' => 'Residential',
		'surWooded' => 'Wooded',
		'surStream' => 'Stream/River',
		'surLake' => 'Lake',
		'surPrairie' => 'Prairie'
	  )
	);
	
	public $cached_fields = array(
	  'id',
	  'user_id',
	  'name',
	  'attributes',
	  'city',
	  'state',
	  'distance',
	  'difficulty',
	  'bounds',
	  'start_lat',
	  'start_lng',
	  'offroad'
	);
  
	
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Please provide a name for your route'
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'difficulty' => array(
			'notempty' => array(
			  'rule' => array('notempty'),
			  'message' => 'Please select a difficulty'
			)
		),
		'offroad' => array(
			'notempty' => array(
			  'rule' => array('notempty'),
			  'message' => 'Please select a ride type'
			)
		),
		'surAsphalt' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'surLimestone' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'surGravel' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'surWoodchips' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'surDirt' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'surSand' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'topFlat' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'topRolling' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'topClimbs' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'surUrban' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'surRural' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'surResidential' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'surWooded' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'surStream' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'surLake' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'surPrairie' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'points' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'approved' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	public $hasMany = array(
		'Upload' => array(
			'className' => 'Upload',
			'foreignKey' => 'route_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)  
	);
	
	public function __construct($id = false, $table = null, $ds = null) {
	  parent::__construct($id, $table, $ds);
	  $this->Template = new Template();
	  $this->Upload = new Upload();
	}
	
    
	public function afterFind($results, $primary = false) {
	  foreach ($results as $key => $val) {
      $results[$key]['Route']['attributes'] = array();
      $results[$key]['Route']['attributes']['surfaces'] = array();
      $results[$key]['Route']['attributes']['surroundings'] = array();
      $results[$key]['Route']['attributes']['topography'] = array();
      
      foreach ($this->route_attributes as $cat => $attrs) {
        foreach ($attrs as $field => $label) {
          if (isset($val['Route'])) {
            if (array_key_exists($field, $val['Route'])) {
              if ($val['Route'][$field]) {
                $results[$key]['Route']['attributes'][$cat][] = $label;
              }
            } else {
              unset($results[$key]['Route']['attributes']);
              return $results;  
            }
          }
        }
      }
	  }
	  
	  return $results;
	}
	
	public function beforeSave($options = array()) {
		parent::beforeSave($options);

		if (!empty($this->data['Route']['points'])) {
		  $points = json_decode($this->data['Route']['points']);
		  $start_pt = $points[0];
		  $end_pt = $points[count($points) - 1];
		  
		  $this->data['Route']['start_lat'] = $start_pt->lat;
		  $this->data['Route']['start_lng'] = $start_pt->lng;
		  $this->data['Route']['end_lat'] = $end_pt->lat;
		  $this->data['Route']['end_lng'] = $end_pt->lng;
		  $this->data['Route']['distance'] = $this->getDistance($points);
		  $this->data['Route']['bounds'] = join(',', $this->getBounds($this->data['Route']['points']));
		}
                
		return true;
	}
	
	public function afterSave($created = false) {
	  //pr($this->data); exit(0);
	  //$id = array_key_exists('Route', $this->data) && array_key_exists('id', $this->data['Route']) ? $this->data['Route']['id'] : false;
	  $this->rebuildKML();
	  $this->rebuildGPX();
	  $this->rebuildFrontend();
	  parent::afterSave($created);
	}
	
	
	public function pointStrToArray($points, $prec = 6) {
	  $pt_ar = split(';', $points);
	  $new_pt_ar = array();
	  foreach ($pt_ar as $idx => $pt) {
		$ll = split(',', $pt);
		$ll[0] = number_format(floatval($ll[0]), $prec);
		$ll[1] = number_format(floatval($ll[1]), $prec);
		$new_pt = new stdClass();
		$new_pt->lat = $ll[0];
		$new_pt->lng = $ll[1];
		$new_pt_ar[] = $new_pt;
	  }
	  
	  return $new_pt_ar;
	}
	
	public function pointArrayToStr($points) {
	  $new_pt_ar = array();
	  foreach ($points as $idx => $pt) {
    	$new_pt_ar[] = "$pt->lat,$pt->lng";
	  }
	  return join(';', $new_pt_ar);
	}
	
	public function pointPrecision($points, $prec = 6) {
	  $pt_ar = split(';', $points);
	  $new_pt_ar = array();
	  foreach ($pt_ar as $idx => $pt) {
      $ll = split(',', $pt);
      $ll[0] = number_format(floatval($ll[0]), $prec);
      $ll[1] = number_format(floatval($ll[1]), $prec);
      $new_pt_ar[] = "{$ll[0]},{$ll[1]}";
	  }
	  return join(';', $new_pt_ar);
	}
	
	public function urlForRoute($id) {
	  return 'http://' . $_SERVER['HTTP_HOST'] . '/#!/routes/view/' . $id;
	}
	
	public function pathToGPX($id) {
	  return WWW_ROOT . $this->wwwPathToGPX($id);
	}
	
	public function wwwPathToGPX($id) {
	   return '/gpx/route-' . $id . '.gpx';
	}
	
	public function rebuildGPX($id = false) {
	  if (!isset($this->data['Route']['id']) && $id === false) { return; }
      if ($id === false) {
		$id = $this->data['Route']['id'];
	  }
	  
	  $route = $this->find('first', array('conditions' => array('Route.id' => $id)));
	  $pt_ar = array();
	  
	  $points = json_decode($route['Route']['points']);
	  
	  $title = htmlentities($route['Route']['name']);
      $author = htmlentities($route['User']['firstname'] . ' ' . $route['User']['lastname']);
	  $year = date('Y', strtotime($route['Route']['created']));  
	  $url = $this->urlForRoute($id);
	  
	  
	  
	  $gpx = <<<__GPX_HEAD
<?xml version="1.0" encoding="utf-8"?>
  <gpx xmlns="http://www.topografix.com/GPX/1/1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd" creator="Ride On Wisconsin">
    <metadata>
        <name>{$title}</name>
        <author>
            <name>{$author}</name>
        </author>
        <copyright>
            <year>{$year}</year>
            <author>Ride On Wisconsin</author>
        </copyright>
    </metadata>
    <trk>
        <name>{$title}</name>
        <link>
            <href>{$url}</href>
        </link>
        <trkseg>
__GPX_HEAD;

		foreach ($points as $pt) {
		  $gpx .= <<<__GPX_POINT
			
			<trkpt lon="{$pt->lng}" lat="{$pt->lat}">
                <ele>0</ele>
            </trkpt>
__GPX_POINT;
		}

$gpx .= <<<__GPX_TAIL
            
        </trkseg>
    </trk>
</gpx>
__GPX_TAIL;

	  file_put_contents($this->pathToGPX($route['Route']['id']), $gpx);

	}
	
	public function pathToKML($id) {
	  return WWW_ROOT . $this->wwwPathToKML($id); //'kml/route-' . $id . '.kml';
	}
	
	public function wwwPathToKML($id) {
	  return '/kml/route-' . $id . '.kml';
	}
	
	public function rebuildKML($id = false) {
    if (!isset($this->data['Route']['id']) && $id === false) { return; }
    if ($id === false) {
			$id = $this->data['Route']['id'];
	  }
	  
	  $route = $this->find('first', array('conditions' => array('Route.id' => $id)));
	  $pt_ar = array();
	  
	  $points = json_decode($route['Route']['points']);
	  foreach ($points as $pt_pair) {
		$pt_ar[] = $pt_pair->lng . ',' . $pt_pair->lat . ',0';
	  }
	  
	  $pt_str = join("\n", $pt_ar);
	  
	  $title = $route['Route']['name'];
	  $comment = $route['Route']['comment'];
          
	  $kml = <<< __KML
<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2">
  <Document>
	<name>{$title}</name>
	<description>{$comment}</description>
	<Style id="style1">
	  <LineStyle>
		<color>BF0000FF</color>
		<width>3</width>
	  </LineStyle>
	</Style>
	<Style id="yellowLineGreenPoly">
	  <LineStyle>
		<color>7f0000ff</color>
		<width>4</width>
	  </LineStyle>
	  <PolyStyle>
		<color>7f00ff00</color>
	  </PolyStyle>
	</Style>
	<Placemark>
	  <name>{$title}</name>
	  <description>{$comment}</description>
	  <styleUrl>#style1</styleUrl>
	  <LineString>
		<tessellate>1</tessellate>
		<coordinates>
		  {$pt_str}
		</coordinates>
	  </LineString>
	</Placemark>
  </Document>
</kml>
__KML;

	  file_put_contents($this->pathToKML($route['Route']['id']), $kml);

	}
        
	//public function coordDist($lat1, $lon1, $lat2, $lon2, $unit = 'M') {
	//  $theta = $lon1 - $lon2;
	//  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	//  $dist = acos($dist);
	//  $dist = rad2deg($dist);
	//  $miles = $dist * 60 * 1.1515;
	//  $unit = strtoupper($unit);
	//  
	//  
	//  if (is_nan($miles)) {
	//	$miles = 0;
	//  }
	//  
	//  
	//  if ($unit == "K") {
	//	return ($miles * 1.609344);
	//  } else if ($unit == "N") {
	//	return ($miles * 0.8684);
	//  } else {
	//	return $miles;
	//  }
	//}



	
	public function getDistance($point_data) {
	  $pt_array = array();
	  
	  if (is_string($point_data)) {
		$pairs = split(';', $point_data);
		foreach ($pairs as $pair) {
		  $ll = split(',', $pair);
		  $latLng = new stdClass();
		  $latLng->lat = $ll[0];
		  $latLng->lng = $ll[1];
		  $pt_array[] = $latLng;
			
		}
	  } elseif (is_array($point_data)) {
		$pt_array = $point_data;;
	  }
	  
	  $dist = 0;
	  $last_pt = $pt_array[0];
	  for ($i = 1; $i < count($pt_array); $i++) {
		$pt = $pt_array[$i];
		$seg_dist = $this->coordDist($last_pt->lat, $last_pt->lng, $pt->lat, $pt->lng);
		$dist += $seg_dist;
		$last_pt = $pt;
	  }
	  
	  return $dist;
	}
	
	public function getBounds($point_data) {
	  $pt_array = array();
	  
	  $min_lat = 360;
	  $min_lng = 360;
	  $max_lat = -360;
	  $max_lng = -360;
	  
	  if (is_string($point_data)) { $point_data = json_decode($point_data); }
	  
	  foreach ($point_data as $ll) {
		if ($ll->lat < $min_lat) { $min_lat = $ll->lat; }
		if ($ll->lat > $max_lat) { $max_lat = $ll->lat; }
		if ($ll->lng < $min_lng) { $min_lng = $ll->lng; }
		if ($ll->lng > $max_lng) { $max_lng = $ll->lng; }
		  
	  }
	  
	  return array(number_format($min_lat, 6), number_format($max_lng, 6), number_format($max_lat, 6), number_format($max_lng, 6));
	}

	
	//public function rebuildCache($user) {
	//  //$cache_file = $this->cachePath($user['username'], 'Route');
	//  //
	//  //$routes = $this->find('all', array('conditions'=>array('Route.user_id'=>$user['id']), 'recursive'=>-1));
	//  //
	//  //file_put_contents($cache_file, json_encode($routes));
	//  //
	//  //$this->rebuildIndex($cache_file,
	//  
	//  //$ROUTE = new Route();
	//  
	//  $cache_root = WWW_ROOT . 'content/routes/';
	//  
	//  $routes = $this->find('all', array('conditions' => array('Route.approved' => 1), 'recursive' => -1));
	//  
	//  $r_obj_ar = array();
	//  foreach ($routes as $route) {
	//	$r = $route['Route'];
	//	$r_obj = new stdClass();
	//	foreach ($this->cached_fields as $f) {
	//	  $r_obj->$f = $r[$f];
	//	}
	//	
	//	$r_obj_ar[] = $r_obj;
	//  }
	//  
	//  foreach ($r_obj_ar as $idx => $r) {
	//	
	//  }
	//  
	//  
	//  //pr($r_obj_ar);
	//  //exit;
	//}
	
	private function xmlErrorHandler($errno, $errstr, $errfile, $errline, $errcontext) {
	  $this->gpx_error = true;
	}
	
	public function gpxToPointArray($gpx_xml) {
	  $gpx = new DOMDocument();
	  
	  $success = @$gpx->loadXML($gpx_xml);
	  
	  
	  $pt_array = array();
	  
	  if (!$success) {
		$this->outputError(libxml_get_last_error());
	  } else {
		$pts = $gpx->getElementsByTagName('trkpt');
		
		for ($i = 0; $i < $pts->length; ++$i) {
		  $pt = $pts->item($i);
		  if ($pt->hasAttribute('lat') && $pt->hasAttribute('lon')) {
			$pt_obj = new stdClass();
			$pt_obj->lat = $pt->getAttribute('lat');
			$pt_obj->lng = $pt->getAttribute('lon');
			$pt_array[] = $pt_obj;
		  }
		}
	  }
	  
	  return $pt_array;
	}
        
    
	public function saveRendered($mk_up) {
	  $content_path = WWW_ROOT . 'content' . DS . 'routes' . DS;
	  
	  $file_path = $content_path .  'list.html';
	  
	  //pr($file_path); exit(0);
	  
	  $writable = is_writable($content_path) && (!file_exists($file_path) || is_writable($file_path));
  
	  if ($writable) {	
		file_put_contents($file_path, $mk_up);
	  } else {
		$this->outputError(' but content file is not writable');
	  }
	}
	
	
	public function cacheable($rec) {
	  $nrec = parent::cacheable($rec);
	  $nrec->difficultyLabel = Formatter::difficulty($nrec->difficulty);
	  $nrec->distance = Formatter::precision($nrec->distance, 1);
	  $nrec->uploads = array();
	  
	  if (!function_exists('cacheable_SortFunction')) {
		function cacheable_SortFunction($a, $b) {
		  if ($a['order'] == $b['order']) {
			return 0;
		  }
		  return ($a['order'] < $b['order']) ? -1 : 1;
		}
	  }
	  
	  usort($rec['Upload'], 'cacheable_SortFunction');
	  
	  $nul = array();
	  foreach ($rec['Upload'] as $idx => $ul) {
		if ($this->Upload->fileExists($ul['id'])) {
		  $nrec->uploads[] = $ul['id'];
		  $nul[] = $ul;
		}
	  }
	  $rec['Upload'] = $nul;
	  
	  
	  $nrec->firstImage = count($rec['Upload']) > 0 ? $rec['Upload'][0]['id'] : false;
	  return $nrec;
	}
	
	public function rebuildJSON($approved_routes) {
      $routes = array();

	  foreach ($approved_routes as $rt) {
		  $cro = $this->cacheable($rt);
		  $routes[] = $cro;
	  }
	  
	  $this->saveJSON($routes); 
	}
	
	public function rebuildSlideShow() {
	  $routes = $this->find('all', array('conditions' => array('Route.featured' => 1)));
	  
	  $featured = array(8, 23, 30);
	  
	  $slides = array();
	  $nav = array();
	  $details = array();
	  
	  foreach ($routes as $idx => $route) {
		$r = $this->cacheable($route);
		if ($idx == 0) { $r->bonusClass = ' active'; }
		$slides[] = $this->render('slideshow.Route.slide', $r);
		$nav[] = $this->render('slideshow.Route.nav', $r);
		$details[] = $this->render('slideshow.Route.detail', $r);
	  }
	  
	  $firstID = array_key_exists(0, $routes) ? $routes[0]['Route']['id'] : 0;
	  
	  $slideshow = $this->render('slideshow.Route.wrapper', array(
	  	'slides' => join("\n", $slides),
		'nav' => join("\n", $nav),
		'details' => join("\n", $details),
		'firstID' => $firstID
	  ));
	  
	  $this->saveContent('routes', 'slideshow', $slideshow);
	  
	  return $slideshow;
	}

	public function generateSearchResults($routes = array()) {
	  $template = $this->Template->find('first', array('conditions' => array('name' => 'search.map.Route.list')));
	  $tmpl = $template['Template']['tmpl'];
	  $mkup = array();
	  
	  $srch_lat =  43.0730517;
	  $srch_lng = -89.4012302;
	  $max_dist = 100;
	  $cnt = 0;
	  $rts = array();
	  foreach ($routes as $idx => $rt) {
		$rt = $this->cacheable($rt);
		$rt->howFar = $this->coordDist($srch_lat, $srch_lng, $rt->start_lat, $rt->start_lng);
		$rt->dispHowFar = number_format($rt->howFar, 2);
		if ($rt->howFar <= $max_dist) {
		  $rts[] = $rt;
		}
	  }
	  
	  if (!function_exists('generateSearchResults_SortFunction')) {
		function generateSearchResults_SortFunction($a, $b) {
		  if ($a->howFar == $b->howFar) {
			return 0;
		  }
		  return ($a->howFar < $b->howFar) ? -1 : 1;
		}
	  }
	  
	  usort($rts, 'generateSearchResults_SortFunction');
	  
	  foreach ($rts as $r) {
		$r->oddEven = $cnt++ % 2 ? 'even' : 'odd';
		$mkup[] = $this->mRender($tmpl, $r);
	  }
	  
	  $this->saveContent('routes', 'rendered-search', join("\n", $mkup));
	}
	
	public function rebuildFrontend() {
	  $approved_routes = $this->find('all', array('conditions' => array('Route.approved' => true)));
  
	  $this->rebuildJSON($approved_routes);
	  $this->generateSearchResults($approved_routes);
	  $this->rebuildSlideShow();
	  
	  //parent::rebuildFrontend();
	}
	
	public function search($options = array()) {
	  $defaults = array(
		
	  );
	}
}
