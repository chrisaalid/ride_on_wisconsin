<?php
App::uses('AppModel', 'Model');
/**
 * Setting Model
 *
 */
class Setting extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'Settings';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
	
	public $indexSearchFields = array('name','description', 'content');

	public $dataTypes = array(
	  'string' => array('input' => 'string'),
	  'text' => array('input' => 'text'),
	  'number' => array('validation' => '^\d+(\.\d+)?$', 'input' => 'string', 'invalid' => 'Enter a numeric value or change the data type'),
	  'JSON' => array('input' => 'JSON', 'extension' => 'json')
	);
	
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'key' => array(
			'A_Za_z' => array(
				'rule' => array('custom', '/^[A-Za-z\.-]+$/'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'data_type' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'content' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	
	public function typeOptions() {
	  $ar = array();
	  foreach ($this->dataTypes as $key => $val) {
		$ar[$key] = $key;
	  }
	  return $ar;
	}
	
	public function getSetting($key) {
	  $setting = $this->find('first', array('conditions' => array('Setting.key' => $key)));
	  return $setting;
	}
	
	public function afterSave($created = false) {
	  $type = $this->dataTypes[$this->data['Setting']['data_type']];
	  $ext = array_key_exists('extension', $type) ? $type['extension'] : 'html';
	  
	  //pr('settings : ' . $this->data['Setting']['key'] . ' : ' . $this->data['Setting']['content'] . ' : ' . $ext); exit(0);
	  $this->saveContent('settings', $this->data['Setting']['key'], $this->data['Setting']['content'], $ext);
	  parent::afterSave($created);
	}
	
}
