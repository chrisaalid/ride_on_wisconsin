<?php

class StaticPage extends AppModel {
  public $pages = array();
  private $_setupComplete = false;
  

  // don't use a database for this model
  var $useTable = false;
  
  function setup() {
	$this->pages['home'] = rtrim(WWW_ROOT, '/') . DS . 'frontend.php';
	$this->pages['home-mobile'] = rtrim(WWW_ROOT, '/') . DS . 'frontend-mobile.php';
	$this->pages['robots'] = rtrim(WWW_ROOT, '/') . DS . 'robots.php';
	$this->pages['missing-dialog'] = rtrim(WWW_ROOT, '/') . DS . 'static-pages' . DS . 'missing-dialog.php';
	$this->pages['classifieds-disclaimer'] = rtrim(WWW_ROOT, '/') . DS . 'static-pages' . DS . 'classifieds-disclaimer.html';
	$this->_setupComplete = true;
  }
  
  function pagePath($name = false) {
	if (!$this->_setupComplete) { $this->setup(); }
	
	if ($name === false) { return false; }
	if (array_key_exists($name, $this->pages)) {
	  return $this->pages[$name];
	} else {
	  return false;
	}
  }
  
  function missing() {
	return $this->pagePath('missing-dialog');
  }
  
}
