<?php
App::uses('AppModel', 'Model');
/**
 * Template Model
 *
 */
class Template extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'Templates';

	public $order = 'Upper(Template.name) ASC';
	
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
	
  public $validate = array(
	'name' => array(
	  'notempty' => array(
		  'rule' => array('notempty'),
		  'message' => 'Please enter a name',
		  //'allowEmpty' => false,
		  //'required' => false,
		  //'last' => false, // Stop validation after this rule
		  //'on' => 'create', // Limit validation to 'create' or 'update' operations
	  ),
	  'pattern' => array(
		  'rule' => '/^[A-Za-z.-]+$/',
		  'message' => 'Letters, dots, and dashes only',
		  //'allowEmpty' => false,
		  //'required' => false,
		  //'last' => false, // Stop validation after this rule
		  //'on' => 'create', // Limit validation to 'create' or 'update' operations
	  ),
	  'customUnique' => array(
		'rule' => array('customUnique'),
		'message' => 'This name is already used by another template'
	  ),
	  //'isUnique' => array(
	  //	'rule' => 'isUnique',
	  //	'required' => 'create',
	  //	'message' => 'This name is already used by another content block'
	  //),
	)
  );
  
//  public function __construct($id = false, $table = null, $ds = null) {
//	parent::__construct($id, $table, $ds);
//  }
  
  public function customUnique($check) {
	$conditions = array(
	  'name' => $check['name']
	);
	
	if (isset($this->data['Template']['id'])) {
	  $conditions['NOT'] = array('id' => $this->data['Template']['id']);
	}
	
	$matches = $this->find('count', array('conditions' => $conditions));
	
	return $matches < 1;
  }
  
  public function getTemplate($name = false) {
	if ($name === false) { return '<strong>MISSING TEMPLATE</strong>'; }
	
	if (is_array($name)) {
	  $name = join('.', $name);
	}
	
	$result = $this->find('first', array('conditions' => array('Template.name' => $name)));
	print_r($result); die;
  }
  
  public function getTemplateTree() {
	$templates = $this->find('all');
	
	
	$root_node = array();
	
	//print '<pre>';
	
	foreach ($templates as $idx => $tmpl) {
	  $tmpl = $tmpl['Template'];
	  $name = $tmpl['name'];
	  $name_parts = preg_split('/\./', $name);
	  
	  $node =& $root_node;
	  
	  foreach ($name_parts as $idx => $n) {
		if ($idx == count($name_parts) - 1) {
		  $node[$n] = $tmpl['tmpl'];
		} else {
		  if (!array_key_exists($n, $node)) {
			$node[$n] = array();
		  }
		  $node =& $node[$n];
		}
	  }
	}
	
	return $root_node;
  }
  
  public function afterSave($created = false) {
	//$templates = $this->find('all');
	//
	//
	//$root_node = array();
	//
	//foreach ($templates as $idx => $tmpl) {
	//  $tmpl = $tmpl['Template'];
	//  $name = $tmpl['name'];
	//  $name_parts = preg_split('/\./', $name);
	//  
	//  $node =& $root_node;
	//  
	//  foreach ($name_parts as $idx => $n) {
	//	if ($idx == count($name_parts) - 1) {
	//	  $node[$n] = $tmpl['tmpl'];
	//	} else {
	//	  if (!array_key_exists($n, $node)) {
	//		$node[$n] = array();
	//	  }
	//	  $node =& $node[$n];
	//	}
	//  }
	//}
	
	$this->saveJSON($this->getTemplateTree());
  }  

}
