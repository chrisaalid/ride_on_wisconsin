<?php
App::uses('AppModel', 'Model');
/**
 * Upload Model
 *
 * @property User $User
 * @property Event $Event
 * @property Classified $Classified
 * @property Route $Route
 * @property Group $Group
 */
class Upload extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'Uploads';
	public $order = '`order` DESC';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'filename';

	public $upload_base = 'user-uploads';
	public $missing_upload = '/images/missing.jpg';
	
	
	private $thumbs_to_generate = array(
	  array(340, 325), // view dialog slideshow
	  array(128, 128), // edit preview thumbs
	  array(450, 250), // featured ride slideshow
	  array(295, 170), // featured event image
	  array(64, 64)    // previous uploads
	);
	
	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Event' => array(
			'className' => 'Event',
			'foreignKey' => 'event_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Classified' => array(
			'className' => 'Classified',
			'foreignKey' => 'classified_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Route' => array(
			'className' => 'Route',
			'foreignKey' => 'route_id',
			'conditions' => '',
			'fields' => '',
			'order' => '`order`'
		),
		'Group' => array(
			'className' => 'Group',
			'foreignKey' => 'group_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Location' => array(
			'className' => 'Location',
			'foreignKey' => 'location_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Merchandise' => array(
			'className' => 'Merchandise',
			'foreignKey' => 'merchandise_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	public function placeholder($absolute = true) {
	  $path = '';
	  
	  if ($absolute) {
		$path .= WWW_ROOT;
	  } else {
		$path = '/';
	  }
	  
	  $path .= 'images/placeholder/orig.gif';
	  
	  return $path;
	}
	
	public function image($id, $variant = null, $classes = array(), $attrs = array()) {
	  $url = $this->getURL($id, $variant);
	  $class = is_array($classes) && count($classes) > 0 ? (' class="' . join(' ', $classes) . '"') : '';
	  return '<img src="' . $url . '"' . $class . ' />';
	}
	
	public function getURL($id = null, $variant = 'orig') {
	  return $this->getPath($id, $variant, false);
	}
	
	public function fileExists($id = null) {
	  $path = $this->getPath($id, 'orig', true);
	  return file_exists($path);
	}
	
	public function getPath($id = null, $variant = 'orig', $absolute = false) {
	  if (!preg_match('/^(\d{1,5}x\d{1,5}|orig)$/', $variant)) {
			$variant = 'orig';
	  }
	  
	  if (!is_int($id) && !preg_match('/^\d+$/', $id)) {
			return $this->missing_upload;
	  }
	  
	  
	  $upload = $this->find('first', array('conditions' => array('Upload.id' => $id), 'recursive' => 0));
	  
	  
	  
	  if (array_key_exists('Upload', $upload)) {
		
			$upload = $upload['Upload'];
				
			$www_rel_path = '/user-uploads/' . $upload['user_id'] . '/' . $upload['md5'] . '/' . $variant . '.jpg';
			
			$abs_path = rtrim(WWW_ROOT, '/') . $www_rel_path;
			
			
			$url = $absolute === true ? $abs_path : $www_rel_path; //rtrim(WWW_ROOT, '/') : '';
		//$url .= $www_rel_path;
		return $url;
	  } else {
		return $this->missing_upload;
	  }
	}
	
	function croppedThumb($image_path, $thumb_width, $thumb_height, $web_path = false) {
  
	  if (!(is_integer($thumb_width) && $thumb_width > 0) && !($thumb_width === "*")) {
		  return false;
	  }
  
	  if (!(is_integer($thumb_height) && $thumb_height > 0) && !($thumb_height === "*")) {
		  return false;
	  }
  
	  $extension = pathinfo($image_path, PATHINFO_EXTENSION);
	  switch ($extension) {
		  case "jpg":
		  case "jpeg":
			  $source_image = imagecreatefromjpeg($image_path);
			  break;
		  case "gif":
			  $source_image = imagecreatefromgif($image_path);
			  break;
		  case "png":
			  $source_image = imagecreatefrompng($image_path);
			  break;
		  default:
			
			  return false;
			  break;
	  }
  
	  $source_width = imageSX($source_image);
	  $source_height = imageSY($source_image);
  
	  if (($source_width / $source_height) == ($thumb_width / $thumb_height)) {
		  $source_x = 0;
		  $source_y = 0;
	  }
  
	  if (($source_width / $source_height) > ($thumb_width / $thumb_height)) {
		  $source_y = 0;
		  $temp_width = $source_height * $thumb_width / $thumb_height;
		  $source_x = ($source_width - $temp_width) / 2;
		  $source_width = $temp_width;
	  }
  
	  if (($source_width / $source_height) < ($thumb_width / $thumb_height)) {
		  $source_x = 0;
		  $temp_height = $source_width * $thumb_height / $thumb_width;
		  $source_y = ($source_height - $temp_height) / 2;
		  $source_height = $temp_height;
	  }
  
	  $target_image = ImageCreateTrueColor($thumb_width, $thumb_height);
  
	  imagecopyresampled($target_image, $source_image, 0, 0, $source_x, $source_y, $thumb_width, $thumb_height, $source_width, $source_height);
  
	  $new_path = dirname($image_path) . '/' . $thumb_width . 'x' . $thumb_height . '.jpg';
  
	  imagejpeg($target_image, $new_path);
	  
	  imagedestroy($target_image);
	  imagedestroy($source_image);
	  
	  if ($web_path) {
		$new_path = '/' . substr($new_path, strlen(WWW_ROOT));
	  }
	  
	  return $new_path;
	  //return true;
  }
  
  public function processImage($path) {
	if (!file_exists($path)) {
	  $this->errors[] = "Uploaded file {$path} does not exist.";
	  return false;
	}
	if (($info = getimagesize($path)) === FALSE) {
	  $this->errors[] = "Uploaded file {$path} does not appear to be an image";
	  return false;
	}
	
	if ($info !== false && count($info) > 2) {
	  $type = $info[2];
	  $w = $info[0];
	  $h = $info[1];
	
	  
	
	  if ($type != IMAGETYPE_JPEG) {
		$path = $this->convertToJpeg($path, $type);
	  }
	  
	  $max_dim = max($w, $h);
	
	  if ($max_dim > 1280) { // scale down large image originals
		$orig = imagecreatefromjpeg($path);
		$nw = 0;
		$nh = 0;
		
		if ($w == $max_dim) {
		  $nw = 1280;
		  $nh = ($nw * $h) / $w;
		} else {
		  $nh = 1280;
		  $nw = ($nh * $w) / $h;
		}
		
		$scaled = ImageCreateTrueColor($nw, $nh);
		imagecopyresampled($scaled, $orig, 0, 0, 0, 0, $nw, $nh, $w, $h);
		imagejpeg($scaled, $path);
	  }
	  
	  $this->createSizes($path, $info);
	  
	  return $path;
	} else {
	  $this->errors[] = "Could not extract metadata from {$path}";
	  return false;
	}
  }
  
  public function createSizes($path) {
	foreach ($this->thumbs_to_generate as $dimensions) {
	  $this->Upload->croppedThumb($path, $dimensions[0], $dimensions[1]);
	}
  }
  
	
}
