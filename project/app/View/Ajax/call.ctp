<?php

$jsonObj = new stdClass();

$mockReq = new stdClass();
$mockReq->named = new stdClass();
$mockReq->pass = array();
$mockReq->action = 'unknown';

$jsonObj->status = isset($status) && $status === false ? false : true;
$jsonObj->message = isset($message) ? $message : 'No message';
$jsonObj->request = isset($request) ? $request : $mockReq;
$jsonObj->payload = isset($payload) ? $payload : new stdClass();

print json_encode($jsonObj);

?>