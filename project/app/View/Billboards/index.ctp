<?php

for ($i = 0; $i < count($billboards); $i++) {
	$b = $billboards[$i];
	$billboards[$i]['Billboard']['photos'] = 0;
	foreach ($b['Upload'] as $idx => $u) {
		$billboards[$i]['Billboard']['photos'] += $UPLOADS->fileExists($u['id']) ? 1 : 0;
	}
}


echo $this->element('display-index', array(
  'records' => $billboards,
  'use_only' => array('name', 'key', 'photos', 'created', 'modified'),
	'is_searchable' => false
));

?>