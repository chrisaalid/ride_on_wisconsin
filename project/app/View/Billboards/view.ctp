<div class="billboards view">
<h2><?php  echo __('Billboard'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($billboard['Billboard']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($billboard['Billboard']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Key'); ?></dt>
		<dd>
			<?php echo h($billboard['Billboard']['key']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Notes'); ?></dt>
		<dd>
			<?php echo h($billboard['Billboard']['notes']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Markup'); ?></dt>
		<dd>
			<?php echo $this->element('fields/code-editor', array('field_name' => 'tmpl', 'label' => 'Markup', 'read_only' => true, 'code' => $billboard['Billboard']['markup'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($billboard['Billboard']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($billboard['Billboard']['modified']); ?>
			&nbsp;
		</dd>
	</dl>

	<div class="edit-route">
		<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $billboard['Billboard']['id'])); ?>
	</div><!-- /.edit-route -->
	
	<?php echo $this->element('content-slide-show', array('record' => $billboard)); ?>
</div>
