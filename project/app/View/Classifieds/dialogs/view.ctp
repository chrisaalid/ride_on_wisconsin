<?php

$c = $classified['Classified'];

$make = '...';
$model = '...';
$partType = '...';

if ($c['type'] == 'bike') {
  $make = $c['bikeMake'];
  $model = $c['bikeModel'];
} else {
  $make = $c['partMake'];
  $model = $c['partModel'];
  $partType = $c['partType'];
}

$bonus_links = array();
if ($search !== false) {
  $bonus_links[] = $this->Html->link('Back To Results', array('action' => 'search', 'cache'), array('class'=>'header-btns edit-icon back-btn'));
}

?>
<div class="dialog-wrap lrg">
	<?php
	
	echo $this->element('dialog-header', array('title' => 'Bike Swap Classifieds', 'bonus_links' => $bonus_links));
	
	?>
	<div class="dl-body first">
		<div class="section bot">
			<div class="col six">
				<div class="col-pad-right">
					<h1><?php echo h($c['title']); ?></h1>
					$<?php echo $c['price'];
					
					if ($c['offers']) { echo ' o.b.o.'; }
					
					?><br><br>
					
					<table>
						<tr>
							<td>Location:</td>
							<td><?php echo h($c['city']) . ', ' . h($c['state']) ?></td>
						</tr>
						<!--<tr>
							<td>Distance:</td>
							<td>12 Miles</td>
						</tr>-->
						<tr>
							<td>MFR:</td>
							<td><?php echo h($make); ?></td>
						</tr>
						<tr>
							<td>Model:</td>
							<td><?php echo h($model); ?></td>
						</tr>
						<?php
						if ($c['type'] == 'part') { ?>
						<tr>
						  <td>Part Type:</td>
						  <td><?php echo h($partType); ?></td>
						</tr>
						<?php
						}
						?>
						<tr>
							<td>Email:</td>
							<td><a href="mailto:<?php echo $classified['User']['email']; ?>"><?php echo h($classified['User']['email']); ?></a></td>
						</tr>
					</table>

					<hr class="dashed min">

					<p><?php echo h($c['details']); ?></p>
					
					<div class="row">
						<a href="/classifieds-disclaimer.html" target="_blank">Disclaimer</a>
					</div><!-- .row -->
				</div><!-- /.col-pad-right -->
			</div><!-- /.col .six -->
			<div class="col six dashed-left">
				<div class="col-pad-left ta-center">
					<?php echo $this->element('content-slide-show', array('record' => $classified)); ?>
				</div><!-- /.col-pad-left -->
			</div><!-- /.col .six -->
		</div><!-- /.section -->
	</div><!-- /.dl-body -->
</div><!-- /.dialog-wrap-sm -->  
</body>
</html>