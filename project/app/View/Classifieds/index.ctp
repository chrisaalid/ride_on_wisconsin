<?php

for ($i = 0; $i < count($classifieds); $i++) {
	$c = $classifieds[$i];
	$classifieds[$i]['Classified']['photos'] = array_key_exists('Upload', $c) ? count($c['Upload']) : 0;
}

echo $this->element('display-index', array(
  'records' => $classifieds,
  'use_only' => array('User.username', 'title', 'approved', 'type', 'price', 'photos', 'city', 'state', 'created', 'modified'),
  'labels' => array('User.username' => 'Owner')
));

?>
