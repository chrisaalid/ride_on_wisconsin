<div class="classifieds view">
<h2><?php  echo __('Classified'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($classified['Classified']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($classified['User']['username'], array('controller' => 'users', 'action' => 'view', $classified['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo h($classified['Classified']['type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($classified['Classified']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('BikeMake'); ?></dt>
		<dd>
			<?php echo h($classified['Classified']['bikeMake']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('BikeModel'); ?></dt>
		<dd>
			<?php echo h($classified['Classified']['bikeModel']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('PartMake'); ?></dt>
		<dd>
			<?php echo h($classified['Classified']['partMake']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('PartModel'); ?></dt>
		<dd>
			<?php echo h($classified['Classified']['partModel']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Condition'); ?></dt>
		<dd>
			<?php echo h($classified['Classified']['condition']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Price'); ?></dt>
		<dd>
			<?php echo h($classified['Classified']['price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Offers'); ?></dt>
		<dd>
			<?php echo h($classified['Classified']['offers']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Photo'); ?></dt>
		<dd>
			<?php echo h($classified['Classified']['photo']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Details'); ?></dt>
		<dd>
			<?php echo $this->FieldFormatter->cleanContent($classified['Classified']['details']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Approved'); ?></dt>
		<dd>
			<?php echo h($classified['Classified']['approved']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($classified['Classified']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($classified['Classified']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
