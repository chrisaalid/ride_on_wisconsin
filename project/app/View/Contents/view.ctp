<div class="contents view">
<h2><?php  echo __('Content'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($content['Content']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($content['Content']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Content'); ?></dt>
		<dd>
			<?php
			
			//h($content['Content']['content']);
			
			echo $this->element('fields/code-editor', array('read_only' => true, 'code' => $content['Content']['content']));
			
			?>
			&nbsp;
		</dd>
	</dl>
</div>
