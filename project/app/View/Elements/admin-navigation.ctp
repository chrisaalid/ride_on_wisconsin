<?php

$sections = array(
	'users' => 'Users',
	'events' => 'Events',
	'classifieds' => 'Classifieds',
	'routes' => 'Routes',
	'locations' => 'Locations',
	'groups' => 'Groups',	
	'contents' => 'Contents',
	'misc' => 'Miscellaneous',
	'merchandises' => 'Merchandises',
	'billboards' => 'Billboards',
	
	'templates' => 'Templates',
	'settings' => 'Settings'
);

$dev_only_sections = array(
  'templates',
  'settings'
);



$PLURAL_CURRENT_MODEL = Inflector::pluralize($CURRENT_MODEL);

if (!empty($logged_in_user) && $logged_in_user['admin']) {

?>

<div class="actions">
	<h3><?php echo __('Manage'); ?></h3>
	<ul>
		<?php
		
		
		foreach ($sections as $ctlr => $lbl) {
			
			if (!in_array($ctlr, $dev_only_sections) || $logged_in_user['developer']) {
			
			  $classes = array();
			  //if ($PLURAL_CURRENT_MODEL == $lbl) {
			  if (strtolower($PLURAL_CURRENT_MODEL) == $ctlr) {
				  $classes[] = 'active';
			  }
			  
			  ?>
			  <li class="<?php echo join(' ', $classes); ?>">
				  <?php
				  
				  echo $this->Link->link(__($lbl), array('controller' => $ctlr, 'action' => 'index'));
				  //if ($PLURAL_CURRENT_MODEL == $ctlr) {
				  if (strtolower($PLURAL_CURRENT_MODEL) == $ctlr) {
					  echo $this->Link->link(__(' + '), array('controller' => $ctlr, 'action' => 'add'), array('class'=>'addButton'));
					  if ($CURRENT_ACTION == 'view') {
						  //echo $this->Link->link(__('Edit'), array('controller' => $ctlr, 'action' => 'edit', $CURRENT_PARAMS[0]), array('class'=>'addButton'));
					  }
				  }
				  
				  ?>
				  
			  </li>
			  <?php
			}
		}
		
		if (isset($user) && isset($user['username'])) {
			echo '<li>' . $this->Html->link(__('Log Out ' . $user['username']), array('controller' => 'users', 'action' => 'logout')) . '</li>';
		}
?>
	</ul>
</div>

<?php

} // admin user?

?>