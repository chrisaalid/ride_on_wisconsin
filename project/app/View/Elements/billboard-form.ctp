<?php echo $this->Form->create('Billboard', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __(($edit_mode ? 'Edit' : 'Add' ) . ' Billboard'); ?></legend>
	<?php
		if ($edit_mode) {
			echo $this->Form->input('id');  
	  }
		echo $this->Form->input('name');
		echo $this->Form->input('key');
		echo $this->Form->input('notes');
		//echo $this->Form->input('markup');
		echo $this->element('fields/code-editor', array('field_name' => 'markup', 'label' => 'Billboard Markup'));
		
		echo $this->element('fields/file-upload', array('label' => 'Billboard Image', 'max' => 1, 'show_urls' => true));
		
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>