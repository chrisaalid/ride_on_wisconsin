<?php

$this->Html->script('classifieds', array('inline' => false));

?>

<style type="text/css">
<?php

$listing_types = array();

if ($add_mode && !array_key_exists('Classified', $this->request->data)) {
  $listing_types[''] = '--- Choose ---';

?>
  .classifiedType {
	display: none;
  }
<?php

} else {
  if ($this->request->data['Classified']['type'] == 'part') {
	print ".bikeClassified { display: none; }";
  } else {
	print ".partClassified { display: none; }";
  }
}

$listing_types = array_merge($listing_types,  $this->FieldFormatter->classified_types());


?>
</style>

<div class="classifieds form">
<?php echo $this->Form->create('Classified', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __(($edit_mode ? 'Edit' : 'Add') . ' Classified'); ?></legend>
	<?php
	
		if ($edit_mode) {
		  echo $this->Form->input('id');
		}
	
		echo $this->element('fields/user');
		
		
		echo $this->Form->input('title');
		
		echo $this->Form->input('type', array('id' => 'listingType', 'type' => 'select', 'options' => $listing_types));
		
		?>
		<div class="classifiedType bikeClassified">
		<?php
		echo $this->Form->input('bikeMake', array('required' => false));
		echo $this->Form->input('bikeModel', array('required' => false));
		?>
		</div>
		
		<div class="classifiedType partClassified">
		<?php
		
		echo $this->Form->input('partMake', array('required' => false));
		echo $this->Form->input('partModel', array('required' => false));
		?>
		</div>
		
		<?php
		echo $this->Form->input('city');
		echo $this->element('fields/state-picker');
		echo $this->Form->input('condition', array('type' => 'select', 'options' => $this->FieldFormatter->classified_condition()));
		echo $this->Form->input('price');
		echo $this->Form->input('offers', array('label' => 'Or Best Offer (O.B.O.)'));
		echo $this->Form->input('details');
		
		echo $this->element('fields/file-upload', array('label' => 'Item Images', 'max' => 5));
		
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
