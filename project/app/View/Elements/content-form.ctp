<?php echo $this->Form->create('Content'); ?>
	<fieldset>
		<legend><?php echo __(($edit_mode ? 'Edit' : 'Add') . ' Content'); ?></legend>
	<?php
	
		if ($edit_mode) {
		  echo $this->Form->input('id');
		}
	
		echo $this->Form->input('name', array('after' => 'Letters, dashes, and dots only.'));
		echo $this->element('fields/code-editor', array('field_name' => 'content', 'label' => 'HTML Content'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>