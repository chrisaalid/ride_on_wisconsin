		  <div class="section bot">
			<h2 class="black">Your Submissions</h2>
			  <div class="controls">
				<strong>Filters:</strong>
				<div class="filters">
				  <?php
				  foreach (array('Routes', 'Locations', 'Events', 'Classifieds', 'Groups') as $ct) {
					echo '<span>';
					echo $this->Form->input($ct, array('type'=>'checkbox', 'data-type' => 'type' . Inflector::singularize($ct), 'div' => false, 'checked' => 'checked'));
					echo '</span>&nbsp;';
				  }
				  
				  ?>
				</div><!-- .filters -->
			  </div><!-- .controls -->
			  <div class="scrollTable" style="max-height: 400px; overflow-y: scroll;">
				<table class="tbl-pad striped contentPage" cellpadding="0" cellspacing="0">
				  <tr>
					  <th>Type</th>
					  <th>Title</th>
					  <th>Status</th>
					  <th></th>
				  </tr>
			
			
			  <?php
				
				$icon_map = array(
				  'route' => 'path',
				  'location' => 'bike-shops',
				  'classified' => 'bike-wheel',
				  'group' => 'local-chapters',
				  'event' => 'event',
				);
				
				$pg_num = 1;
				$pg_cnt = 0;
				$per_pg = 15;
				
				foreach ($content as $c) {
				  $tr_classes = array();
				  
				  $icon = array_key_exists($c['content_type'], $icon_map) ? $icon_map[$c['content_type']] : 'path';
				  $title = array_key_exists('title', $c) ? $c['title'] : $c['name'];
				  $status = $c['approved'] ? 'Active' : 'Awaiting Approval';
				  $status_class = $c['approved'] ? 'status-approved' : 'status-pending';
				  
				  
				  $tr_classes[] = $status_class;
				  $tr_classes[] = 'type' . ucfirst($c['content_type']);
				  if (array_key_exists('type', $c)) {
					$tr_lasses[] = 'subType' . $c['type'];
				  }
				  ?>
				  <tr class="<?php echo join(' ', $tr_classes); ?>">
					<td class="ta-center"><span aria-hidden="true" class="icon-<?php echo $icon; ?>"></span></td>
					<td><?php echo $title; ?></td>
					<td><?php echo $status; ?></td>
					<td class="ta-center">
						<?php echo $this->Html->link('View', array('controller' => $c['controller'], 'action' => 'view', $c['id']), array('class' => 'btn utility')); ?>
						<?php echo $this->Html->link('Edit', array('controller' => $c['controller'], 'action' => 'edit', $c['id']), array('class' => 'btn utility')); ?>
						<?php echo $this->Html->link('Delete', array('controller' => $c['controller'], 'action' => 'delete', $c['id']), array('class' => 'btn utility')); ?>
					</td>
				  </tr>
				  <?php
				  
				} // foreach content
				
			  
				?>
				</table>
			  </div><!-- .scrollTable -->
				<script>
				  $(function() {
					var $filterCBs = $('.filters input[type="checkbox"]');
					var $scrollTable = $('.scrollTable');
					var changing = false;
					
					$filterCBs.change(function(e) {
					  var $this = $(this);
					  if (changing) {
						return;
					  }
					  $filterCBs.prop('disabled', true);
					  changing = true;
					  
					  var ctClass = '.' + $this.data('type');
					  setTimeout(function() {
						if ($this.is(':checked')) {
						  var $toShow = $scrollTable.find(ctClass);
						  if ($toShow.length > 0) {
							$toShow.show(250, function() {
							  changing = false;
							  $filterCBs.prop('disabled', false);
							});
						  } else {
							changing = false;
							$filterCBs.prop('disabled', false);
						  }
						} else {
						  var $toHide = $scrollTable.find(ctClass);
						  if ($toHide.length > 0) {
							
							$toHide.hide(250, function() {
							  changing = false;
							  $filterCBs.prop('disabled', false);
							});
						  } else {
							changing = false;
							$filterCBs.prop('disabled', false);
						  }
						}
					  }, 10);
					});
				  });
				</script>
			
		</div><!-- /.section -->