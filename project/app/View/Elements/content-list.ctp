		  <div class="section bot">
			<h2 class="black">Your Submissions</h2>
			  <?php
				
				$icon_map = array(
				  'route' => 'path',
				  'location' => 'bike-shops',
				  'classified' => 'bike-wheel',
				  'group' => 'local-chapters',
				  'event' => 'event',
				);
				
				$pg_num = 1;
				$pg_cnt = 0;
				$per_pg = 15;
				
				foreach ($content as $c) {
				  if ($pg_cnt == 0) {
					$style = $pg_num != 1 ? ' style="display:none;"' : '';
					
					?>
					<table class="tbl-pad striped contentPage<?php echo $pg_num; ?>" cellpadding="0" cellspacing="0"<?php echo $style; ?>>
					  <tr>
						  <th>Type</th>
						  <th>Title</th>
						  <th>Status</th>
						  <th></th>
					  </tr>
					<?php
				  }
				  
				  $pg_cnt++;
				  
				  
				  $icon = array_key_exists($c['content_type'], $icon_map) ? $icon_map[$c['content_type']] : 'path';
				  $title = array_key_exists('title', $c) ? $c['title'] : $c['name'];
				  $status = $c['approved'] ? 'Active' : 'Awaiting Approval';
				  $status_class = $c['approved'] ? 'status-approved' : 'status-pending';
				  
				  ?>
				  <tr class="<?php echo $status_class; ?>">
					<td class="ta-center"><span aria-hidden="true" class="icon-<?php echo $icon; ?>"></span></td>
					<td><?php echo $title; ?></td>
					<td><?php echo $status; ?></td>
					<td class="ta-center">
						<?php echo $this->Html->link('View', array('controller' => $c['controller'], 'action' => 'view', $c['id']), array('class' => 'btn utility')); ?>
						<?php echo $this->Html->link('Edit', array('controller' => $c['controller'], 'action' => 'edit', $c['id']), array('class' => 'btn utility')); ?>
						<?php echo $this->Html->link('Delete', array('controller' => $c['controller'], 'action' => 'delete', $c['id']), array('class' => 'btn utility')); ?>
					</td>
				  </tr>
				  <?php
				  
				  
				  if ($pg_cnt > $per_pg) {
					?></table><?php
					
					$pg_num++;
					$pg_cnt = 0;
				  }
				  
				}
				
				
				if ($pg_num > 1) {
				  
				  ?><div class="contentPagination"><?php
				  
				  $pages = array();
				  
				  for ($i = 1; $i < $pg_num; $i++) {
					$class = $i == 1 ? ' class="active"' : '';
					$pages[] = "<a href=\"#\" class=\"pagePicker{$i}\" data-page-number=\"{$i}\"{$class}>{$i}</a>";
				  }
				  
				  echo join(' | ', $pages);
				  
				  ?></div><!-- .pagination --><?php
				}
				?>
				<script>
				  $(function() {
					var active_page = 1;
					var $active_picker = $('pagePicker' + active_page);
					
					$('.contentPagination').on('click', 'a', function(e) {
					  var $this = $(this);
					  var pg = $this.data('page-number');
					  
					  var $cur = $('table.contentPage' + active_page);
					  var $next = $('table.contentPage' + pg);
					  
					  $active_picker.removeClass('active');
					  $this.addClass('active');
					  
					  $cur.hide();
					  $next.show();
					  
					  active_page = pg;
					  $active_picker = $('pagePicker' + active_page);
					  
					  if (e.preventDefaukt) { e.preventDefault(); }
					  return false;
					});
				  });
				</script>
			
		</div><!-- /.section -->