<?php

$record = isset($record) ? $record : false;
$width = isset($width) && is_int($width) && $width > 0 ? $width : 340;
$height = isset($height) && is_int($height) && $height > 0 ? $height : 325;
$placeholder_override = isset($placeholder_override) ? $placeholder_override : false;

$images = false;

if ($record !== false) {
  if (array_key_exists('Upload', $record)) {
	$images = array();
	foreach ($record['Upload'] as $img) {
	  if ($UPLOADS->fileExists($img['id'])) {
		$images[] = $img;
	  }
	}
	
    if (count($images) < 1) { $images = false; }
  }
}

$mk_up = array();

if ($images === false || (is_array($images) && count($images) < 1)) {
  if ($placeholder_override === false) {
	$placeholder = $UPLOADS->croppedThumb($UPLOADS->placeholder(true), $width, $height, true);
  } else {
	$placeholder = 'http://maps.googleapis.com/maps/api/streetview?size=' . $width . 'x' . $height . '&sensor=false&location=' . urlencode($placeholder_override);
  }
  
?>
<div class="contentSlideShowWrapper" style="height: <?php echo ($height + 8); ?>px;">
  <img src="<?php echo $placeholder; ?>" class="pic-border slide slide-1" alt="slide" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
</div>
<?php
    
  
} else {
  if (!function_exists('imageOrderSort')) {
	function imageOrderSort($a, $b) {
	  if ($a['order'] == $b['order']) {
		return 0;
	  }
	  return ($a['order'] < $b['order']) ? -1 : 1;
	}
  }
  
  usort($images, 'imageOrderSort');
  
  foreach ($images as $idx => $img) {
    $i = $idx + 1;
    $class = $i == 1 ? '' : 'hiddenSlide';
    $url = $this->Html->url(array('controller'=>'uploads','action'=>'fetch', $img['id'], "{$width}x{$height}"));
    $mk_up[] = "<img src=\"{$url}\" width=\"{$width}\" height=\"{$height}\" class=\"pic-border slide slide-{$i} {$class}\" alt=\"slide\" />";
  }

?>

<div class="contentSlideShowWrapper" style="height: <?php echo ($height + 8); ?>px;">
  <div class="slides" style="width: <?php echo $width + 10; ?>px; height: <?php echo $height + 10; ?>px;">
  <?php
  
  echo join("\n", $mk_up);
  
  ?>
  </div>
  <div class="slide-control">
	<a href="" class="slide-prev btn left"><img src="/images/arrow-left.png" alt="Previous"></a>
	Image <span class="currentSlide">1</span> of <?php echo count($mk_up); ?>
	<a href="" class="slide-next btn right"><img src="/images/arrow-right.png" alt="Next"></a>
  </div><!-- /.slide-control -->
</div><!-- .contentSlideShowWrapper -->

<?php

echo $this->Html->script('content-slide-show-new');

}

?>
