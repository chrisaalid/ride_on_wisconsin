<?php

$title = $edit_mode ? 'Edit Classified' : 'Post to Bike Swap Classified';

echo $this->Html->script('classifieds');

?><!--<div class="dialog-wrap lrg">-->

<style type="text/css">
<?php

$listing_types = array();

if ($add_mode && !array_key_exists('Classified', $this->request->data)) {
  //$listing_types[''] = '--- Choose ---';

  $classifiedType = 'bike';
?>
  .partClassified {
	display: none;
  }
<?php

} else {
  if ($this->request->data['Classified']['type'] == 'part') {
	print ".bikeClassified { display: none; }";
  } else {
	print ".partClassified { display: none; }";
  }
  $classifiedType = $this->request->data['Classified']['type'];
}

$listing_types = array_merge($listing_types,  $this->FieldFormatter->classified_types());


?>
</style>

<div class="dialog-wrap lrg">
	<?php
	
	echo $this->element('dialog-header', array('title' => $title));
	
	
	echo $this->Form->create('Classified', array('type' => 'file'));
	
	
	if ($edit_mode) {
	  echo $this->Form->input('id');
	}
	
	?><div class="dl-body first">
		<div class="section bot">
			<div class="clear row">
			  <div class="col twelve clear">			      	
				<div class="field">
				  <?php
				  
				  echo $this->Form->input('type', array('id' => 'listingType', 'type' => 'select', 'options' => $listing_types, 'label' => 'I\'m selling a', 'value' => $classifiedType));
				  
				  ?>
				  </div><hr class="solid min"> 
			  </div>
			</div>
			<div class="clear row">
			<div class="col half dashed-right">
				<div class="col-pad-right">
					<div class="field">
					  <?php echo $this->Form->input('title', array('label' => 'Listing Title')); ?>
			      	</div>
			      	<hr class="dashed min">
					  
					<div class="classifiedType bikeClassified">
					<?php
					
					echo $this->Form->input('bikeMake', array('required' => false, 'label' => 'Manufacturer'));
					echo $this->Form->input('bikeModel', array('required' => false, 'label' => 'Model No.'));
					
					?>
					<hr class="dashed min">		
					</div>
					
					<div class="classifiedType partClassified">
					<?php
					
					echo $this->Form->input('partType', array('required' => false, 'label' => 'Part Type', 'class' => 'tagIt', 'data-tag-key' => 'parts'));
					echo $this->Form->input('partMake', array('required' => false, 'label' => 'Manufacturer'));
					echo $this->Form->input('partModel', array('required' => false, 'label' => 'Model No.'));
					
					?>
					<hr class="dashed min">		
					</div>
			      		      	
			      	<div class="field">
					  <?php echo $this->Form->input('condition', array('type' => 'select', 'options' => $this->FieldFormatter->classified_condition())); ?>
				    </div>
			      	<div class="field">
					  <?php
					  echo $this->Form->input('price');
					  echo $this->Form->input('offers', array('label' => 'Or Best Offer (O.B.O.)'));
					  ?>
			      	</div>
				</div><!-- /.col-pad-right -->
			</div><!-- /.col .six -->
			<div class="col half">
				<div class="col-pad-left">

			      	<div class="field">
					  <?php
					  
					  //echo $this->element('fields/location', array('show_addr' => false, 'show_map' => false, 'show_regions' => false));
					  
					  echo $this->Form->input('city');
					  
					  echo $this->element('fields/state-picker');
					  
					  ?>
				        <!--<label for="Asking Price">City</label>
				        <input type="text" name="City" id="City" value="">-->
			      	</div>				
			      	<hr class="dashed min">
					<div class="field">
					  <?php echo $this->element('fields/file-upload', array('label' => 'Photos', 'max' => 5)); ?>
				        <!--<label for="name">Photos <span class="small">(optional)</span></label>
				        <input type="file" name="file" id="file" value=""><br>
				        <a href="#">Add Another Photo</a>-->
			      	</div>
			      	<div class="field">
			      		<hr class="dashed min">
						  <?php
						  
						  echo $this->Form->input('details', array('label' => 'Description/Details <span class="small">(500 Characters Max)</span>'));
						  
						  ?>
				        <!--<label for="description">Description/Details <span class="small">(500 Characters Max)</span></label>
				        <textarea name="description"></textarea>-->
				        <hr class="dashed min">	
				        <span class="left checkbox">
						  <?php
						  
						  echo $this->Form->input('agree', array('type'=>'checkbox','label' => 'I agree to the<br/> <a href="#">terms</a> and <a href="#">conditions</a>', 'value' => 'yes'));
						  
						  ?>
						  <!--<label style="line-height:20px;" for="ClassifiedAgree">I agree to the<br/> <a href="#">terms</a> and <a href="#">conditions</a></label>-->
						</span>
						<?php echo $this->Form->end(__('Submit')); ?>
						<!--<a href="#" class="btn submit right no-margin">Submit</a> -->
			      	</div>
				</div><!-- /.col-pad-left -->
			</div><!-- /.col .six -->
		</div><!--ends row-->
		<div class="row">
			<a href="/classifieds-disclaimer.html" target="_blank">Disclaimer</a>
		</div><!-- .row -->
		</div><!-- /.section -->
	</div><!-- /.dl-body -->
</div><!-- /.dialog-wrap-sm -->  