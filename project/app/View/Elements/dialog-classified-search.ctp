<?php

$title = 'Search the Classifieds';

$range_options = array(
  10 => '10 miles',
  20 => '20 miles',
  30 => '30 miles',
  50 => '50 miles',
  100 => '100 miles',
  5000 => '5,000 miles'
);

?>
</style>

<div class="dialog-wrap lrg">
	<?php
	
	echo $this->element('dialog-header', array('title' => $title));
	echo $this->Form->create(); //'Classified', array('type' => 'file'));
	
	?>
	<div class="dl-body first">
	  <div class="section bot">
		<div class="clear row">
			<div class="searchBox">
				<div class="col four">
					<?php
				
						echo $this->Form->input('zipCode', array('id'=>'searchZip', 'label' => 'City/State or Zip Code'));
						
					?>
				</div>
				<div class="col three">
					<?php
			  
					  echo $this->Form->input('searchRange', array('id' => 'searchRange', 'type' => 'select', 'options' => $range_options, 'value' => 100));
					  
					?>
				</div>
				<div class="col three">
					<?php
			  
					  echo $this->Form->input('searchTerms', array('id' => 'searchTerms'));
					  
					?>
				</div>
				<div class="col two">
					<?php echo $this->Form->input('Search', array('label' => false, 'type' => 'submit', 'id' => 'doSearch')); ?>
				</div>
			</div><!-- .searchBox -->
		</div><!-- .row -->
		<hr class="min">
		<div class="clear row">
			<div class="col three">
				<?php echo $this->Form->input('searchBikes', array('id' => 'searchBikes', 'type' => 'checkbox', 'checked' => 'checked', 'label' => 'Full Bikes')); ?>
			</div>
			<div class="col three">
				<?php echo $this->Form->input('searchParts', array('id' => 'searchParts', 'type' => 'checkbox', 'checked' => 'checked', 'label' => 'Bike Parts')); ?>
			</div>
			<div class="col four">
				<?php echo $this->Form->input('searchOnlyPhotos', array('id' => 'searchOnlyPhotos', 'type' => 'checkbox', 'label' => 'Only Posts With Images')); ?>
			</div>
		</div><!-- .row -->
		<hr class="min">
		<?php
		
		echo $this->Form->end(); 
		
		?>
		
		<div class="clear row">
		  <table id="searchResults" class="table striped full-width">
			
		  </table>
		</div><!-- .row -->
		<div class="row">
			<a href="/classifieds-disclaimer.html" target="_blank">Disclaimer</a>
		</div><!-- .row -->
	  </div><!-- /.section -->
	</div><!-- /.dl-body -->
</div><!-- /.dialog-wrap lrg -->
<?php

echo $this->Html->script('classifieds');
echo $this->Html->script('search');

if (isset($results)) {
  ?>
<script>
  $(function() {
	bfw.classifieds.displayResults(<?php echo json_encode($results); ?>);
  });
</script>
  
  
  <?php
}

?>