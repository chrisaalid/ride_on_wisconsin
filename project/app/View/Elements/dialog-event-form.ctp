<?php

$title = $edit_mode ? 'Edit Event' : 'Share an Event';

echo $this->Html->script('events');

echo $this->Form->create('Event', array('type' => 'file'));

if ($edit_mode) {
  echo $this->Form->input('id');
}

?>
<style>
  #ui-datepicker-div {
	z-index: 100;
  }
</style>
<div class="dialog-wrap lrg">
	<?php
	
	echo $this->element('dialog-header', array('title' => $title, 'section' => 'events'));
	
	?>
	<div class="dl-body first">
		<div class="section bot dashed-mid">
			<div class="col six">
				<div class="col-pad-right">
					<div class="field">
				        <?php echo $this->Form->input('name', array('label' => 'Event Name')); ?>
			      	</div>
			      	<div class="field col half no-margin">
					  <?php
				        echo $this->element('fields/date-time', array('field_name'=>'start','label'=>'Start','class'=>'short'));
						?>
					</div>
					<div class="field col half no-margin">
					  <?php
						echo $this->element('fields/date-time', array('field_name'=>'end','label'=>'End','class'=>'short'));
					  ?>
			      	</div>
					<div style="clear: both;"></div>
			      	<hr class="dashed min">
					<div class="field">
					  <?php
					  
					  $options = array('options' => array(0 => 'Yes', 1 => 'No (free)'), 'legend' => 'Is There a Cost?', 'type' => 'radio');
					  
					  if ($add_mode) { $options['value'] = 1; }
					  
					  echo $this->Form->input('free', $options);
					  
					  ?>
					</div>
			      	<div style="clear: both;"></div>
			      	<hr class="dashed min">
				    <div class="field">
						<?php echo $this->Form->input('details', array('label' => 'Description/Details <span class="small">(500 Characters Max)</span>','type'=>'textarea')); ?>
			      	</div>
					<hr class="dashed min">
				    <div class="field">
						<?php
						
						if ($add_mode) {
						  $tags = array('' => '--- Choose ---');
						  foreach ($MODEL_OBJ->validTags as $tag) {
							$tags[$tag] = $tag;
						  }
						  
						  echo $this->Form->input('tags', array('label' => 'Event Type','type'=>'select', 'options' => $tags));
						} else {
						  echo '<label>Event Type:</label>' . $this->data['Event']['tags'];
						}
						
						?>
			      	</div>
				    <div style="clear: both;"></div>
			      	<hr class="dashed min">
			      	<div class="field">
						<?php
						  echo $this->element('fields/file-upload', array('label' => 'Upload Photos <br><span class="small">(Upon submission, we have the right to use photos)</span>', 'max' => 5));
						?>
			      	</div>
				</div><!-- /.col-pad-right -->
			</div><!-- /.col .six -->
			<div class="col six">
				<div class="col-pad-left ta-left">
					<div class="field">
						<?php echo $this->Form->input('venue'); ?>
			      	</div>
				    <div class="field">
				        
						<?php echo $this->Form->input('address'); ?>
			      	</div>
			      	
			      	<div class="field col half no-margin">
					  <?php echo $this->Form->input('city', array('class'=>'short')); ?>
			      	</div>
			      	<div class="field col half no-margin">
					  <?php
					  
						echo $this->element('fields/state-picker', array('classes'=>array('short')));
						echo $this->Form->input('lat', array('id' => 'locLat', 'type' => 'hidden'));
						echo $this->Form->input('lng', array('id' => 'locLng', 'type' => 'hidden'));
					  ?>
				    </div>
				    <div style="clear: both;"></div>
				    <hr class="dashed min">
				    <div class="field">
						<?php echo $this->Form->input('phone', array('label' => 'Phone <span class="small">(optional)</span>')); ?>
			      	</div>
			      	<div class="field">
					  <?php echo $this->Form->input('email', array('label' => 'Email <span class="small">(optional)</span>')); ?>
			      	</div>
			      	<div class="field">
					  <?php echo $this->Form->input('url', array('label' => 'Event Website <span class="small">(optional)</span>', 'placeholder' => 'http://')); ?>
			      	</div>
			      	<div class="field">
						<?php echo $this->Form->input('sponsor', array('label' => 'Sponsor <span class="small">(optional)</span>')); ?>
			      	</div>
					<?php echo $this->Form->end(__('Submit')); ?>
				</div><!-- /.col-pad-left -->
			</div><!-- /.col .six -->
		</div><!-- /.section -->
	</div><!-- /.dl-body -->
</div><!-- /.dialog-wrap-sm -->  