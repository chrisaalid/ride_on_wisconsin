<?php

$title = $edit_mode ? 'Edit Group' : 'Add Group';

echo $this->Html->script('groups');
echo $this->Html->script('routes');

?>
<div class="dialog-wrap lrg">
<?php
	
  echo $this->element('dialog-header', array('title' => $title));
  
  
  echo $this->Form->create('Group', array('type' => 'file'));
  
  
  if ($edit_mode) {
	echo $this->Form->input('id');
  }
	
?>
	<div class="dl-body first">
		<div class="section bot dashed-mid">
			<div class="col six">
				<div class="col-pad-right">
					<div class="field">
						<?php echo $this->Form->input('name', array('label' => 'Group Name')); ?>
			      	</div>
			      	<hr class="dashed min">
			      	<div class="field">
				        <?php echo $this->Form->input('contact', array('label' => 'Contact Name')); ?>
			      	</div>
			      	<div class="field">
				        <?php echo $this->Form->input('email'); ?>
			      	</div>
			      	<div class="field">
						<?php echo $this->Form->input('url', array('label' => 'Website <small>(Optional)</small>', 'required' => false)); ?>
			      	</div>
			      	<div class="field">
						<?php echo $this->Form->input('phone', array('label' => 'Phone <small>(Optional)</small>', 'required' => false)); ?>
			      	</div>
				</div><!-- /.col-pad-right -->
			</div><!-- /.col .six -->
			<div class="col six">
				<div class="col-pad-left ta-left clear cg-form">
					<h2>Ride Departure Point</h2>
					<div class="col five">
					  <?php echo $this->Form->input('venue'); ?>
					</div>
					<div class="col five col-pad-left">
					  <?php echo $this->Form->input('address'); ?>
					</div>
					
					<div class="col five">
						<?php echo $this->Form->input('city'); ?>
					</div><!-- /.col .five -->
					<div class="col five col-pad-left">
						<div class="field">
						  <?php
						  
						  echo $this->element('fields/state-picker');
						  echo $this->Form->input('lat', array('type' => 'hidden'));
						  echo $this->Form->input('lng', array('type' => 'hidden'));
						  
						  
						  ?>
				        </div>
					</div><!-- /.col .five -->
					<div style="clear: both;"></div>
			      	<hr class="dashed min">
			      	<div class="field">
						<?php echo $this->Form->input('details', array('label' => 'Description/Details <span class="small">(500 Characters Max)</span>')); ?>
			      	</div>
			      	<?php echo $this->Form->end(__('Submit')); ?>
				</div><!-- /.col-pad-left -->
			</div><!-- /.col .six -->
		</div><!-- /.section -->
	</div><!-- /.dl-body -->
</div><!-- .dialog-wrap.lrg -->
