<?php

$title = 'Search Groups';

$range_options = array(
  10 => '10 miles',
  20 => '20 miles',
  30 => '30 miles',
  50 => '50 miles',
  100 => '100 miles',
  5000 => '5,000 miles'
);

?>
</style>

<div class="dialog-wrap lrg">
	<?php
  
	
	echo $this->element('dialog-header', array('title' => $title));
	echo $this->Form->create();
	
	?>
	<div class="dl-body first">
	  <div class="section bot">
		<div class="clear row">
			<div class="searchBox">
				<div class="col five">
					<?php
				
						echo $this->Form->input('zipCode', array('id'=>'searchZip', 'label' => 'City/State or Zip Code'));
						
					?>
				</div>
				<div class="col five">
					<?php
			  
					  echo $this->Form->input('searchRange', array('id' => 'searchRange', 'type' => 'select', 'options' => $range_options, 'value' => 100));
					  
					?>
				</div>
				</div>
				<div class="col two">
					<?php echo $this->Form->input('Search', array('label' => false, 'type' => 'submit', 'id' => 'doSearch')); ?>
				</div>
			</div><!-- .searchBox -->
		</div><!-- .row -->
		<hr class="min">
		<?php
		
		echo $this->Form->end(); 
		
		?>
		
		<div class="clear row">
		  <table id="searchResults" class="table striped full-width">
			
		  </table>
		</div><!-- .row -->
	  </div><!-- /.section -->
	</div><!-- /.dl-body -->
</div><!-- /.dialog-wrap lrg -->
<?php

echo $this->Html->script('groups');
echo $this->Html->script('search');

if (isset($results)) {
  ?>
<script>
  $(function() {
	bfw.groups.displayResults(<?php echo json_encode($results); ?>);
  });
</script>
  
  
  <?php
}