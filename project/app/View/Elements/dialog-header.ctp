<?php

$title = isset($title) ? $title : 'Ride On Wisconsin';

$disp_title = $title;
$trailing = '';
if (strlen($disp_title) > 27) {
  $disp_title = substr($disp_title, 0, 27);
  $trailing = '&hellip;';
}

$icon = isset($icon) ? $icon : "bike";

$section = isset($section) ? $section : strtolower(Inflector::pluralize($CURRENT_MODEL));

$sub_section = $section . '-' . strtolower($CURRENT_ACTION);

$bonus_links = isset($bonus_links) && is_array($bonus_links) ? $bonus_links : array();
  
if ($logged_in) {
  if ($CURRENT_ID !== false && $CURRENT_ACTION == 'view' && $CURRENT_MODEL != 'User' && ($CONTENT_OWNER === true || $is_admin)) {
	$bonus_links[] = $this->Html->link('Edit', array('action'=>'edit', $CURRENT_ID), array('class'=>'header-btns edit-icon'));
  }
  
  if ($CURRENT_MODEL !== 'User' || $CURRENT_ACTION !== 'view') {
	$bonus_links[] = $this->Html->link('My Account', array('controller'=>'users','action'=>'view', $logged_in_user['id']), array('class'=>'header-btns gear-icon'));
  }
}

if (in_array($CURRENT_ACTION, $MODEL_OBJ->printable)) {
  $bonus_links[] =  $this->Html->link('Print', '#', array('class'=>'header-btns print-icon', 'id'=>'printButton'));
}

?>
<div class="dl-header <?php echo $section . ' ' . $sub_section; ?>">
	<div class="seven header-btns-float-l"><span aria-hidden="true" class="<?php echo $section . ' ' . $sub_section; ?>"></span> <h1 title="<?php echo __($title); ?>"><?php echo __($disp_title) . $trailing; ?></h1></div>
	<div class="five header-btns-float-r">
	  <?php echo join(' ', $bonus_links); ?>
	</div>	
</div><!-- /.dl-header -->
<?php

  echo $this->element('edit-notice');

  echo $this->Session->flash();
  
?>
<script>
  $(function() {
	$flash = $('#flashMessage');
	if ($flash.length > 0) {
	  setTimeout(function() {
		$flash.animate({ opacity: 0 }, 250, function() {
		  $flash.animate({
			height: 0,
			padding: 0,
			margin: 0
		  }, 250); // slideup 250 ms
		}); // fadeout 250 ms
	  }, 5000); // wait 5 seconds
	}
	
	$('#printButton').click(function(e) {
	  window.print();
	  e.preventDefault();
	});
	
  });
</script>
  