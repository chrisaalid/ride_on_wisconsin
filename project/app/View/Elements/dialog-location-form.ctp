<?php

$title = $edit_mode ? 'Edit Point of Interest' : 'Add Point of Interest';

echo $this->Form->create('Location', array('type' => 'file'));

echo $this->element('fields/owner');

if ($edit_mode) {
  echo $this->Form->input('id');
}

?><div class="dialog-wrap lrg">
	<?php echo $this->element('dialog-header', array('title' => $title, 'section' => 'rides')); ?>
	<div class="dl-body first">
		<div class="section bot">
			<div class="col six dashed-right">
				<div class="col-pad-right">
					<div class="field">
						<?php echo $this->Form->input('name'); ?>
			      	</div>
			      	<hr class="dashed min">
			      	<div class="field">
				        <?php echo $this->Form->input('type', array('type' => 'select', 'options' => $this->FieldFormatter->loc_types())); ?>
				    </div>
				    <div class="field">
				        <?php echo $this->Form->input('address', array('class'=>'address')); ?>
			      	</div>
			      	<div class="field col half no-margin">
				        <?php echo $this->Form->input('city', array('class'=>'city')); ?>
			      	</div>
			      	<div class="field col half no-margin">
				        <?php
						echo $this->element('fields/state-picker', array('classes' => array('state short')));
						echo $this->Form->input('lat', array('id' => 'locLat', 'type' => 'hidden'));
						echo $this->Form->input('lng', array('id' => 'locLng', 'type' => 'hidden'));
						 ?>
				    </div>
				    <div class="field">
				        <?php echo $this->Form->input('description', array('type'=>'textarea')); ?>
			      	</div>
				    <div style="clear: both;"></div>
			      	<hr class="dashed min">
			      	<div class="field">
					  <?php echo $this->element('fields/file-upload', array('label' => 'Photos <span class="small">(optional)</span>', 'max' => 5)); ?>
			      	</div>
				</div><!-- /.col-pad-right -->
			</div><!-- /.col .six -->
			<div class="col six">
				<div class="col-pad-left ta-center">
					<?php echo $this->element('fields/location', array('show_addr' => false, 'show_city' => false, 'show_state' => false, 'show_map' => true, 'width' => 338, 'height' => 321)); ?>
					<br>
					<span class="checkbox">
						<?php echo $this->Form->input('member', array('label' => 'Wisconsin Bike Fed Member', 'type' => 'checkbox')); ?>
					</span>
					<span class="checkbox">
					  <?php echo $this->Form->input('discount', array('label' => 'Wisconsin Bike Fed Discount', 'type' => 'checkbox')); ?>
					</span>
					<?php echo $this->Form->end(__('Submit')); ?>
				</div><!-- /.col-pad-left -->
			</div><!-- /.col .six -->
		</div><!-- /.section -->
	</div><!-- /.dl-body -->
</div><!-- /.dialog-wrap-sm -->
<?php

echo $this->Html->script('locations');

?>
