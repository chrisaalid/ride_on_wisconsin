<?php

$title = $edit_mode ? 'Edit Ride/Trail' : 'Share a Ride/Trail';

?><div class="dialog-wrap lrg">
	<?php
	
	//pr($this->data);
	
	
	echo $this->element('dialog-header', array('title' => $title));
	
	if ($edit_resets && $this->data['Route']['approved']) {
		echo '<span class="notice">This route has been approved.  Editing it will put it back in a pended status.';
	}
	
	echo $this->Html->script('routes');
	
	$surface_fields = array('surAsphalt'=>'Asphalt/Concrete','surLimestone'=>'Crushed Limestone','surGravel'=>'Gravel','surWoodchips'=>'Woodchips','surDirt'=>'Dirt','surSand'=>'Sand');
	$topography_fields = array('topFlat'=>'Flat','topRolling'=>'Rolling','topClimbs'=>'Long Climbs');
	$surroundings_fields = array('surUrban'=>'Urban','surRural'=>'Rural/Agricultural','surResidential'=>'Residential','surWooded'=>'Wooded','surStream'=>'Stream/River','surLake'=>'Lake','surPrairie'=>'Prairie');
	
	?>
	
	<div class="dl-body first">
		<div class="section">
			<div class="col six dashed-right ride-details" style="height: 560px;">
				<div class="col-pad-right">
					<?php

					echo $this->Form->create('Route', array('type' => 'file'));
					
					if ($edit_mode) {
					  echo $this->Form->input('id');
					}
					
					//echo $this->element('fields/owner');
					
					echo $this->Form->input('name', array('label' => 'Title'));
					
					?>
			      	<hr class="dashed min">
					<?php
					
					echo $this->Form->input('comment', array('label' => 'Description/Details <span class="small">(500 Characters Max)</span>', 'type' => 'textarea'));
					
					?>
			      	<hr class="dashed min">
					<?php
					  echo $this->element('fields/file-upload', array('label' => 'Upload Photos From Ride <br><span class="small">(Upon submission, we have the right to use photos)</span>', 'max' => 5));
					?>
				</div><!-- /.col-pad-right -->
			</div><!-- /.col .six -->
			<div class="col six">
				<div class="col-pad-left">
					<?php
					
					echo $this->Form->input('offroad', array('type' => 'radio', 'options' => array(0 => '<span class="small">Road Ride</span><br>', 1 => '<span class="small">Off-Road Ride</span><br>'), 'div' => false, 'legend' => 'Overall Ride Type'));
					
					?>
			      	<hr class="dashed min">
				</div><!-- /.col-pad-left -->
			</div><!-- /.col .six -->
			<div class="col six">
				<div class="col-pad-left">
					<?php
					
					echo $this->Form->input('difficulty', array('type' => 'radio', 'options' => array('Easy <span class="small">(For all riders)</span><br/>', 'Medium <span class="small">(For intermediate riders)</span><br/>', 'Hard <span class="small">(For experienced riders)</span>'), 'div' => false, 'label' => ''));
					
					?>
			      	<hr class="dashed min">
				</div><!-- /.col-pad-left -->
			</div><!-- /.col .six -->
			<div class="col six">
				<div class="col-pad-left"><h3>Surface <span class="small">(Choose all that apply)</span></h3></div>
			</div>
			<div class="col three">
				<div class="col-pad-left">
				  <?php
				  
				  foreach (array('surAsphalt', 'surLimestone', 'surGravel') as $field) {
					$label = $surface_fields[$field];
					echo $this->Form->input($field, array('div'=>false,'label'=>$label, 'before'=>'<span class="checkbox">', 'after'=>'</span>'));
				  }
				  
				  ?>
				</div>
			</div>
			<div class="col three">
				  <?php
				  
				  foreach (array('surWoodchips', 'surDirt', 'surSand') as $field) {
					$label = $surface_fields[$field];
					echo $this->Form->input($field, array('div'=>false,'label'=>$label, 'before'=>'<span class="checkbox">', 'after'=>'</span>'));
				  }
				  
				  ?>
			</div>
			<div class="col six">
				<div class="col-pad-left"><h3 class="h-pad-top">Topography</h3></div>
			</div>
			<div class="col three">
				<div class="col-pad-left">
				  <?php
				  
				  foreach (array('topFlat', 'topRolling') as $field) {
					$label = $topography_fields[$field];
					echo $this->Form->input($field, array('div'=>false,'label'=>$label, 'before'=>'<span class="checkbox">', 'after'=>'</span>'));
				  }
				  
				  ?>
				</div>
			</div>
			<div class="col three">
				  <?php
				  
				  foreach (array('topClimbs') as $field) {
					$label = $topography_fields[$field];
					echo $this->Form->input($field, array('div'=>false,'label'=>$label, 'before'=>'<span class="checkbox">', 'after'=>'</span>'));
				  }
				  
				  ?>
			</div>
			<div class="col six">
				<div class="col-pad-left"><h3 class="h-pad-top">Surroundings</h3></div>
			</div>
			<div class="col three">
				<div class="col-pad-left">
					<?php
				  
					foreach (array('surUrban', 'surRural', 'surResidential', 'surWooded') as $field) {
					  $label = $surroundings_fields[$field];
					  echo $this->Form->input($field, array('div'=>false,'label'=>$label, 'before'=>'<span class="checkbox">', 'after'=>'</span>'));
					}
					
					?>
				</div>
			</div>
			<div class="col three bot">
				  <?php
				  
					foreach (array('surStream', 'surLake', 'surPrairie') as $field) {
					  $label = $surroundings_fields[$field];
					  echo $this->Form->input($field, array('div'=>false,'label'=>$label, 'before'=>'<span class="checkbox">', 'after'=>'</span>'));
					}
					
					?>
			</div>

		</div><!-- /.section -->
		<hr class="min">
		<div class="section bot">
			<div class="col twelve gpx-import">
				<h2 class="left">Route
				
				<?php
				if ($add_mode) {
				  
				?>&nbsp;&nbsp;<span class="small">Click on the map to start mapping your route or upload a GPX file. <?php echo $this->element('gpx-import', array('submit' => false)); ?></span><?php
				
				}
				?>
				</h2>
				<!--<a href="#" class="btn reset right">Reset Map</a>
				<img src="/images/fpo-ride-detail-map.jpg" alt="Map" class="map-detail pic-border"><br>-->
				<?php
			  
				
				echo $this->element('fields/route-builder', array('name'=>'points', 'width' => 780,'height' => 440, 'gpx_import' => false));
				
				?>
				<!--<span class="left">Total Distance  <span class="lrg-txt">6.8 mi</span></span>-->
				<!--<span class="right">Total Elevation Change  800 ft</span><br>-->
				<?php echo $this->Form->end(__('Submit')); ?>
				<!--<a href="#" class="btn submit right">Submit</a>-->
			</div><!-- /.col .twelve -->
		</div><!-- /.section -->
	</div><!-- /.dl-body -->
	<div class="dl-footnote">
		Want to add restuarants and lodging near the route? Add a <strong>Point of Interest</strong> when you're done building your route.</a>
	</div><!-- /.dl-footnote -->
</div><!-- /.dialog-wrap-sm -->  