<?php
$edit_mode = isset($this->data['User']['id']);

echo $this->Form->create('User');

if ($edit_mode) {
  echo $this->Form->input('id');  
}

$dlg_title = $edit_mode ? 'Edit Account' : 'Create Account';

?>
<div class="dialog-wrap lrg">
<?php echo $this->element('dialog-header', array('title' => $dlg_title, 'icon'=>'x')); ?>
  <div class="dl-body first">
	<fieldset>
		<div class="dl-scroll">
			<div class="dl-body">
				<div class="section bot">
					<div class="col six dashed-right">
						<?php
						
							if ($add_mode) {
							  echo $this->Form->input('username');
							} else {
							  echo $this->Form->input('id', array('type' => 'hidden'));
							}
							
							
							
							echo $this->Form->input('city');
							echo $this->element('fields/state-picker');
							echo $this->Form->input('profile');
							//echo $this->Form->input('approved');
						?>
					</div>
					<div class="col six">
						<?php
							echo $this->Form->input('firstname');
							echo $this->Form->input('lastname');
						
							echo $this->Form->input('email');
							if ($add_mode) {
							  echo $this->Form->input('pwd', array('type'=>'password','label'=>'Password'));
							  echo $this->Form->input('pwd_repeat', array('type'=>'password','label'=>'Confirm Password'));
							}
						?>
						<?php echo $this->Form->end(__('Submit')); ?>
					</div>
				</div><!-- /.section -->
			</div><!-- /.dl-body -->
		</div><!-- dl-scroll -->
	</fieldset>
  </div>
</div>