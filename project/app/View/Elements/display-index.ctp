<?php



$records = isset($records) ? $records : array();

//pr($records); exit(0);


$use_only = isset($use_only) && is_array($use_only) ? $use_only : array();
$labels = isset($labels) && is_array($labels) ? $labels : array();
$actions = isset($actions) && is_array($actions) ? $actions : array();
$format = isset($format) && is_array($format) ? $format : array();
$no_approve = isset($no_approve) && $no_approve ? true : false;
$no_batch = isset($no_batch) && $no_batch === true ? true : false;

//$no_batch = true;

if (!array_key_exists('created', $format)) { $format['created'] = array('date', array('format' => 'shortDateTime')); }
if (!array_key_exists('modified', $format)) { $format['modified'] = array('date', array('format' =>'shortDateTime')); }

$actions['View']  = array_key_exists('View', $actions) ? $actions['View'] : true;
$actions['Edit']  = array_key_exists('Edit', $actions) ? $actions['Edit'] : true;
$actions['Delete']  = array_key_exists('Delete', $actions) ? $actions['Delete'] : true;

$lc_cm = strtolower($CURRENT_MODEL);
$singular_model = Inflector::singularize($CURRENT_MODEL);

$singular_model = strtoupper(substr($singular_model, 0, 1)) . strtolower(substr($singular_model, 1));

$can_be_approved = false;

$schema = $MODEL_OBJ->schema();

?>

<div class="<?php echo $lc_cm; ?> index batchActionScope">
  
	<h2><?php echo __(Inflector::pluralize($CURRENT_MODEL)); ?></h2>
	<?php
	if (isset($is_searchable) && $is_searchable === true) {
	?>
	<a href="#" id="searchToggle"><?php echo $show_search ? '-' : '+'; ?> Search</a>
	<div class="actions wide" id="searchBox"<?php if (!$show_search) { print ' style="display: none;"'; } ?>>
	  <?php
	  echo $this->Form->create($CURRENT_MODEL, array('type' => 'file'));
	  echo $this->Form->input('Terms');
	  echo $this->Form->end(__('Search'));
	  ?>
	</div>
	<?php
	} // searchable index?

	if ($is_developer) {
	  print '<div class="actions wide batch">';
	  print $this->Html->link('Schema Comparator', array('action' => 'schema')) . ' ';
	  print $this->Html->link('Data Dumper', array('action' => 'dump')) . ' ';
	  if ($MODEL_OBJ->hasField('approved')) {
		print $this->Html->link(__('Approve Selected'), array('action' => 'batch', 'approve'), array('class' => 'batchAction')) . ' ';
		print $this->Html->link(__('Disapprove Selected'), array('action' => 'batch', 'disapprove'), array('class' => 'batchAction')) . ' ';
	  }
	  print '</div>';
	}
	
	?>
	<table cellpadding="0" cellspacing="0">
	<tr>
	  <?php
	  if (!$no_batch) { ?>
	  <th><?php echo $this->Form->input('checkAll', array('type' => 'checkbox', 'class' => 'batchActionAllCB', 'label' => false, 'label' => false)); ?></th>
	  <?php
	  }
	  
	  foreach ($use_only as $field) {
		
		
		?>
		<th><?php
		if (array_key_exists($field, $labels)) {
		  echo $this->Paginator->sort($field, $labels[$field]);
		} else {
		  echo $this->Paginator->sort($field);
		}
		
		?></th>
		<?php
	  }
	  ?>
	  <th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($records as $rec) { ?>
	<tr>
	  <?php
	  if (!$no_batch)  {
	  ?>
	  <td><?php
	  
	  echo $this->Form->input('checkBoxFor' . $singular_model . '_' . $rec[$singular_model]['id'], array('type' => 'checkbox', 'class' => 'batchActionCB', 'data-id' => $rec[$singular_model]['id'], 'label' => false));
	  
	  ?></td>
	  <?php
	  }
	  
	  if (!$can_be_approved && array_key_exists('approved', $rec[$singular_model]) && !$no_approve) {
		$can_be_approved = true;
	  }
	  
	  foreach ($use_only as $field) {
		$field_model = $singular_model;
		if (strpos($field, '.') > 0) {
		  list($field_model, $field) = preg_split('/\./', $field);
		}
		
		$display_val = $rec[$field_model][$field];
		if (array_key_exists($field, $format)) {
		  $fmt = $format[$field];
		  $fmt_func = $fmt[0];
		  $display_val = $this->FieldFormatter->$fmt_func($display_val, $fmt[1]);
		} else {
		  //if (strtolower($field) === 'approved') {
		  if (is_array($schema) && array_key_exists($field, $schema) && $schema[$field]['type'] === 'boolean') {
			$display_val = $this->FieldFormatter->bool($display_val); // ? 'YES' : 'NO';
		  } elseif (is_array($schema) && array_key_exists($field, $schema) && $schema[$field]['type'] === 'date') {
			$display_val = $this->FieldFormatter->date($display_val, $fmt[1]);
		  } else {
			$display_val = $display_val;
		  }
		}
		
		if ($field == $MODEL_OBJ->displayField) {
		  $text = $this->Html->link(__($display_val), array('action' => 'view', $rec[$singular_model]['id']));
		} else {
		  $text = $display_val;
		}
		
		?>
		<td><?php echo $text ?>&nbsp;</td>
		<?php
	  }
	  ?>
		<td class="actions" style="text-align: left;">
			<?php
			
			if ($actions['View'] === true) { echo $this->Html->link(__('View'), array('action' => 'view', $rec[$singular_model]['id'])); }
			if ($actions['Edit'] === true) { echo $this->Html->link(__('Edit'), array('action' => 'edit', $rec[$singular_model]['id'])); }
			if ($actions['Delete'] === true) { echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $rec[$singular_model]['id']), null, __('Are you sure you want to delete "%s"?', $rec[$singular_model][$MODEL_OBJ->displayField])); }
			
			foreach ($actions as $label => $cfg) {
			  if (is_array($cfg) && array_key_exists('action', $cfg)) {
				$params = array('action' => $cfg['action']);
				
				if (array_key_exists('field', $cfg)) {
				  $params[] = $rec[$singular_model][$cfg['field']];
				}
				
				echo $this->Html->link(__($label), $params);
			  }
			}
			
			if ($can_be_approved) {
			  //echo $this->Form->postLink(__($rec[$singular_model]['approved'] ? 'Disapprove' : 'Approve'), array('action' => $rec[$singular_model]['approved'] ? 'disapprove' : 'approve', $rec[$singular_model]['id']), null);
			  echo $this->Html->link(__($rec[$singular_model]['approved'] ? 'Disapprove' : 'Approve'), array('action' => $rec[$singular_model]['approved'] ? 'disapprove' : 'approve', $rec[$singular_model]['id']));
			}
			
			?>
		</td>
	</tr>
	<?php
	} // foreach
	?>
	</table>
	<p>
	<?php
	
	
	
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>