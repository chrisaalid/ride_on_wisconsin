<?php

$record = isset($record) ? $record : array();

$use_only = isset($use_only) && is_array($use_only) ? $use_only : false;
$exclude = isset($exclude) && is_array($exclude) ? $exclude : false;
$labels = isset($labels) && is_array($labels) ? $labels : array();

?>

<dl>
<?php

foreach ($record as $key => $val) {
  $show_it = true;
  
  if ($use_only !== false && !in_array($key, $use_only)) {
	$show_it = false;
  } elseif ($exclude !== false && in_array($key, $exclude)) {
	$show_it = false;
  }

  if (array_key_exists($key, $labels)) {
	$label = $labels[$key];
  } else {
	$label = strtoupper(substr($key, 0, 1)) . strtolower(substr($key, 1));
  }

  if ($show_it) {

?>
  <dt><?php echo __($label); ?></dt>
  <dd>
	<?php
	
	if (strtolower($key) === 'approved') {
	  echo $val ? 'YES' : 'NO';  
	} else {
	  echo h($val);
	}
	
	?>
	&nbsp;
  </dd>
<?php
  } // show check

} // foreach()
?>
</dl>