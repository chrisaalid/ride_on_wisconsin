<?php

//$records = isset($records) ? $records : array();
$record = isset($record) && is_array($record) ? $record : false;
$related_model = isset($related_model) ? $related_model : false;

if ($record !== false && $related_model !== false && array_key_exists($related_model, $record)) {
  $records = $record[$related_model];

  $use_only = isset($use_only) && is_array($use_only) ? $use_only : array();
  $labels = isset($labels) && is_array($labels) ? $labels : array();
  $actions = isset($actions) && is_array($actions) ? $actions : array();
  $format = isset($format) && is_array($format) ? $format : array();
  $no_approve = isset($no_approve) && $no_approve ? true : false;
  $no_paging = isset($no_paging) && $no_paging ? true : false;
  $title = isset($title) ? $title : 'Related ' . Inflector::humanize(Inflector::pluralize($related_model));
  
  if (!array_key_exists('created', $format)) { $format['created'] = array('date', array('format' => 'shortDateTime')); }
  if (!array_key_exists('modified', $format)) { $format['modified'] = array('date', array('format' =>'shortDateTime')); }
  
  $actions['View']  = array_key_exists('View', $actions) ? $actions['View'] : true;
  $actions['Edit']  = array_key_exists('Edit', $actions) ? $actions['Edit'] : true;
  $actions['Delete']  = array_key_exists('Delete', $actions) ? $actions['Delete'] : true;
  
  $lc_cm = strtolower($related_model);
  $singular_model = Inflector::singularize($related_model);
  
  $singular_model = strtoupper(substr($singular_model, 0, 1)) . strtolower(substr($singular_model, 1));
  
  $can_be_approved = false;
  
  $related_controller = strtolower(Inflector::pluralize($related_model));
  
  $RELATED_MODEL = ClassRegistry::init($related_model);
  
  $schema = $RELATED_MODEL->schema();
  
  
  
  ?>
  
  <style>
	.relatedScroller {
	  max-height: 500px;
	  overflow-y: auto;
	  overflow-x: hidden;
	}
  </style>
  
	<div class="related batchActionScope">
		  <div class="batch actions wide">
			<h3><?php echo __($title); ?></h3>
			<?php echo $this->Html->link(__('Approve Selected'), array('controller' => $related_controller, 'action' => 'batch', 'approve'), array('class' => 'batchAction')); ?>
			<?php echo $this->Html->link(__('Disapprove Selected'), array('controller' => $related_controller, 'action' => 'batch', 'disapprove'), array('class' => 'batchAction')); ?>
		  </div>
		  
		  <?php
		  
		  if (count($records) > 0) {
		  
		  ?>
		  <div style="clear: both;"></div>
		  <div class="relatedScroller">
		  <table cellpadding = "0" cellspacing = "0">
		  <tr>
			<th><?php echo $this->Form->input('', array('type' => 'checkbox', 'class' => 'batchActionAllCB', 'label' => false)); ?></th>
			  <?php
			  
			  foreach ($use_only as $field) {
				?><th><?php
				if (array_key_exists($field, $labels)) {
				  echo Inflector::humanize(preg_replace('/_id$/', '', $labels[$field]));
				} else {
				  echo Inflector::humanize(preg_replace('/_id$/', '', $field));
				}
				
				?></th><?php
			  }
			  ?>
			  <th class="actions"><?php echo __('Actions'); ?></th>
		  </tr>
		  <?php
			  foreach ($records as $r) { ?>
			  <tr>
				<td><?php
	  
				echo $this->Form->input('', array('type' => 'checkbox', 'class' => 'batchActionCB', 'data-id' => $r['id'], 'label' => false));
				
				?></td>
				  <?php
				  
				  
				  
				  
				  foreach ($use_only as $field) {
					$display_val = $r[$field];
					if (array_key_exists($field, $format)) {
					  $fmt = $format[$field];
					  $fmt_func = $fmt[0];
					  $display_val = $this->FieldFormatter->$fmt_func($display_val, $fmt[1]);
					} else {
					  //if (strtolower($field) === 'approved') {
					  if (is_array($schema) && array_key_exists($field, $schema) && $schema[$field]['type'] === 'boolean') {
						$display_val = $this->FieldFormatter->bool($display_val); // ? 'YES' : 'NO';
					  } elseif (is_array($schema) && array_key_exists($field, $schema) && $schema[$field]['type'] === 'date') {
						$display_val = $this->FieldFormatter->date($display_val, $fmt[1]);
					  } else {
						if ($field == 'title' || $field == 'name') {
						  $display_val = $this->Html->link($display_val, array('controller' => $related_controller, 'action' => 'view', $r['id']));
						} else {
						  $display_val = h($display_val);
						}
					  }
					}
					
					
					
					?>
					<td><?php echo __($display_val) ?>&nbsp;</td>
					<?php
				  }
				  
				  ?>
				  <td class="actions">
					  <?php echo $this->Html->link(__('View'), array('controller' => $related_controller, 'action' => 'view', $r['id'])); ?>
					  <?php echo $this->Html->link(__('Edit'), array('controller' => $related_controller, 'action' => 'edit', $r['id'])); ?>
					  <?php echo $this->Form->postLink(__('Delete'), array('controller' => $related_controller, 'action' => 'delete', $r['id']), null, __('Are you sure you want to delete "%s"?', $r[$RELATED_MODEL->displayField])); ?>
				  </td>
			  </tr>
		  <?php } ?>
		  </table>
		  </div><!-- .relatedScroller -->
		  <?php
		  
		  } else {
			print '<span class="noRecords">No related records</span>';
		  }
		  
		  ?>
	  </div><!-- .related -->
<?php

}

?>