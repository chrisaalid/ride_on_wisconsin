<?php

if ($edit_resets && array_key_exists($CURRENT_MODEL, $this->data) && array_key_exists('approved', $this->data[$CURRENT_MODEL]) && $this->data[$CURRENT_MODEL]['approved']) {
	?><span class="notice">This <?php echo strtolower(Inflector::singularize($CURRENT_MODEL)); ?> has been approved.  Editing it will return it to pending status.<span><?php
}

?>