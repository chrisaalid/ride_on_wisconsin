<?php

$title = 'Error';

?>
<div class="dialog-wrap lrg">
	<?php
	
	echo $this->element('dialog-header', array('title' => $title, 'section' => 'events'));
	
	?>
	<div class="dl-body first">
		<div class="section bot dashed-mid">
			<h1>Oh Dear</h1>
			<p>Something has gone wrong.  Please try your request again.</p>
			<?php if ($is_admin) { ?>
				<div>
					<pre><?php echo $error; ?></pre>
					<?php echo $this->element('sql_dump');?>
				</div>
			<?php } /* admin ? */ ?>
		</div><!-- /.section -->
	</div><!-- /.dl-body -->
</div><!-- /.dialog-wrap-sm -->  