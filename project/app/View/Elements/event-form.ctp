<?php


//$add_form = isset($edit) && $edit === true ? false : true;

$this->Html->script('events', array('inline' => false));

echo $this->Form->create('Event');

$data = array();
if (isset($this->request->data[$CURRENT_MODEL])) {
	$data = $this->request->data[$CURRENT_MODEL];
}


?>
<fieldset>
	<legend><?php echo __(($edit_mode ? 'Edit' : 'Add') . ' Event'); ?></legend>
	
	<?php

		if ($edit_mode) {
			echo $this->Form->input('id');
		}
		
		echo $this->element('fields/owner');
		
		echo $this->Form->input('name');
		
		echo $this->element('fields/date-time', array('field_name'=>'start','label'=>'Start'));
		echo $this->element('fields/date-time', array('field_name'=>'end','label'=>'End'));
		
		echo $this->Form->input('venue');
		echo $this->element('fields/location', array('show_regions'=>true));
		echo $this->Form->input('lat', array('id' => 'locLat', 'type' => 'hidden'));
		echo $this->Form->input('lng', array('id' => 'locLng', 'type' => 'hidden'));
		
		echo $this->Form->input('tags', array('class'=>'tagIt','data-tags'=>'eventTags'));
		echo $this->Form->input('sponsor');
		echo $this->Form->input('email');
		echo $this->Form->input('phone');
		echo $this->Form->input('url');
		echo $this->Form->input('details');
		echo $this->Form->input('approved');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>