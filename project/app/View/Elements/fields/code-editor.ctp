<?php

$mode = isset($mode) ? $mode : 'mustache';
$field_name = isset($field_name) ? $field_name : 'codeEditor';
$label = isset($label) ? $label : 'Code Editor';
$read_only = isset($read_only) ? $read_only === true : false;
$class = isset($class) ? $class : '';
$editor_type = isset($editor_type) ? $editor_type : 'mustache';
$changeEvent = isset($changeEvent) ? $changeEvent : false;

$code = isset($code) ? $code : (array_key_exists($CURRENT_MODEL, $this->data) && array_key_exists($field_name, $this->data[$CURRENT_MODEL]) ? $this->data[$CURRENT_MODEL][$field_name] : '');
$id = isset($id) ? $id : $field_name . '_codeEditor';

$this->Html->css(array('codemirror.css'), null, array('inline' => false));

if ($editor_type == 'mustache') {
  $this->Html->script(array('codemirror.js','addon/mode/overlay.js','mode/xml/xml.js','mode/mustache/mustache.js'), array('inline'=>false));
} elseif ($editor_type == 'javascript' || strtolower($editor_type) == 'json') {
  $mode = 'javascript';
  //<script src="../../lib/codemirror.js"></script>
  //<script src="../../addon/edit/matchbrackets.js"></script>
  //<script src="../../addon/comment/continuecomment.js"></script>
  //<script src="../../addon/comment/comment.js"></script>
  $this->Html->script(array('codemirror.js','mode/javascript/javascript.js','addon/edit/matchbrackets.js','addon/comment/continuecomment.js', 'addon/comment/comment.js'), array('inline'=>false));
}

$wrapper_id = isset($wrapper_id) ? $wrapper_id : false;

$field_opts =  array('id'=> $id, 'data-mode' => $mode, 'label' => $label);

if (isset($value)) {
  $field_opts['value'] = $value;
}

if (!$read_only) {
  echo $this->Form->input($field_name, $field_opts);
} else {
  echo "<div id=\"$id\"></div>";
}

$options = new stdClass();

$options->mode = $mode;
$options->readOnly = $read_only;
$options->lineNumbers = true;

if ($read_only) {
  $options->value = $code;
  $options->readOnly = 'nocursor';
}

?>

<script>
  $(function() {
	var $cmElem = $('#<?php echo $id; ?>');
	var cmElem = $cmElem.get(0);
    var editor = CodeMirror<?php echo $read_only ? '' : '.fromTextArea'; ?>(cmElem, <?php echo json_encode($options); ?>);

	$cmElem.removeAttr('required');
	$('.CodeMirror').addClass('<?php echo $class; ?>');
	<?php
	if ($wrapper_id !== false) {
	  ?>
	  $('.CodeMirror').attr('id', '<?php echo $wrapper_id; ?>');
	  <?php
	}
	?>
	
	editor.on('change', function(cm, changes) {
	  var newVal = cm.getValue();
	  $cmElem.val(newVal);
	  
	  <?php
	  
	  if ($changeEvent !== false) {
		echo $changeEvent . "(newVal);\n";
	  }
	  
	  ?>
	  
	});
	
	
  });
</script>