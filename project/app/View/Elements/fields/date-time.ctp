<?php

echo $this->Html->script('jquery-ui-timepicker-addon');

$field_name = isset($field_name) ? $field_name : 'DateTime';

$label = isset($label) ? $label : $field_name;
$class = isset($class) ? "dateTimePickerTrigger $class" : 'dateTimePickerTrigger';

$default_value = '';

$def_month = '';
$def_day = '';
$def_year = '';
$def_hour = '';
$def_minute = '';
$def_meridian = '';


if (isset($this->request->data[$CURRENT_MODEL])) {
	$data = $this->request->data[$CURRENT_MODEL];
	
	
	$to_convert = $data[$field_name];
	
	if (is_array($data[$field_name])) {
	  $to_convert = array_key_exists($field_name . 'Trigger', $data) ? $data[$field_name . 'Trigger'] : date(time());
	  if (is_string($to_convert) && strlen(trim($to_convert)) < 1) {
		$to_convert = time();
	  }
	  
	  if (is_int($to_convert) && $to_convert == 0) {
		$to_convert = time();
	  }
	}
		
	$ts_obj = is_string($to_convert) ? strtotime($to_convert) : $to_convert;
	
	$default_value = isset($data[$field_name]) ? date('m/d/Y h:i a', $ts_obj) : '';
	
	$def_month = date('m', $ts_obj);
	$def_day = date('d', $ts_obj);
	$def_year = date('Y', $ts_obj);
	$def_hour = date('h', $ts_obj);
	$def_minute = date('i', $ts_obj);
	$def_meridian = date('a', $ts_obj);	
	
}

?>
<div class="input text required">
	<?php
	
	echo $this->Form->input($field_name . 'Trigger', array('type'=>'text','class'=>$class,'label'=>$label,'div'=>false,'value'=>$default_value,'readonly'=>true));
	
	echo $this->Form->input($field_name . '.month', array('type'=>'hidden','class'=>'dateTimePickerMonth','value'=>$def_month));
	echo $this->Form->input($field_name . '.day', array('type'=>'hidden','class'=>'dateTimePickerDay','value'=>$def_day));
	echo $this->Form->input($field_name . '.year', array('type'=>'hidden','class'=>'dateTimePickerYear','value'=>$def_year));
	echo $this->Form->input($field_name . '.hour', array('type'=>'hidden','class'=>'dateTimePickerHour','value'=>$def_hour));
	echo $this->Form->input($field_name . '.min', array('type'=>'hidden','class'=>'dateTimePickerMin','value'=>$def_minute));
	echo $this->Form->input($field_name . '.meridian', array('type'=>'hidden','class'=>'dateTimePickerMeridian','value'=>$def_meridian));
	
	?>
</div>