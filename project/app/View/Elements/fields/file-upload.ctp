<?php

$classes = array('uploadList');

$label = isset($label) ? $label : 'Choose File';
$count = isset($count) && is_int($count) && $count >= 1 ? $count : 1;
$auto_add = isset($auto_add) && $auto_add === false ? false : true;
$max = isset($max) && is_int($max) && $max > 0 ? $max : 1;
$show_current = isset($show_current) && $show_current === false ? false : true;
$allow_delete = isset($allow_delete) && $all_delete === false ? false : true;

if ($auto_add) {
  $classes[] = 'autoAdd';
}

$error_class = count($uploader_component_errors) > 0 ? ' error' : '';

?>
<div class="fileUploads input<?php echo $error_class; ?>">
  <label><?php echo $label; ?></label>
<?php

$this->Html->script('uploads', array('inline' => false));

$uploaded = isset($prev_uploads) && is_array($prev_uploads) ? $prev_uploads : array();

if (array_key_exists('Upload', $this->request->data) && is_array($this->request->data['Upload'])) {
  foreach ($this->request->data['Upload'] as $upload) {
    if (array_key_exists('id', $upload)) {
      $uploaded[] = $upload['id'];
    }
  }
}

$upload = array_unique($uploaded);

$max = $max - count($uploaded);

echo $this->Form->input('previous_uploads', array('type'=>'hidden', 'value' => json_encode($uploaded)));


?>
  <ul class="<?php echo join(' ', $classes); ?>" data-max="<?php echo $max; ?>">
<?php
for ($i = 0; $i < $count; $i++) {
  if ($i > 0) { $label = false; }
  echo '<li>' . $this->Form->input('file_upload_' . ($i + 1), array('div' => false, 'label' => false, 'type' => 'file', 'id' => 'fileUpload_' . ($i + 1))) . '</li>';
}




?>
  </ul>
<?php

if ($show_current && array_key_exists('Upload', $this->data)) {
  $uploads = $this->data['Upload'];
  
  $width = 64;
  $height = 64;
  
  ?>
  <style>
	.currentUploads {
	  position: relative;
	  width: auto;
	  height: auto;
	  padding: 2px;
	  display: inline-block;
	}
	
	.actionBlocker {
	  position: absolute;
	  left: 0;
	  top: 0;
	  background-color: #000;
	  width: 100%;
	  height: 100%;
	  z-index: 1001;
	  display: none;
	}
	
	
	.killBox {
	  position: absolute;
	  left: 0;
	  top: 0;
	  background-color: #000;
	  width: <?php echo $width; ?>px;
	  height: <?php echo $height; ?>px;
	  z-index: 1000;
	  display: none;
	}
	
	.thumbnail {
	  position: absolute;
	  left: 0px;
	  top: 0px;
	}
	
	.deleteImage, .imageWrapper {
	  position: relative;
	  display: inline-block;
	  width: <?php echo $width; ?>px;
	  height: <?php echo $height; ?>px;
	  padding: 2px;
	}
	
	.imageControls {
	  text-align: center;
	  position: absolute;
	  left: 0;
	  top: 0;
	  background-color: #444;
	  width: <?php echo $width; ?>px;
	  z-index: 1001;
	  display: none;
	}
	
	.imageControls a {
	  font-size: 8px;
	  color: #fff;
	}
	
	.uploadURL {
		width: 90%;
		position: absolute;
		top: <?php echo $height; ?>px;
		font-size: 10px !important;
		box-shadow: none;
		padding: 2px;
		margin: 1px;
		border: 1px solid #666;
	}
	
  </style>
  
  
  <div class="currentUploads">
	<div class="actionBlocker"></div>
  <?php
	
  if (!function_exists('uploadOrderSort')) {
	function uploadOrderSort($a, $b) {
	  if ($a['order'] == $b['order']) {
		return 0;
	  }
	  return ($a['order'] < $b['order']) ? -1 : 1;
	}
  }
  
  usort($uploads, 'uploadOrderSort');
  
	
	foreach ($uploads as $idx => $u) {
	  if ($UPLOADS->fileExists($u['id'])) {
		$url = $this->Html->url(array('controller'=>'uploads','action'=>'fetch', $u['id'], "{$width}x{$height}"));
		$raw_url = $this->Html->url(array('controller'=>'uploads','action'=>'fetch', $u['id']));
		?>
		<div class="imageWrapper" data-id="<?php echo $u['id']; ?>">
		  <div class="killBox">
		  </div><!-- .killBox -->
		  
		  <div class="imageControls">
				<a href="#" class="btnShift btnShiftLeft" data-dir="left" data-id="<?php echo $u['id']; ?>" title="Shift image left">&lt;&lt;</a> |
				<a href="#" class="btnDelete" data-id="<?php echo $u['id']; ?>" title="Delete image">X</a> |
				<a href="#" class="btnShift btnShiftRight" data-dir="right" data-id="<?php echo $u['id']; ?>" title="Shift image right">&gt;&gt;</a><br>
		  </div><!-- .imageControls -->
		  
		  <img src="<?php echo $url; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="thumbnail" />
			<?php
			
			if ($show_urls === true) {
			  ?><input class="uploadURL" type="text" value="<?php echo htmlspecialchars($raw_url); ?>"><?php
			}
			
			?>
		</div><!-- .imageWrapper -->
		<?php
	  } else { // exitst?
		print "<!-- missing image {$u['id']} -->\n";
	  }
	}
	
  ?>
  </div><!-- .currentUploads -->
  
  <script>
	$(function() {
	  
	  var $prev = $('#LocationPreviousUploads');
	  var $actionBlocker = $('.actionBlocker');
	  
	  $('.imageWrapper').hover(function(e) {
		var $this = $(this);
		var $kill = $this.find('.killBox');
		var $ctrls = $this.find('.imageControls');
		
		$kill.stop();
		$ctrls.stop();
		
		if (e.type == 'mouseenter') {
		  $kill.fadeTo(250, 0.5);
		  $ctrls.fadeTo(250, 1);
		} else if (e.type == 'mouseleave') {
		  $kill.fadeTo(250, 0);
		  $ctrls.fadeTo(250, 0);
		}
	  });
	  
	  var blockAction = function(yes) {
		yes = yes === false ? false : true;
		
		if (yes) {
		  $actionBlocker.fadeTo(100, 0.5);
		} else {
		  $actionBlocker.fadeTo(100, 0);
		}
	  }
	  
	  $('.btnDelete').click(function(e) {
		var $this = $(this);
		var $img = $this.closest('.imageWrapper');
		var id = $this.data('id');
		if (confirm("Delete this image?")) {
		  blockAction(true);
		  $.getJSON('/uploads/del/' + id, {}, function(data, status, xhr) {
			blockAction(false);
			if (data.success) {
			  var npua = [];
			  
			  var $cua = $('.currentUploads a');
			  for (var i = 0; i < $cua.length; i++) {
				var $ua = $($cua.get(i));
				var ua_id = $ua.data('img-id');
				if (ua_id != id) {
				  npua.push(ua_id);
				}
				
			  }
			  
			  $prev.val(JSON.stringify(npua));
			
			  $img.fadeOut(250, function() {
				$img.remove();
			  });
			}
		  });
		}
		if (e.preventDefault) { e.preventDefault(); }
		return false;
	  });;
	  
	  $('.btnShift').click(function(e) {
		var $this = $(this);
		var $img = $this.closest('.imageWrapper');
		var id = $this.data('id');
		var dir = $this.data('dir');
		var $swap;
		var swap;
		var method = false;
		
		if (dir == 'left') {
		  $swap = $img.prev();
		  
		  if ($swap.length > 0) {
			method = 'insertBefore';
		  }	
		} else {
		  $swap = $img.next();
		  
		  if ($swap.length > 0) {
			method = 'insertAfter';
		  }
		}
		
		if (method !== false) {
		  swap = $swap.data('id');
		  blockAction(true);
		  $.getJSON('/uploads/swap/' + id + '/' + swap, {}, function(data, status, xhr) {
			blockAction(false);
			if (data.success) {
			  $img[method]($swap);
			} else {
			  if (data.message) {
				alert("Uh Oh! " + data.message);
			  }
			}
		  });
		}
		
		
		if (e.preventDefault) { e.preventDefault(); }
		return false;
	  });

	});
  </script>
  
  
  <?php
}

if (isset($uploader_component_errors) && count($uploader_component_errors) > 0) {
  print '<div class="uploadErrors error-message">';
  print join("<br/>\n", $uploader_component_errors);
  print "</div><!-- .uploadErrors -->";
}

?>  
</div><!-- .fileUploads -->