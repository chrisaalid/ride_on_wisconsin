<div class="location"><?php

$field_prefix = isset($field_prefix) ? $field_prefix . '_' : '';
$show_addr = isset($show_addr) ? $show_addr : true;
$show_city = isset($show_city) ? $show_city : true;
$show_state = isset($show_state) ? $show_state : true;
$show_map = isset($show_map) ? $show_map : true;
$show_regions = isset($show_regions) ? $show_regions : false;

$map_width = isset($width) ? $width : false;
$map_height =  isset($height) ? $height : false;


if ($show_addr) {
    echo $this->Form->input($field_prefix . 'address', array('class'=>'address'));
}

if ($show_city) {
    echo $this->Form->input($field_prefix . 'city', array('class'=>'city ui-autocomplete-input'));
}

if ($show_state) {
	echo $this->element('fields/state-picker', array('field_name' => $field_prefix . 'state'));
}

if ($show_map) {
  
	$style = '';
	if ($map_width !== false) { $style .= 'width: ' . $map_width . 'px;'; }
	if ($map_height !== false) { $style .= 'height: ' . $map_height . 'px;'; }
  
	echo "<div class=\"locationMap\" style=\"{$style}\"></div>";
}

echo $this->Form->input($field_prefix . 'regions', array('class'=>'regions', 'type'=>'hidden'));

if ($show_regions) {
  ?>
	<div class="input text">
		<strong>Regions:</strong>
		<span class="regions"></span>
	</div>
	
	<?php
}

?></div><!-- .location -->