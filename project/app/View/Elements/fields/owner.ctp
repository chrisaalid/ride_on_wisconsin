<?php

$opts = array();

if (!$is_admin) {
  $opts['type'] = 'hidden';
  $opts['value'] = $logged_in_user['id'];
}

if ($is_dialog) {
  $opts['type'] = 'hidden';
}

echo $this->Form->input('user_id', $opts);



?>