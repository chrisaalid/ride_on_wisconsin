<?php

$field_name = isset($name) ? $name : 'points';

$gpx_import = isset($gpx_import) && is_bool($gpx_import) ? $gpx_import : true;

$width = isset($width) && is_int($width) && $width > 0 ? $width : 550;
$height = isset($height) && is_int($height) && $height > 0 ? $height : 450;
//pr(array_keys($errors));

$is_err = false;
if (isset($errors) && is_array($errors)) {
  if (array_key_exists('points', $errors)) {
	$is_err = true;
  }
}


echo $this->Form->input('city', array('type'=>'hidden', 'id' => 'cityFld'));
echo $this->Form->input('state', array('type'=>'hidden', 'id' => 'stateFld'));

?>
<style type="text/css">
	div.mapModal {
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		display: none;
		background-color: #000;
		opacity: 0.8;
		z-index: 999;
	}

	div.mapWrapper {
		z-index: 1000;
		width: <?php echo $width; ?>px;
		height: <?php echo $height + 50; ?>px;
	}

	div.mapWrapper div{
		clear: none;
	}
	
	div.routeMap {
		width: <?php echo $width; ?>px;
		height: <?php echo $height; ?>px;
	}
	
	.mapToggle {
		width: 150px;
	}
</style>
<script type="text/javascript">
	
	
	$(function() {
		var $points_field = $('#points_<?php echo $field_name; ?>');
		var $map = $('#map_<?php echo $field_name; ?>');
		var $mapModal = $('.mapModal');
		var $mapToggle = $('input.mapToggle');
		var $distance = $('.distance');
		var gMap = null;
		var rt_build = null;
	
	
		function initialize(o) {
			var mapOptions = {
			  scrollwheel: false,
			  center: new google.maps.LatLng(43.0731, -89.4011),
			  zoom: 8,
			  mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			
			var $routes = $('div.routeMap');
			var routeMap = $routes.get(0);
			
			
			
			gMap = new google.maps.Map(routeMap, mapOptions);
				
			rt_build = new RouteDraw(gMap, o);
			
			var pt_array;
			
			try {
			  pt_array = JSON.parse($points_field.val());
			  rt_build.loadPoints(pt_array);
			} catch(e) {
			  pt_array = [];
			}
		}
		
		
		
		function mapActionCallback (action, data) {
			var ptlist = [];
			var dist = bfw.routes.getRouteDistance(data.points);
			for (var i = 0; i < data.points.length; i++) {
				var pt = data.points[i].latLng;
				ptlist.push({'lat':pt.lat().toFixed(6), 'lng': pt.lng().toFixed(6)});
			}
			
			$points_field.val(JSON.stringify(ptlist)); //ptlist.join(';'));
			$distance.html(dist.toFixed(2) + ' mi.');
		};
		
		initialize({
			'callback': mapActionCallback
		});
		
	});
	
</script>

<div class="mapModal"></div>

<div class="input text mapWrapper<?php echo $is_err ? ' error' : ''; ?>">
	<div class="mapHeader">
		<strong>Distance:</strong> <span class="distance">0</span>
	</div>
	<div class="routeMap" id="map_canvas" data-xid="<?php echo $field_name; ?>"></div>

	<?php
	
	
	if ($is_err) {
	  ?><div class="clearfix"></div><div class="error-message">You must include at least two points for a route.</div><?php
	}
	
	?>
</div><!-- .mapWrapper -->

<?php

	echo $this->Form->input($field_name, array('type'=>'hidden','id'=>'points_' . $field_name));
	
	if (!$edit_mode && !$importing) {
		echo '<div class="info">Click to start drawing your route on the map, or upload a GPX file.</div>';
	  
		if ($gpx_import) {
			echo $this->Form->input('gpx', array('type' => 'file', 'label' => 'GPX File'));
		}
	  	if (!$is_dialog) {
			echo $this->Form->input('Upload GPX', array('type' => 'submit', 'label' => false));
		}
	}

?>