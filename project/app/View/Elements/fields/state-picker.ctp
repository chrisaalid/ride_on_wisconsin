<?php

$field_name = isset($field_name) ? $field_name : 'state';
$id = isset($id) ? $id : false;
$state_list = array('AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY');

$classes = isset($classes) && is_array($classes) ? $classes : array();
$kv_list = array();

foreach ($state_list as $idx => $state) {
  $kv_list[$state] = $state;
}

$input_options = array('class' => 'statePicker state', 'type'=>'select', 'options' => $kv_list);

if ($id) {
  $input_options['id'] = $id;
}

if (!$edit_mode) {
  $input_options['value'] = 'WI';
}

$input_options['class'] = join(' ', $classes);


echo $this->Form->input($field_name, $input_options);

?>