<?php

$options = array();

if (!$logged_in_user['admin']) {
  $options['type'] = 'hidden';
}

echo $this->Form->input('user_id', $options);

?>