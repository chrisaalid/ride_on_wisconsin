<?php

$key = isset($key) ? $key : false;
$default = isset($default) ? $default : false;

if ($key !== false) {
  if (array_key_exists($key, $SETTINGS)) {
	print $SETTINGS[$key]['content'];
  } else {
	if ($default !== false) {
	  print $default;
	}
  }
}

?>