<?php

$submit = isset($submit) && is_bool($submit) ? $submit : true;

echo $this->Form->create('Route', array('type' => 'file', 'controller' => 'routes', 'action' => 'add')); 

if ($is_dialog) {

	?>

		<?php
			echo $this->Form->input('gpx', array('type' => 'file', 'label' => 'Select a GPX file:'));
		?>
	<?php


} else {

	?>
		<fieldset>
			<legend><?php echo __('Select a GPX file to import'); ?></legend>
			<!-- <p>Select a GPX file to import</p> -->
		<?php
			echo $this->Form->input('gpx', array('type' => 'file', 'label' => 'GPX File'));
		?>
		</fieldset>	
	<?php

	
}

if ($submit) {
  echo $this->Form->end(__('Submit'));
}