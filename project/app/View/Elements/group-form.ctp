<?php

echo $this->Html->script('groups', array('inline' => false));
echo $this->Html->script('routes', array('inline' => false));

?>
<div class="groups form">
<?php echo $this->Form->create('Group', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __(($edit_mode ? 'Edit' : 'Add') . ' Group'); ?></legend>
	<?php
		if ($edit_mode) {
		  echo $this->Form->input('id');  
		}
		
		echo $this->element('fields/owner');
		
	
		
		echo $this->Form->input('name');
		echo $this->Form->input('contact');
		echo $this->Form->input('email');
		echo $this->Form->input('url');
		echo $this->Form->input('phone');
		//echo $this->Form->input('venue', array('label' => 'Ride Departure Location'));
		
		echo $this->element('fields/location', array('show_addr' => false));
		echo $this->Form->input('lat', array('type' => 'hidden'));
		echo $this->Form->input('lng', array('type' => 'hidden'));
		
		echo $this->Form->input('details', array('label' => 'Group Details/Description'));
		echo $this->Form->input('approved');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
