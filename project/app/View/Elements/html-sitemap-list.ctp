<?php


  $url = isset($type_url) ? $type_url : false;
  
  if (isset($records) && is_array($records) && isset($type)) {
	print '<h2>';
	//if ($url !== false) { print '<a href="' . $url . '" target="_top" onclick="DLG.close();">'; }
	
	$title = isset($title) ? $title : Inflector::pluralize($type);
	
	print $title;
	//if ($url !== false) { print '</a>'; }
	print '</h2>';
	print '<ul>';
	foreach ($records as $idx => $rt) {
	  print '<li>' . $this->Html->link(__($rt[$type]['label']), array('controller' => Inflector::pluralize(strtolower($type)), 'action' => 'view', $rt[$type]['id'])) . '</li>';
	}
	print '</ul>';
  }