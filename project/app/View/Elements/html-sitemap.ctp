<div class="dialog-wrap lrg">
	<?php echo $this->element('dialog-header', array('title' =>'Sitemap', 'section' => 'volunteer')); ?>
	<div class="dl-body first">
<?php

echo $this->element('html-sitemap-list', array('records' => $routes, 'type' => 'Route', 'type_url' => '/#sitemap-Route'));
echo $this->element('html-sitemap-list', array('records' => $locations, 'type' => 'Location'));
echo $this->element('html-sitemap-list', array('records' => $events, 'type' => 'Event'));
echo $this->element('html-sitemap-list', array('records' => $classifieds, 'type' => 'Classified'));
echo $this->element('html-sitemap-list', array('records' => $groups, 'type' => 'Group'));
echo $this->element('html-sitemap-list', array('records' => $merch, 'type' => 'Merchandise', 'title' => "Merchandise"));

?>

	</div><!-- .dl-body -->
</div><!-- .dialog-wrap.lrg -->