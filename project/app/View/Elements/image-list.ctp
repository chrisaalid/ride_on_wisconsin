<?php

$images = isset($images) && is_array($images) ? $images : array();
$format = isset($format) ? $format : '128x128';
$classes = isset($classes) && is_array($classes) ? $classes : array();

$classes[] = 'uploadedImages';

if (!function_exists('imageOrderSort')) {
  function imageOrderSort($a, $b) {
	if ($a['order'] == $b['order']) {
	  return 0;
	}
	return ($a['order'] < $b['order']) ? -1 : 1;
  }
}

usort($images, 'imageOrderSort');

print "<div class=\"" . join(' ', $classes) . "\">\n";
foreach ($images as $idx => $upload) {
  $img_classes = array('upload');
  if ($idx == 0) { $img_classes[] = 'first'; }
  if ($idx == count($images) - 1) { $img_classes[] = 'last'; }
  echo $UPLOADS->image($upload['id'], $format, $img_classes);
}
print "</div>";


?>