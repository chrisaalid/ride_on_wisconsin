<?php echo $this->Form->create('Location', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __(($edit_mode ? 'Edit' : 'Add' ) . ' Location'); ?></legend>
	<?php
	
	  if ($edit_mode) {
		echo $this->Form->input('id');  
	  }
		
	  //echo $this->Form->input('user_id');
	  
	  echo $this->element('fields/owner');
	  
	  echo $this->Form->input('name');
	  echo $this->Form->input('type', array('type' => 'select', 'options' => $this->FieldFormatter->loc_types()));
	  
	  echo $this->element('fields/location', array());
	  echo $this->Form->input('lat', array('id' => 'locLat', 'type' => 'hidden'));
	  echo $this->Form->input('lng', array('id' => 'locLng', 'type' => 'hidden'));
	  
	  //echo $this->Form->input('address');
	  //echo $this->Form->input('state');
	  //echo $this->Form->input('city');
	  echo $this->Form->input('description');
	  echo $this->Form->input('member');
	  echo $this->Form->input('discount');
	  
	  echo $this->element('fields/file-upload', array('label' => 'Location Images', 'max' => 5));
	  
	  if ($is_admin) {
		echo $this->Form->input('approved');
	  }
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>