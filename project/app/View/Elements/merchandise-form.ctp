
<?php echo $this->Form->create('Merchandise', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __(($edit_mode ? 'Edit' : 'Add' ) . ' Merchandise'); ?></legend>
	<?php
		if ($edit_mode) {
		  echo $this->Form->input('id');  
		}
	
		echo $this->Form->input('name');
		echo $this->Form->input('price');
		echo $this->Form->input('url');
		echo $this->Form->input('details');
		
		echo $this->element('fields/file-upload', array('label' => 'Merchandise Images', 'max' => 5));
		
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
