<?php



//if (isset($importing) && $importing === true) {
//  echo $this->Form->create('Route', array('url' => array('action' => 'add'))); 
//} else {
//
//  echo $this->Form->create('Route');
//}




echo $this->Form->create('Route', array('type' => 'file')); 

?>
	<fieldset>
		<legend><?php echo __(($edit_mode ? 'Edit' : 'Add') . ' Route'); ?></legend>
	<?php
		$surface_fields = array('surAsphalt'=>'Asphalt/Concrete','surLimestone'=>'Crushed Limestone','surGravel'=>'Gravel','surWoodchips'=>'Woodchips','surDirt'=>'Dirt','surSand'=>'Sand');
		$topography_fields = array('topFlat'=>'Flat','topRolling'=>'Rolling','topClimbs'=>'Long Climbs');
		$surroundings_fields = array('surUrban'=>'Urban','surRural'=>'Rural/Agricultural','surResidential'=>'Residential','surWooded'=>'Wooded','surStream'=>'Stream/River','surLake'=>'Lake','surPrairie'=>'Prairie');
	
		if ($edit_mode) { // isset($this->data['Route']['id'])) {
			echo $this->Form->input('id');
		}
		
		
		
		echo $this->element('fields/owner');
		
		//if ($CURRENT_ACTION === 'add' || $CURRENT_ACTION === 'import') {
		//  echo $this->Form->input('user_id', array('type'=>'hidden','value'=>$logged_in_user['id']));
		//} else {
		//  echo $this->Form->input('user_id', array('type'=>'hidden'));
		//}
		
		echo $this->Form->input('name', array('label'=>'Route Name'));
		
		print '<div class="mainFieldContainer">';
		
		print '<div class="fleft">';
		
		print '<div class="input text">';
		echo $this->Form->input('offroad', array('type' => 'radio', 'options' => array(0 => '<span class="small">Road Ride</span><br>', 1 => '<span class="small">Off-Road Ride</span><br>'), 'div' => false, 'legend' => 'Overall Ride Type'));
		print '</div>';
		
		print '<div class="input text">';
		echo $this->Form->input('difficulty', array('type' => 'radio', 'options' => $this->FieldFormatter->difficulty_types(), 'div' => false, 'label' => ''));
		print '</div>';
		
		print '<hr class="min dashed">';

		print '<div class="input text">';
		print '<h3 class="label">Surface Attributes</h3>';
		foreach ($surface_fields as $field => $label) {
			echo $this->Form->input($field, array('div'=>false,'label'=>$label));
		}
		print '</div>';

		print '<hr class="min dashed">';
		
		print '<div class="input text">';
		print '<h3 class="label">Topography</h3>';
		foreach ($topography_fields as $field => $label) {
			echo $this->Form->input($field, array('div'=>false,'label'=>$label));
		}
		print '</div>';

		print '<hr class="min dashed">';
		
		
		print '<div class="input text">';
		print '<h3 class="label">Surroundings</h3>';
		foreach ($surroundings_fields as $field => $label) {
			echo $this->Form->input($field, array('div'=>false,'label'=>$label));
		}
		print '</div>';
		
		print '</div><!-- .fleft -->';
		
		print '<div class="fleft">';
		echo $this->element('fields/route-builder', array('name'=>'points'));
		
		print '</div><!-- .fleft -->';
		
		print '<div style="clear: both;" />';
		
		print '</div><!-- .mainFieldContainer -->';
		
		echo $this->Form->input('comment');
		
		echo $this->element('fields/file-upload', array('label' => 'Route Images', 'max' => 5));
		
	?>
	</fieldset>
<?php

	echo $this->Form->end(__('Submit'));
	
	echo $this->Html->script('routes');
	

