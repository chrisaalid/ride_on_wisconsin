<li class="issue">

<?php

$issue = isset($issue) ? $issue : false;

if ($issue !== false) {
  switch ($issue->type) {
	case 'missing':
	  print "Missing column <strong>{$issue->field}</strong> of type <strong>{$issue->auth['TypeName']}</strong>";
	  break;
	case 'type':
	  print "Has a column <strong>{$issue->field}</strong> of type <strong>{$issue->db['TypeName']}</strong> but should be <strong>{$issue->auth['TypeName']}</strong>";
	  break;
	case 'default':
	  print "Has a column <strong>{$issue->field}</strong> of type <strong>{$issue->auth['TypeName']}</strong> with a default of <strong>{$issue->db['Default']}</strong> but should be <strong>{$issue->auth['Default']}</strong>";
	  break;
	
  }
}



?>
</li>