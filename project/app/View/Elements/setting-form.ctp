<?php

echo $this->Html->script('settings');

?>
<script>
  $(function() {
	bfw.settings.init({
	  dataTypes: <?php echo json_encode($MODEL_OBJ->dataTypes); ?>
	});
  });
</script>
<style>
  .contentInput, .CodeMirror, #errorMessage {
	display: none;
  }
</style>
<?php echo $this->Form->create('Setting'); ?>
	<fieldset>
		<legend><?php echo __('Edit Setting'); ?></legend>
	<?php
		$init_val = '';
		if ($edit_mode) {
		  echo $this->Form->input('id');
		  $init_val = $this->data['Setting']['content'];
		}
		
		echo $this->Form->input('key', array('after' => 'Alphas, dots, and dashes only'));
		echo $this->Form->input('name');
		echo $this->Form->input('data_type', array('options' => $MODEL_OBJ->typeOptions(), 'type' => 'select', 'id' => 'dataTypeChooser'));
		echo $this->Form->input('description', array('required' => false));
		
		?>
		<div class="input textarea required">
		  <label>Content</label>
		<?php
		echo $this->Form->input('contentString', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'contentInput', 'id' => 'inpType_string', 'value' => $init_val));
		echo $this->Form->input('contentText', array('type' => 'textarea', 'label' => false, 'div' => false, 'class' => 'contentInput', 'id' => 'inpType_text', 'value' => $init_val));
		echo $this->element('fields/code-editor', array('field_name' => 'content', 'label' => false, 'class' => 'contentInput', 'wrapper_id' => 'inpType_JSON', 'editor_type' => 'javascript', 'changeEvent' => 'bfw.settings.setRealVal', 'value' => $init_val));
		?>
		<p class="error" id="errorMessage"></p>
		</div>
		<?php
		
		echo $this->Form->input('content', array('type' => 'hidden', 'id' => 'realInput'));
	?>
	</fieldset>
	
<?php

echo $this->Form->end(__('Submit'));

?>
