<?php
//$edit_mode = isset($this->data['Template']['id']);
?>

<?php echo $this->Form->create('Template'); ?>
	<fieldset>
		<legend><?php echo __(($edit_mode ? 'Edit' : 'Add') . ' Template'); ?></legend>
	<?php
	  if ($edit_mode) {
		echo $this->Form->input('id');
	  }
	  echo $this->Form->input('name');
	  echo $this->Form->input('description');
	  echo $this->element('fields/code-editor', array('field_name' => 'tmpl', 'label' => 'Template Code'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>