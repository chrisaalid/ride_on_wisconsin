<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend><?php echo __(($edit_mode ? 'Edit' : 'Add') . ' User'); ?></legend>
	<?php
		if ($edit_mode) {
		  echo $this->Form->input('id');  
		} else {
		  echo $this->Form->input('username');
		}
		
		echo $this->Form->input('firstname', array('label'=>'First Name'));
		echo $this->Form->input('lastname', array('label'=>'Last Name'));
		echo $this->Form->input('email');
		
		if (!$edit_mode) {
		  echo $this->Form->input('pwd', array('type'=>'password','label'=>'Password'));
		  echo $this->Form->input('pwd_repeat', array('type'=>'password','label'=>'Confirm Password'));
		}
		
		//if (array_key_exists('admin', $logged_in_user) && $logged_in_user['admin'] && array_key_exists('User', $this->data) && $this->data['User']['id'] != $logged_in_user['id']) {
		if ($is_admin && ($add_mode || $this->data['User']['id'] != $logged_in_user['id'])) {
		  echo $this->Form->input('admin');
		}
		
		//echo $this->Form->input('state');
		echo $this->element('fields/state-picker');
		echo $this->Form->input('city');
		echo $this->Form->input('profile');
		
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>