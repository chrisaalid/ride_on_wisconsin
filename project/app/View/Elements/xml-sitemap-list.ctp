<?php

if (isset($records) && is_array($records) && isset($type)) {
  if (isset($type_url)) {
	?>
	 <url>
	   <loc><?php
	   
	   echo $this->Html->url($type_url, true);
	   
	   ?></loc>
	   <lastmod><?php echo date('Y-m-d', time()); ?></lastmod>
	  </url>
	<?php
  }
  
  foreach ($records as $idx => $rt) {
  ?>
  <url>
	   <loc><?php
	   
	   echo $this->Html->url(array('controller' => Inflector::pluralize(strtolower($type)), 'action' => 'view', $rt[$type]['id']), true);
	   
	   ?></loc>
	   <lastmod><?php echo date('Y-m-d', $rt[$type]['modified']); ?></lastmod>
  </url>
 <?php
  }
}