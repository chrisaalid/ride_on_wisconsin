<?php

$url  = 'http://' . $_SERVER['SERVER_NAME'] . '/#!/users/activate/' . $user['resetkey'];
?>

<p>Hello <?php echo htmlentities($user['username']) ?>,</p>

<p>You're one step away from activating your account.  Please click the link below to confirm your email address.</p>

<p><a href="<?php echo $url; ?>"><?php echo $url; ?></a></p>

<p>Thanks,</p>

<p>The Wisconsin Bicycle Federation</p>