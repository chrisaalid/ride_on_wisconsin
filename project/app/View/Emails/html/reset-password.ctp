<?php

$url = 'http://' . $_SERVER['SERVER_NAME'] . '/#!/users/reset/' . $user['resetkey'];

?>
<p>Hello <?php echo $user['username']; ?>,</p>

<p>A request was made to reset your password.  If you did not make this request, do nothing.  If you did, please visit the link below to change your password.</p>

<p><a href="<?php echo $url; ?>"><?php echo $url; ?></a></p>

<p>Thanks,</p>

<p>The Wisconsin Bicycle Federation</p>