Hello <?php echo $user['username'] ?>,



You're one step away from activating your account.  Please click the link below to confirm your email address.

http://<?php echo $_SERVER['SERVER_NAME']; ?>/users/activate/<?php echo $user['resetkey']; ?>

Thanks,

The Wisconsin Bicycle Federation