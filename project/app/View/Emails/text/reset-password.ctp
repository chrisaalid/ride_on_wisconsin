Hello <?php echo $user['username']; ?>,

A request was made to reset your password.  If you did not make this request, do nothing.  If you did, please visit the link below to change your password.

http://<?php echo $_SERVER['SERVER_NAME']; ?>/#!/users/reset/<?php echo $user['resetkey']; ?>

Thanks,

The Wisconsin Bicycle Federation