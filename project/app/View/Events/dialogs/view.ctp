<div class="dialog-wrap lrg">
	<?php echo $this->element('dialog-header', array('title' => $event['Event']['name'], 'icon'=>'x')); ?>
	<div class="dl-body first">
		<div class="section bot">
			<div class="col six">
				<div class="col-pad-right">
					<!--<h1>Sat, Jun 2, 2012</h1>
					2:00 PM-->
					<h1><?php echo $this->FieldFormatter->date($event['Event']['start'], array('format' => 'eventDate')); ?></h1>
					<?php echo $this->FieldFormatter->date($event['Event']['start'], array('format' => 'eventTime')); ?>
					
					<hr class="dashed min">

					<p><?php echo h($event['Event']['venue']); ?><br>
						<?php echo h($event['Event']['address']); ?> <br>
						<?php echo h($event['Event']['city']); ?>, <?php echo h($event['Event']['state']); ?></p>
						
					
					<table>
						<tr>
							<td>Cost:</td>
							<td><?php echo $event['Event']['free'] ? 'FREE' : 'YES' ; ?></td>
						</tr>
						<tr>
							<td>Phone: </td>
							<td><?php echo h($event['Event']['phone']); ?></td>
						</tr>
						<tr>
							<td>Email:</td>
							<td class="truncate"><a href="mailto:<?php echo $event['Event']['email']; ?>"><?php echo h($event['Event']['email']); ?></a></td>
						</tr>
						<tr>
							<td>Website:</td>
							<td class="truncate"><a href="<?php echo $event['Event']['url']; ?>" target="_blank"><?php echo h($event['Event']['url']); ?></a></td>
						</tr>
						<tr>
							<td>Sponsor:</td>
							<td><?php echo h($event['Event']['sponsor']); ?></td>
						</tr>
					</table>

					<p><?php echo $this->FieldFormatter->cleanContent($event['Event']['details']); ?></p>
				</div><!-- /.col-pad-right -->
			</div><!-- /.col .six -->
			<div class="col six dashed-left">
				<div class="col-pad-left ta-center">
					<?php echo $this->element('content-slide-show', array('record' => $event)); ?>
				</div><!-- /.col-pad-left -->
			</div><!-- /.col .six -->
		</div><!-- /.section -->
	</div><!-- /.dl-body -->
</div><!-- /.dialog-wrap-sm -->  