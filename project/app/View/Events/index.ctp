<?php

for ($i = 0; $i < count($events); $i++) {
	$e = $events[$i];
	$events[$i]['Event']['photos'] = array_key_exists('Upload', $e) ? count($e['Upload']) : 0;
}

echo $this->element('display-index', array(
  'records' => $events,
  'use_only' => array('User.username', 'name', 'approved', 'featured', 'photos', 'start', 'end', 'city', 'state', 'created', 'modified'),
  'format' => array(
    'start' => array('date', array('format' => 'shortDateTime')),
	'end' => array('date', array('format' => 'shortDateTime')),
  ),
  'actions' => array(
	'Feature' => array('action' => 'feature', 'field' => 'id')
  ),
  'labels' => array(
	'User.username' => 'Owner'	
  )
));

?>