<?php

$date_format = array_key_exists('event-xml-date-format', $SETTINGS) ? $SETTINGS['event-xml-date-format']['content'] : 'r';

foreach ($events as $idx => $e) { ?>

<item>
	<name><?php echo h($e['Event']['name']); ?></name>
	<link><?php echo $this->Html->url(array('controller' => 'events', 'action' => 'view', $e['Event']['id']), true); ?></link>
	<date><?php echo date($date_format, strtotime($e['Event']['start'])); ?></date>
	<venue><?php echo h($e['Event']['venue']); ?></venue>
	<address><?php echo h($e['Event']['address']); ?></address>
	<city><?php echo h($e['Event']['city']); ?></city>
	<state><?php echo h($e['Event']['state']); ?></state>
	<type><?php echo h($e['Event']['tags']); ?></type>
	<organizer><?php echo h($e['Event']['sponsor']); ?></organizer>
	<URL><?php echo h($e['Event']['url']); ?></URL>
	<phone><?php echo h($e['Event']['phone']); ?></phone>
	<email><?php echo h($e['Event']['email']); ?></email>
	<extra><![CDATA[<?php echo $this->FieldFormatter->escapeEntitiesPreserveMarkup($e['Event']['details']); ?>]]></extra>
</item>
	<?php
}