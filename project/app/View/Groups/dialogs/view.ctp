<?php

$g = $group['Group'];

$bonus_links = array();
if ($search !== false) {
  $bonus_links[] = $this->Html->link('Back To Results', array('action' => 'search', 'cache'), array('class'=>'header-btns edit-icon back-btn'));
}

?>



<div class="dialog-wrap lrg">
	<?php
	
	echo $this->element('dialog-header', array('title' => 'Ride Groups', 'bonus_links' => $bonus_links));
	
	?>
	<div class="dl-body first">
		<div class="section">
			<div class="col twelve">
				<h1><?php echo $g['name']; ?></h1>
				 <?php echo $g['city'] . ', ' . $g['state']; ?><!--/ 12 Miles-->
			</div>
		</div>
		<hr>
		<div class="section bot">
			<div class="col six">
				<div class="col-pad-right">
					<table>
						<tr>
							<td>CONTACT:</td>
							<td><?php echo $g['contact']; ?></td>
						</tr>
						<tr>
							<td>EMAIL:</td>
							<td class="truncate"><a href="mailto:<?php echo $g['email']; ?>?subject=<?php echo urlencode($g['name']); ?>"><?php echo $g['email']; ?></a></td>
						</tr>
						<tr>
							<td>WEBSITE:</td>
							<td class="truncate"><a href="<?php echo $g['url']; ?>" target="_blank"><?php echo $g['url']; ?></a></td>
						</tr>
						<tr>
							<td>PHONE:</td>
							<td><?php echo $g['phone']; ?></td>
						</tr>
					</table>

					<p><?php echo $g['details']; ?></p>
				</div><!-- /.col-pad-right -->
			</div><!-- /.col .six -->
			<div class="col six dashed-left">
				<div class="col-pad-left ta-center">
					<div class="depart-info ">
						RIDE DEPARTURE POINT: <br>
						<?php echo $g['venue']; ?><br>
						<?php echo $g['address']; ?> <br>
						<?php echo $g['city'] . ', ' . $g['state']; ?><br><br>
					</div>
					
					<div class="googleMap">
					  <?php
					  
					  $addr_parts = array();
					  if (strlen(trim($g['venue'])) > 0) { $addr_parts[] = $g['venue']; }
					  if (strlen(trim($g['address'])) > 0) { $addr_parts[] = $g['address']; }
					  if (strlen(trim($g['city'])) > 0) { $addr_parts[] = $g['city']; }
					  if (strlen(trim($g['state'])) > 0) { $addr_parts[] = $g['state']; }
					  
					  ?>
					  <iframe
						width="330"
						height="313"
						frameborder="0"
						scrolling="no"
						marginheight="0"
						marginwidth="0"
						src="https://maps.google.com/maps?ie=UTF8&amp;q=<?php echo urlencode(join(', ', $addr_parts)); ?>&amp;t=h&amp;output=embed"></iframe><br />
					</div>
				</div><!-- /.col-pad-left -->
			</div><!-- /.col .six -->
		</div><!-- /.section -->
	</div><!-- /.dl-body -->
</div><!-- /.dialog-wrap.lrg -->  