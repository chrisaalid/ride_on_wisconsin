<?php

App::uses('App', 'Helper');

//class FieldFormatterHelper extends AppHelper {
class EmailImageHelper extends AppHelper {
  public function imageUrl($rel, $opts = array()) {
	$base_url  = 'http://' . $_SERVER['SERVER_NAME'];
	//$base_path = WWW_ROOT;
	return $base_url . $rel;
  }
  
  public function imageTag($rel, $opts = array()) {
	$o = array(
	  'alt' => false
	);
	
	$o = array_merge($o, $opts);
	
	$img_tag = '<img src="' . $this->imageUrl($rel) . '"';
	
	if ($o['alt'] !== false) {
	  $img_tag .= ' alt="' . htmlentities($o['alt']) . '"';
	}
	
	$img_tag .= ' />';
	return $img_tag;
  
  }
  
}
