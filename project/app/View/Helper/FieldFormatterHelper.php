<?php

App::uses('Formatter', 'Data');
App::uses('App', 'Helper');

//class FieldFormatterHelper extends AppHelper {
class FieldFormatterHelper extends AppHelper {
//  public $date_formats = array(
//    'longDateTime'   => 'M. j<\s\u\p>S</\s\u\p>, Y g:i a',
//	'longDate'       => 'M. j<\s\u\p>S</\s\u\p>, Y',
//	'shortDateTime'  => 'n/j/Y  g:i a',
//	'shortDate'      => 'n/j/Y',
//	'eventDate'      => 'D, M j, Y',
//	'eventTime'      => 'g:i A'
//  );
//  
//  public $loc_types = array(
//	'bike-shop' => 'Bike Shop',
//	'lodging' => 'Lodging',
//	'restaurant' => 'Restaurant',
//	'mtb-trail-head' => 'MTB Trail Head'
//  );
//  
//  public $difficulty_types = array(
//	'Easy <span class="small">(For all riders)</span><br/>',
//	'Medium <span class="small">(For intermediate riders)</span><br/>',
//	'Hard <span class="small">(For experienced riders)</span>'
//  );
//

  public function date_formats() {
	return Formatter::$date_formats;
  }

  public function loc_types() {
	return Formatter::$loc_types;
  }

  public function difficulty_types() {
	return Formatter::$difficulty_types;
  }
  
  public function simple_difficulty_types() {
	return Formatter::$simple_difficulty_types;
  }
  
  public function classified_types() {
	return Formatter::$classified_types;
  }
  
  public function classified_condition() {
	return Formatter::$classified_condition;
  }

  public function bool($value, $options = array()) {
	return $value ? 'YES' : 'NO';
  }

  public function date($value, $options = array()) {
	return Formatter::date($value, $options);
  }

  public function locationType($type) {
	return Formatter::locationType($type);
  }

  public function precision($value, $decimal_places = 2) {
	return Formatter::precision($value, $decimal_places);
  }
  
  public function currency($value) {
	return Formatter::currency($value);
  }

  public function calculated($record, $func, $params = array()) {
	return Formatter::calculated($record, $func, $params);
  }
  
  public function cleanContent($content, $opts = array()) {
	return Formatter::cleanContent($content, $opts);
  }
	
	public function escapeEntitiesPreserveMarkup($content) {
		return Formatter::escapeEntitiesPreserveMarkup($content);
	}
  
}
