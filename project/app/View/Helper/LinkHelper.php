<?php

App::uses('Helper', 'View');


class LinkHelper extends AppHelper {
  
  private $dialog = null;
  private $controller = null;
  private $action = null;

  public function init() {
	if ($this->controller != null) { return; }
	
	$this->controller = $this->request->params['controller'];
	$this->action = $this->request->params['action'];
	$this->dialog = preg_match('/^\/dialog/', $this->request->here);
  }
  
  private function ine($ar, $key, $val) {
	if (array_key_exists($key, $ar)) {
	  return $ar[$key];
	} else {
	  return $val;
	}
  }
  
  public function url($url = NULL, $full = false) {
	$this->init();
	$controller = $this->ine($url, 'controller', 'users');
	$action = $this->ine($url, 'action', 'index');
	$mode = $this->ine($url, 'mode', false);
	
	
	$link_url = array();
	if ($mode !== false) {
	  if ($mode == 'dialog') {
		$link_url[] = 'dialog';
	  }
	} else {
	  if ($this->dialog) {
		$link_url[] = 'dialog';
	  }
	}
	
	$link_url[] = $controller;
	$link_url[] = $action;
	
	foreach ($url as $idx => $val) {
	  if (is_int($idx)) {
		$link_url[] = $val;
	  }
	}
	
	
	return join('/', $link_url);
  }
  
  public function link($label, $url = array(), $options = array()) {
	$link_url = $this->url($url);
	
	$link_classes = '';
	if (array_key_exists('class', $options)) {
	  $link_classes .= ' class="';
	  if (is_array($options['class'])) {
		$link_classes .= join(' ', $options['class']);
	  } else {
		$link_classes .= $options['class'];
	  }
	  $link_classes .= '"';
	}
	
	return '<a href="/' . $link_url . '"' . $link_classes . '>' . $label . '</a>';
  }
}
