<head>
<body>
	<table cellpadding="0" cellspacing="0" style="border: 0; width: 600px;">
 		<tr>
 			<td>
			  <?php echo $this->EmailImage->imageTag('/images/email/row-email-header.png', array('alt' => 'Ride on Wisconsin')); ?>
 			</td>
 		</tr>
 		<tr>
 			<td style="background: #EEE8D8; padding: 35px 30px;">
			  <?php
			  
			  if (isset($topMsg)) {
			  ?>
			  
 				<h1 style="color: #412E1F; font-family: Arial, Helvetica, sans-serif; font-size: 27px; font-weight: normal;"><?php echo $topMsg; ?></h1>
			  <?php
			  }
			  ?>
				
				<div style="color: #412E1F; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 21px;">
					<?php

					echo $this->fetch('content');
					
					?>
				</div>
				
				<table cellspacing="0" cellspacing="0" style="border: 0; width: 100%;">
					<tr>
						<td style="padding-top: 15px;">
							<a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/" target="_blank"><?php echo $this->EmailImage->imageTag('/images/email/btn-row.png', array('alt' => 'Visit Ride on Wisconsin')); ?></a>
						</td>
						<td style="text-align: right; padding-top: 15px;">
							<a href="https://www.facebook.com/bike.fed.wi" target="_blank"><?php echo $this->EmailImage->imageTag('/images/email/btn-fb.png', array('alt' => 'Find us on Facebook')); ?></a>
							<a href="https://twitter.com/BikeFed" target="_blank"><?php echo $this->EmailImage->imageTag('/images/email/btn-twitter.png', array('alt' => 'Follow us on Twitter')); ?></a>
							<a href="https://www.youtube.com/user/WisconsinBikeFed" target="_blank"><?php echo $this->EmailImage->imageTag('/images/email/btn-yt.png', array('alt' => 'See us on YouTube')); ?></a>
						</td>
					</tr>
				</table>
 			</td>
 		</tr>
 		<tr>
 			<td style="background: #412E1F; color: #fff; font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; line-height: 18px; padding: 35px 30px;">
 				&copy; 2013 Wisconsin Bike Fed. RideOnWisconsin.org is a project brought to you by the Wisconsin Bike Fed. We&rsquo;re an organization committed to making Wisconsin a better, more bike-friendly state.

 				<p>
 					This email was sent to <?php echo $user['email']; ?> by The Wisconsin Bike Fed. <br>
					<a href="http://wisconsinbikefed.org/privacy/" target="_blank" style="color: #009EA7; text-decoration: none;">Privacy Policy</a> | <a href="http://wisconsinbikefed.org" target="_blank" style="color: #009EA7; text-decoration: none;">WisconsinBikeFed.org</a>
 				</p>
 				<p style="border-top: 1px solid #4B392B; padding-top: 10px;">
 					<em>Wisconsin Bike Fed Milwaukee Office</em> <br>
					3618 W. Pierce Street  &bull;  Milwaukee, WI 53215  &bull;  <a href="mailto:info@wisconsinbikefed.org" target="_blank" style="color: #009EA7; text-decoration: none;">Info@WisconsinBikeFed.org</a>
 				</p>
 				<p>
 					<em>Wisconsin Bike Fed Madison Office</em> <br>
					409 E Main Street, Suite 203  &bull;  Madison, WI 53703  &bull;  <a href="mailto:info@wisconsinbikefed.org" target="_blank" style="color: #009EA7; text-decoration: none;">Info@WisconsinBikeFed.org</a>
 				</p>
 			</td>
 		</tr>
 	</table> 	
</body>
</html>