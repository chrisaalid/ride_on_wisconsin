<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

 
  //$this->Html->script('json2', array('inline' => false));
  //$this->Html->script('jquery-1.9.1.min', array('inline' => false));
  //$this->Html->script('//maps.googleapis.com/maps/api/js?key=AIzaSyApdxs0KEfjJGCqPxbN4DAcyZn2vkZxaIE&sensor=false', array('inline' => false));
  //$this->Html->script('jquery-ui-1.10.0.custom.min', array('inline' => false));
  //$this->Html->script('jquery-ui-timepicker-addon', array('inline' => false));
  //$this->Html->script('tag-it.min', array('inline' => false));
  //$this->Html->script('ui', array('inline' => false));
  //$this->Html->script('bfw', array('inline' => false));
  //$this->Html->script('locations', array('inline' => false));
  
  $this->Html->script(array(
	'json2',
	'jquery-ui-1.10.0.custom.min',
	'jquery-ui-timepicker-addon',
	'tag-it.min',
	'admin',
    'json-loader',
	//'ui',
	'locations'), array('inline' => false));
  
  
  $this->Html->css(array(
	'cake.generic',
	'jquery-ui-timepicker-addon',
	'jquery.tagit',
	'smoothness/jquery-ui-1.10.0.custom.min.css',
	'backend.css'), 'stylesheet', array('inline' => false));

$cakeDescription = __d('cake_dev', 'Ride On Wisconsin');
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->fetch('meta');
		
		echo $this->fetch('css');
		
		echo $this->Html->script('jquery-1.9.1.min');
		echo $this->Html->script('//maps.googleapis.com/maps/api/js?key=AIzaSyApdxs0KEfjJGCqPxbN4DAcyZn2vkZxaIE&sensor=false');
		echo $this->Html->script('bfw');
		echo $this->fetch('script');
		
		
	?>
  
	
</head>
<body>
	<div id="container">
		<div id="header" class="clear">
			<h1><?php echo $this->Html->link($cakeDescription, 'http://rideonwisconsin.org'); ?></h1>
			<ul class="topNav">
				<?php
				
				if (isset($logged_in_user) && isset($logged_in_user['username'])) {
				?>
				<li><a href="/users/logout">Log Out <?php echo $logged_in_user['username']; ?></a></li>
				<li><a href="/users/dialogs/yes">Dialog Interface</a></li>
				<?php
				
				} else {
					
				?>
				<li><a href="/users/login">Log In</a></li>
				<?php
				
				}
				
				?>
			</ul>
		</div>
		<div id="content">

			<?php
			
			echo $this->Session->flash();
			
			echo $this->element('admin-navigation');
			
			echo $this->fetch('content');
			
			?>
			
			
			
		</div>
		<div id="footer">
			<?php echo $this->Html->link(
					$this->Html->image('cake.power.gif', array('alt' => $cakeDescription, 'border' => '0')),
					'http://www.cakephp.org/',
					array('target' => '_blank', 'escape' => false)
				);
			?>
		</div>
	</div>
	
	
	<?php
	
	  //debug($logged_in_user);
	  //debug($CURRENT_PARAMS);
	  //echo $this->element('sql_dump');
	
	?>
	
	<script>
		// break admin users out of dialogs
		$(function() {
		  var DLG = parent.DLG || false;
		  if (DLG !== false) {
			DLG.navTo(document.location);
		  }
		});
		
	</script>
	
</body>
</html>
