<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<?php

  $this->Html->script(
	array(
	  'json2',
	  'jquery-ui-1.10.0.custom.min',
	  'jquery-ui-timepicker-addon',
	  'row-dialog-inner.js',
	  'mustache',
	  'tag-it.min',
	  'locations'
	),
	array('inline' => false)
  );
  
  $this->Html->css(array(
	'jquery-ui-timepicker-addon',
	'jquery.tagit',
	'smoothness/jquery-ui-1.10.0.custom.min.css',
	'http://fonts.googleapis.com/css?family=Alfa+Slab+One|Jockey+One|Crete+Round|Montserrat:400,700',
	'normalize.min.css',
	'dialog.css',
	//'style.css',
	'print.css'
	),
	'stylesheet',
	array('inline' => false)
  );
  
  
?>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Ride On Wisconsin</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
		<meta name="fragment" content="!">
		<?php
		
		if ($CURRENT_ACTION == 'view')  {
		
		?>
		<meta property="og:title" content="<?php echo htmlentities($content_title); ?>"/>
		<meta property="og:description" content="<?php echo htmlentities($content_title); ?>"/>
		<meta property="og:image" content="http://<?php echo $_SERVER['HTTP_HOST']; ?>/images/admin/bfw-logo.png"/>
		<?php
		
		} // view?
		
		echo $this->Html->meta('icon');

		echo $this->fetch('meta');
		
		echo $this->fetch('css');
		
		echo $this->Html->script('jquery-1.9.1.min');
		echo $this->Html->script('//maps.googleapis.com/maps/api/js?key=AIzaSyApdxs0KEfjJGCqPxbN4DAcyZn2vkZxaIE&sensor=false');
		//echo $this->Html->script('row-dialog-inner');
		
		//echo $this->Html->script('bfw');
		//echo $this->Html->script('home');
		//echo '<!--[if lte IE 7]><script src="js/lte-ie7x.js"></script><![endif]-->';

		echo $this->fetch('script');
		
		//sleep(15);
	
?>
		
		
		
		
		
    </head>
    <body>
	  <div id="fb-root"></div>
	  <script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=674678265876041";
		fjs.parentNode.insertBefore(js, fjs);
	  }(document, 'script', 'facebook-jssdk'));</script>
	  
<?php

if ($error === false) {
  echo $this->fetch('content');
} else {
  echo "<strong>ERROR:</strong> $error";
}

		
if (isset($close_dialog) && $close_dialog === true) {
  ?>
  
  <script>
	var readyFunc = function(DLG) {
	  DLG.close();
	}
  </script>
  
  <?php
}


echo $this->element('google-analytics');

?>

	</body>
  </html>