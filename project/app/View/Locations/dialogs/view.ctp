<?php
$l = $location['Location'];
$loc_types = $this->FieldFormatter->loc_types();
$loc_address = $l['address'] . ', ' . $l['city'] . ', ' . $l['state'];

?><div class="dialog-wrap lrg">
	<?php echo $this->element('dialog-header', array('title' => $location['Location']['name'])); ?>
	<div class="dl-body first">
		<div class="section bot">
			<div class="col six">
				<div class="col-pad-right">
					<?php echo h($location['Location']['name']); ?> (<?php echo $loc_types[$location['Location']['type']]; ?>)<br>
					<?php echo h($location['Location']['address']); ?><br>
					<?php echo h($location['Location']['city']) . ', ' . h($location['Location']['state']); ?><br>
					<?php
					
					if ($location['Location']['member']) {
					  ?><p><a href="#">Wisconsin Bike Fed Member</a></p><?php
					}
					
					if ($location['Location']['discount']) {
					  ?><p><a href="#">Wisconsin Bike Discount</a></p><?php
					}
					
					?>
					<hr class="dashed min">
					<?php
					
					echo $this->FieldFormatter->cleanContent($location['Location']['description']);
					
					?>
				</div><!-- /.col-pad-right -->
			</div><!-- /.col .six -->
			<div class="col six dashed-left">
				<div class="col-pad-left ta-center">
					<?php echo $this->element('content-slide-show', array('record' => $location, 'placeholder_override' => $loc_address )); ?>
				</div><!-- /.col-pad-left -->
			</div><!-- /.col .six -->
		</div><!-- /.section -->
	</div><!-- /.dl-body -->
</div><!-- /.dialog-wrap-sm -->  