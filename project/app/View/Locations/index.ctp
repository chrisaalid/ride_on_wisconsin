<?php

echo $this->element('display-index', array(
  'records' => $locations,
  'use_only' => array('User.username', 'name', 'approved', 'type', 'city', 'state', 'member', 'discount', 'created', 'modified'),
  'labels' => array('User.username' => 'Owner'),
  'format' => array('type' => array('locationType', ''))
));

?>