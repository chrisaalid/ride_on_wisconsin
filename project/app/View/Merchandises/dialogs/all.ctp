<div class="dialog-wrap lrg">
	<?php echo $this->element('dialog-header', array('title' => 'Merchandise', 'section' => 'merch')); ?>

	<div class="dl-body first">
		<div class="section bot">
			<div class="col twelve">
				<div class="col-pad-right">
					<h2 class="orange">Buy Bike Fed merchandise, state trail passes or Wisconsin bicycle maps. You'll enjoy our cool Bike Fed gear plus all the profit supports our work to improve bicycling across the state.</h2>
					
					<p>If you prefer, you can mail in your order. To purchase Bike Fed clothing, Wisconsin bike maps or Wisconsin trail passes, please download the Bike Fed Online Purchase form, fill it out and mail it in. You can browse our selections below.</p>

					<p>All merchandise prices include sales tax, except DNR Trail Passes, which are tax exempt. Want to give a gift membership or merchandise? Click here to print out a gift form to mail in.</p>
				</div><!-- /.col-pad-right -->
			</div><!-- /.col .twelve -->
		</div><!-- /.section -->
	</div><!-- /.dl-body -->
	
	<?php
	
	foreach ($merchandises as $idx => $merch) {
	  $m = $merch['Merchandise'];
	  $classes = array('dl-body','clear', 'merch');
	  $classes[] = $idx % 2 == 0 ? 'highlight' : 'margin-bot-30';
	  $classes[] = 'merchItem' . $m['id']; 

	?>
	<a name="item-<?php echo $m['id']; ?>"></a>
	<div class="<?php echo join(' ', $classes); ?>">
		
		<div class="col eight">
			<div class="content">
				
				<h2><?php echo $m['name']; ?></h2>
				
				<p><strong><?php echo $this->FieldFormatter->currency($m['price']); ?> each</strong></p>

				<?php
				
				echo $m['details'];
				
				?>
				
				<a href="https://bfw.memberclicks.net/online-store" class="btn submit" target="_blank">Buy Now</a>
			</div><!-- /.content -->
		</div>
		<div class="col four">
			<div class="ta-center">
			  <?php echo $this->element('content-slide-show', array('record' => $merch, 'width' => 225, 'height' => 225)); ?>
			</div><!-- /.col-pad-left -->
		</div>
	</div><!-- /.dl-body -->
	<?php
	
	}
	
	?>
</div><!-- /.dialog-wrap.lrg -->  