<?php

for ($i = 0; $i < count($merchandises); $i++) {
	$r = $merchandises[$i];
	$merchandises[$i]['Merchandise']['photos'] = array_key_exists('Upload', $r) ? count($r['Upload']) : 0;
}

echo $this->element('display-index', array(
  'records' => $merchandises,
  'use_only' => array('name', 'price', 'photos', 'created', 'modified'),
  'format' => array('price' => array('currency', 2)),
  'labels' => array(
	'User.username' => 'Owner'	
  ),
  'actions' => array(
	'Up' => array('action' => 'moveup', 'field' => 'id'),
	'Down' => array('action' => 'movedown', 'field' => 'id')	
  )
));

?>