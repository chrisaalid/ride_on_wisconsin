<div class="merchandises view">
<h2><?php  echo __('Merchandise'); ?></h2>
	<dl>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($merchandise['Merchandise']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Price'); ?></dt>
		<dd>
			<?php echo $this->FieldFormatter->currency($merchandise['Merchandise']['price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('URL'); ?></dt>
		<dd>
			<?php
			
			if (strlen(trim($merchandise['Merchandise']['url'])) > 0) {
			  echo $this->Html->link($merchandise['Merchandise']['url'], $merchandise['Merchandise']['url']);
			}
			//
			//pr(APP);
			//pr(WWW_ROOT);
			
			//echo h($merchandise['Merchandise']['price']);
			
			?>
			&nbsp;
		</dd>
		<dt><?php echo __('Details'); ?></dt>
		<dd>
			<?php echo h($merchandise['Merchandise']['details']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($merchandise['Merchandise']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($merchandise['Merchandise']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Photos'); ?></dt>
		<dd>
		  <?php
		  
		  echo $this->element('content-slide-show', array('record' => $merchandise));
		  
		  ?>
		</dd>
	</dl>
</div>