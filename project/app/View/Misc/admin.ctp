<div class="form"><?php

$route_opts = array();
foreach ($all_routes as $idx => $route) {
  $route_opts[$route['Route']['id']] = $route['Route']['name'] . ' (' . $route['User']['username'] . ')';
}

?>
<style>
  /*#RoutesSection {
	display: none;
  }*/
  
  td input[type="button"] {
	width: auto;
  }
  
  .MiscWorkArea {
	position: relative;
	display: block;
	width: auto;
	height: auto;
  }
  
  .MiscOverlay {
	position: absolute;
	left: 0;
	top: 0;
	right: 0;
	bottom: 0;
	background: #000;
	display: block;
	z-index: 10000;
  }
  
  .MiscContent {
	position: absolute;
	left: 0;
	top: 0;
	display: block;
  }
  
  
  
</style>
<script>
  $(function() {
	
	
	var deactivateWorkArea = function(wa, after) {
	  var $MWA = $(wa);
	  var $content = $MWA.find('.MiscContent');
	  var $overlay = $MWA.find('.MiscOverlay');
	  $overlay.css({'width': $content.width(), 'height': $content.height()});
	  $overlay.fadeTo(250, 0.8, function() {
		if (typeof after === 'function') {
		  after();
		}
	  });
	};
	
	var activateWorkArea = function(wa, after) {
	  var $MWA = $(wa);
	  var $content = $MWA.find('.MiscContent');
	  var $overlay = $MWA.find('.MiscOverlay');
	  
	  $overlay.fadeOut(250, function() {
		if (typeof after === 'function') {
		  after();
		}
	  });
	  
	};
	
	var $routesSection = $('#RoutesSection');
	var $allRoutes = $('#AllRoutes');
	var $featuredRoutes = $('#FeaturedRoutes');
	var $addRoute = $('#AddRoute');
	var $removeRoute = $('#RemoveRoute');
	var $routeUp = $('#RouteUp');
	var $routeDown = $('#RouteDown');
	var $routeSave = $('#RouteSave');
	
	
	var featured = {
	  routes: {
		add: function(id) {
		  var sel = ':selected';
		  if (typeof id !== 'undefined') { sel = '[value="' + id + '"]'; }
		  $allRoutes.find('option' + sel).remove().appendTo($featuredRoutes);
		  this.sort($allRoutes);
		},
		remove: function(id) {
		  var sel = ':selected';
		  if (typeof id !== 'undefined') { sel = '[value="' + id + '"]'; }
		  $featuredRoutes.find('option' + sel).remove().appendTo($allRoutes);
		  this.sort($allRoutes);
		},
		up: function(id) {
		  var sel = ':selected';
		  if (typeof id !== 'undefined') { sel = '[value="' + id + '"]'; }
		  
		  $toMove = $featuredRoutes.find('option' + sel);
		  var $before = $toMove.prev();
		  if ($before.length > 0) {
			$toMove = $toMove.remove();
			$before.before($toMove);
		  }
		},
		down: function(id) {
		  var sel = ':selected';
		  if (typeof id !== 'undefined') { sel = '[value="' + id + '"]'; }
		  
		  $toMove = $featuredRoutes.find('option' + sel);
		  var $after = $toMove.next();
		  if ($after.length > 0) {
			$toMove = $toMove.remove();
			$after.after($toMove);
		  }
		},
		loaded: function(data) {
		  //console.log(data);
		  
		  $featuredRoutes.css({width: $allRoutes.width()});
		  
		  for (var i = 0; i < data.length; i++) {
			var rt = data[i];
			featured.routes.add(rt);
		  }
		  activateWorkArea('#FeaturedRoutesWA');
		},
		sort: function(select) {
		  var $select = $(select);
		  var options = $select.find('option');
		  
		  options.sort(function(a,b) {
			var at = a.text.toLowerCase();
			var bt = b.text.toLowerCase();
			if (at > bt) { return 1; }
			else if (at < bt) { return -1; }
			else { return 0; }
		  })
		  
		  $select.empty().append(options);
		}
	  }
	};
	
	$addRoute.click(function() { featured.routes.add(); });
	$removeRoute.click(function() { featured.routes.remove(); });
	$routeUp.click(function() { featured.routes.up(); });
	$routeDown.click(function() { featured.routes.down(); });
	$routeSave.click(function() {
	  var $rts = $featuredRoutes.find('option');
	  var routes = [];
	  for (var i = 0; i < $rts.length; i++) {
		var $r = $($rts.get(i));
		routes.push($r.val());
	  }
	  deactivateWorkArea('#FeaturedRoutesWA', function() {
		$.post('/misc/save', {section: 'featured-routes', payload: routes}, function(data, status, xhr) {
		  if (!data.success) {
			alert("ERROR: " + data.message);
		  }
		  activateWorkArea('#FeaturedRoutesWA');
		}, 'json');
	  });
	});
	
	
	deactivateWorkArea('#FeaturedRoutesWA', function() {
	  bfw.loadJSON('Misc', {
		after: featured.routes.loaded,
		name:'featured-routes',
		error: function() {
		  featured.routes.loaded([]);
		}
	  });
	});
	
	
  });
</script>
<div class="MiscWorkArea BlurAtStart" id="FeaturedRoutesWA">
  <div class="MiscOverlay"></div>
  <div class="MiscContent">
	<table id="RoutesSection" style="width: 650px;">
	  <tr>
		<td>
		  <?php
		  
		  echo $this->Form->input('all_routes', array('options' => $route_opts, 'size' => 10, 'id'=>'AllRoutes'));
		  
		  ?>
		</td><td>
		  <?php
		  
		  echo $this->Form->input('featured_routes', array('type'=>'select','size' => 10, 'id'=>'FeaturedRoutes'));
		  
		  ?>
		</td>
	  </tr><tr>
		<td>
		  <input type="button" id="AddRoute" value="Add" />
		</td><td>
		  <input type="button" id="RemoveRoute" value="Remove" /><br/>
		  <input type="button" id="RouteUp" value="Up" /> <input type="button" id="RouteDown" value="Down" />
		</td>
	  </tr><tr>
		<td colspan="2">
		  <input type="button" id="RouteSave" value="Save Featured Routes" />
		</td>
	  </tr>
	</table>
  </div><!-- .MiscContent -->
</div><!-- .MiscWorkArea -->








</div>