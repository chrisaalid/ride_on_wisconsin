<?php

$kml_url = 'http://' . $_SERVER['SERVER_NAME'] . '/routes/kml/' . $route['Route']['id'];

//$bounds = split(',', $route['Route']['bounds']);
$bounds = preg_split('/,/', $route['Route']['bounds']);

$center_lat = min($bounds[0], $bounds[2]) + (abs($bounds[0] - $bounds[2]) / 2);
$center_lng = min($bounds[1], $bounds[3]) + (abs($bounds[1] - $bounds[3]) / 2);

$difficulty = $this->FieldFormatter->difficulty_types();

?><div class="dialog-wrap lrg">
	<?php echo $this->element('dialog-header', array('title' => $route['Route']['name'], 'icon'=>'x')); ?>
	<div class="dl-body first">
		<div class="section">
			<div class="col six dashed-right">
				<div class="col-pad-right">
					<span class="route-distance"><?php echo number_format($route['Route']['distance'], 2); ?> mi</span>
					
					<?php echo $this->element('social-media-links'); ?>
					<hr class="dashed min">
					<table class="tbl-pad">
						<tr>
							<td class="title">Start City:</td>
							<td><?php echo h($route['Route']['city']) . ', ' . h($route['Route']['state']); ?></td>
						</tr>
						<tr>
							<td class="title">Ride Type:</td>
							<td><?php echo $route['Route']['offroad'] ? 'Off-Road' : 'Road'; ?></td>
						</tr>
						<tr>
							<td class="title">Difficulty:</td>
							<td><?php echo $difficulty[$route['Route']['difficulty']]; ?></td>
						</tr>
						<tr>
							<td class="title">Surface:</td>
							<td><?php echo join(', ', $route['Route']['attributes']['surfaces']); ?></td>
						</tr>
						<tr>
							<td class="title">Topography:</td>
							<td><?php echo join(', ', $route['Route']['attributes']['topography']); ?></td>
						</tr>
						<tr>
							<td class="title">Surroundings:</td>
							<td><?php echo join(', ', $route['Route']['attributes']['surroundings']); ?></td>
						</tr>
					</table>

					<?php echo $this->FieldFormatter->cleanContent($route['Route']['comment']); ?>
				</div><!-- /.col-pad-right -->
			</div><!-- /.col .six -->
			<div class="col six">
				<div class="col-pad-left ta-center">
					<?php echo $this->element('content-slide-show', array('record' => $route, 'placeholder_override' => $route['Route']['start_lat'] . ',' . $route['Route']['start_lng'])); ?>
				</div><!-- /.col-pad-left -->
			</div><!-- /.col .six -->
		</div><!-- /.section -->
		<hr class="min">
		<div class="section">
			<div class="col twelve ta-center">
				<iframe
				  width="780"
				  height="448"
				  frameborder="0"
				  scrolling="no"
				  marginheight="0"
				  marginwidth="0"
				  src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=<?php echo urlencode($kml_url); ?>&amp;t=m&amp;ie=UTF8&amp;output=embed"></iframe>
			</div><!-- /.col .twelve -->
		</div><!-- /.section -->
		<div class="section bot">
		  <div class="actions">
			<a href="/routes/kml/<?php echo $route['Route']['id']; ?>" target="_blank">Download KML</a> |
			<a href="/routes/gpx/<?php echo $route['Route']['id']; ?>" target="_blank">Download GPX</a>
		  </div><!-- .actions -->
			<div class="col twelve">
				
			</div>
		</div>
	</div><!-- /.dl-body -->
</div><!-- /.dialog-wrap-sm -->  
