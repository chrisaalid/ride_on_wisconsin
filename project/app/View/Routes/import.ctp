<?php echo $this->Form->create('Route', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Import GPX'); ?></legend>
	<?php
		echo $this->Form->input('gpx', array('type' => 'file', 'label' => 'GPX File'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>