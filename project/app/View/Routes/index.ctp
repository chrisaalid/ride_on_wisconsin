<?php

for ($i = 0; $i < count($routes); $i++) {
	$r = $routes[$i];
	$routes[$i]['Route']['photos'] = array_key_exists('Upload', $r) ? count($r['Upload']) : 0;
}

echo $this->element('display-index', array(
  'records' => $routes,
  'use_only' => array('User.username', 'name', 'featured', 'approved', 'distance', 'city', 'state', 'photos', 'created', 'modified'),
  'actions' => array(
	'KML' => array('action' => 'kml', 'field' => 'id'),
	'GPX' => array('action' => 'gpx', 'field' => 'id')
  ),
  'format' => array('distance' => array('precision', 2)),
  'labels' => array(
	'User.username' => 'Owner'	
  )
));

?>