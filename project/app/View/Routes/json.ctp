<?php

$to_send = isset($json_obj) ? $json_obj : new stdClass();

$resp = new stdClass();
$resp->status = is_object($to_send) && property_exists($to_send, '_loaded') && $to_send->_loaded === true ? true : false;
$resp->message = isset($message) ? $message : ($resp->status ? 'Success' : 'Failure');
$resp->payload = $to_send;

print json_encode($resp);

?>