<div class="routes index">
	<h2><?php echo __('Routes'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('distance'); ?></th>
			<th><?php echo $this->Paginator->sort('approved'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($routes as $route): ?>
	<tr>
		<td>
			<?php echo $this->Html->link($route['User']['username'], array('controller' => 'users', 'action' => 'view', $route['User']['id'])); ?>
		</td>
		<td>
		
			<?php echo $this->Html->link($route['Route']['name'], array('controller' => 'routes', 'action' => 'view', $route['Route']['id'])); ?>
		
		</td>
		<td><?php echo h(number_format($route['Route']['distance'], 2)); ?> mi.</td>
		<td><?php echo h($route['Route']['approved'] ? 'YES' : 'NO'); ?>&nbsp;</td>
		<td><?php echo h($route['Route']['created']); ?>&nbsp;</td>
		<td><?php echo h($route['Route']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $route['Route']['id'])); ?>
			<?php echo $this->Html->link(__('KML'), array('action' => 'kml', $route['Route']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $route['Route']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $route['Route']['id']), null, __('Are you sure you want to delete # %s?', $route['Route']['id'])); ?>
			<?php
			
			echo $this->Form->postLink(__($route['Route']['approved'] ? 'Disapprove' : 'Approve'), array('action' => $route['Route']['approved'] ? 'disapprove' : 'approve', $route['Route']['id']), null);
			
			?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
