<?php

$kml_url = 'http://' . $_SERVER['SERVER_NAME'] . '/routes/kml/' . $route['Route']['id'];

$bounds = preg_split('/,/', $route['Route']['bounds']);

$center_lat = min($bounds[0], $bounds[2]) + (abs($bounds[0] - $bounds[2]) / 2);
$center_lng = min($bounds[1], $bounds[3]) + (abs($bounds[1] - $bounds[3]) / 2);

?><div class="routes view">
<h2><?php  echo __('Route'); ?></h2>
	<dl>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($route['User']['username'], array('controller' => 'users', 'action' => 'view', $route['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($route['Route']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Surfaces'); ?></dt>
		<dd>
			<?php echo h(join(', ', $route['Route']['attributes']['surfaces'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Topography'); ?></dt>
		<dd>
			<?php echo h(join(', ', $route['Route']['attributes']['topography'])); ?>
			&nbsp;
		</dd>
		
		<dt><?php echo __('Surroundings'); ?></dt>
		<dd>
			<?php echo h(join(', ', $route['Route']['attributes']['surroundings'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Comment'); ?></dt>
		<dd>
			<?php echo $this->FieldFormatter->cleanContent($route['Route']['comment']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Approved'); ?></dt>
		<dd>
			<?php echo h($route['Route']['approved'] ? 'YES' : 'NO'); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($route['Route']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($route['Route']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
	
	<div class="edit-route">
		<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $route['Route']['id'])); ?>
	</div><!-- /.edit-route -->

	<div class="mapPreview">
		<iframe
		  width="425"
		  height="350"
		  frameborder="0"
		  scrolling="no"
		  marginheight="0"
		  marginwidth="0"
		  src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=<?php echo urlencode($kml_url); ?>&amp;t=m&amp;ie=UTF8&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=http:%2F%2Frideonwisconsin.snmsource.com%2Froutes%2Fkml%2F2&amp;sll=44.900771,-89.56949&amp;sspn=14.269147,33.815918&amp;t=h&amp;ie=UTF8&amp;z=7" class="btn-view-large">View Larger Map &raquo;</a></small>
	</div>
	<?php
	
	echo $this->element('content-slide-show', array('record' => $route));
	//if (array_key_exists('Upload', $route) && count($route['Upload']) > 0) {
	//  echo $this->element('image-list', array('images' => $route['Upload'], 'format' => '128x128'));
	//}
	
	//pr($route);
	
	?>
	
	
</div>
