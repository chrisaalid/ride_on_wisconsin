<div class="templates view">
<h2><?php  echo __('Template'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($template['Template']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($template['Template']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($template['Template']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tmpl'); ?></dt>
		<dd>
			<?php
			
			//echo h($template['Template']['tmpl']);
			
			echo $this->element('fields/code-editor', array('field_name' => 'tmpl', 'label' => 'Template Code', 'read_only' => true, 'code' => $template['Template']['tmpl']));
			
			?>
			&nbsp;
		</dd>
	</dl>
</div>
