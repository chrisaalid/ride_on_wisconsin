<div class="users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<?php echo $this->element('dialog-header', array('title'=>'Password&nbsp;Reset')); ?>
		<div class="dl-body first short">
		  	<p>Enter your email address to send yourself a password reset link.</p>
			<?php
				echo $this->Form->input('email',array("label"=>'Email Address'));
			?>
		</div><!-- /.dl-body -->			
	</fieldset>
	<div class="dl-body sub">
		<?php echo $this->Form->end(__('Submit')); ?>
		<div style="clear: both;"></div>
	</div><!-- /.dl-body -->
</div>
<script>
  <?php
  
  if ($logged_in) { // logged in
  ?>
  var readyFunc = function(DLG) {
	DLG.close();
  };
  <?php
  } else {
	// not logged in
  ?>
  var readyFunc = function(DLG) {
	var height = 300;
	DLG.setMaxDim(470, height);
	DLG.resizeDialog(470, height);
  };
  <?php
  } // logged in?
  ?>
  
</script>