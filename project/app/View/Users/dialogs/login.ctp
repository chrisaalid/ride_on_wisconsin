<div class="dialog-wrap">
<?php echo $this->Form->create('User'); ?>
		<?php echo $this->element('dialog-header', array('title'=>'User Log In', 'icon'=>'x')); ?>
		<div class="dl-body first short">
			<?php
				echo $this->Form->input('username',array("label"=>'User Name <span class="small">(or email address)</span>'));
				echo $this->Form->input('password');
			?>
		</div><!-- /.dl-body -->			
	<div class="dl-body sub">
	  <span class="smallNote">Forgotten password?  Click <?php echo $this->Html->link('here', array('action' => 'beginprr')); ?> to reset it.</span>
	</div>
	<div class="dl-body sub">
	  <?php
	  
	  echo $this->Form->end(__('Submit'));
	  
	  ?>
	  <div style="clear: both;"></div>
	</div><!-- /.dl-body -->
	<div class="dl-footnote">
		You must have an account to add rides/trails, points of interest, events, classifieds, and cycling groups.
		<p>Don’t have an account? Click <?php echo $this->Html->link('here', array('action'=>'add')); ?> to create one.</p>
	</div><!-- /.dl-footnote -->
</div>
<script>
  <?php
  
  if ($logged_in) { // logged in
  ?>
  var readyFunc = function(DLG) {
	DLG.close();
  };
  <?php
  } else {
	// not logged in
  ?>
  var readyFunc = function(DLG) {
	var height = 430;
	var width = 470;
	DLG.setMaxDim(width, height);
	DLG.resizeDialog(width, height);
  };
  <?php
  } // logged in?
  ?>
  
</script>