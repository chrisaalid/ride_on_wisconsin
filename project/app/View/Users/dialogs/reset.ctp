<div class="users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset>
	<?php echo $this->element('dialog-header', array('title'=>'Password&nbsp;Reset')); ?>
		<div class="dl-body first short">
		<?php
			echo $this->Form->input('id');
			echo $this->Form->input('pwd', array('type'=>'password','label'=>'Password'));
			echo $this->Form->input('pwd_repeat', array('type'=>'password','label'=>'Confirm Password'));
		?>
		</div><!-- /.dl-body -->	
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<script>
  <?php
  
  if ($logged_in) { // logged in
  ?>
  var readyFunc = function(DLG) {
	DLG.close();
  };
  <?php
  } else {
	// not logged in
  ?>
  var readyFunc = function(DLG) {
	var height = 300;
	DLG.setMaxDim(470, height);
	DLG.resizeDialog(470, height);
  };
  <?php
  } // logged in?
  ?>
  
</script>