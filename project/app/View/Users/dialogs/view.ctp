<?php

$u = $user['User'];

?><div class="dialog-wrap lrg">
	<?php echo $this->element('dialog-header', array('title'=>'My Account / Submissions', 'icon'=>'x')); ?>
	<div class="dl-body first">
		<div class="section">
			<div class="row clear">
				<div class="col five">
					<h2 class="orange no-margin"><?php echo $u['firstname'] . ' ' . $u['lastname']; ?></h2>
				</div>
				<div class="col seven right ta-right">
					<?php
					
					if ($is_admin) {
					  $url = $this->Html->url(array('controller' => 'users', 'action' => 'dialogs', 'no'));
					  echo $this->Html->link('Use Admin Interface', $url, array('class'=>'btn utility', 'id' => 'useAdmin'));
					  ?>
					  <script>
						$('#useAdmin').click(function(e) {
						  parent.location.href = '<?php echo $url; ?>';
						  
						  if (e.preventDefault) { e.preventDefault(); }
						  return false;
						});
					  </script>
					  <?php
					}
					
					?>
					
					<?php
					
					echo $this->Html->link('Edit Account', array('controller' => 'users', 'action' => 'edit', $logged_in_user['id']), array('class'=>'btn utility'));
					
					?>
					<a href="#" class="btn utility">Delete Account</a>
				</div>
			</div><!-- /.row -->
			<div class="row clear">
				<div class="col twelve">
					<table class="tbl-pad">
						<tr>
							<td class="title">USERNAME:</td>
							<td><?php echo $u['username']; ?></td>
						</tr>
						<tr>
							<td class="title">EMAIL:</td>
							<td><?php echo $u['email']; ?></td>
						</tr>
						<tr>
							<td class="title">LOCATION:</td>
							<td><?php echo $u['city'] . ', ' . $u['state']; ?></td>
						</tr>
					</table>
				</div>
			</div><!-- /.row -->
			<hr>
			<div class="row clear">
				<div class="col two">
					<span class="title">CREATE/ADD</span> 
				</div>
				<div class="col ten ta-right">
				  <?php echo $this->Html->link('Ride/Trail',        array('controller' => 'routes', 'action' => 'add'), array('class'=>'btn utility')); ?>
				  <?php echo $this->Html->link('Point of Interest', array('controller' => 'locations', 'action' => 'add'), array('class'=>'btn utility')); ?>
				  <?php echo $this->Html->link('Event',             array('controller' => 'events', 'action' => 'add'), array('class'=>'btn utility')); ?>
				  <?php echo $this->Html->link('Bike Classified',   array('controller' => 'classifieds', 'action' => 'add'), array('class'=>'btn utility')); ?>
				  <?php echo $this->Html->link('Cycling Group',     array('controller' => 'groups', 'action' => 'add'), array('class'=>'btn utility')); ?>
				</div>
			</div><!-- /.row -->
			<hr class="dashed">
		</div><!-- /.section -->
		<?php
		
		echo $this->element('content-list-new');
		
		?>
	</div><!-- /.dl-body -->
</div><!-- /.dialog-wrap-sm -->  