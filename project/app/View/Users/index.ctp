<?php

echo $this->element('display-index', array(
  'records' => $users,
  'use_only' => array('username', 'approved', 'admin', 'firstname', 'lastname', 'email', 'created', 'modified'),
  'labels' => array('username' => 'User'),
  'actions' => array(
	'Delete' => false
  ),
  'no_approve' => true,
));

?>