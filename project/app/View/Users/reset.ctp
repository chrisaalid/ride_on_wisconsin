<div class="users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend><?php echo __('Reset Password'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('pwd', array('type'=>'password','label'=>'Password'));
		echo $this->Form->input('pwd_repeat', array('type'=>'password','label'=>'Confirm Password'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>