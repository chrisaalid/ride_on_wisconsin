<div class="users view">
<h2><?php  echo __('User'); ?></h2>
	<?php
	
	echo $this->element('display-record', array(
	  'record' => $user['User'],
	  'use_only' => array('username', 'realname', 'email', 'city', 'state', 'approved'),
	  'labels' => array('username' => 'Name')
	));
	?>
	
	
	
	<div class="actions wide">
	<?php
	echo $this->Html->link('Edit', array('action' => 'edit', $user['User']['id']));
	echo $this->Html->link('Reset Password', array('action' => 'prr', $user['User']['email']));
	if (!$user['User']['approved']) {
	  echo $this->Html->link('Resend Activation', array('action' => 'resendActivation', $user['User']['email']));
	  echo $this->Html->link('Activate', array('action' => 'approve', $user['User']['id']));
	} else {
	  echo $this->Html->link('Deactivate', array('action' => 'disapprove', $user['User']['id']));
	}
	?>
	</div><!-- /.actions -->
	
	<div class="allRelatedContent">
	
	<?php
	
	echo $this->element('display-related', array(
	  'record' => $user,
	  'related_model' => 'Route',
	  'use_only' => array('name', 'distance', 'approved', 'created', 'modified'),
	  'format' => array('distance' => array('precision', 2))
	));
	
	echo $this->element('display-related', array(
	  'record' => $user,
	  'related_model' => 'Location',
	  'use_only' => array('name', 'approved', 'created', 'modified')
	));
	
	echo $this->element('display-related', array(
	  'record' => $user,
	  'related_model' => 'Event',
	  'use_only' => array('name', 'start', 'end', 'approved', 'created', 'modified'),
	  'format' => array(
		'start' => array('date', array('format' => 'shortDateTime')),
		'end' => array('date', array('format' => 'shortDateTime')),
	  )
	));
	
	echo $this->element('display-related', array(
	  'record' => $user,
	  'related_model' => 'Classified',
	  'use_only' => array('type', 'title', 'approved', 'created', 'modified')
	));
	
	
	echo $this->element('display-related', array(
	  'record' => $user,
	  'related_model' => 'Group',
	  'use_only' => array('name', 'approved', 'created', 'modified')
	));
	
	
	
	
	
	?>
	</div><!-- /.allRelatedContent -->
	
</div><!-- /.users.view -->
