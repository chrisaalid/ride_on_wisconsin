<h2>Table: <?php echo $table; ?></h2>

<?php

if (count($issues) > 0) {

?>
<h3>Database Issues</h3>
<ul>
<?php

$resolution = array();

foreach ($issues as $idx => $issue) {
  echo $this->element('schema-issue', array('issue' => $issue));
  $resolution[] = $issue->resolution;
}


?>
</ul>
<br>&nbsp;<br>
<h3>Database Resolution:</h3>

<div style="white-space: pre; font-family: Consolas, Courier New, monospace, sans-serif;">
<?php

print join("\n", $resolution);

?>
</div>
<div class="actions">
<?php

echo $this->Html->link('Fix Database', array('action'=>'schema','update'), null, __('Are you sure you want to execute these queries?'));
?></div><?php

} else {
  print "Schema appears to be up to date!";
}
?>