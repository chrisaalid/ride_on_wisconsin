<?php

require_once('../Lib/Frontend.php');
require_once(APP . DS . 'Lib' . DS . 'MobileDetect.php');

$DETECTION = new MobileDetect();

$is_tablet = $DETECTION->isTablet();
$is_mobile = $DETECTION->isMobile();

$body_classes = array();

if ($is_mobile) {
  $body_classes[] = 'mobile';
  $body_classes[] = $is_tablet ? 'table' : 'phone';
}

?><html>
  <head>
	<title>Ride On Wisconsin Mobile</title>
  </head>
  <body class="<?php echo join(' ', $body_classes); ?>">
	<h1>Hey Buddy!</h1>
	
	<a href="/non-mobile" class="btn" id="useFullSite">Use Full Site</a>
	
	<div id="routeList">
	  <table>
		
	  </table>
	</div>
	
	<script src="/js/json2.js"></script>
	<script src="/js/plugins.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyApdxs0KEfjJGCqPxbN4DAcyZn2vkZxaIE&amp;sensor=false"></script>
	<script src="/js/mustache.js"></script>
	<script src="/js/mobile.js"></script>
	<script src="/js/json-loader.js"></script>
	<script src="/js/routes.js"></script>
	<script src="/js/search.js"></script>
	<script>
	  $(function() {
		  bfw.mobile.init();	
	  });
	</script>
  </body>
</html>