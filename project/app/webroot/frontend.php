<?php

require_once('../Lib/Frontend.php');

?>
<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="fragment" content="!">
        <title>Ride On Wisconsin - find bike friendly routes, events, and tips in Wisconsin - Brought to you by the Wisconsin Bike Fed</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/favicon.ico?v=2" />
        <link href='http://fonts.googleapis.com/css?family=Alfa+Slab+One|Jockey+One|Crete+Round|Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/css/normalize.min.css">
        <link rel="stylesheet" href="/css/main.css">
        <link rel="stylesheet" href="/css/bfw.css" />
        <link rel="stylesheet" href="/css/dialog.css" />
        <link rel="stylesheet" href="/css/row-dialog.css" />
        <link rel="stylesheet" href="/css/jquery-ui-timepicker-addon.css" />
        <link rel="stylesheet" href="/css/jquery.tagit.css" />
        <link rel="stylesheet" href="/css/smoothness/jquery-ui-1.10.0.custom.min.css" />

        <script src="/js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <!--[if lt IE 7]>      <body class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
    <!--[if IE 7]>         <body class="lt-ie9 lt-ie8"> <![endif]-->
    <!--[if IE 8]>         <body class="lt-ie9"> <![endif]-->
    <!--[if gt IE 8]><!--> <body> <!--<![endif]-->
        <div class="top-menu" id="top-menu">
            <ul class="nav-list">
                <li><a href="#" class="btn-find-ride">Find a Ride</a></li>
                <li><a href="#" class="btn-events">Events</a></li>
                <li><a href="#" class="btn-riding-tips">Riding Tips</a></li>
                <li><a href="#" class="btn-get-involved">Get Involved</a></li>
                <li><a href="#" class="btn-get-connected">Get Connected</a></li>
                <li class="logo"><a href="#" class="last btn-home"><img src="/images/logo-top-menu.png" alt="Ride On Wisconsin"> <span aria-hidden="true" class="icon-bikefed-logo"></span></a></li>
            </ul>
        </div><!-- /.top-menu -->
        <div class="container sec01">
            <div class="content-layer">
                <div class="content">
                    <div class="ride-on">
                        <img src="/images/ride-on.png" alt="Ride On Wisconsin"><br>
                        <span class="sub-head">Explore the Amazingly Bikeable Badger State.</span>
                    </div><!-- /.ride-on -->
                    <div class="nav-container">
                        <div class="search-wrapper">
                            <input type="text" class="city-search" placeholder="Enter city or zip code" id="rtAddressTop">
							<input type="submit" name="submit" value="GO" class="btn-go" id="rtSearchTop">
                        </div><!-- /.search-wrapper -->
                        <div class="nav-wrapper">
                            <table class="logo-tag">
                                <tr>
                                    <td>Brought to you by the
                                Wisconsin Bike Fed</td>
                                    <td><span aria-hidden="true" class="icon-bikefed-logo-rev"></span>  </td>
                                </tr>
                            </table>    
                            <ul class="nav-list">
                                <li><a href="#" class="first btn-events">Events</a></li>
                                <li><a href="#" class="btn-riding-tips">Riding Tips</a></li>
                                <li><a href="#" class="btn-get-involved">Get Involved</a></li>
                                <li><a href="#" class="last btn-get-connected">Get Connected</a></li>
                            </ul>
                        </div><!-- /.nav-wrapper -->
                    </div><!-- /.nav-container -->
                </div><!-- /.content -->
            </div><!-- /.content-layer -->
            <div class="anim-layer">
                <div class="content">
					<div id="pathPaper" style="position: absolute; z-index: 13; width: 1085px; height: 7700px; top: 0; left: 0; margin-left: -135px; margin-top: -45px;"></div>
					<div id="treePaper" style="position: absolute; z-index: 13; width: 1200px; height: 7700px; top: 0; left: 0; margin-left: -135px; margin-top: -45px;"></div>
                    <div class="scroll-tip" id="scroll-tip">
                        <img src="/images/scroll-down.png" alt="Scroll Down">
                    </div>
                    <div class="intro-aqua-rider" id="intro-aqua-rider">
                        <img src="/images/anim-male-aqua.png" alt="">
                    </div>
                    <div class="orange-rider" id="intro-orange-rider">
                        <img src="/images/anim-male-orange.gif" alt="">
                    </div>
                    <div class="green-rider" id="intro-green-rider">
                        <img src="/images/anim-male-green-mtn.gif" alt="">
                    </div>
                    <div class="sec01-trees" id="sec01-trees">
                        <img src="/images/sec01-trees.png" alt="">
                    </div>
                    <div class="map" id="intro-map">
                        <img src="/images/wi-state-map.jpg" alt="">
                    </div><!-- /.map -->
                </div><!-- /.content -->
            </div><!-- /.anim-layer -->
        </div><!-- /.container sec01 -->
		
        <div class="sec01-zigzag zigzag"></div>
		
        <div class="container sec02" data-offset="-50">
            <div class="content-layer">
                <div class="content">
					<a name="sitemap-Route"></a>
					<a name="sitemap-Location"></a>
                    <h1>Find <br>a ride</h1>
                    <div class="col1">
                        <input type="text" id="rtAddress" value="Madison, WI"><br>
                        <select name="find-ride" tabindex="1" class="default" id="rtDistance">
                            <option value="10">within 10 miles</option>
                            <option value="20">within 20 miles</option>
                            <option value="30">within 30 miles</option>
							<option value="50">within 50 miles</option>
							<option value="100" selected="selected">within 100 miles</option>
							<!--<option value="5000">within 5000 miles</option>-->
                        </select>
                    </div><!-- /.col1 -->
                    <div class="col2">
                        <table class="tbl-option-select rtCBs">
                            <tr>
                                <td class="centered"><span aria-hidden="true" class="icon-path"></span></td>
                                <td><label for="srchRoadRides"><input type="checkbox" name="vehicle" value="Bike" data-type="route" data-sub-type="road" checked="checked" id="srchRoadRides" /> Road Rides</label></td>
								
                                <td class="centered"><span aria-hidden="true" class="icon-bike-shops"></span></td>
                                <td><label for="srchBikeShops"><input type="checkbox" name="vehicle" value="Bike" data-type="location" data-sub-type="bike-shop" id="srchBikeShops" /> Bike Shops</label></td>
                            </tr>
                            <tr>
                                <td class="centered"><span aria-hidden="true" class="icon-mt-bike-rides"></span></td>
                                <td><label for="srchOffRoadRides"><input type="checkbox" name="vehicle" value="Bike" data-type="route" data-sub-type="off-road" checked="checked" id="srchOffRoadRides" /> Off-road Rides</label></td>
								
                                <td class="centered"><span aria-hidden="true" class="icon-lodging"></span></td>
                                <td><label for="srchLodging"><input type="checkbox" name="vehicle" value="Bike" data-type="location" data-sub-type="lodging" id="srchLodging" /> Lodging</label></td>
                            </tr>
                            <tr>
                                <td class="centered"><span aria-hidden="true" class="icon-gmaps-bike-latyer"></span></td>
                                <td><label for="cbToggleBikeLayer"><input type="checkbox" name="vehicle" value="Bike" id="cbToggleBikeLayer" /> Google Maps Bike Layer</label></td>
								
                                <td class="centered"><span aria-hidden="true" class="icon-restaurants"></span></td>
                                <td><label for="srchRestaurant"><input type="checkbox" name="vehicle" value="Bike" data-type="location" data-sub-type="restaurant" id="srchRestaurant" /> Restaurants</label></td>
                            </tr>
                        </table>
                    </div><!-- /.col2 -->
                    <div class="col3">
                        <input type="submit" value="Search" class="btn search" id="rtSearch" />
                    </div><!-- /.col3 -->
                    <div style="clear:both;"></div>
                    <div class="col-search-results">
                        <div class="search-results-loc">
                            <div>Search Results</div>
                        </div>
						<div style="height: 563px; overflow-y: scroll;">
						  <table class="tbl-search-results striped" id="routeList" >
							<?php
								
								outputContentBlock('rendered-search', 'routes');
								
							?>
						  </table><!-- .tbl-search-results -->
						</div>
                        <a href="#" class="btn share-ride row-dialog-trigger" data-dialog-action="/routes/add"><span aria-hidden="true" class="icon-add-ride-trail"></span> Share a Ride/Trail</a><a href="#" class="btn poi row-dialog-trigger" data-dialog-action="/locations/add"><!-- <span aria-hidden="true" class="icon-add-ride-trail"></span> --> Add Point of Interest</a>
                    </div><!-- /.search-results -->
                    <div class="col-map-view">
                        <div id="rtMap" style="width: 100%; height: 100%;"></div>
                    </div><!-- /.col-map-view -->
                    <div style="clear:both;"></div>
					
					<?php
					
					outputContentBlock('slideshow', 'routes');
					
					?>
					
                    <div class="ad-featured">
                        <?php outputContentBlock('billboard-water-tower', 'billboards', '<img src="/images/ad-temp-01.gif" alt="Banner Ad" class="banner-ad">'); ?>
                        <img src="/images/billboard-featured-rides.png" alt="banner" class="banner-bg">
                    </div><!-- /.ad-featured -->
                </div><!-- /.content -->
            </div><!-- /.content-layer -->
            <div class="anim-layer">
                <div class="content">
                    <img class="static-cyclist cyclist01" src="/images/bicycles/static-failover/Cyclist-Mtn-Male-WhiteShirt.png" alt="cyclist">
                    <img class="static-cyclist cyclist02" src="/images/bicycles/static-failover/Cyclist-Road-Female-GreenShirt.png" alt="cyclist">
                    <img class="static-cyclist cyclist03" src="/images/bicycles/static-failover/Cyclist-Child-Female-WhiteShirt.png" alt="cyclist">
                    <div class="sec02-trees" id="sec02-trees">
                        <img src="/images/sec02-trees.png" alt="">
                    </div><!-- /.sec02-trees -->
                    <div class="sec02-bike-cross" id="sec02-bike-cross">
                        <img src="/images/bike-crossing.png" alt="Bike Crossing" class="bg-slide">
                    </div>
                     <img src="/images/logo-post-bikefed-r.png" alt="Brought to you by the Wisconsin BikeFed" class="sec02-logo">
                    <img src="/images/water-tower.png" alt="Watet Tower" class="sec02-tower">
                </div><!-- /.content -->
            </div><!-- /.anim-layer -->
            <div class="sec02-skyline">
                <div class="bg-skyline"> </div>
            </div><!-- /.sec02-skyline -->
        </div><!-- /.container .sec02 -->
        
		<div class="sec02-zigzag zigzag"></div>
		
		<div class="container sec03" data-offset="-50">
			<div class="content-layer">
			  <a name="sitemap-Event"></a>
				<h1>Event<br>Calendar</h1>
				<?php
				outputContentBlock('featured', 'events');
				?>
				<div class="events">
					<div class="event-search">
                        <table>
                            <tr>
                                <td><input type="text" placeholder="Search Events" class="input-search-events" id="eventKeyword"></td>
                                <td>Between <input type="text" placeholder="MM/DD/YYYY" class="event-date" id="eventStartDate" value="<?php echo date('n/j/Y'); ?>"></td>
                                <td>&amp; <input type="text" placeholder="MM/DD/YYYY" class="event-date" id="eventEndDate"></td>
                                <td rowspan="2" style="vertical-align:top;"><input type="submit" value="Search" class="btn search-events" id="eventSubmit"></td>
                            </tr>
                            <tr>
                                <td>
                                    <select name="categories" tabindex="2" class="sel-brown cat" id="eventCategories">
                                        <option value="">All Categories</option>
                                    </select>
                                </td>
                                <td>
                                    <select name="regions" tabindex="3" class="sel-brown reg" id="eventRegions">
                                        <option value="" selected="selected">All Regions</option>
                                    </select>
                                </td>
                                <td>
                                    <label><input type="checkbox" id="eventFree"> Free Events Only</label>
                                </td>
                            </tr>
                        </table>
					</div><!-- /.event-search -->
                    <div class="search-results-content">    
    					<table class="tbl-search-results striped" id="eventList">
    					  <!-- results go here -->
						  <?php
						  
						  outputContentBlock('rendered-search', 'events');
						  
						  ?>
    					</table>
                    </div><!-- /.serch-results-content -->
                    <a href="#" class="btn share-event row-dialog-trigger" data-dialog-action="/events/add"><span aria-hidden="true" class="icon-add-event"></span> Share an Event</a>               
				</div><!-- /.events-->
                <div class="event-btns">
                    <a href="http://wisconsinbikefed.org/events/bike-summit/" target="_blank" class="btn-bike-summit"></a>
                    <a href="http://wisconsinbikefed.org/events/publications/" target="_blank" class="btn-ride-guide"></a>
                </div><!-- /.event-btns -->            
                <div class="ad-featured sec03-ad">
                    <?php outputContentBlock('billboard-bike-shop', 'billboards', '<img src="/images/ad-temp-02.gif" alt="Banner Ad" class="banner-ad">'); ?>
                    <img src="/images/billboard-featured-rides.png" alt="banner" class="banner-bg">
                </div><!-- /.ad-featured -->
			</div><!-- /.content-layer -->
			
			<div class="anim-layer">
                <div class="content">
                    <img class="static-cyclist cyclist04" src="/images/bicycles/static-failover/Cyclist-Recumbent-Male-WhiteShirt.png" alt="cyclist">
                    <img src="/images/logo-post-bikefed-l-01.png" alt="Brought to you by the Wisconsin BikeFed" class="sec03-logo">
                    <img src="/images/sec03-apartments.png" class="sec03-apt" alt="Apartments">
                    <img src="/images/shadow-featured-event.png" class="sec03-thumb-shadow" alt="Shadow">
                    <img src="/images/sec03-bike-shop.png" class="sec03-bike-shop" alt="Bike Shop">
                </div><!-- ./content -->         
			</div><!-- /.anim-layer -->
            <div class="sec03-skyline">
                <div class="bg-skyline"> </div>
            </div><!-- /.sec03-skyline -->
		</div><!-- /.container sec03 -->
		
		<div class="sec03-zigzag zigzag"></div>
		
		<div class="container sec04" data-offset="-50">
			<div class="content-layer">
				<h1>Riding<br>Tips</h1>
				
				<div class="tips-wrapper-wrapper">
					<div class="tips-wrapper"><div class="tips">
						<div class="body">
							<?php
							  
							  $content_blocks = array('health','maintenance','safety','family','group','etiquette');
							  
							  foreach ($content_blocks as $idx => $block) {
									?><div class="tipcontent-<?php echo $block . ($idx == 0 ? ' active' : ''); ?>"><?php
									outputContentBlock('riding-tips.' . $block);
									?></div><?php
							  }
							  
							  
							?>
						</div>
					</div></div>
					<div class="types">
						<div class="active-bg"></div>
						<a data-type="health" class="active">Health Tips</a>
						<a data-type="maintenance">Basic Maintenance</a>
						<a data-type="safety">Riding Safety</a>
						<a data-type="family">Family Bicycling</a>
						<a data-type="group">Group Rides</a>
						<a data-type="etiquette">Trail/Path Etiquette</a>
						<div style="clear:both;"></div>
					</div>
				</div>
                <div style="clear:both;"></div>            
                <div class="ad-featured sec04-ad">
									<?php outputContentBlock('billboard-rv', 'billboards', '<img src="/images/ad-temp-03.gif" alt="Banner Ad" class="banner-ad">'); ?>
                    <img src="/images/billboard-featured-rides.png" alt="banner" class="banner-bg">
                </div><!-- /.ad-featured -->            
			</div><!-- /.content-layer -->
			
			<div class="anim-layer">
	           <div class="content">
                    <img class="static-cyclist cyclist05" src="/images/bicycles/static-failover/Cyclist-Dutch-Female-RedShirt.png" alt="cyclist">
                    <img class="static-cyclist cyclist06" src="/images/bicycles/static-failover/Cyclist-Mtn-Male-GreenShirt.png" alt="cyclist">
                    <img src="/images/logo-post-bikefed-l-02.png" alt="Brought to you by the Wisconsin BikeFed" class="sec04-logo">
                    <img src="/images/sec04-lake.gif" class="sec04-lake" alt="Lake Monona">
                    <img src="/images/sec04-rv.png" class="sec04-rv" alt="Recreational Vehicle">
                    <img src="/images/sec04-eagle.gif" class="sec04-eagle" alt="Eagle">
               </div><!-- /.content -->		
            </div><!-- /.anim-layer -->
		</div><!-- /.container sec04 -->
		
		<div class="sec04-zigzag zigzag"></div>
		
		<div class="container sec05" data-offset="-50">
			<div class="content-layer">
                <h2>Get Involved</h2>   
                <div class="get-involved">
                    <table>
                        <tr>
                            <td>
                                <a href="/contents/dialog/volunteer" class="row-dialog-trigger" data-dialog-action="/contents/dialog/volunteer"><img src="/images/btn-volunteer.png" alt="Volunteer"></a>
                            </td>
                            <td class="mid">
                                <a href="/contents/dialog/get-involved" class="row-dialog-trigger" data-dialog-action="/contents/dialog/get-involved"><img src="/images/btn-donate.png" alt="Donate"></a>
                            </td>
                            <td>
                                <!--<a href="#" class="row-dialog-trigger" data-dialog-action="/contents/dialog/merchandise"><img src="/images/btn-merch.png" alt="Buy Merchandise"></a>-->
								<a href="/merchandises/all" class="row-dialog-trigger" data-dialog-action="/merchandises/all"><img src="/images/btn-merch.png" alt="Buy Merchandise"></a>
                            </td>
                        </tr>
                    </table>
                </div><!-- /.get-involved -->        
                <div class="get-connected" data-offset="-120">
                    <h2>Get Connected</h2>
                    <table>
                        <tr>
                            <td>
                                <!--<img src="/images/coming-soon-banner.png" alt="Coming Soon" class="coming-soon">-->
                                <a href="#!/classifieds/search" class="row-dialog-trigger" data-dialog-action="/classifieds/search"><img src="/images/btn-classifieds.png" alt="Bike Swap Classifieds"></a>
                            </td>
                            <td>
                                <!--<img src="/images/coming-soon-banner.png" alt="Coming Soon" class="coming-soon">--->
                                <a href="#!/groups/search" class="row-dialog-trigger" data-dialog-action="/groups/search"><img src="/images/btn-cycle-groups.png" alt="Cycle Groups"></a>
                            </td>
                            <!-- <td>
                                <img src="/images/btn-local-chap.svgz" alt="Local Chapters">
                            </td> -->
                        </tr>
                    </table>
                </div><!-- /.get-connected -->   
                <div class="ad-featured">
										<?php outputContentBlock('billboard-cow', 'billboards', '<img src="/images/ad-temp-04.gif" alt="Banner Ad" class="banner-ad">'); ?>
                    <img src="/images/billboard-featured-rides.png" alt="banner" class="banner-bg">
                </div><!-- /.ad-featured -->  
			</div><!-- /.content-layer -->
			
			<div class="anim-layer">
                <div class="content">
                    <img class="static-cyclist cyclist07" src="/images/bicycles/static-failover/Cyclist-Road-Male-OrangeShirt.png" alt="cyclist">
                    <img class="static-cyclist cyclist08" src="/images/bicycles/static-failover/Cyclist-Child-Male-GrayShirt.png" alt="cyclist">
                    <img src="/images/logo-post-bikefed-l-03.png" alt="Brought to you by the Wisconsin BikeFed" class="sec05-logo">
                    <img src="/images/sec05-barn.png" class="sec05-barn" alt="Barn">
                    <img src="/images/sec05-windmill.gif" class="sec05-windmill" alt="Windmill">
                    <img src="/images/sec05-cow.gif" class="sec05-cow" alt="Cow!">
                </div><!-- /.content -->
			</div><!-- /.anim-layer -->
            <div class="sec05-skyline">
                <div class="bg-green"></div><!-- /.bg-green //grass that covers the hill and extends down --> 
                <div class="hill"></div>
                <div class="bg-skyline"></div>
            </div><!-- /.sec05-skyline -->         
		</div>
		
		<div class="sec05-zigzag zigzag"></div>
		
		<div class="container sec06">
            <div class="content-layer">
                <div class="left-col">
                    <img src="/images/logo-footer.png" alt="Wisconsin Bike Federation" class="footer-logo">
                </div>
                <div class="right-col">
                    RideOnWisconsin.org is a project brought to you by the Wisconsin Bike Fed.
                We’re an organization committed to making Wisconsin a better, more bike-friendly state. <br>
                <a href="http://www.wisconsinbikefed.com" target="_blank" class="more-info">Find out more or join up at WisconsinBikeFed.com</a>
                </div>
            </div><!-- /.content-layer -->
            <div id="contact-us">
                <div class="contact">
                    <div class="contact-headline">
                        <img src="images/logo-dark.png" alt="">
                        <h3>contact the wisconsin bike fed</h3>
                        <div id="contact-exit">X</div>
                    </div>
                    <div class="contact-content">
                        <div class="column">
                            <img src="" class="contact-location-map" id="map-madison" alt="">
                            <div class="details">
                                <h4>Madison Office</h4>
                                <p>409 E. Main St, Suite 203 <br> Madison, WI 53703</p>
                                <ul>
                                    <li><span aria-hidden="true" class="icon-email"></span> Info@WisconsinBikeFed.org</li>
                                    <li><span aria-hidden="true" class="icon-phone"></span> 608-251-4456</li>
                                </ul>
                            </div>
                        </div>
                        <div class="column">
                            <img src="" class="contact-location-map" id="map-milwaukee" alt="">
                            <div class="details">
                                <h4>Milwaukee Office</h4>
                                    <p>3618 W. Pierce St.<br> Milwaukee, WI 53215</p>
                                <ul>
                                    <li><span aria-hidden="true" class="icon-email"></span> Info@WisconsinBikeFed.org</li>
                                    <li><span aria-hidden="true" class="icon-phone"></span> 414-431-1798</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.contact-slider -->
            <div class="content-layer-bot">
                <div class="left-col">&copy; 2013 Wisconsin Bike Fed</div>
                <div class="mid-col">
                    <ul>
                        <li><a href="http://wisconsinbikefed.org/terms/" target="_blank">Terms</a></li>
                        <li><a href="#!/misc/sitemap" class="row-dialog-trigger" data-dialog-action="/misc/sitemap">Site Map</a></li>
                        <li><a href="http://wisconsinbikefed.org/privacy/" target="_blank">Privacy</a></li>
                        <li><a href="http://wisconsinbikefed.org/blog/" target="_blank">Blog/News</a></li>
                        <li><a href="http://wisconsinbikefed.org/feedback/#RideOnWisconsin.com" target="_blank">Feedback</a></li>
                        <li><a href="http://wisconsinbikefed.org" target="_blank">Organization Site</a></li>
                    </ul>
                </div>
                <div class="right-col">
                    <div class="btn contact-us">Contact</div>
					<a href="#!/contents/dialog/get-invovled" class="btn support row-dialog-trigger" data-dialog-action="/contents/dialog/get-involved" target="_blank">Support/Donate</a>
                    <div class="social-links">
                        <a href="#" class="social-icon fb"><img src="/images/btn-fb.png" alt="Find us on Facebook"></a> <a href="#" class="social-icon tw"><img src="/images/btn-tw.png" alt="Follow us on Twitter"></a> <a href="#" class="social-icon yt"><img src="/images/btn-yt.png" alt="See us at YouTube"></a>
                    </div>
                </div>
            </div><!-- /.content-layer-bot -->
            
            <div class="anim-layer">
                <div class="content">
                    <img src="/images/tent.png" alt="tent" class="tent">
                    <img src="/images/fire.png" alt="fire" class="fire">
                </div>
            </div>
        </div><!-- /.container .sec06 -->
		
		<div class="footer">
		  <div class="centerWrapper" style="display: inline-block;">
			<ul class="footerNav loggedOut">
				  <li class="icon-lock"><a href="#!/users/login" class="footer-btn row-dialog-trigger" data-dialog-action="/users/login">Log In</a></li>
				  <li class="icon-plus"><a href="#!/users/add" class="footer-btn row-dialog-trigger" data-dialog-action="/users/add">Create Account</a></li>
				  <li class="icon-calendar"><a href="#!/events/add" class="footer-btn row-dialog-trigger" data-dialog-action="/events/add">Add Event</a></li>
				  <li class="icon-path"><a href="#!/routes/add" class="footer-btn row-dialog-trigger" data-dialog-action="/routes/add">Add Route</a></li>
				  <li class="icon-heart"><a href="http://wisconsinbikefed.org/join/supportdonate/" class="footer-btn" target="_blank">Support/Donate</a></li>
				  <li class="icon-bikefed-logo-rev"><a href="http://wisconsinbikefed.org/" class="footer-btn" target="_blank">Visit Bike Fed Site</a></li>
			</ul>
			
			<ul class="footerNav loggedIn">
				  <li class="icon-trail-sign welcome" data-message="{{username}}"></li>
				  <li class="icon-settings myaccount"><a href="#!/users/view" class="footer-btn row-dialog-trigger" data-dialog-action="/users/view/x">My Account</a></li>
				  <li class="icon-calendar"><a href="#!/events/add" class="footer-btn row-dialog-trigger" data-dialog-action="/events/add">Add Event</a></li>
				  <li class="icon-path"><a href="#!/routes/add" class="footer-btn row-dialog-trigger" data-dialog-action="/routes/add">Add Route</a></li>
				  <li class="icon-heart support"><a href="#!/contents/dialog/get-involved" class="footer-btn row-dialog-trigger" data-dialog-action="/contents/dialog/get-involved" target="_blank">Support/Donate</a></li>
				  <li class="icon-bikefed-logo-rev visit"><a href="http://wisconsinbikefed.org/" class="footer-btn" target="_blank">Visit Bike Fed Site</a></li>
				  <li class="icon-power logout"><a href="/users/logout" class="footer-btn">Log Out</a></li>
			</ul>
		  </div><!-- .centerWrapper -->
		</div><!-- /.footer -->
		
	
		<script src="/js/json2.js"></script>
        <script src="/js/plugins.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyApdxs0KEfjJGCqPxbN4DAcyZn2vkZxaIE&amp;sensor=false"></script>
		<script src="/js/jquery-ui-1.10.0.custom.min.js"></script>
		<script src="/js/jquery-ui-timepicker-addon.js"></script>
        <script src="/js/plugins/jquery.scrollTo.min.js"></script>
		<script src="/js/tag-it.min.js"></script>
        
		<!--[if lte IE 7]><script src="js/lte-ie7.js"></script><![endif]-->
        <script src="/js/TweenMax.min.js"></script>
        <script src="/js/plugins/ScrollToPlugin.min.js"></script>
        <script src="/js/raphael-min.js"></script>
        
		<script src="/js/mustache.js"></script>
		<script src="/js/bfw.js"></script>
		<script src="/js/json-loader.js"></script>
		<script src="/js/events.js"></script>
		<script src="/js/locations.js"></script>
		<script src="/js/routes.js"></script>
		
		
		<script src="/js/search.js"></script>
		
		<script src="/js/row-dialog-outer.js"></script>
		
		<script src="/js/home.js" defer="defer"></script>
		
		
		<script src="/js/ui.js"></script>
		<script src="/js/trees.js"></script>
  	
		<script src="/js/path.js"></script>
        <script src="/js/bicycleAnimations.js"></script>
        
        <script src="/js/jquery.stellar.js"></script>
        <script src="/js/parallax.js"></script>
	
		<script>
		  $(function() {
			bfw.init();
			
			var pDLG = parent.DLG || false;
			
			if (parent != window && pDLG !== false) {
			  pDLG.close();
			}
			
			bfw.locations.offices.getOfficeMaps();
			
			
		  });		  
		</script>
		
	
		<?php outputContentBlock('google-analytics', 'settings'); ?>

    <!-- dcsiv -->    
    </body>
</html>
