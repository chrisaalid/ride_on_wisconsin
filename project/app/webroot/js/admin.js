$(function() {
	$searchToggleLink = $('#searchToggle');
	$searchBox = $('#searchBox');
	$searchToggleLink.click(function(e) {
		
		if ($searchBox.is(':visible')) {
			$searchBox.slideUp();
			$searchToggleLink.html('+ Search');
		} else {
			$searchBox.slideDown();
			$searchToggleLink.html('- Search');
		}
	
		e.preventDefault();
	});
});