var API = {};

$(function() {
	
	API = {
		call: function(action, toSend, callback) {
			if (typeof toSend['_method'] === 'undefined') { toSend['_method'] = 'POST'; }
			
			callback = typeof callback === 'function' ? callback : function() { /* ... */ }
			
			$.ajax('/ajax/' + action, {
				data: toSend,
				dataType: 'json',
				type: 'post',
				success: function(d, x, s) {
					callback({
						success: true,
						data: d,
						xhr: x,
						status: s
					});
				},
				error: function(x, s, e) {
					callback({
						success: false,
						xhr: x,
						status: s,
						error: e
					});
				}
			});
		},
		
		login: function(username, password, callback) {
			var toSend = {
				'_method': 'POST',
				'data[User][username]': username,
				'data[User][password]': password
			};
			
			API.call('login', toSend, callback);
		},
		
		logout: function(callback) {
			var toSend = {'_method': 'POST'};
			
			API.call('logout', toSend, callback);
		},
		
		status: function(callback) {
			var toSend = {'_method': 'POST'};
			API.call('status', toSend, callback);
		}
	};
	
});
