var bfw = bfw || {};

console = typeof console !== 'undefined' ? console : {log: function(o) { /* ... */ }};

if(!Array.prototype.indexOf) {
    Array.prototype.indexOf = function(needle) {
        for(var i = 0; i < this.length; i++) {
            if(this[i] === needle) {
                return i;
            }
        }
        return -1;
    };
}

$(function() {


	bfw.geolocation = false;

	$('a.opensDialog').click(function(e) {
		var $this = $(this);
		var path = '/users/login';
		var cust_path = $this.data('dialog-path');

		if (typeof cust_path !== 'undefined' && cust_path.length > 0) {
			path = cust_path;
		}
		UI.dialog(path);
		if (e.preventDefault) { e.preventDefault(); }
		return false;
	});


	if (typeof $.dropkick !== 'undefined') {
		$('.default').dropkick();
		$('.sel-green').dropkick({
			theme: 'green'
		});
		$('.sel-brown').dropkick({
			theme: 'brown'
		});
	}

	$('.batchActionAllCB').click(function(e) {
		var $this = $(this);
		var $CBs = $this.closest('table').find('.batchActionCB');

		$CBs.each(function(idx, elem) {
			var $elem = $(elem);
			$elem.prop('checked', !$elem.prop('checked'));
		});

		if (e.preventDefault) { e.preventDefault(); }
		return false;
	});

	$('.batchAction').click(function(e) {
		var $this = $(this);
		var action = $this.attr('href');
		var $CBs = $this.closest('.batchActionScope').find('.batchActionCB');

		var batch = [];

		for (var i = 0; i < $CBs.length; i++) {
			var $cb = $($CBs[i]);
			if ($cb.prop('checked')) {
				batch.push($cb.data('id'));
			}
		}

		if (batch.length > 0) {
			$.post(action, {batch: batch}, function(data, status, xhr) {
				location.reload();
			});

		}

		if (e.preventDefault) { e.preventDefault(); }
		return false;
	});

	$('.contact-us').click(function(e) {
		DLG.shadowFadeIn();
		$('#contact-us').animate({'bottom':'0'}, 500);
	});

	$('#contact-exit').click(function(e) {
		DLG.shadowFadeOut();
		$('#contact-us').animate({'bottom':'-360px'}, 500);
	});
	
	bfw.whereAmI = false;

	
	// Start of JSON code
	//bfw.JSON = {};
	//bfw.getJSON = function(type, opts) {
	//	var o = {name: 'all', field: 'id', val: 0};
	//	$.extend(o, opts);
	//
	//	var keyName = type + (o.name !== 'all' ? '-' + o.name : '')
	//
	//	if (bfw.JSONloaded(type, opts)) {
	//		for (var i = 0; i < bfw.JSON[keyName].length; i++) {
	//			var json = bfw.JSON[keyName][i];
	//			if (typeof json[o.field] !== 'undefined' && json[o.field] == o.val) {
	//				return json;
	//			}
	//		}
	//	}
	//
	//	return false;
	//};
	//
	//
	//bfw.loadJSON = function(type, opts) {
	//	var o = {name: 'all', after: null};
	//	$.extend(o, opts);
	//
	//	var url = '/js/data/' + type + '/' + o.name + '.json';
	//
	//	var keyName = type + (o.name !== 'all' ? '-' + o.name : '')
	//
	//	$.ajax({
	//		url: url,
	//		dataType: 'json',
	//		data: {},
	//		type: 'get',
	//		error: function(xhr, status, err) {
	//			bfw.JSON[keyName] = [];
	//			if (typeof o.error === 'function') {
	//				o.error(xhr, status, err);
	//			}
	//		},
	//		success: function(data, status, xhr) {
	//			for (var i = 0; i < data.length; i++) {
	//				if (typeof data[i].dataType === 'undefined') { data[i].dataType = type; }
	//				if (typeof data[i].start_lat !== 'undefined' && typeof data[i].start_lng !== 'undefined') {
	//					data[i].lat = data[i].start_lat
	//					data[i].lng = data[i].start_lng
	//				}
	//			}
	//
	//			//bfw.taffy[keyName] = new TAFFY(data);
	//
	//			bfw.JSON[keyName] = data;
	//			if (typeof o.after === 'function') {
	//				o.after(data);
	//			}
	//		}
	//	});
	//};
	//
	//bfw.JSONloaded = function(type, opts) {
	//	var o = {name: 'all'};
	//	$.extend(o, opts);
	//	var keyName = type + (o.name !== 'all' ? '-' + o.name : '');
	//	return typeof bfw.JSON[keyName] !== 'undefined';
	//};
	//
	//bfw.getJSONByField = function(type, opts, field, val) {
	//	var ret_val = false;
	//	if (this.JSONloaded(type, opts)) {
	//		var o = {name: 'all'};
	//		$.extend(o, opts);
	//		var keyName = type + (o.name !== 'all' ? '-' + o.name : '');
	//
	//		var set = bfw.JSON[keyName];
	//		for (var i = 0; i < set.length; i++) {
	//			var rec = set[i];
	//			if (typeof rec[field] !== 'undefined' && rec[field] == val) {
	//				ret_val = rec;
	//				return ret_val;
	//			}
	//		}
	//	}
	//
	//	return ret_val;
	//};
	
	// End of JSON


	bfw.getLocation = function() {
		bfw.geolocation = false;

		if(navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position) {

				var loc = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);

				bfw.routes.reverseGeoCode(loc, function(addr) {
					var search = addr.city !== false && addr.state !== false ? addr.city + ', ' + addr.state : (addr.zip !== false ? addr.zip : '');
					bfw.detectedLocation = search;
					bfw.geolocation = true;
					bfw.whereAmI = addr;
					bfw.routes.UI.$address.val(search);
					bfw.routes.UI.$topAddress.val(search);
				});
			}, function() {

			});
		}
	};

	bfw.showMapResults = function(data, bounds) {
		var i = 0;

		for (var i = 0; i < data.length; i++) {
			var r = data[i];
			r.oddEven = i % 2 ? 'even' : 'odd';
			var list_tmpl = bfw.templates.search.map[r.dataType].list;
			var info_tmpl = bfw.templates.search.map[r.dataType].info;

			bfw.addMapResult({
				idx: i,
				obj: r,
				listTmpl: list_tmpl,
				infoTmpl: info_tmpl,
				paginate: 6
			});
		}
		bfw.map.fitBounds(bounds);
		//console.log(bounds);


	};


	bfw.addMapResult = function(opts) {
		var i = opts.idx, p = opts.obj, list_tmpl = opts.listTmpl, info_tmpl = opts.infoTmpl;

		var lat = typeof p.lat !== 'undefined' ? p.lat : p.start_lat;
		var lng = typeof p.lng !== 'undefined' ? p.lng : p.start_lng;

		var $entry = $(Mustache.render(list_tmpl, p));

		$entry.hide();

		bfw.routes.UI.$routeList.append($entry);


		var mult = i <= 6 ? i : 6;
		var ms_delay = (i + 1) * 50;
		setTimeout(function() {
			$entry.fadeIn(50);
		}, ms_delay);

		var markerLatLng = new google.maps.LatLng(lat, lng);

		var markerURL = '/images/map-pins/' + p.dataType;
		if (p.dataType == 'Route') {
			markerURL += '/' + (p.offroad ? 'off' : '') + 'road.png';
		} else {
			markerURL += '/' + p.subType + '.png';
		}

		var marker = new google.maps.Marker({
			position: markerLatLng,
			map: bfw.map,
			title: p.name,
			icon: markerURL
		});



		var info_content = Mustache.render(info_tmpl, p);

		var infowindow = new google.maps.InfoWindow({
			content: info_content
		});

		google.maps.event.addListener(marker, 'click', function() {
			if (bfw.search.activeInfoWindow !== null) {
				bfw.search.activeInfoWindow.close();
			}
			infowindow.open(bfw.map, marker);
			bfw.search.activeInfoWindow = infowindow;
		});

		bfw.search.infoWindows[p.dataType + ':' + p.id] = infowindow;
		bfw.search.resultsMarkers[p.dataType + ':' + p.id] = marker;

	};

	bfw.showCalendarResults = function(data) {
		var $list = bfw.events.UI.$eventList;
		var tmpl = bfw.templates.search.calendar['Event'].list;

		if (data.length > 0) {
			bfw.events.UI.$eventListContainer.addClass('results');
		} else {
			bfw.events.UI.$eventListContainer.removeClass('results');
		}

		for (var i = 0; i < data.length; i++) {
			var ev = data[i];
			ev.oddEven = i % 2 ? 'even' : 'odd';
			var $entry = $(Mustache.render(tmpl, ev));
			$list.append($entry);
		}
	};


	bfw.setupResultsMap = function(opts) {
		var o = {
			latLng: new google.maps.LatLng(43.0740558, -89.38954780000002)
		};

		$.extend(o, opts);

		bfw.searchLatLng = o.latLng;

		var mapOptions = {
			scrollwheel: false,
			zoom: 8,
			center: o.latLng,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}

		bfw.map = new google.maps.Map(document.getElementById("rtMap"), mapOptions);

		bfw.bikeLayer = new google.maps.BicyclingLayer();
		//bfw.bikeLayer.setMap(bfw.map);

		bfw.resultMapReady = true;



	}

	bfw.dialogLinks = function(opts) {
		$('a.row-dialog-trigger').each(function(idx, elem) {
			var $this = $(elem);
			var action = $this.data('dialog-action');
			if (typeof action !== 'undefined') {
				$this.attr('href', '#!' + action);
			}

		});
	};


	bfw.featuredRideSlideshow = function(slides) {
		var ctxt = this;
		ctxt.$container = $('.ss-featured-rides-container');
		ctxt.$slideFrame = ctxt.$container.find('.slides');
		ctxt.$detailFrame = ctxt.$container.find('.texts');
		ctxt.$prev = ctxt.$container.find('.btn-prev');
		ctxt.$next = ctxt.$container.find('.btn-next');
		ctxt.$nav = ctxt.$container.find('.indexes');
		ctxt.$details = ctxt.$container.find('.details');
		ctxt.active = 0;
		ctxt.animating = false;

		//ctxt.addRoute = function(slide, detail, nav) {
		//	ctxt.$slideFrame.append(slide);
		//	ctxt.$detailFrame.append(detail);
		//	ctxt.$nav.append(nav);
		//};

		ctxt.$next.click(function(e) {
			ctxt.next();
			if (e.preventDefault) {
				e.preventDefault();
			}
			return false;
		});
		ctxt.next = function() {
			var ns = ctxt.active + 1;
			if (ns > ctxt.max) {
				ns = 0;
			}
			ctxt.goto(ns);
		};

		ctxt.$prev.click(function(e) {
			ctxt.prev();
			if (e.preventDefault) {
				e.preventDefault();
			}
			return false;
		});
		ctxt.prev = function() {
			var ns = ctxt.active - 1;
			if (ns < 0) {
				ns = ctxt.max;
			}
			ctxt.goto(ns);
		};

		ctxt.goto = function(idx) {
			if (ctxt.animating || idx == ctxt.active) {
				return;
			}
			var $cur_slide = ctxt.$slideFrame.find('img:eq(' + ctxt.active + ')');
			var $next_slide = ctxt.$slideFrame.find('img:eq(' + idx + ')');
			var $cur_detail = ctxt.$detailFrame.find('div:eq(' + ctxt.active + ')');
			var $next_detail = ctxt.$detailFrame.find('div:eq(' + idx + ')');
			var $cur_nav = ctxt.$nav.find('a:eq(' + ctxt.active + ')');
			var $next_nav = ctxt.$nav.find('a:eq(' + idx + ')');

			var anim_dur = 250;
			var next_rt_id = $next_detail.data('route-id');

			ctxt.animating = true;
			$cur_slide.fadeOut(anim_dur, function() {
				$cur_slide.removeClass('active');
				$cur_detail.removeClass('active');
				$next_nav.addClass('active');
				ctxt.active = idx;
				ctxt.animating = false;
				ctxt.$details.data('dialog-action', '/routes/view/' + next_rt_id);
			});
			$cur_detail.fadeOut(anim_dur);

			$next_slide.fadeIn(anim_dur);
			$next_detail.fadeIn(anim_dur);
			$cur_nav.removeClass('active');

		};

		ctxt.$nav.on('click', 'a', function(e) {
			ctxt.goto($(this).index());
			if (e.preventDefault) { e.preventDefault(); }
			return false;
		});

		ctxt.max = ctxt.$slideFrame.find('img').length - 1;
	};


	bfw.init = function() {
		var slideshow = new bfw.featuredRideSlideshow();

        bfw.pauseAnimations = false;
        
		bfw.dialogLinks();
		bfw.taffy = {};

		function searchMapIfReady() {
			if (bfw.autoMapSearchComplete !== true && bfw.JSONloaded('Route') && bfw.JSONloaded('Template')) {
				bfw.autoMapSearchComplete = true;
				bfw.search.map({
					address: bfw.routes.UI.$address.val(),
					max_dist: 100,
					result_handler: bfw.showMapResults,
					use_location: false
				});
			}
		}

		function searchCalendarIfReady(args) {
			if (bfw.autoCalendarSearchComplete !== true && bfw.JSONloaded('Event') && bfw.JSONloaded('Template')) {
				bfw.autoCalendarSearchComplete = true;
				var sDate = Math.round((new Date()).getTime() / 1000);
				var eDate = 99999999999;

				if (eDate < sDate) {
					var tmp = eDate;
					eDate = sDate;
					sDate = tmp;
				}


				var search_opts = {
					result_handler: bfw.showCalendarResults,
					region: false,
					category: false,
					free_only: false,
					keywords: false,
					startTS: sDate,
					endTS: eDate
				};

				bfw.search.calendar(search_opts);
			}
		}

		bfw.path.init();

        //init animated bicycles if SVG is supported
        if (Modernizr.svg && bfw.bicycleAnimations !== undefined) {
            bfw.bicycleAnimations.init({
                imgPath: "/images/bicycles",
                vectorPath: bfw.path.bikePath,
                //bicycle height/width scaled by 75/90 of actual
                bicycleData: [
                    {
                        baseURL: "/Mtn-Male-WhiteShirt/Cyclist-Mtn-Male-WhiteShirt",
                        height:75,
                        width:75,
                        start:0.12,
                        end:0.14
                    },
                    {
                        baseURL: "/Road-Female-GreenShirt/Cyclist-Road-Female-GreenShirt",
                        height:75,
                        width:75,
                        start:0.346,
                        end:0.352,
                        group: "group1",
                        duration: 1.5
                    },
                    {
                        baseURL: "/Child-Female-WhiteShirt/Cyclist-Child-Female-WhiteShirt",
                        height:75.8,
                        width:71.7,
                        start:0.355,
                        end:0.36,
                        group: "group1",
                        duration: 1.5
                    },
                    {
                        baseURL: "/Recumbent-Male-WhiteShirt/Cyclist-Recumbent-Male-WhiteShirt",
                        height:90,
                        width:107.5,
                        start:0.38,
                        end:0.398
                    },
                    {
                        baseURL: "/Dutch-Female-RedShirt/Cyclist-Dutch-Female-RedShirt",
                        height:87.5,
                        width:85,
                        start:0.545,
                        end:0.56
                    },
                    {
                        baseURL: "/Mtn-Male-GreenShirt/Cyclist-Mtn-Male-GreenShirt",
                        height:75,
                        width:75,
                        start:0.603,
                        end:0.608,
                        duration: 1
                    },
                    {
                        baseURL: "/Road-Male-OrangeShirt/Cyclist-Road-Male-OrangeShirt",
                        height:75,
                        width:75,
                        start:0.8,
                        end:0.814,
                        group: "group2"
                    },
                    {
                        baseURL: "/Child-Male-GrayShirt/Cyclist-Child-Male-GrayShirt",
                        height:75.8,
                        width:71.7,
                        start:0.81,
                        end: 0.824,
                        group: "group2"
                    }
                ]
            });
        }
		var treeDefs = [
			{"x": 133, "y": 2497, "c": 0, "w": 25, "h": 54, "tw": 8, "th": 12},
			{"x": 92, "y": 2511, "c": 0, "w": 35, "h": 96, "tw": 9, "th": 11},
			{"x": 1011, "y": 2530, "c": 1, "w": 22, "h": 60, "tw": 7, "th": 12},
			{"x": 1040, "y": 2553, "c": 0, "w": 34, "h": 74, "tw": 8, "th": 15},
			{"x": 180, "y": 2576, "c": 1, "w": 31, "h": 100, "tw": 5, "th": 12},
			{"x": 1181, "y": 2939, "c": 5, "w": 35, "h": 61, "tw": 8, "th": 12},
			{"x": 1075, "y": 3021, "c": 5, "w": 37, "h": 65, "tw": 8, "th": 12},
			{"x": 1006, "y": 3079, "c": 2, "w": 46, "h": 79, "tw": 8, "th": 12},
			{"x": 1029, "y": 3084, "c": 4, "w": 33, "h": 55, "tw": 10, "th": 10},
			{"x": 1090, "y": 4383, "c": 0, "w": 30, "h": 67, "tw": 9, "th": 11},
			{"x": 1115, "y": 4435, "c": 5, "w": 32, "h": 39, "tw": 8, "th": 14},
			{"x": 197, "y": 4566, "c": 0, "w": 31, "h": 46, "tw": 6, "th": 14},
			{"x": 245, "y": 4570, "c": 2, "w": 30, "h": 95, "tw": 7, "th": 15},
			{"x": 221, "y": 4606, "c": 0, "w": 45, "h": 89, "tw": 7, "th": 11},
			{"x": 1050, "y": 4713, "c": 6, "w": 50, "h": 95, "tw": 6, "th": 13},
			{"x": 1041, "y": 4714, "c": 1, "w": 23, "h": 80, "tw": 9, "th": 10},
			{"x": 498, "y": 4736, "c": 0, "w": 43, "h": 53, "tw": 8, "th": 11},
			{"x": 1077, "y": 4761, "c": 5, "w": 49, "h": 86, "tw": 6, "th": 13},
			{"x": 1025, "y": 4764, "c": 2, "w": 31, "h": 50, "tw": 7, "th": 14},
			{"x": 445, "y": 4771, "c": 1, "w": 33, "h": 46, "tw": 5, "th": 14},
			{"x": 468, "y": 4781, "c": 3, "w": 30, "h": 76, "tw": 6, "th": 13},
			{"x": 1099, "y": 5615, "c": 1, "w": 32, "h": 80, "tw": 8, "th": 10},
			{"x": 1146, "y": 5620, "c": 2, "w": 27, "h": 53, "tw": 6, "th": 11},
			{"x": 1056, "y": 5641, "c": 0, "w": 46, "h": 99, "tw": 5, "th": 11},
			{"x": 169, "y": 5736, "c": 3, "w": 37, "h": 84, "tw": 8, "th": 13},
			{"x": 110, "y": 5771, "c": 1, "w": 49, "h": 87, "tw": 10, "th": 13},
			{"x": 208, "y": 5778, "c": 0, "w": 33, "h": 92, "tw": 9, "th": 10},
			{"x": 145, "y": 5800, "c": 1, "w": 49, "h": 96, "tw": 6, "th": 12},
			{"x": 1010, "y": 5813, "c": 0, "w": 37, "h": 55, "tw": 5, "th": 12},
			{"x": 1055, "y": 5820, "c": 1, "w": 44, "h": 82, "tw": 5, "th": 15},
			{"x": 1107, "y": 6612, "c": 1, "w": 49, "h": 100, "tw": 9, "th": 13},
			{"x": 1066, "y": 6638, "c": 0, "w": 39, "h": 93, "tw": 8, "th": 13},
			{"x": 1079, "y": 7052, "c": 4, "w": 44, "h": 79, "tw": 9, "th": 12},
			{"x": 1118, "y": 7072, "c": 0, "w": 45, "h": 75, "tw": 8, "th": 12},
			{"x": 81, "y": 7089, "c": 3, "w": 33, "h": 93, "tw": 8, "th": 12},
			{"x": 1056, "y": 7115, "c": 1, "w": 41, "h": 59, "tw": 9, "th": 13},
			{"x": 116, "y": 7130, "c": 0, "w": 37, "h": 99, "tw": 8, "th": 12}
		];
		TREES.init({
			selector: '#treePaper',
			trees: treeDefs,
			addMode: false,
			growOnly: true
		});

		bfw.loadJSON('Template', {
			after: function(data) {
				bfw.templates = data;

				bfw.loadJSON('Route', {
					after: function(data) {
						bfw.setupResultsMap();
						bfw.getLocation();
						searchMapIfReady();
					}
				});



				bfw.loadJSON('Event', {
					after: function(data) {
						//searchCalendarIfReady();
					}
				});

				bfw.loadJSON('Location');

				bfw.loadJSON('Classified');

				bfw.loadJSON('Group');
			}
		});


	};



});