/*jslint browser: true, plusplus: true, vars: true, white: true */
/*global console, $, Raphael, bfw */

/**
 * bfw.bicycleAnimations - data and methods related to animating the cyclists
 *
 * init         - function: initialize the animations
 * bicycles     - array: Raphael image elements; the animated bicycles
 * animators    - array: the animator functions; call them to start the animations
 *                  animators[x] will start the animation of bicycles[x]
 */
bfw.bicycleAnimations = {};

(function () {
    'use strict';
    
    //don't support VML
    if (Modernizr.svg) {
        
        // http://paulirish.com/2011/requestanimationframe-for-smart-animating/
        // http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
        // requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel
        // MIT license
        (function () {
            var lastTime = 0,
                vendors = ['ms', 'moz', 'webkit', 'o'],
                x;
                
            for (x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
                window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
                window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] 
                                           || window[vendors[x]+'CancelRequestAnimationFrame'];
            }
         
            if (!window.requestAnimationFrame) {
                window.requestAnimationFrame = function (callback, element) {
                    var currTime = new Date().getTime(),
                        timeToCall = Math.max(0, 16 - (currTime - lastTime)),
                        id = window.setTimeout(function () { callback(currTime + timeToCall); }, 
                      timeToCall);
                    lastTime = currTime + timeToCall;
                    return id;
                };
            }
         
            if (!window.cancelAnimationFrame) {
                window.cancelAnimationFrame = function (id) {
                    clearTimeout(id);
                };
            }
        }());
        //end requestAnimationFrame polyfill
        
        var debug = function (element, message, object) {
            var printElement = "", printMessage = "", printObject = "";
            
            if (element) {
                printElement = "bike " + element.customAttrs.id;
            }
            
            if (message) {
                printMessage = message;
            }
            
            if (object) {
                printObject = object;
            }
            
            console.log("bicycleAnimations debug:", printElement, printMessage, printObject);
        };
        
        var perror = function (message, fatal) {
            console.log("bicycleAnimations error: " + message);
            if (fatal) {
                console.log("exiting...");
            }
        };
        
        var onScreen = function (element) {
            var el = $(element),
                winTop = $(window).scrollTop(),
                winHeight = $(window).height(),
                top = $(el).offset().top,
                height = $(el).height();
                
            if (height === 0) {
                height = element.getBoundingClientRect().height;
            }
            
            return ((top + height) > winTop && top < (winTop + winHeight));
        };
        
        var inAnimationZone = function (element) {
            var el = $(element),
                winTop = $(window).scrollTop(),
                winHeight = $(window).height(),
                top = $(el).offset().top,
                height = $(el).height();
                
            if (height === 0) {
                height = element.getBoundingClientRect().height;
            }
            
            return (top > (winTop + (winHeight * 0.25)) && (top + height) < (winTop + (winHeight * 0.50)));
        };

        /**
         * Generate a stepper object that will animate 'element' along Raphael vector 'path' 
         * from 'start' (position on path expressed as % of path, between 0 and 1) to 'end' (same format as start).
         *
         * element - a Raphael Element; here a Raphael image element, the bicycle
         * path - a Raphael Element (a path)
         * start - a number between 0 and 1
         * end - a number between 0 and 1 and strictly greater than start
         * duration - # of seconds, total desired time of travel along 'path' from 'startPos' to 'endPos'
         *            NOTE: this is only an approximation as it does not take into account any easing
         *
         * If either 'start' or 'end' do not follow proper format, defaults of 0 and 1 will be used
         */
        var step = function (element, path, start, end, duration) {
            var length = path.getTotalLength(),     //total length of 'path', in path units
                partialLength,                      //the length of the part we will be traversing, in path units
                maxUnitsPerSec,                     //upper bound for travel speed, path units per second
                MIN_UNITS_PER_SEC = 30,             //lower bound for travel speed, path units per second
                curPosition,                        //bicycle's current position along 'path', in path units
                startPos = 0,                       //starting position as a percentage of 'length', between 0 and 1
                endPos = 1,                         //ending position as a percentage of 'length', between 0 and 1, > 'startPos'
                BRAKE_POS = 0.85,                   //when to start slowing down as a percentage of 'length'
                ACCEL_RATE = 10/30,                 //accelerate 10 units per second every 30 milliseconds
                DECEL_RATE = -5/30,                 //decelerate 5 units per second every 30 milliseconds
                frame = 1, frameWait = 0,           //which frame of the animated bicycle image to display, milliseconds elapsed since last frame update
                FRAME_INTERVAL = 40,                //number of milliseconds to wait between updating bicycle animation frame
                LAST_FRAME = 8,                     //index of last frame of the animation
                unitsPerSec = 1,                    //target speed of bicycle in path units per second
                point1,                             //point along 'path' of lower left vertex of bicycle bounding box
                point2,                             //point along 'path' of lower right vertex of bicycle bounding box
                midY,                               //the y value in the middle of the line between point1 and point2, this is the y val we will use to position the bicycle (this way, the wheels don't go below the path (as much))
                point,                              //point object ({x:<#>, y:<#>}) of bicycle position along 'path', absolute coords of position relative to containing vector image
                angle;                              //angle in degrees, rotational transform of bicycle
                
            /**
             * Estimate angle line tangent to 'path' at position 'length'
             *
             * path - a Raphael element representing a vector path
             * length - an integer representing the number of path length units
             */
            var calcAngle = function (element, path, length) {
                //use secant line estimation
                var p1 = path.getPointAtLength(length - (element.attr("width") / 2)),
                    p2 = path.getPointAtLength(length + (element.attr("width") / 2)),
                    dx = p1.x - p2.x,
                    dy = p1.y - p2.y,
                    a = Math.atan(dy/dx) * (180 / Math.PI);
                
                return a;
            };
            
            /**
             * The stepper object.
             */
            var stepper = {
                element: element,
                
                /**
                 * The step function. Advance the animation according to how much time has elapsed.
                 *
                 * elapsed - # of ms since last invocation, calculated and passed in by animator
                 */
                step: function (elapsed) {
                    var progress = (curPosition - (startPos * length)) / partialLength, //percent of distance covered along 'partialLength' of 'path', between 0 and 1
                        delta,                  //change in position along 'path', in path units
                        advanceFrame = true;    //whether to animate the cyclist's legs
                    
                    //easing, simulate acceleration and deceleration of bicycle
                    if (progress < BRAKE_POS) {
                        //before BRAKE_POSition, accelerate to calculated 'maxUnitsPerSec', then travel at 'maxUnitsPerSec'
                        if (element.customAttrs.debug) {
                            debug(element, "less than "+ BRAKE_POS * 100 +"%", null);
                        }
                        
                        if (unitsPerSec < maxUnitsPerSec) {
                            unitsPerSec += elapsed * ACCEL_RATE;
                            if (unitsPerSec > maxUnitsPerSec) {
                                unitsPerSec = maxUnitsPerSec;
                            }
                        }
                    } else if (progress >= BRAKE_POS) {
                        //after BRAKE_POSition, stop animating the cyclist (they are braking now), and decelerate to MIN_UNITS_PER_SEC before stopping at 'stopPos'
                        advanceFrame = false;
                        
                        if (element.customAttrs.debug) {
                            debug(element, "greater than "+ BRAKE_POS * 100 +"%", null);
                        }
                        
                        if (unitsPerSec > MIN_UNITS_PER_SEC) {
                            unitsPerSec += elapsed * DECEL_RATE;
                            if (unitsPerSec < MIN_UNITS_PER_SEC) {
                                unitsPerSec = MIN_UNITS_PER_SEC;
                            }
                        }
                    }
                    
                    delta = unitsPerSec / 1000 * elapsed; //unitsPerSec / 1000 = unitsPerMilliSec
                    
                    if (element.customAttrs.debug) {
                        debug(element, "", {"unitsPerSec": unitsPerSec, "delta": delta, "progress": progress, "elapsed": elapsed});
                    }
                    
                    if ((curPosition + delta) >= (endPos * length)) {
                        //if we at our destination, stop
                        return false;
                    }
                    
                    //calculate new position and rotation
                    point1 = path.getPointAtLength(curPosition + delta - element.attr("width") / 2);    //where the left wheel should be
                    point2 = path.getPointAtLength(curPosition + delta + element.attr("width") / 2);    //where the right wheel should be
                    midY = (point1.y + point2.y) / 2;                                                   //use this y value to keep the bicycle wheels on the path
                    point = path.getPointAtLength(curPosition + delta);
                    angle = calcAngle(element, path, (curPosition + delta));
                    
                    //move the cyclist
                    element.attr({x: (point.x - element.attr("width") / 2), y: (midY - element.attr("height"))});
                    element.transform("R" + angle + "," + point.x + "," + point.y);
                    
                    if (element.customAttrs.drawPath) {
                        element.paper.rect(point.x, point.y, 1, 1);
                    }
                    
                    if (advanceFrame && !element.customAttrs.wireFrame) {
                        if (frameWait >= FRAME_INTERVAL) {
                            frameWait = 0;
                            
                            if (frame > LAST_FRAME) {
                                frame = 1;
                            }
                            element.attr("src", element.customAttrs.baseURL + frame + ".png");
                            if (Raphael.vml) {
                                //transform gets cleared when we change the image src, reset it
                                element.transform("R" + angle);
                            }
                            frame++;
                        } else {
                            frameWait += elapsed;
                        }
                    }
                    
                    curPosition += delta;
                    
                    //keep the animation going
                    return true;
                },//end stepper.step
                
                /**
                 * Reset the animation.
                 */
                reset: function () {
                    curPosition = startPos * length;
                    
                    //reset to initial position and rotation
                    point1 = path.getPointAtLength(curPosition - element.attr("width") / 2);    //where the left wheel should be
                    point2 = path.getPointAtLength(curPosition + element.attr("width") / 2);    //where the right wheel should be
                    midY = (point1.y + point2.y) / 2;                                           //use this y value to keep the bicycle wheels on the path
                    point = path.getPointAtLength(curPosition);
                    angle = calcAngle(element, path, curPosition);
                    
                    //place element back at starting point of animation
                    element.attr({x: (point.x - element.attr("width") / 2), y: (midY - element.attr("height"))});
                    element.transform("R" + angle + "," + point.x + "," + point.y);
                }// end stepper.reset
            };// end stepper
            
            //sanity checks
            if (start !== undefined && typeof start === 'number' && start > 0 && start <= 1) {
                startPos = start;
            }
            
            if (end !== undefined && typeof end === 'number' && end > 0 && end <= 1 && end > startPos) {
                endPos = end;
            }
            
            //initialization
            curPosition = startPos * length;
            partialLength = ((endPos * length) - curPosition);
            maxUnitsPerSec = partialLength / duration;
            
            //calculate initial position and rotation
            point1 = path.getPointAtLength(curPosition - element.attr("width") / 2);    //where the left wheel should be
            point2 = path.getPointAtLength(curPosition + element.attr("width") / 2);    //where the right wheel should be
            midY = (point1.y + point2.y) / 2;                                           //use this y value to keep the bicycle wheels on the path
            point = path.getPointAtLength(curPosition);
            angle = calcAngle(element, path, curPosition);
            
            //place element at starting point of animation
            element.attr({x: (point.x - element.attr("width") / 2), y: (midY - element.attr("height"))});
            element.transform("R" + angle + "," + point.x + "," + point.y);
            
            return stepper;
        };
        
        /**
         * Generate an animator that takes a stepper object and runs stepper.step which should take an elapsed number of ms
         *
         * stepper - an object containing a step function and a reset function
         *  step - a function which will manipulate the DOM to animate it (in this case, a stepper function as above)
         *  reset - a function which will reset the animation to the initial state
         */
        var animate = function (stepper) {
            var time,
                deferred,
                isResetting = false;
            
            var animator = {
                isTriggered: false,
                
                //trigger the animation; it will run to completion once triggered
                animate: function a() {
                    var now = new Date().getTime(),
                        dt = now - (time || now);
                    
                    deferred = deferred || $.Deferred();
                    
                    if (!bfw.pauseAnimations) {
                        time = now;
                        animator.isTriggered = true;
                        
                        //if stepper.step returns true, keep doing the animation
                        if (stepper.step(dt) === true) {
                            window.requestAnimationFrame(a);
                        } else {
                            time = undefined;
                            deferred.resolve();
                        }
                    } else {
                        deferred.resolve();
                    }
                },
                
                //reset the animation, deferred until the animation is complete, will not
                //run if the element has scrolled back onto the screen since the reset was queued
                reset: function () {
                    var that = this;
                    
                    if (deferred && !isResetting) {
                        isResetting = true;
                    
                        deferred.done(function() {
                            //don't reset the bicycle if the user has scrolled such that it's back on the screen
                            if (!onScreen(stepper.element.node)) {
                                stepper.reset();
                                that.isTriggered = false;
                                deferred = null;
                            }
                            
                            //either we've successfully reset or our reset was interrupted by the user scrolling the bicycle back into view;
                            //in the former case, we want to be able to reset again in the event of the animation being triggered once more
                            //in the latter, we want to retry the reset when the bicycle scrolls off the screen again
                            isResetting = false;
                        });
                    }
                }
            };
            
            return animator;
        };
        
        /**
         * Set up the bicycleAnimations object, prep the animations
         *
         * params - object, contains three properties: imgPath, vectorPath, and bicycleData
         *          imgPath         - a string, the base portion of image urls that is common to all bicycles, eg '/images/bicycles'
         *          vectorPath   - a Raphael Path Element, the vector path that the bicycles will travel along
         *          bicycleData     - an array of objects, one per bicycle, each object containing:
         *                          baseURL     - a string, the portion of the bicycle image url unique to this bike, but common to all animation frames
         *                                      eg: if the animation frames for all bicycles were in /images/bicycles, and each bicycle had a subdirectory like: '/Mtn-Male-WhiteShirt/'
         *                                      filled with files like 'Cyclist-Mtn-Male-WhiteShirt1.png', 'Cyclist-Mtn-Male-WhiteShirt2.png', 'Cyclist-Mtn-Male-WhiteShirt3.png', etc
         *                                      then imgPath would be '/images/bicycles' and baseURL for this bike would be '/Mtn-Male-WhiteShirt/Cyclist-Mtn-Male-WhiteShirt', 
         *                                      containing the unique subdirectory of 'imgPath' and the common prefix of the frame file names. NOTE: the frame file names should follow
         *                                      the example format
         *                              
         *                          height      - an integer, the image height in pixels
         *                          width       - an integer, the image width in pixels
         *                          start       - a float, the start position of the bicycle animation along the path as a percentage of the path, between 0 and 1
         *                          end         - a float, the end position of the bicycle animation along the path as a percentage of the path, between 0 and 1
         *                          duration    - a number, optional (default 2), the duration of the animation in seconds, NOTE: only an approximation, it doesn't take into account easing
         *                          group       - a string, optional, the name of an animation group; bicycles with the same animation group will be be animated together
         */
        var init = function (params) {
            var animators = [],                      //holds the bicycle animators
                bicycles = [], bicycle, i, j,         //the Raphael bicycle images
                stepper,
                imgPath,
                bikePath,
                bicycleData,
                animationGroups = [],
                startFrame,
                preCache;
            
            //sanity checks
            if (!params || !params.imgPath || !params.vectorPath || !params.bicycleData) {
                perror("init(): parameters missing! You must pass in a params object with imgPath, vectorPath, and bicycleData properties", true);
                return;
            }
            if (params.debug) {
                debug(null, "init(): params object included with required properties");
                debug(null, "init(): params:", params);
            }
            
            if (typeof params.imgPath !== "string") {
                perror("init(): imgPath must be a string!", true);
                return;
            }
            if (params.debug) {
                debug(null, "init(): imgPath is a string");
            }
            
            //simple duck type-checking, if it has the right structure and provides functions under the right names, we'll assume it's really a Raphael Path Element
            //...though it could be an elaborate ruse
            if (typeof params.vectorPath !== "object" || 
                typeof params.vectorPath.paper !== "object" || 
                typeof params.vectorPath.paper.rect !== "function" ||
                typeof params.vectorPath.paper.image !== "function" ||
                typeof params.vectorPath.getTotalLength !== "function" ||
                typeof params.vectorPath.getPointAtLength !== "function") {
                perror("init(): vectorPath type not recognized! It must be a Raphael Path Element (Raphael v2.1.0 compatible)");
                return;
            }
            if (params.debug) {
                debug(null, "init(): vectorPath appears to be a Raphael Path Element");
            }
            
            if (!$.isArray(params.bicycleData)) {
                perror("init(): bicycleData must be an array!", true);
                return;
            }
            if (params.debug) {
                debug(null, "init(): bicycleData is an array");
            }
            
            if (params.bicycleData.length < 1) {
                perror("init(): bicycleData doesn't contain any bicycle data! Nothing to do...", true);
                return;
            }
            if (params.debug) {
                debug(null, "init(): bicycleData has at least one element");
            }
            
            for (i in params.bicycleData) {
                if (params.bicycleData.hasOwnProperty(i)) {
                    bicycle = params.bicycleData[i];
                    
                    if (typeof bicycle.baseURL !== "string") {
                        perror("init(): bicycleData[" + i + "].baseURL missing or not a string!", true);
                        return;
                    }
                    
                    if (typeof bicycle.height !== "number") {
                        perror("init(): bicycleData[" + i + "].height missing or not a number!", true);
                        return;
                    }
                    
                    if (typeof bicycle.width !== "number") {
                        perror("init(): bicycleData[" + i + "].width missing or not a number!", true);
                        return;
                    }
                    if (params.debug) {
                        debug(null, "init(): bicycleData[" + i + "] required data appears valid");
                    }
                    
                    if (typeof bicycle.start !== "number" || !(bicycle.start >= 0 && bicycle.start <= 1)) {
                        perror("init(): bicycleData[" + i + "].start missing, not a number, or not between 0 and 1. Default (0) will be used.");
                    }
                    
                    if (typeof bicycle.end !== "number" || !(bicycle.end >= 0 && bicycle.end <= 1 && bicycle.end > bicycle.start)) {
                        perror("init(): bicycleData[" + i + "].end missing, not a number, not between 0 and 1, or not strictly greater than bicycle.start. Default (1) will be used.");
                    }
                }
            }
            //end sanity checks
            
            imgPath = params.imgPath;
            bikePath = params.vectorPath;
            bicycleData = params.bicycleData;
            
            if (params.debug) {
                bfw.bicycleAnimations.debug = true;
            }
            
            //set default duration for any bicycles where duration isn't specified
            if (bfw.bicycleAnimations.debug) {
                debug(null, "init(): setting default animation durations where not specified");
            }
            for (i = 0; i < bicycleData.length; i++) {
                if (!bicycleData[i].hasOwnProperty('duration')) {
                    if (bfw.bicycleAnimations.debug) {
                        debug(null, "init(): bicycleData[" + i + "] has no duration specified, using default");
                    }
                    bicycleData[i].duration = 2;
                }
            }
            
            //generate the bicycles as vector graphics with Raphael
            if (bfw.bicycleAnimations.debug) {
                debug(null, "init(): generating bicycle vector elements");
            }
            for (i = 0; i < bicycleData.length; i++) {
                //draw them on the bikePath canvas
                if (bicycleData[i].wireFrame) {
                    bicycle = bikePath.paper.rect(0, 0, bicycleData[i].width, bicycleData[i].height);
                } else {
                
                    //randomize the starting frame
                    startFrame = Math.floor(Math.random() * 8 + 1);
                    if (bfw.bicycleAnimations.debug) {
                        debug(null, "init(): randomly choosing intial frame " + startFrame + " for bicycle " + i);
                    }
                    bicycle = bikePath.paper.image(imgPath + bicycleData[i].baseURL + startFrame + ".png", 0, 0, bicycleData[i].width, bicycleData[i].height);
                    
                    //pre-cache the frames
                    if (bfw.bicycleAnimations.debug) {
                        debug(null, "init(): pre-caching bicycle pedal animation frames");
                    }
                    for (j = 1; j <= 8; j++) {
                        preCache = document.createElement('img');
                        preCache.src = imgPath + bicycleData[i].baseURL + j + ".png";
                    }
                }
                
                //hide the bicycle
                bicycle.node.style.display = "none";
                
                //store the image baseURL
                bicycle.customAttrs = {};
                bicycle.customAttrs.baseURL = imgPath + bicycleData[i].baseURL;
                
                //set up animation groups
                if (bicycleData[i].group) {
                    if (animationGroups[bicycleData[i].group] !== undefined) {
                        animationGroups[bicycleData[i].group].push(i);
                    } else {
                        animationGroups[bicycleData[i].group] = [i];
                    }
                    
                    bicycle.customAttrs.group = animationGroups[bicycleData[i].group];
                    
                    if (bfw.bicycleAnimations.debug) {
                        debug(null, "init(): added bicycle " + i + " to group " + bicycleData[i].group);
                    }
                }
                
                if (bicycleData[i].debug) {
                    if (bfw.bicycleAnimations.debug) {
                        debug(null, "init(): bicycleData[" + i + "] debugging messages enabled");
                    }
                    bicycle.customAttrs.debug = true;
                    bicycle.customAttrs.id = i;
                }
                
                if (bicycleData[i].wireFrame) {
                    if (bfw.bicycleAnimations.debug) {
                        debug(null, "init(): bicycleData[" + i + "] wireframe-mode enabled");
                    }
                    bicycle.customAttrs.wireFrame = true;
                }
                
                if (bicycleData[i].drawPath) {
                    if (bfw.bicycleAnimations.debug) {
                        debug(null, "init(): bicycleData[" + i + "] drawpath enabled");
                    }
                    bicycle.customAttrs.drawPath = true;
                }
                
                bicycles.push(bicycle);
            }
            if (bfw.bicycleAnimations.debug) {
                debug(null, "bicycles:", bicycles);
            }
            
            //set up the animators
            if (bfw.bicycleAnimations.debug) {
                debug(null, "init(): initializing animators");
            }
            for (i in bicycles) {
                if (bicycles.hasOwnProperty(i)) {
                    if ($.isNumeric(i)) {
                        bicycle = bicycleData[i];
                        stepper = step(bicycles[i], bikePath, bicycle.start, bicycle.end, bicycle.duration);
                        animators.push(animate(stepper));
                        
                        //un-hide the bicycle
                        bicycles[i].node.style.display = "";
                    }
                }
            }
            if (bfw.bicycleAnimations.debug) {
                debug(null, "animators: ", animators);
            }
            
            //event listener
            if (bfw.bicycleAnimations.debug) {
                debug(null, "init(): setting up scroll event listener");
            }
            $(window).on('scroll', function () {
                $.each(bicycles, function (index, value) {
                    var group = value.customAttrs.group,
                        member, multiplier;
                    
                    //trigger the animation when the bicycle is half way up the window
                    if (inAnimationZone(value.node)) {
                        //if the bicycle is in an animation group, trigger all group members
                        if (group) {
                        
                            if (bfw.bicycleAnimations.debug) {
                                debug(null, "scroll event listener: bicycle is in an animation group. animating group");
                            }
                            
                            multiplier = 0;
                            for (member = group.length - 1; member >= 0; member--) {
                                //if the animation is triggered, no need to trigger it again
                                if (!animators[group[member]].isTriggered) {
                                    //this will be set by .animate(), but here we are delaying the call, so multiple triggers could be queued
                                    //to prevent that, we'll set it here
                                    animators[group[member]].isTriggered = true;
                                    
                                    window.setTimeout(animators[group[member]].animate, (100 * multiplier));
                                    multiplier++;
                                }
                            }
                        } else {
                            //if the animation is triggered, no need to trigger it again
                            if (!animators[index].isTriggered) {
                                animators[index].animate();
                            }
                        }
                    } else if (!onScreen(value.node)) {
                        //no need to reset if the animation hasn't been triggered
                        if (animators[index].isTriggered) {
                            animators[index].reset();
                        }
                    }
                });
            });
            
            if (bfw.bicycleAnimations.debug) {
                debug(null, "init(): making bicycles and animators public");
            }
            bfw.bicycleAnimations.bicycles = bicycles;
            bfw.bicycleAnimations.animators = animators;
            
            if (bfw.bicycleAnimations.debug) {
                debug(null, "init(): complete: bfw.bicycleAnimations:", bfw.bicycleAnimations);
            }
        };
        
        bfw.bicycleAnimations.init = init;
    }
}());
