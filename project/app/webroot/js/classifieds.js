bfw.classifieds = {};

$(function() {
	
	bfw.classifieds = {
		TAGS: {
			'parts': ['wheel','saddle','chain ring', 'crank', 'stem', 'headset', 'bottom bracket', 'hub', 'brakes']
		},
		
		registerEditor: function() {
			var ctxt = this;
			$('input.tagIt').each(function(idx, elem) {
				var $elem = $(elem);
				var tagKey = $elem.data('tag-key');
				if (typeof ctxt.TAGS[tagKey] !== 'undefined') {
					$elem.tagit({
						availableTags: ctxt.TAGS[tagKey]
					});
				}
			});
			
			var $listingType = $('#listingType');
			var $types = $('.classifiedType');
			var $partClassified = $('.partClassified');
			var $bikeClassified = $('.bikeClassified');
			
			var show = function(elem, then) {
				var $elem = $(elem);
				if (!$elem.is(':visible')) {
					$elem.show(250, function() {
						if (typeof then === 'function') {
							then();
						}
					});
				} else {
					if (typeof then === 'function') {
						then();
					}
				}
			};
			
			var hide = function(elem, then) {
				var $elem = $(elem);
				
				if ($elem.is(':visible')) {
					$elem.hide(250, function() {
						if (typeof then === 'function') {
							then();
						}
					});
				} else {
					if (typeof then === 'function') {
						then();
					}
				}
			};
			
			var hideEmpty = function() {
				
				$listingType.find('option').each(function(idx, elem) {
					var $elem = $(elem);
					if ($elem.val() == '') {
						$elem.remove();
					}
				});
			};
			
			
			$listingType.change(function(e) {
				var $this = $(this);
				var val = $this.val();
				
				console.log(val);
				
				if (val == 'part') {
					hide($bikeClassified, function() { show($partClassified); });
				} else {
					hide($partClassified, function() { show($bikeClassified); });
				}
				
				hideEmpty();
			});
		}, // registerEditor()
	
		flds: {
			
		},
		
		registerSearch: function(args) {
			var ctxt = this;
			
			$(document).on('click', '#searchResults tr', {}, function(e) {
				var $this = $(this);
				var $a = $this.find('a');
				//$a.click();
				//var id = $this.data('id');
				DLG.open($a.attr('href'));
			});
			
			
			ctxt.flds = {
				$zip: $('#searchZip'),
				$range: $('#searchRange'),
				$terms: $('#searchTerms'),
				$bikes: $('#searchBikes'),
				$parts: $('#searchParts'),
				$photos: $('#searchOnlyPhotos'),
				$submit: $('#doSearch'),
				$results: $('#searchResults')
			};
			
			if (bfw.geolocation) {
				ctxt.flds.$zip.val(bfw.detectedLocation);	
			}
			
			ctxt.flds.$submit.click(function(e) {
				if (ctxt.flds.$zip.val().trim().length < 1) {
					ctxt.flds.$zip.closest('div').addClass('error');
				} else {
					ctxt.flds.$zip.closest('div').removeClass('error');
					var searchOptions = {
						zip: ctxt.flds.$zip.val(),
						range: ctxt.flds.$range.val(),
						terms: ctxt.flds.$terms.val(),
						bikes: ctxt.flds.$bikes.is(':checked'),
						parts: ctxt.flds.$parts.is(':checked'),
						onlyImages: ctxt.flds.$photos.is(':checked'),
						callback: ctxt.displayResults
					};
					
					bfw.search.classifieds.start(searchOptions);
				}
						
				if (e.preventDefault) { e.preventDefault(); }
				return false;
			});
				
		},
		
		searchError: function(msg, data) {
			alert("Please enter a valid city/state or ZIP code for search");
		},
		
		displayResults: function(data) {
			var tmpl = bfw.templates.search.classifieds.Classified.list;
			
			bfw.classifieds.flds.$results.empty();
			
			var start_lat = data.request.lat;
			var start_lng = data.request.lng;
			
			if (data.results) {
				var srl = [];
				
				for (var i = 0; i < data.results.length; i++) {
					var id = data.results[i];
					var rec = bfw.getJSONByField('Classified', {name: 'all'}, 'id', id);
					
					rec.howFar = bfw.routes.getDistance(rec.lat, rec.lng, start_lat, start_lng);
					srl.push(rec);
				}
				
				srl.sort(function(a, b) {
					return a.howFar > b.howFar;
				});
				
				for (var i = 0; i < srl.length; i++) {
					var rec = srl[i];
					rec.oddEven = i % 2 ? 'even' : 'odd';
					rec.howFar = rec.howFar.toFixed(2);
					var $entry = $(Mustache.render(tmpl, rec));
					bfw.classifieds.flds.$results.append($entry);	
				}
				
				
				DLG.resizeDialog(undefined, $('body').height());
			}
			
		},
		
		init: function() {
			this.registerEditor();
			this.registerSearch();
		},
		
		_xyz: null
	}; // bfw.classifieds
	
	bfw.classifieds.init();
	
});