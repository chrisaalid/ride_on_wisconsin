$(function() {
	
	function ContentSlideShow(container) {
		var ctxt = this;
		ctxt.active = 1;
		ctxt.count = 0;
		ctxt.$wrapper = null;
		ctxt.$current = null;
		ctxt.$slides = [];
		
		ctxt.changing = false;
		
		ctxt.registerHandlers = function() {
			var ctxt = this;
			this.$wrapper.find('.slide-control .slide-next').click(function(e) {
				ctxt.next();
				if (e.preventDefault) { e.preventDefault(); }
				return false;
			});
			this.$wrapper.find('.slide-control .slide-prev').click(function(e) {
				ctxt.prev();
				if (e.preventDefault) { e.preventDefault(); }
				return false;
			});
		}; // registerHandlers()
		
		ctxt.init = function(wrapper) {
			//var ctxt = ;
			ctxt.$wrapper = $(wrapper); //$('.contentSlideShowWrapper');
			
			ctxt.$slides = ctxt.$wrapper.find('.slides img');
			ctxt.$current = ctxt.$wrapper.find('.currentSlide');
			ctxt.count = ctxt.$slides.length;
			ctxt.registerHandlers();
		}; // init()
		
		ctxt.next = function() {
			var nextSlide = this.active >= this.count ? 1 : this.active + 1;
			this.goto(nextSlide);
		}; // next()
		
		ctxt.prev = function() {
			var prevSlide = this.active == 1 ? this.count : this.active - 1;
			this.goto(prevSlide);
		}; // prev()
		
		ctxt.goto = function(i) {
			if (this.changing || this.count == 1) { return; }
			
			if (i < 1) { i = 1; }
			if (i > this.count) { i = this.count; }
			
			var ctxt = this;
			var $cur = this.$slides.filter('.slide-' + this.active);
			var $nxt = this.$slides.filter('.slide-' + i);
			
			$cur.fadeOut(250);
			
			$nxt.fadeIn(250, function() {
				var $this = $(this);
				$this.removeClass('hiddenSlide');
				ctxt.$current.html(i);
			});
			
			this.active = i;
		}; // goto
		
		ctxt.init(container);
	} // ContentSlideShow()
	
	
	console.log(ContentSlideShow);
	
	$('.contentSlideShowWrapper').each(function(idx, elem) {
		var ss = new ContentSlideShow(elem);
	});
	
	//SS.init();
});