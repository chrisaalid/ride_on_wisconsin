var SS = {};

$(function() {
	
	SS = {
		active: 1,
		count: 0,
		$wrapper: null,
		$current: null,
		$slides: [],
		
		$slideshows: [],
		changing: false,
		
		registerHandlers: function() {
			var ctxt = this;
			this.$wrapper.find('.slide-control .slide-next').click(function(e) {
				ctxt.next();
				if (e.preventDefault) { e.preventDefault(); }
				return false;
			});
			this.$wrapper.find('.slide-control .slide-prev').click(function(e) {
				ctxt.prev();
				if (e.preventDefault) { e.preventDefault(); }
				return false;
			});
		},
		
		init: function() {
			var ctxt = this;
			ctxt.$wrapper = $('.contentSlideShowWrapper');
			ctxt.$wrapper.each(function() {
				ctxt.$slides = ctxt.$wrapper.find('.slides img');
				ctxt.$current = ctxt.$wrapper.find('.currentSlide');
				ctxt.count = ctxt.$slides.length;
				ctxt.registerHandlers();
			});
		},
		
		next: function() {
			var nextSlide = this.active >= this.count ? 1 : this.active + 1;
			this.goto(nextSlide);
		},
		
		prev: function() {
			var prevSlide = this.active == 1 ? this.count : this.active - 1;
			this.goto(prevSlide);
		},
		
		goto: function(i) {
			if (this.changing || this.count == 1) { return; }
			
			if (i < 1) { i = 1; }
			if (i > this.count) { i = this.count; }
			
			var ctxt = this;
			var $cur = this.$slides.filter('.slide-' + this.active);
			var $nxt = this.$slides.filter('.slide-' + i);
			
			$cur.fadeOut(250);
			
			$nxt.fadeIn(250, function() {
				var $this = $(this);
				$this.removeClass('hiddenSlide');
				ctxt.$current.html(i);
			});
			
			this.active = i;
		},
		
		_xyz: null
	};
	
	
	SS.init();
});