(function() {

var pad = 4;

$(function() {
	$(".devdrag")
		.draggable({
			containment: "parent",
			start: updateCoords,
			drag: updateCoords,
			stop: updateCoords
		})
		.each(function() {
			var display = $("<div class='dev-drag-display'></div>");
			display.css({
				position: "absolute",
				"font-size": "12px",
				"background-color": "yellow",
				color: "black",
				padding: pad + "px"
			});

			$(this).before(display);
			$(this).data("display", display);
			$.proxy(updateCoords, this)();
		})
	;
});

function updateCoords(e) {
	var pos = $(this).position();
	var display = $(this).data("display");
	display.html("top: " + pos.top + "px; left: " + pos.left + "px;"); 

	display.css({
		top: -parseFloat(display.height()) + pos.top - pad*2,
		left: -parseFloat(display.width()) + pos.left - pad*2
	});
};

})();