bfw.events = {
	eventTags: ['Partner Events','Ride','Road Race','Off-Road Race','Road Time Trial','Off-Road Time Trial','Multi-Sport Event','Weekly Club Road Ride','Meeting','Class','Special Event','Trail Building','Cyclocross Race','Fun Ride',]
};

$(function() {
	
	$('input.tagIt').each(function(idx, elem) {
		var $elem = $(elem);
		var tags = window[$elem.data('tags')];
		$elem.tagit({
			availableTags: bfw.events.eventTags//,
			//beforeTagAdded: function(event, ui) {
			//	return bfw.events.eventTags.indexOf(ui.tagLabel) > -1 ? true : false;
			//}
		})
	});
	
	function customRange(input) {
		setTimeout(function(){
			$('.ui-datepicker').css('z-index', 100);
		}, 0);
		
		if (input.id == 'eventEnd') {
			var minDate = new Date($('#eventStart').val());
			minDate.setDate(minDate.getDate() + 1)

			return {
				minDate: minDate
			};
		} else {
			var maxDate = new Date($('#eventEnd').val());
			maxDate.setDate(maxDate.getDate() - 1)

			return {
				maxDate: maxDate
			};
		}
	};

	
	var handleDateChange = function(date, obj) {
		var $this = $(this);
		var $parent = $this.closest('div.input');
		var newDate = $this.datetimepicker('getDate');
		//console.log(newDate);
		var dateObj = new Date(newDate);
		
		var dateString = '';
		
		var year = dateObj.getFullYear();
		var month = (dateObj.getMonth() + 1);
		var day = dateObj.getDate();
		
		var hours = dateObj.getHours();
		var minutes = dateObj.getMinutes();
		var meridian = 'am';
		
		if (hours >= 12) { meridian = 'pm'; }
		if (hours > 12) { hours -= 12; }
		if (hours < 10) { hours = '0' + hours.toString(); }
		if (month < 10) { month = '0' + month.toString(); }
		if (day < 10) { day = '0' + day.toString(); }
		if (minutes < 10) { minutes = '0' + minutes.toString(); }
		
		var $monthFld = $parent.find('input.dateTimePickerMonth');
		var $dayFld = $parent.find('input.dateTimePickerDay');
		var $yearFld = $parent.find('input.dateTimePickerYear');
		
		var $hourFld = $parent.find('input.dateTimePickerHour');
		var $minFld = $parent.find('input.dateTimePickerMin');
		var $meridianFld = $parent.find('input.dateTimePickerMeridian');
		
		$monthFld.val(month);
		$dayFld.val(day);
		$yearFld.val(year);
		$hourFld.val(hours);
		$minFld.val(minutes);
		$meridianFld.val(meridian);
	};
	
	var $dtpt = $('.dateTimePickerTrigger');
	$dtpt.datetimepicker({
		changeMonth: true,
		changeYear: true,
		minuteGrid: 15,
		stepMinute: 15,
		ampm: true,
		beforeShow: customRange,
		dateFormat: 'mm/dd/yy',
		timeFormat: 'hh:mm tt',
		onClose: handleDateChange,
		onSelect: handleDateChange,
		onChangeMonthYear: function(year, month, inst) {
			var $this = $(this);
			var curDate = new Date($this.datetimepicker('getDate'));
			$this.datetimepicker("setDate", month + '/' + curDate.getDate() + '/' + year);
			handleDateChange.call(this);
		},
	});
	
	
});