bfw.groups = {};

$(function() {
	
	bfw.groups = {
		$city: null,
		$state: null,
		$lat: null,
		$lng: null,
		
		registerEditor: function() {
			var ctxt = this;
			this.$city = $('#GroupCity');
			this.$state = $('#GroupState');
			this.$lat = $('#GroupLat');
			this.$lng = $('#GroupLng');
			
			var locationChanged = function(e) {
				bfw.routes.geoCodeAddress(ctxt.$city.val() + ', ' + ctxt.$state.val(), function(latLng) {
					ctxt.$lat.val(latLng.lat());
					ctxt.$lng.val(latLng.lng());
				});
			};
			
			this.$city.change(locationChanged);
			this.$state.change(locationChanged);
		}, // registerEditor()
	
		flds: {
			
		},
		
		registerSearch: function(args) {
			var ctxt = this;
			
			$(document).on('click', '#searchResults tr', {}, function(e) {
				var $this = $(this);
				var $a = $this.find('a');
				DLG.open($a.attr('href'));
			});
			
			
			ctxt.flds = {
				$zip: $('#searchZip'),
				$range: $('#searchRange'),
				$terms: $('#searchTerms'),
				$bikes: $('#searchBikes'),
				$parts: $('#searchParts'),
				$photos: $('#searchOnlyPhotos'),
				$submit: $('#doSearch'),
				$results: $('#searchResults')
			};
			
			if (bfw.geolocation) {
				ctxt.flds.$zip.val(bfw.detectedLocation);	
			}
			
			ctxt.flds.$submit.click(function(e) {
				if (ctxt.flds.$zip.val().trim().length < 1) {
					ctxt.flds.$zip.closest('div').addClass('error');
				} else {
					ctxt.flds.$zip.closest('div').removeClass('error');
					var searchOptions = {
						zip: ctxt.flds.$zip.val(),
						range: ctxt.flds.$range.val(),
						callback: ctxt.displayResults
					};
					
					bfw.search.groups.start(searchOptions);
				}
						
				if (e.preventDefault) { e.preventDefault(); }
				return false;
			});
				
		},
		
		searchError: function(msg, data) {
			alert("Please enter a valid city/state or ZIP code for search");
		},
		
		displayResults: function(data) {
			var tmpl = bfw.templates.search.groups.Group.list;
			
			bfw.groups.flds.$results.empty();
			
			var start_lat = data.request.lat;
			var start_lng = data.request.lng;
			
			if (data.results) {
				var srl = [];
				
				for (var i = 0; i < data.results.length; i++) {
					var id = data.results[i];
					var rec = bfw.getJSONByField('Group', {name: 'all'}, 'id', id);
					if (rec !== false) {
						rec.howFar = bfw.routes.getDistance(rec.lat, rec.lng, start_lat, start_lng);
						srl.push(rec);
					}
				}
				
				srl.sort(function(a, b) {
					return a.howFar > b.howFar;
				});
				
				for (var i = 0; i < srl.length; i++) {
					var rec = srl[i];
					rec.oddEven = i % 2 ? 'even' : 'odd';
					rec.howFar = rec.howFar.toFixed(2);
					var $entry = $(Mustache.render(tmpl, rec));
					bfw.groups.flds.$results.append($entry);	
				}
				
				
				DLG.resizeDialog(undefined, $('body').height());
			}
			
		},
		
		init: function() {
			this.registerEditor();
			this.registerSearch();
		},
		
		_xyz: null
	}; // bfw.groups
	
	bfw.groups.init();
	
});