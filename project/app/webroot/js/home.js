$(function(){
	var scrollTab = document.getElementById("scroll-tip");
	var wheelieEggGreen = document.getElementById("intro-green-rider");
	var wheelieEggOrange = document.getElementById("intro-orange-rider");
    var scrollTime = 2;    //Time (in seconds) for nav elements to scroll from point a to point b 

	// Intro (Top Page) Animationsnav
	// ---------------------------------------------------------------------- //

	//tween the "left" and "top" css properties through the supplied values (notice we put bezier inside the css object and we're passing the array directly to the bezier rather than creating an object with "values" because we're accepting the defaults)
	TweenMax.from("#intro-map", 1.5, {autoAlpha: 0, ease:Strong.easeOut});
	TweenMax.from("#scroll-tip", 1, {top:"-150px", ease:Back.easeOut, delay: .8});
    if (Modernizr.svg) {
        TweenMax.to("#intro-aqua-rider", 1.5, {css:{bezier:{type:"cubic", values:[{x:-40, y:-150}, {x:-95, y:0}, {x:-50, y:0}, {x:13, y:0}], autoRotate:["x","y","rotation",25.1,true]}}, ease:Back.easeInOut});
        TweenMax.from("#intro-orange-rider", 1, {right:"120px", autoAlpha: 0, ease:Strong.easeOut, delay: 1.2});
        TweenMax.from("#intro-green-rider", 1, {right:"320px", autoAlpha: 0, ease:Strong.easeOut, delay: 1.4} );
    }
	TweenMax.to("#sec01-trees", 1, {top:628, autoAlpha: 1, ease:Strong.easeOut, delay: 1.6});



	scrollTab.onmouseover = function(){
		TweenMax.to("#scroll-tip", .5, {top:-20, autoAlpha: 1, ease:Strong.easeOut});
    }
    scrollTab.onmouseout = function(){
    	TweenMax.to("#scroll-tip", .5, {top:-40, autoAlpha: 1, ease:Strong.easeOut});
    }
    //Scroll Intro down to next section
    scrollTab.onclick = function(){
    	bfw.scroll.toRoutes();
    }


    //wheelie easter egg (dudes on map pop a sweet wheelie on mouseover)
    if (Modernizr.svg) {
        wheelieEggGreen.onmouseover = function(){
            TweenMax.to("#intro-green-rider", .5, {rotation: -30, top: "468", ease:Strong.easeOut});
        }
        wheelieEggGreen.onmouseout = function(){
            TweenMax.to("#intro-green-rider", .5, {rotation: 0, top: "473", ease:Strong.easeOut});
        }
        wheelieEggOrange.onmouseover = function(){
            TweenMax.to("#intro-orange-rider", .5, {rotation: 30, top: "220", ease:Strong.easeOut});
        }
        wheelieEggOrange.onmouseout = function(){
            TweenMax.to("#intro-orange-rider", .5, {rotation: 0, top: "228", ease:Strong.easeOut});
        }
    }


    // Show/Hide Top Menu
    // ---------------------------------------------------------------------- //
    var showMenu = function(){
        TweenMax.to("#top-menu", .5, {top: 0, autoAlpha: 1, ease:Strong.easeOut});
    }
    var hideMenu = function(){
        TweenMax.to("#top-menu", .5, {top: "-80", autoAlpha: 0, ease:Strong.easeOut});
    }

    // Section 02 Animations
    // ---------------------------------------------------------------------- //

    var sec02AnimShow = function(){
        if (!bfw.pauseAnimations) {
            TweenMax.to("#sec02-trees", .5, {scaleX: 1, scaleY: 1, top:80, autoAlpha: 1, ease:Back.easeOut, transformOrigin:"center bottom"});
            
            if (Modernizr.svg) {
                TweenMax.to("#sec02-bike-cross", 1, {rotation: 0, autoAlpha: 1, ease:Strong.easeOut, transformOrigin:"left bottom"});
            }
        }
    }
    var sec02AnimHide = function(){
        if (!bfw.pauseAnimations) {
            TweenMax.to("#sec02-trees", .5, {scaleX: .1, scaleY: .1, top:100, autoAlpha: 0, ease:Back.easeOut});
            
            if (Modernizr.svg) {
                TweenMax.to("#sec02-bike-cross", 1, {rotation: -180, autoAlpha: 0, ease:Strong.easeOut, transformOrigin:"left bottom"});
            }
        }
    }

    bfw.scroll = {
            //make duration proportional to distance, but no shorter than 1 second
            scrollElem: function () {
                return $(window)._scrollable();
            },
            duration: function (destination) {
                return Math.max(1000, Math.abs($(window).scrollTop() - destination.offset().top) * 0.4);
            },
            offset: function (elem) {
                var offset = elem.offset().top;
                
                if (elem.data("offset") !== undefined) {
                    offset += parseInt(elem.data("offset"), 10);
                }
                
                return offset;
            },
            easing: "linear",
            before: function () {
                bfw.pauseAnimations = true;
            },
            complete: function () {
                bfw.pauseAnimations = false;
            },
            doScroll: function (elem) {
                bfw.scroll.before();
                
                $(bfw.scroll.scrollElem()).stop(true, false).scrollTo(bfw.scroll.offset(elem), {
                    duration: bfw.scroll.duration(elem),
                    easing: bfw.scroll.easing,
                    onAfter: bfw.scroll.complete
                });
            },
            
			toHome: function() {
                bfw.scroll.doScroll($(".sec01"));
			},
			
			toRoutes: function() {
                bfw.scroll.doScroll($(".sec02"));
			},
			
			toEvents: function() {
                bfw.scroll.doScroll($(".sec03"));
			},
			
			toTips: function() {
                bfw.scroll.doScroll($(".sec04"));
			},
			
			toGetInvolved: function() {
                bfw.scroll.doScroll($(".sec05"));
			},
			
			toGetConnected: function() {
                bfw.scroll.doScroll($(".get-connected"));
			},
			
			_xyz: null
		};
    

    // Navigation ScrollTo events
    // ---------------------------------------------------------------------- //
    $(".btn-home").click(function(event){
        event.preventDefault();
        bfw.scroll.toHome();
    });
    $(".btn-find-ride").click(function(event){
        event.preventDefault();
        bfw.scroll.toRoutes();
    });
    $(".btn-events").click(function(event){
        event.preventDefault();
        bfw.scroll.toEvents();
    });
    $(".btn-riding-tips").click(function(event){
        event.preventDefault();
        bfw.scroll.toTips();
    });
    $(".btn-get-involved").click(function(event){
        event.preventDefault();
        bfw.scroll.toGetInvolved();
    });
    $(".btn-get-connected").click(function(event){
        event.preventDefault();
        bfw.scroll.toGetConnected();
    });



    // Detect Scrolling and trigger animations
    // ---------------------------------------------------------------------- //
    $(window).scroll(function() {
        var top = $(window).scrollTop();
        //console.log(top);
        logToPanel("Scroll detected: " + top)
         // Top Menu animation trigger
        if (top > 680) {
            showMenu();
        }
        if (top < 680){
            hideMenu();
        } 

        // Section 02 animation trigger
        if (top > 400) {
            sec02AnimShow();
        }
        // if (top < 700){
        //     sec02AnimHide();
        // } 
    });

    //hide everything initially
    sec02AnimHide();
    hideMenu();



    // Debug 
    // ---------------------------------------------------------------------- //
    function logToPanel(msg) {
        $('#debug').append(msg +  "<br/>");
        var objDiv = document.getElementById("debug");
		if(objDiv)
			objDiv.scrollTop = objDiv.scrollHeight;
    }

    $("#shower").click(function () {
      TweenMax.to("#debug", 1, {bottom:0, ease:Strong.easeOut});
      //$("#debug").hide(1000);
    });
    $("#hider").click(function () {
      TweenMax.to("#debug", 1, {bottom:-235, ease:Strong.easeOut});
      //$("#debug").hide(1000);
    });
});


$(document).on("click", ".tips-wrapper-wrapper .types a", function(e) {
	var $elem = $(this);
	var type = $elem.data("type");
	var $bg = $elem.siblings(".active-bg").first();
	var $container = $elem.parents(".tips-wrapper-wrapper").first();
	var $targetContent = $(".tipcontent-" + type, $container);

	$bg
		.stop()
		.animate(
			{
				"top": $elem.position().top
			},
			300
		);

	var runOnce = true;
	$targetContent.siblings(".active")
		.stop()
		.fadeOut(200, function() {
			$(this).removeClass("active")
			if(runOnce) {
				//console.log('done');
				$targetContent
					.fadeIn(200)
					.addClass("active");
				runOnce = false;
			}
		});
});

(function() {
	
	function registerCalendarHandlers(opts) {
		
		
		bfw.events.UI = {
			$eventListContainer: $('.search-results-content'),
			$eventList: $('#eventList'),
			$eventKeyword: $('#eventKeyword'),
			$eventStart: $('#eventStartDate'),
			$eventEnd: $('#eventEndDate'),
			$categories: $('#eventCategories'),
			$regions: $('#eventRegions'),
			$submit : $('#eventSubmit'),
			$free: $('#eventFree')
		};
		
		bfw.events.eventTags.sort();
		
		for (var i = 0; i < bfw.events.eventTags.length; i++) {
			var cat = bfw.events.eventTags[i];
			var $cat = $('<option value="' + cat + '">'  + cat + '</option>');
			bfw.events.UI.$categories.append($cat);
		}
		
		var doEventSearch = function(e) {
			var flds = bfw.events.UI;
		
			flds.$eventList.empty();
			
			//var sDate = Math.round((new Date(flds.$eventStart.datetimepicker('getDate'))).getTime() / 1000);
			//var eDate = Math.round((new Date(flds.$eventEnd.datetimepicker('getDate'))).getTime() / 1000);
			var sDate = Math.round((new Date(flds.$eventStart.val())).getTime() / 1000);
			var eDate = Math.round((new Date(flds.$eventEnd.val())).getTime() / 1000);
			
			
			if (sDate == 0) { sDate = Math.round((new Date()).getTime() / 1000); }
			if (eDate == 0) { eDate = 99999999999; }
			
			if (eDate < sDate) {
				var tmp = eDate;
				eDate = sDate;
				sDate = tmp;
			}
			
			
			var search_opts = {
				result_handler: bfw.showCalendarResults,
				region: flds.$regions.val() == '' ? false : flds.$regions.val(),
				category: flds.$categories.val() == '' ? false : flds.$categories.val(),
				free_only: flds.$free.is(':checked'),
				keywords: flds.$eventKeyword.val() == '' ? false : flds.$eventKeyword.val(),
				startTS: sDate,
				endTS: eDate
			};
			
			bfw.search.calendar(search_opts);
			
			
			if (e.preventDefault) { e.preventDefault(); }
			return false;
		};
		
		
		bfw.events.UI.$submit.click(doEventSearch);
		bfw.events.UI.$eventKeyword.keypress(function(e) {
			if (e.charCode == 13) {
				doEventSearch(e);
			}
		});
		
		var datePickerOpts = {
			changeMonth: true,
			changeYear: true,
			showTime: false,
			dateFormat: 'mm/dd/yy',
			timeFormat: ''
		};
		
		var startDatePickerOpts = {
			onClose: function(dateText, inst) {
				if (bfw.events.UI.$eventEnd.val() != '') {
					var testStartDate = bfw.events.UI.$eventStart.datetimepicker('getDate');
					var testEndDate = bfw.events.UI.$eventEnd.datetimepicker('getDate');
					if (testStartDate > testEndDate)
						bfw.events.UI.$eventEnd.datetimepicker('setDate', testStartDate);
				}
				else {
					bfw.events.UI.$eventEnd.val(dateText);
				}
			},
			onSelect: function (selectedDateTime){
				bfw.events.UI.$eventEnd.datetimepicker('option', 'minDate', bfw.events.UI.$eventStart.datetimepicker('getDate') );
			}
		};
		var endDatePickerOpts = {
			onClose: function(dateText, inst) {
				if (bfw.events.UI.$eventStart.val() != '') {
					var testStartDate = bfw.events.UI.$eventStart.datetimepicker('getDate');
					var testEndDate = bfw.events.UI.$eventEnd.datetimepicker('getDate');
					if (testStartDate > testEndDate)
						bfw.events.UI.$eventStart.datetimepicker('setDate', testEndDate);
				}
				else {
					bfw.events.UI.$eventStart.val(dateText);
				}
			},
			onSelect: function (selectedDateTime){
				bfw.events.UI.$eventStart.datetimepicker('option', 'maxDate', bfw.events.UI.$eventEnd.datetimepicker('getDate') );
			}
		};
		
		$.extend(startDatePickerOpts, datePickerOpts);
		$.extend(endDatePickerOpts, datePickerOpts);
		
		bfw.events.UI.$eventStart.datetimepicker(startDatePickerOpts);
		bfw.events.UI.$eventEnd.datetimepicker(endDatePickerOpts);
		
		$(document).on('click', 'tr.evSearchResult', {}, function(e) {
			var $this = $(this);
			var id = $this.data('id');
			DLG.open('/events/view/' + id);
		});
	}
	
	function registerMapHandlers(opts) {
		bfw.routes.UI = {
			$address: $('#rtAddress'),
			$distance: $('#rtDistance'),
			$CBs: $('.rtCBs input[type="checkbox"]'),
			$submit: $('#rtSearch'),
			$searchedFor: $('#rtSearchedFor'),
			$routeList: $('#routeList'),
			$topAddress: $('#rtAddressTop'),
			$topSubmit: $('#rtSearchTop'),
			$toggleBikes: $('#cbToggleBikeLayer')
		};
		
		var flds = bfw.routes.UI;
		
		flds.$toggleBikes.change(function(e) {
			if ($(this).is(':checked')) {
				bfw.bikeLayer.setMap(bfw.map);
			} else {
				bfw.bikeLayer.setMap(null);
			}
		});
		
		
		var doTopSubmit = function(e) {
			flds.$address.val(flds.$topAddress.val());
			flds.$distance.val(50);
			
			bfw.search.map({
				address: flds.$topAddress.val(),
				use_location: false,
				max_dist: 100,
				result_handler: bfw.showMapResults
			});
			
			
			
			bfw.scroll.toRoutes();
			
			if (e.preventDefault) { e.preventDefault(); }
			return false;
		};
		
		var doSubmit = function(e) {
			
			var loc_crit = [];
			var rt_crit = [];
			
			for (var i = 0; i < bfw.routes.UI.$CBs.length; i++) {
				var $cb = $(bfw.routes.UI.$CBs[i]);
				
				var val = $cb.is(':checked');
				var type = $cb.data('type');
				var subType = $cb.data('sub-type');
				if (val === true) {
					if (type == 'location') {
						loc_crit.push(subType);
					} else if (type == 'route') {
						rt_crit.push(subType);
					}
				}
			}
			
			
			bfw.search.map({
				address: flds.$address.val(),
				max_dist: parseInt(flds.$distance.val()),
				result_handler: bfw.showMapResults,
				location_crit: loc_crit,
				route_crit: rt_crit
			});
			if (e.preventDefault) { e.preventDefault(); }
			return false;
		};
		
		
		flds.$topSubmit.click(doTopSubmit);
		flds.$topAddress.keypress(function(e) {
			if (e.charCode == 13) {
				doTopSubmit(e);
			}
		});
		
		flds.$submit.click(doSubmit);
		flds.$address.keypress(function(e) {
			if (e.charCode == 13) {
				doSubmit(e);
			}
		});
		
		$(document).on('click', 'tr.rtSearchResult', {}, function(e) {
			var $this = $(this);
			var id = $this.data('id');
			var type = $this.data('type');
			var key = type + ':' + id;
			bfw.search.openInfoWindow(key);
		});
		
		
	}
	
	function registerClassifiedHandlers(args) {
		
	}
	
	function registerHandlers(opts) {
		registerMapHandlers();
		registerCalendarHandlers();
	}
	
	
	function setupUI(opts) {
		registerHandlers();
		//setupResultsMap();
	}
	
	setupUI();

}())
