console = typeof console !== 'undefined' ? console : false;

$(function() {
	
	if (typeof console.log === 'undefined' || typeof console.log === 'undefined') {
		var $console = $('<div style="position: fixed; left: 10px; bottom: 10px; background: #fff; padding: 10px; width: 90%; height: 200px; overflow: scroll; color: #000; z-index: 10000;"></div>');
		var $log = $('<ul style="list-style-type: none;"></ul>');
		$console.append($log);
		
		$('body').append($console);
		
		console = {
			log: function(msg) {
				$log.append('<li>' + msg + '</li>');
			}
		}
	} else {
		//alert(typeof console.log);
	}
});