/*
 *  Project:
 *  Description:
 *  Author:
 *  License:
 */

// the semi-colon before function invocation is a safety net against concatenated
// scripts and/or other plugins which may not be closed properly.
;(function ( $, window, document, undefined ) {

    // undefined is used here as the undefined global variable in ECMAScript 3 is
    // mutable (ie. it can be changed by someone else). undefined isn't really being
    // passed in so we can ensure the value of it is truly undefined. In ES5, undefined
    // can no longer be modified.

    // window and document are passed through as local variable rather than global
    // as this (slightly) quickens the resolution process and can be more efficiently
    // minified (especially when both are regularly referenced in your plugin).

    // Create the defaults once
    var pluginName = "rowDialog",
        defaults = {
            path: '/users/login'
        };

    // The actual plugin constructor
    function Plugin( element, options ) {
        this.element = element;

        // jQuery has an extend method which merges the contents of two or
        // more objects, storing the result in the first object. The first object
        // is generally empty as we don't want to alter the default options for
        // future instances of the plugin
        this.options = $.extend( {}, defaults, options );

        this._defaults = defaults;
        this._name = pluginName;

        this.init();
    }

    Plugin.prototype = {
				$shadow: null,
				$dialog: null,
				$frame: null,
				setupComplete: false,
		
				setup: function() {
					this.$shadow = $('<div class="row-dialog-shadow"></div>');
					this.$frame = $('<iframe class="row-dialog-frame"></iframe>');
					this.$dialog = $('<div class="row-dialog-wrapper"></div>');
					this.$dialog.append(this.$frame);
					
					$('body').append(this.$shadow).append(this.$dialog);
					this.setupComplete = true;
				},
		
        init: function() {
            // Place initialization logic here
            // You already have access to the DOM element and
            // the options via the instance, e.g. this.element
            // and this.options
            // you can add more functions like the one below and
            // call them like so: this.yourOtherFunction(this.element, this.options).
						if (!this.setupComplete) { this.setup(); }
						this.open(this.options.path);
        },
				
				fit: function() {
					var ctxt = this;
					
					var vp = this.viewPort();
					var dlg = this.dimensions(this.$dialog);
					
					var left = Math.floor((vp.width / 2) - (dlg.width / 2));
					var top = Math.floor((vp.height / 2) - (dlg.height / 2));
					
					$(window).off('resize').on('resize', function(e) {
						ctxt.fit();
					});
					
					//$(window).resize(function() {
					//	ctxt.fit();
					//});
					
					this.$dialog.css({
						left: left,
						top: top
					});
				},
				
				open: function(path) {
					this.$frame.attr('src', path);
					
					this.fit();
					
					this.$shadow.fadeIn(250);
					this.$dialog.fadeIn(250);
					
					//console.log(path);
				},
				
				dimensions: function(elem) {
					var $elem = $(elem);
					
					var w = $elem.width();
					var h = $elem.height();
					
					return {
						width: w,
						height: h
					};
					
				},
				
				viewPort: function() {
					return this.dimensions(window);
					//var viewportWidth = $(window).width();
					//var viewportHeight = $(window).height();
					//
					//return {
					//	width: viewportWidth,
					//	height: viewportHeight
					//}
					
					//$(window).resize(function() {
					//
					//});
				}
    };

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function ( options ) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName, new Plugin( this, options ));
            }
        });
    };

})( jQuery, window, document );