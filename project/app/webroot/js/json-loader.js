bfw.JSON = {};
bfw.getJSON = function(type, opts) {
	var o = {name: 'all', field: 'id', val: 0};
	$.extend(o, opts);

	var keyName = type + (o.name !== 'all' ? '-' + o.name : '')

	if (bfw.JSONloaded(type, opts)) {
		for (var i = 0; i < bfw.JSON[keyName].length; i++) {
			var json = bfw.JSON[keyName][i];
			if (typeof json[o.field] !== 'undefined' && json[o.field] == o.val) {
				return json;
			}
		}
	}

	return false;
};


bfw.loadJSON = function(type, opts) {
	var o = {name: 'all', after: null};
	$.extend(o, opts);

	var url = '/js/data/' + type + '/' + o.name + '.json';

	var keyName = type + (o.name !== 'all' ? '-' + o.name : '')

	$.ajax({
		url: url,
		dataType: 'json',
		data: {},
		type: 'get',
		error: function(xhr, status, err) {
			bfw.JSON[keyName] = [];
			if (typeof o.error === 'function') {
				o.error(xhr, status, err);
			}
		},
		success: function(data, status, xhr) {
			for (var i = 0; i < data.length; i++) {
				if (typeof data[i].dataType === 'undefined') { data[i].dataType = type; }
				if (typeof data[i].start_lat !== 'undefined' && typeof data[i].start_lng !== 'undefined') {
					data[i].lat = data[i].start_lat
					data[i].lng = data[i].start_lng
				}
			}

			//bfw.taffy[keyName] = new TAFFY(data);

			bfw.JSON[keyName] = data;
			if (typeof o.after === 'function') {
				o.after(data);
			}
		}
	});
};

bfw.JSONloaded = function(type, opts) {
	var o = {name: 'all'};
	$.extend(o, opts);
	var keyName = type + (o.name !== 'all' ? '-' + o.name : '');
	return typeof bfw.JSON[keyName] !== 'undefined';
};

bfw.getJSONByField = function(type, opts, field, val) {
	var ret_val = false;
	if (this.JSONloaded(type, opts)) {
		var o = {name: 'all'};
		$.extend(o, opts);
		var keyName = type + (o.name !== 'all' ? '-' + o.name : '');

		var set = bfw.JSON[keyName];
		for (var i = 0; i < set.length; i++) {
			var rec = set[i];
			if (typeof rec[field] !== 'undefined' && rec[field] == val) {
				ret_val = rec;
				return ret_val;
			}
		}
	}

	return ret_val;
};