
(function($) {

var defaultPointOptions = {
	autoFollow: false,
	followType: "walk", //walk, bike, drive
	avoidHighways: true,
	units: "imperial" //imperial, metric
};

var pointSymbol = {
	fillColor: "#FFFF00",
	fillOpacity: 1,
	path: google.maps.SymbolPath.CIRCLE,
	strokeWeight: 0,
	scale: 5
};

var pointSymbolStart = $.extend({}, pointSymbol, {
	fillColor: "#00ff00"
});
var pointSymbolEnd = $.extend({}, pointSymbol, {
	fillColor: "#ff0000"
});

var addPointSymbol = $.extend({}, pointSymbol, {
	fillColor: "#0000ff",
	fillOpacity: .7,
	scale: 8
});
var addPointMarkerOptions = {
	clickable: true,
	cursor: "pointer",
	draggable: false,
	flat: true,
	icon: addPointSymbol,
	optimized: false,
	position: new google.maps.LatLng(0,0),
	raiseOnDrage: false,
	visible: false,
	zIndex: 500
};

var DS = new google.maps.DirectionsService();

RouteDraw = function(gmap, userOptions) {
	var self = this;
	
	this.map = gmap;
	
	this.history = [];
	this.points = [];
	
	
	
	this.options = {
		units: "imperial", //imperial, metric
		wheelZoom: true,
		centerOnAdd: false,
		showDistanceMarkers: true
	};
	
	this.pointOptions = defaultPointOptions;
	
	if(typeof userOptions == "object")
		$.extend(this.options, userOptions);
	
	this._clear();

	//handle map click
	google.maps.event.addListener(gmap, "click", function(e) {
		self.addPoint(e.latLng, self.pointOptions, -1);
	});
};


	
RouteDraw.prototype.redraw = function(force) {
	var self = this;

	var len = this.points.length;
	for(var i = 0; i != len; i++) {
		var p = this.points[i];
		if(force || p.changed) {
			if(i > 0) {
				p.update(true);
			}
		}
	}
	
	//this._redraw();
};
RouteDraw.prototype._redraw = function(reload) {
	this.lineArr.clear();
	
	var pLen = this.points.length;
	for(var i=0; i != pLen; i++) {
		var points = this.points[i].points;
		for(var x=0; x != points.length; x++) {
			this.lineArr.push(points[x]);
		}
	}

	this.line.setPath(this.lineArr);
};

RouteDraw.prototype.setOption = function(option, value) {
	this.options[option] = value;
	
	switch(option) {
		
	}
	
	//this.redraw();
};

RouteDraw.prototype.undo = function() {
	if(this.history.length > 0) {
		var item = this.history.pop();
		switch(item.action) {
			case "reverse":
				this.points = this.points.reverse();
				//this.redraw(true);
				break;
			default:
				console.log("can't undo: " + item.action);
		}
		
	}
};

RouteDraw.prototype.clear = function() {
	var msg = "Are you sure you want to clear the map?";
	if(confirm(msg)) {
		this._clear();
	}
};
RouteDraw.prototype._clear = function() {
	this.history = [];
	
	var pLen = this.points.length;
	for(var i=0; i != pLen; i++) {
		this.points[i].clear();
	}
	this.points = [];
};

//accepts point or lat, lng
RouteDraw.prototype.centerOn = function(p, lng) {
	var latLng = false;
	if(typeof lng != "undefined") {
		latLng = new google.maps.LatLng(p, lng);
	} else {
		latLng = p.latLng;
	}
	this.map.panTo(latLng);
};

RouteDraw.prototype.outAndBack = function() {
	if(this.points.length > 1) {			
		var pLen = this.points.length;
		//var rPoints = [];
		for(var i = pLen-2; i >= 0; i--) {
			//var newP = this.points[i].clone();
			//newP.options = this.points[i+1].options;
			//rPoints.push(newP);
			this.addPoint(this.points[i].latLng, this.points[i+1].options);
		}
		//this.points = this.points.concat(rPoints);
		/*
		this.history.push({
			action: "outandback",
			data: rPoints
		});
		*/
		//this.redraw();
	}
};

RouteDraw.prototype.reverse = function() {
	this.points = this.points.reverse();

	var pLen = this.points.length;
	if(pLen > 0) {		
		for(var i=0; i < pLen; i++) {
			var p = this.points[i];

			if(i > 0)
				p.prevPoint = this.points[i-1];
			else
				p.prevPoint = null;
			
			if(i < pLen-1)
				p.nextPoint = this.points[i+1];
			else
				p.nextPoint = null;
				
			p.clear();
			
		}
		
		var holdOptions = this.points[pLen-1].options;
		for(i = pLen-1; i > 0; i--) {
			this.points[i].options = this.points[i-1].options;
			this.points[i].update(true);
		}
		this.points[0].options = holdOptions;
		this.points[0].update(true);
	
		this.points[0].updateMarker();
		this.points[pLen-1].updateMarker();
	}

	
	this.history.push({
		action: "reverse",
		data: null
	});
};

RouteDraw.prototype.returnToStart = function() {
	if(this.points.length > 1) {
		//var p = this.points[0].clone();
		//this.addPoint(p, this.pointOptions);
		var latLng = this.points[0].latLng;
		//this.lineArr.push(new google.maps.LatLng(latLng.lat(), latLng.lng()));
		this.addPoint(new google.maps.LatLng(latLng.lat(), latLng.lng()));
	}
};

RouteDraw.prototype.addPoint = function(latLng, pOptions, index) {
	var useOptions = $.extend({}, this.pointOptions);
	if(typeof pOptions == "object") {
		$.extend(useOptions, pOptions);
	}
	
	var p = new RouteDrawPoint(latLng, useOptions, this.map, this);

	if(typeof index == "undefined" || index < 0) {
		this.points.push(p);
	} else {
		this.points.splice(index, 0, p);
	}
	
	i = this.points.indexOf(p);

	if(i != -1) {
		if(i > 0) {
			p.prevPoint = this.points[i-1];
			this.points[i-1].nextPoint = p;
		}
		
		if(i < this.points.length-1) {
			p.nextPoint = this.points[i+1];
			this.points[i+1].prevPoint = p;
		}
	}
	if(p.prevPoint)
		p.prevPoint.updateMarker();
	if(p.nextPoint)
		p.nextPoint.updateMarker();
	
	
	//p.marker.setMap(this.map);
	p.update(true);
	
	
	
	if(this.options.centerOnAdd) {
		this.centerOn(p);
	}
	
	
	this.history.push({
	action: "addPoint",
	data: {
		"p": p,
		"pOptions": useOptions
	}
	});
};

RouteDraw.prototype.removePoint = function(index) {
	var p = this.points[index];
	p.clear();
	this.points.splice(index, 1);
	if(index < this.points.length) {
		this.points[index].changed = true;
	}
	
	this.history.push({
		action: "removePoint",
		data: {
			"p": p,
			"index": index
		}
	});
};


RouteDrawPoint = function(latLng, options, map, parent) {
	this.latLng = latLng;
	this.parent = parent;
	this.map = map;
	this.latLng.obj = this;
	this.points = [this.latLng];
	this.prevPoint = null;
	this.nextPoint = null;
	
	this.marker = new google.maps.Marker({
		clickable: true,
		cursor: "pointer",
		draggable: true,
		flat: true,
		icon: pointSymbol,
		optimized: false,
		position: this.latLng,
		raiseOnDrag: false,
		zIndex: 1000
	});
	
	//google.maps.event.addListener(this.marker, "drag", $.proxy(this.handleDrag, this));
	google.maps.event.addListener(this.marker, "dragend", $.proxy(this.handleDragEnd, this));
	
	this.addPointMarker = new google.maps.Marker(addPointMarkerOptions)
	this.addPointMarker.setMap(this.map);
	
	
	this.lineArr = new google.maps.MVCArray([]);
	this.line = new google.maps.Polyline({
		clickable: true,
		draggable: false,
		editable: false,
		strokeWeight: 6
	});
	this.line.setPath(this.lineArr);
	
	this.setLineEvents();
		
	this.options = $.extend({}, defaultPointOptions);
	if(typeof options == "object")
		$.extend(this.options, options);
	
	this.changed = true;
};

RouteDrawPoint.prototype.clone = function() {
	var p = new RouteDrawPoint(new google.maps.LatLng(this.latLng.lat(), this.latLng.lng()), this.options, this.map, this.parent);
	return p;
};

RouteDrawPoint.prototype.handleDrag = function(e) {
	this.latLng = e.latLng;
	
	if(this.nextPoint) {
		this.nextPoint.update();
	}
	this.update();
};
RouteDrawPoint.prototype.handleDragEnd = function(e) {
	this.latLng = e.latLng;
	
	if(this.nextPoint) {
		this.nextPoint.update(true);
	}
	this.update(true);
};

RouteDrawPoint.prototype.update = function(force) {
	this.changed = false;
	
	this.marker.setMap(this.map);
	this.line.setMap(this.map);
	
	if(this.prevPoint != null) {
		if(this.options.autoFollow && force) {
			var followType = google.maps.TravelMode.BICYCLING;
			switch(this.options.followType) {
				case "walk":
					followType = google.maps.TravelMode.WALKING;
					break;
				case "bike":
					followType = google.maps.TravelMode.BICYCLING;
					break;
				case "drive":
					followType = google.maps.TravelMode.DRIVING;
					break;
			}
			
			var units = google.maps.UnitSystem.METRIC;
			switch(this.options.units) {
				case "imperial":
					units = google.maps.UnitSystem.IMPERIAL;
					break;
				case "metric":
					units = google.maps.UnitSystem.METRIC;
					break;
			}
		
			var dirRequest = {
				avoidHighways: this.options.avoidHighways,
				destination: this.latLng,
				durationInTraffic: false,
				origin: this.prevPoint.latLng,
				provideRouteAlternatives: false,
				travelMode: followType,
				unitSystem: units
			};
			
			DS.route(dirRequest, $.proxy(this.handleDS, this));
			
		} else {
			//no route
			this.points = [
				this.prevPoint.latLng,
				this.latLng
			];
			
			this.redraw();
		}
	} else {
		//singleton
	}
	
	if(force)
		this.updateMarker();
};

RouteDrawPoint.prototype.handleDS = function(dirResult, dirStatus) {
	if(dirStatus == google.maps.DirectionsStatus.OK) {
		var leg = dirResult.routes[0].legs[0];
		this.leg = leg;
		var steps = leg.steps;
		var paths = [];
		for(var i=0; i != steps.length; i++) {
			paths = paths.concat(steps[i].path);
		}
		
		this.points = paths;
		this.simplePoints = dirResult.routes[0].overview_path;
	} else {
		console.log('DirRequest error: ' + dirStatus);
		
		this.points = [
			this.prevPoint.latLng,
			this.latLng
		];
		
	}
	
	this.redraw();
};

RouteDrawPoint.prototype.redraw = function() {
	this.line.setPath(this.points);
};

RouteDrawPoint.prototype.clear = function() {
	this.marker.setMap(null);
	this.line.setMap(null);
	this.line.getPath().clear();
}

RouteDrawPoint.prototype.updateMarker = function() {
	var type = "";
	if(!this.nextPoint)
		type = "end";
	if(!this.prevPoint)
		type = "start";
		
	switch(type) {
		case "start":
			this.marker.setIcon(pointSymbolStart);
			break;
		case "end":
			this.marker.setIcon(pointSymbolEnd);
			break;
		default:
			this.marker.setIcon(pointSymbol);
			break;
	}
};

RouteDrawPoint.prototype.setLineEvents = function() {
	var self = this;
	this.clearLineEvents();

	google.maps.event.addListener(this.line, "mouseover", function(e) {
		self.addPointMarker.setPosition(e.latLng);
		self.addPointMarker.setVisible(true);
	});
	google.maps.event.addListener(this.line, "mousemove", function(e, pe) {
		//console.log(typeof e);
		self.addPointMarker.setPosition(e.latLng);
	});
	google.maps.event.addListener(this.line, "mouseout", function(e) {
		self.addPointMarker.setVisible(false);
	});
	
	google.maps.event.addListener(this.addPointMarker, "click", function(e) {
		var myIndex = self.parent.points.indexOf(self);
		//console.log(e);
		//console.log(myIndex);
		self.parent.addPoint(e.latLng, {}, myIndex);
	});
};

RouteDrawPoint.prototype.clearLineEvents = function() {
	google.maps.event.clearInstanceListeners(this.lineArr)
	google.maps.event.clearInstanceListeners(this.addPointMarker)
}


function confirm(msg) {
	return window.confirm(msg);
}


})(jQuery);