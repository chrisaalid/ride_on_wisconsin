var bfw = {
	mobile: {
		$: {},
		geolocation: false,
		detectedLocation: false,
		mapSearched: false,
		
		init: function() {
			var ctxt = this;
			
			ctxt.getLocation(function() {
				ctxt.startMapSearch();
			});
			
			ctxt.selectElements();
			
			bfw.loadJSON('Template', {
				after: function(data) {
					bfw.templates = data;
	
					bfw.loadJSON('Route', {
						after: function(data) {
							ctxt.startMapSearch();
						}
					});
	
					bfw.loadJSON('Event', {
						after: function(data) {
							
						}
					});
				}
			});
		}, // init()
		
		selectElements: function() {
			var ctxt = this;
			
			ctxt.$.routeList = $('#routeList table');
			ctxt.$.useFullSite = $('#useFullSite');
		}, // selectElements()
		
		getLocation: function(cb) {
			var ctxt = this;
			ctxt.geolocation = false;
	
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function(position) {
	
					var loc = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
	
					bfw.routes.reverseGeoCode(loc, function(addr) {
						var search = addr.city !== false && addr.state !== false ? addr.city + ', ' + addr.state : (addr.zip !== false ? addr.zip : '');
						ctxt.detectedLocation = search;
						
						if (typeof cb == 'function') {
							cb(loc, search);
						}
					});
				}, function() {
	
				});
			}
		}, // getLocation()
		
		startMapSearch: function() {
			var ctxt = this;
			
			if (ctxt.mapSearched) { return; }
			if (ctxt.detectedLocation == false) { return; }
			if (!bfw.JSONloaded('Route')) { return; }
			
			ctxt.mapSearched = true;
			
			bfw.search.map({
				address: ctxt.detectedLocation,
				result_handler: function(data) {
					ctxt.displayMapResults(data)
				},
				use_location: false
			});
		},
		
		displayMapResults: function(data) {
			var ctxt = this;
			
			ctxt.$.routeList.empty();
			
			for (var i = 0; i < data.length; i++) {
				var r = data[i];
				r.oddEven = i % 2 ? 'even' : 'odd';
				var list_tmpl = bfw.templates.search.map[r.dataType].list;
				//var info_tmpl = bfw.templates.search.map[r.dataType].info;
				
				var $entry = $(Mustache.render(list_tmpl, r));
				ctxt.$.routeList.append($entry);
				//bfw.addMapResult({
				//	idx: i,
				//	obj: r,
				//	listTmpl: list_tmpl,
				//	infoTmpl: info_tmpl,
				//	paginate: 6
				//});
			}
			
			
			//console.log(data);
		}, // displayMapResults()
		
		_xyz: null
	}
};