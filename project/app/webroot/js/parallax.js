$(window).on('load', function () {
    //add parallax effect to skylines
    $(".bg-skyline").attr("data-stellar-ratio", "1.1").attr("data-stellar-vertical-offset", ($(window).height() * 0.10));
    $(window).stellar();
});