var DLG = parent.DLG || false;
var bfw = parent.bfw || {};

$(function() {

	if (DLG !== false) {
		DLG.loaded();

		if (typeof DLG.ready === 'function') {
			if (typeof readyFunc === 'function') {
				DLG.ready(readyFunc);
			} else {
				var $dlg = $('.dialog-wrap');

				if ($dlg.length > 0) {

					var height = $dlg.height();
					var width = $dlg.width() + 15;

					DLG.ready(function() {
						DLG.setMaxDim(width, height);
						DLG.fit();
					});
				}
			}
		}

		parent.location.hash = '#!' + document.location.pathname;

	} else {
		window.location.href = '/#!' + window.location.pathname;
	}

});