var DLG = {};


$(function() {
	//var $dlg = $('#dialogWrapper');
	//var $iframe = $('#dialogIframe');

	DLG = {
		noOpen: false,
		$shadow: null,
		$spinner: null,
		$dialog: null,
		$content: null,
		$closer: null,
		$frame: null,
		openingTimer: null,

		callbacks: {
			onLoad: function(o) {},
			onBeforeOpen: function(o) {},
			onBeforeClose: function(o) {},
			onAfterClose: function(o) {},
			onResize: function(w, h) {}
		},


		$dlgTrig: null,

		setupComplete: false,
		options: {},

		registerHandlers: function() {
			var ctxt = this;

			$(document).on('click', 'a.row-dialog-trigger', {}, function(e) {
				var $this = $(this);
				var action = $this.data('dialog-action');
				ctxt.open(action);
				if (e.preventDefault) {
					e.preventDefault();
				}

				return false;
			});

			this.$closer.click(function(e) {
				ctxt.close();
				if (e.preventDefault) {
					e.preventDefault();
				}

				return false;
			});
		},

		registerCallbacks: function(o) {
			if (typeof o.onLoad === 'function') { this.callbacks.onLoad = o.onLoad; }
			if (typeof o.onBeforeOpen === 'function') { this.callbacks.onBeforeOpen = o.onBeforeOpen; }
			if (typeof o.onBeforeClose === 'function') { this.callbacks.onBeforeClose = o.onBeforeClose; }
			if (typeof o.onAfterClose === 'function') { this.callbacks.onAfterClose = o.onAfterClose; }
			if (typeof o.onResize === 'function') { this.callbacks.onResize = o.onResize; }

		},

		setup: function(o) {
			var ctxt = this;

			o = typeof o === 'undefined' ? {} : o;

			$.extend(this.options, this.options, o);

			this.registerCallbacks(o);

			this.$shadow = $('<div class="row-dialog-shadow"></div>');
			//this.$spinner = $('<img src="/images/spinner.gif" class="spinner" />');
			this.$spinner = $('<img src="/images/drive-train-preloader.gif" class="spinner" />');

			this.$frame = $('<iframe class="row-dialog-frame"></iframe>');

			this.$content = $('<div class="row-dialog-content"></div>');
			this.$closer = $('<div class="row-dialog-closer">X</div>');
			this.$dialog = $('<div class="row-dialog-wrapper"></div>');

			//this.$shadow.append(this.$spinner);

			this.$content.append(this.$frame);

			this.$dialog.append(this.$closer).append(this.$content);

			$('body').append(this.$shadow).append(this.$spinner).append(this.$dialog);

			$(window).on('resize', function(e) {
				ctxt.fit();
			});

			this.$dlgTrig = $('.row-dialog-trigger')
			this.registerHandlers();

			var hash = window.location.hash;

			if ($.trim(hash).length > 2) {
				hash = hash.substring(2);
				this.open(hash);
			}


			this.setupComplete = true;
		},

		dimensions: function(elem) {
			var $elem = $(elem);

			var w = $elem.width();
			var h = $elem.height();

			return {width: w, height: h};

		},

		setMaxDim: function(w, h) {
			this.$dialog.data('max-width', w).data('max-height', h);
		},

		fit: function() {
			var ctxt = this;

			var dlg = this.dimensions(this.$dialog);
			var vp = this.viewport();

			dlg.width = Math.min(vp.width, this.$dialog.data('max-width'));
			dlg.height = Math.min(vp.height - 100, this.$dialog.data('max-height'));

			if (dlg.height < 200) { dlg.height = 200;}

			var dleft = Math.floor((vp.width / 2) - (dlg.width / 2));
			var dtop = Math.floor((vp.height / 2) - (dlg.height / 2));

			if (dtop < 50) { dtop = 50; }

			this.$dialog.css({
				left: dleft,
				top: dtop,
				width: dlg.width,
				height: dlg.height
			});
		},

		hideSpinner: function() {
			this.$spinner.fadeOut(250);
		},

		showSpinner: function() {
			var spn = this.dimensions(this.$spinner);
			var vp = this.viewport();

			var sleft = Math.floor((vp.width / 2) - (spn.width / 2));
			var stop = Math.floor((vp.height / 2) - (spn.height / 2));

			this.$spinner.css({
				left: sleft,
				top: stop
			});

			this.$spinner.fadeIn(250);
		},

		open: function(path) {
			var ctxt = this;

			if (typeof this.callbacks.onBeforeOpen === 'function') {
				this.callbacks.onBeforeOpen(path);
			}

			if (!this.$dialog.is(':visible')) {
				var vp = this.viewport();

				var dw = vp.width;
				var dh = vp.height;

				var init_dim = 100;

				this.$dialog.css({
					'left': (Math.floor(dw / 2) - Math.floor(init_dim / 2)) + 'px',
					'top': (Math.floor(dh / 2) - Math.floor(init_dim / 2)) + 'px',
					'width': init_dim + 'px',
					'height': init_dim + 'px'
				});
			}

			this.shadowFadeIn(path);

			this.showSpinner();

			this.$frame.attr('src', path);
			this.openingTimer = setTimeout(function() {
				if (typeof window.frames[0].stop == 'function') {
					window.frames[0].stop();
				} else {
					window.frames[0].document.execCommand('Stop');
				}

				ctxt.close();

				alert("Uh oh!  Something went wrong on our end.  We're working to fix the problem.  Please reload the page and try again.  Sorry!");

			}, 15000);


		},

		shadowFadeIn: function (path) {
			path = typeof path == 'undefined' ? '' : path;
			if (typeof this.callbacks.onBeforeOpen === 'function') {
				this.callbacks.onBeforeOpen(path);
			}
			this.$shadow.fadeIn(250);
		},

		shadowFadeOut: function (duration) {
			var ctxt = this;
			
			if (typeof this.callbacks.onBeforeClose === 'function') {
				this.callbacks.onBeforeClose();
			}
			
			this.$shadow.fadeOut(duration, function() {
				if (typeof ctxt.callbacks.onAfterClose === 'function') {
					ctxt.callbacks.onAfterClose();
				}
			});
		},

		close: function() {
			var ctxt = this;

			if (typeof this.callbacks.onBeforeClose === 'function') {
				this.callbacks.onBeforeClose();
			}

			clearTimeout(this.openingTimer);
			var vp = this.viewport();

			var dw = vp.width;
			var dh = vp.height;
			var end_w = 100;
			var end_h = 100;

			var new_css = {
				width: end_w,
				height: end_h,
				left: (Math.floor(dw / 2) - Math.floor(end_w / 2)),
				top: (Math.floor(dh / 2) - Math.floor(end_h / 2))
			}

			var duration = 250;
			this.shadowFadeOut(duration);

			this.hideSpinner();
			this.$dialog.animate(new_css, duration).fadeOut(duration);
			window.location.hash = '';
		},

		navTo: function(url) {
			document.location = url;
		},

		ready: function(f) {
			if (typeof f === 'function') {
				f(this);
			}
		},

		resizeDialog: function(w, h) {
			var vp = this.viewport();

			var dw = vp.width;
			var dh = vp.height;

			if (typeof w === 'undefined') {
				w = 870;
			}

			if (typeof h === 'undefined') {
				h = vp.height - 100;
			} else {
				h += 30
			}

			if (h > dh - 100) {
				h = dh - 100;
			}

			if (h < 200) {
				h = 200
			}


			var cx = Math.floor(dw / 2);
			var cy = Math.floor(dh / 2);
			var l = Math.floor(cx - (w / 2));
			var t = Math.floor(cy - (h / 2));

			var resized_css = {
				position: 'fixed',
				left: l,
				top: t,
				width: w,
				height: h
			};

			if (typeof this.callbacks.onResize === 'function') {
				var ret_val = this.callbacks.onResize(resized_css);
				if (typeof ret_val !== 'undefined' && typeof ret_val.left !== 'undefined' && typeof ret_val.top !== 'undefined' && typeof ret_val.width !== 'undefined' && typeof ret_val.height !== 'undefined') {
					resized_css = ret_val;
				}
			}

			//this.$dialog.css(resized_css);
			this.$dialog.animate(resized_css, 250);
		},

		viewport: function() {
			var e = window, a = 'inner';
			if (!( 'innerWidth' in window ) ) {
				a = 'client';
				e = document.documentElement || document.body;
			}
			return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
		},

		loaded: function(opts) {
			this.$spinner.hide();
			this.$dialog.fadeIn(250);
			clearTimeout(this.openingTimer);
			if (typeof this.callbacks.onLoad === 'function') {
				this.callbacks.onLoad(opts);
			}
		},

		_xyz: null
	}; // DLG


	//DLG.setup();



});