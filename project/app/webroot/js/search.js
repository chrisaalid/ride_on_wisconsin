bfw.search = {};

$(function() {
	
	bfw.search = {
		
		
		
		resultsMarkers: [],
		infoWindows: [],
		activeInfoWindow: null,

		/*** Locations/Routes ***/
		map: function(opts) {
			var ctxt = this;
			var o = {
				type: 'address',
				address: '53022',
				max_dist: 50,
				use_route: true,
				use_location: true,
				result_handler: function(data) {
					//console.log('Search Results:');
					//console.log(data);
				},
				location_crit: ['bike-shop','lodging','restaurant','mtb-trail-head'],
				route_crit: ['road','off-road']
			};
			
			$.extend(o, opts);
			
			if (o.type == 'address') {
				bfw.routes.geoCodeAddress(o.address, function(latLng) {
					o.latLng = latLng;
					ctxt.doMapSearch(o);
				});
			} else {
				ctxt.doMapSearch(o);
			}
		},
		
		openInfoWindow: function(key) {
			if (typeof bfw.search.infoWindows[key] !== 'undefined') {
				if (bfw.search.activeInfoWindow !== null) {
					bfw.search.activeInfoWindow.close();
				}
				
				var infowindow = bfw.search.infoWindows[key];
				var marker = bfw.search.resultsMarkers[key];
				
				bfw.map.panTo(marker.getPosition());
				bfw.map.setZoom(12);
				
				infowindow.open(bfw.map, marker);
				bfw.search.activeInfoWindow = infowindow;
			}
		},
		
		clearResults: function() {
			for (var m in this.resultsMarkers) {
				this.resultsMarkers[m].setMap(null);
			}
			
			if (typeof bfw.routes.UI !== 'undefined') {
				bfw.routes.UI.$routeList.empty();
			}
			
			this.resultsMarkers = [];
			this.infoWindows = [];
			this.activeInfoWindow = null;
		},
		
		doMapSearch: function(opts) {
			var o = {};
			
			var boundsBuilder = new google.maps.LatLngBounds();
			
			this.clearResults();
			
			$.extend(o, opts);
			
			var latLng = o.latLng;
			
			boundsBuilder.extend(latLng);
			
			if (typeof bfw.map !== 'undefined') {
				bfw.map.panTo(latLng);
			}
			
			var matches = [];
			
			function searchType(type, filter) {
				if (bfw.JSONloaded(type)) {
					for (var i = 0; i < bfw.JSON[type].length; i++) {
						var r = bfw.JSON[type][i];
						var dist = bfw.routes.getDistance(latLng.lat(), latLng.lng(), r.lat, r.lng);
						
						r.howFar = dist;
						r.dispHowFar = dist.toFixed(2);
						
						if (r.howFar < o.max_dist) {
							var filter_ok = typeof filter === 'function' ? filter(r) : true;
							if (filter_ok) {
								matches.push(r);
								boundsBuilder.extend(new google.maps.LatLng(r.lat,r.lng));
							}
						}
					} // for()
				} // loaded?
			}
			
			if (o.use_location) {
				searchType('Location', function(loc) {
					if (opts.location_crit.length > 0) {
						for (var i = 0; i < opts.location_crit.length; i++) {
							var lc = opts.location_crit[i];
							if (loc.subType == lc) {
								return true;
							}
						}
					}
					return false;
				});
			}
			
			
			
			if (o.use_route) {
				searchType('Route', function(loc) {
					return ($.inArray('road', opts.route_crit) >= 0 && !loc.offroad) || ($.inArray('off-road', opts.route_crit) >= 0 && loc.offroad);
				});
			}
			
			function distComp(a,b) {
				if (a.howFar < b.howFar) {
					 return -1;
				} else if (a.howFar > b.howFar) {
					return 1;
				} else {
					return 0;
				}
			}
			
			matches.sort(distComp);
			
			
			if (typeof o.result_handler === 'function') {
				o.result_handler(matches, boundsBuilder);
			}
			
			
		},
		
		/*** Events ***/
		calendar: function(opts) {
			
			var def_start = Math.round(new Date().getTime() / 1000);
			var def_end = 99999999999;
			
			var o = {
				'free_only': false,
				'startTS': def_start,
				'endTS': def_end,
				'category': false,
				'region': false,
				'keywords': false
			}; 
			
			var matches = [];
			
			$.extend(o, opts);
			
			o.startTS = isNaN(o.startTS) ? def_start : o.startTS;
			o.endTS = isNaN(o.endTS) ? def_end : o.endTS;
			
			function searchType(type, filter) {
				for (var i = 0; i < bfw.JSON[type].length; i++) {
					var ev = bfw.JSON[type][i];
					
					var filter_ok = typeof filter === 'function' ? filter(ev) : true;
					
					if (filter_ok) {
						matches.push(ev);
					}
				}
			}
			
			
			searchType('Event', function(ev) {
				if (ev.startTS >= o.startTS) {
					if (ev.endTS <= o.endTS) {
						if (o.free_only === false || ev.free) {
							if (o.region === false || $.inArray(o.region, ev.regions) >= 0) {
								if (o.category === false || $.inArray(o.category, ev.tags) >= 0) {
									ev.normName = ev.name.toLowerCase();
									ev.normDetail = ev.details.toLowerCase();
									
									if (o.keywords === false || bfw.search.string([ev.normName, ev.normDetail], o.keywords)) {//ev.name.toLowerCase().indexOf(o.keywords.toLowerCase()) !== -1 || ev.details.toLowerCase().indexOf(o.keywords.toLowerCase()) !== -1) {
										return true;
									}
 								}
							}
						}
					}
				}
				return false;
			});
			
			function dateComp(a,b) {
				if (a.startTS < b.startTS) {
					 return -1;
				} else if (a.startTS > b.startTS) {
					return 1;
				} else {
					return 0;
				}
			}
			
			matches.sort(dateComp);
			
			
			if (typeof o.result_handler === 'function') {
				o.result_handler(matches);
			}
			
		},
		
		string: function(haystacks, needles, opts) {
			if (typeof haystacks.length !== 'undefined' && typeof needles.length !== 'undefined') {
				if (typeof haystacks !== 'object') { haystacks = haystacks.split(/\s+/); }
				if (typeof needles !== 'object') { needles = needles.split(/\s+/); }
				
				haystacks = $.map(haystacks, function(o, i) {
					return o.toLowerCase();
				});
				
				needles = $.map(needles, function(o, i) {
					return o.toLowerCase();
				});
				
				for (var hi = 0; hi < haystacks.length; hi++) {
					var h = haystacks[hi];
					for (var ni = 0; ni < needles.length; ni++) {
						var n = needles[ni];
						if (h.indexOf(n) >= 0) {
							return true;
						}
					}
				}
			}
			
			return false;
		},
		
		/*** Groups ***/
		
		groups: {
			start: function(opts) {
				var ctxt = this;
				var o = {
					zip: 53704,
					lat: 0,
					lng: 0,
					bikes: true,
					parts: true,
					onlyImages: false,
					range: 50,
					callback: function() {
						
					},
					error: function(msg, data) {
						
					}
				};
				
				$.extend(o, opts);
				
				bfw.routes.geoCodeAddress(o.zip, function(latLng) {
					o.lat = latLng.lat();
					o.lng = latLng.lng();
					ctxt.latLngSearch(o);
				}, o.error);
			}, // startSearch()
			
			latLngSearch: function(opts) {
				var cb = typeof opts.callback === 'function' ? opts.callback : function() { /* ... */ };
				delete opts.callback;
				
				$.ajax('/groups/search/query', {
					data: opts,
					dataType: 'json',
					type: 'POST',
					
					error: function(xhr, status, err) {
						console.log("ERROR!");
						console.log(err);
					},
					success: function(data, status, xhr) {
						cb(data);
					}
				});
			}
		},
		
		/*** Classifieds ***/
		
		classifieds: {
			start: function(opts) {
				var ctxt = this;
				var o = {
					zip: 53704,
					lat: 0,
					lng: 0,
					bikes: true,
					parts: true,
					onlyImages: false,
					range: 50,
					callback: function() {
						
					},
					error: function(msg, data) {
						
					}
				};
				
				$.extend(o, opts);
				
				bfw.routes.geoCodeAddress(o.zip, function(latLng) {
					o.lat = latLng.lat();
					o.lng = latLng.lng();
					ctxt.latLngSearch(o);
				}, o.error);
			}, // startSearch()
			
			latLngSearch: function(opts) {
				var cb = typeof opts.callback === 'function' ? opts.callback : function() { /* ... */ };
				delete opts.callback;
				
				$.ajax('/classifieds/search/query', {
					data: opts,
					dataType: 'json',
					type: 'POST',
					
					error: function(xhr, status, err) {
						console.log("ERROR!");
						console.log(err);
					},
					success: function(data, status, xhr) {
						cb(data);
					}
				});
			}
		},
		
		_xyz: null
	}; // bfw.search
	
	
});