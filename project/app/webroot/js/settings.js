$(function() {
	bfw.settings = {
	  dataTypes: {},
	  $chooser: $('#dataTypeChooser'),
	  $inputs: $('.contentInput'),
	  $realInput: $('#realInput'),
	  activeType: 'string',
	  
	  typeChanged: function() {
			var val = bfw.settings.$chooser.val();
			var dts = bfw.settings.dataTypes[val];
			bfw.settings.activeType = val;
			$('.contentInput').hide();
			$('#inpType_' + dts.input).show();
	  },
	  
	  setRealVal: function(val) {
			bfw.settings.$realInput.val(val);
	  },
	  
	  init: function(opts) {
			if (typeof opts.dataTypes !== 'undefined') {
				bfw.settings.dataTypes = opts.dataTypes;
			}
			
			bfw.settings.$chooser.change(function(e) {
				bfw.settings.typeChanged();
			});
			
			bfw.settings.$inputs.keyup(function(e) {
				bfw.settings.setRealVal($(this).val());
			});
			
			$('form').submit(function(e) {
				var s_type = bfw.settings.dataTypes[bfw.settings.activeType];
				if (typeof s_type.validation !== 'undefined') {
				var rgx = new RegExp(s_type.validation);
				if (!rgx.test(bfw.settings.$realInput.val())) {
					$('#errorMessage').html(s_type.invalid).show();
					e.preventDefault();
					return false;	
				}
				}
				$('#errorMessage').hide();
				return true;
			});
			
			bfw.settings.typeChanged();
		}
	}; // bfw.settings
});