bfw.templates = {};

$(function() {
	
	bfw.templates = {
		search: {
			map: {
				Route: {
					list: '<tr class="rtSearchResult route" data-type="{{dataType}}" data-id="{{id}}">\
										<td class="icon"><span aria-hidden="true" class="icon-path"></td>\
										<td class="search-info">\
												<h4>{{name}} </h4>\
												<span class="result-type">{{difficultyLabel}} Ride/Trail {{distance}} miles</span> <br>\
												{{city}}, {{state}}{{#howFar}} - {{dispHowFar}} miles away{{/howFar}}\
										</td>\
									</tr>',
					
					info:		'<div class="content{{dataType}}">\
											<strong>{{name}}</strong><br>\
											{{city}}, {{state}} - {{distance}} miles<br>\
											<a href="#!/{{ctrl}}/view/{{id}}" class="row-dialog-trigger" data-dialog-action="/{{ctrl}}/view/{{id}}">View Details</a>\
										</div>'
				},
				Location: {
					list: '<tr class="rtSearchResult location" data-type="{{dataType}}" data-id="{{id}}">\
										<td class="icon"><span aria-hidden="true" class="icon-location icon-{{subType}}"></td>\
										<td class="search-info">\
												<h4>{{name}} </h4>\
												<span class="result-type">{{subTypeName}}</span> <br>\
												{{city}}, {{state}}{{#howFar}} - {{dispHowFar}} miles away{{/howFar}}\
										</td>\
									</tr>',
					
					info:	'<div class="content{{dataType}}">\
										<strong>{{name}}</strong><br>\
										<span>{{subTypeName}}</span><br>\
										<div class="address">\
										 {{address}}<br>\
										 {{city}}, {{state}}<br>\
										</div>\
										<a href="#!/{{ctrl}}/view/{{id}}" class="row-dialog-trigger" data-dialog-action="/{{ctrl}}/view/{{id}}">View Details</a>\
									</div>'
				}
			},
			
			calendar: {
				Event: {
					list: '<tr class="evSearchResult" data-type="{{dataType}}" data-id="{{id}}">\
									<td class="date">{{startDate}}<!-- Sat Jun 2, 2013--></td>\
									<td class="search-info">\
										<h4>{{name}}</h4>\
										<span class="location">{{venue}}, {{city}}, {{state}}</span>\
									</td>\
								</tr>'
				}
			}
		},
		slideshow: {
			Route: {
				detail: '<div class="routeDetail" data-route-id="{{id}}">\
										<h3>{{name}}</h3>\
										<span class="location">{{city}}, {{state}}</span>\
									</div>',
				slide: '<img src="/uploads/fetch/{{firstImage}}/450x250" class="slide" data-route-id="{{id}}" alt="Slide" >',
				nav: '<a class="index" data-route-id="{{id}}"></a>'
			}
		}
	};
	
});