$(function() {
	var dateRangeRgx = /(\d+)\/(\d+)\/(\d+)(\s*-\s*(\d+)\/(\d+)\/(\d+))?/i;


	var entityHelper = {
		fractionRgx: /(\d+)\/(\d+)/g,
		
		replaceEntities: function(markup) {
			if (typeof markup == 'undefined') { return ''; }
			var val = markup;
			
			var ents = this.languages.general;
			
			for (var i = 0; i < ents.length; i++) {
				var en = ents[i];
				var rgx = new RegExp(en.match, 'g');
				val = val.replace(rgx, en.entity);
			}
			
			entityHelper.fractionRgx.lastIndex = 0;
			while (entityHelper.fractionRgx.test(val)) {
				entityHelper.fractionRgx.lastIndex = 0;
				var fracMatch = entityHelper.fractionRgx.exec(val);
				val = val.replace(fracMatch[0], specialFrac(fracMatch[1], fracMatch[2]));
			}
			return val;
		},
		
		languages: {
			general: [
				{'entity': '&copy;',   'match': '\u00A9'},
				{'entity': '&deg;',    'match': '\u00B0'},
				{'entity': '&reg;',    'match': '\u00AE'},
				{'entity': '&trade;',  'match': '\u2122'},
				{'entity': '&frac14;', 'match': '\u00BC'},
				{'entity': '&frac12;', 'match': '\u00BD'},
				{'entity': '&frac34;', 'match': '\u00BE'},
				{'entity': '&frac14;', 'match': '1/4'},
				{'entity': '&frac12;', 'match': '1/2'},
				{'entity': '&frac34;', 'match': '3/4'},
				{'entity': '&ldquo;',  'match': '\u201C'},
				{'entity': '&rdquo;',  'match': '\u201D'},
				{'entity': '&lsquo;',  'match': '\u2018'},
				{'entity': '&rdquo;',  'match': '\u2019'},
				{'entity': '&ndash;',  'match': '\u2013'},
				{'entity': '&mdash;',  'match': '\u2014'},
				{'entity': '&#8226;',  'match': '\u2022'},
			],
			
			spanish: [
				{'entity': '&aacute;', 'match': '\u00E1'},
				{'entity': '&eacute;', 'match': '\u00E9'},
				{'entity': '&iacute;', 'match': '\u00ED'},
				{'entity': '&ntilde;', 'match': '\u00F1'},
				{'entity': '&oacute;', 'match': '\u00F3'},
				{'entity': '&uacute;', 'match': '\u00FA'},
				{'entity': '&Aacute;', 'match': '\u00C1'},
				{'entity': '&Eacute;', 'match': '\u00C9'},
				{'entity': '&Iacute;', 'match': '\u00CD'},
				{'entity': '&Ntilde;', 'match': '\u00D1'},
				{'entity': '&Oacute;', 'match': '\u00D3'},
				{'entity': '&Uacute;', 'match': '\u00DA'},
				{'entity': '&iquest;', 'match': '\u00BF'},
				{'entity': '&iexcl;',  'match': '\u00A1'},
				{'entity': '&Uuml;',   'match':	'\u00DC'},
				{'entity': '&uuml;',   'match': '\u00FC'},
				{'entity': '&laquo;',  'match': '\u00AB'},
				{'entity': '&raquo;',  'match': '\u00BB'},
			]
		} // mappings by language
	};	
	
	var schema_columns = {
		user_id: 'user_id',
		name: 'name',
		start: 'start',
		end: 'end',
		venue: 'venue',
		address: 'address',
		state: 'state',
		city: 'city',
		sponsor: 'sponsor',
		email: 'email',
		phone: 'phone',
		url: 'url',
		details: 'notes'
	};
	
	var cleanupRecord = function(rec) {
		if (dateRangeRgx.test(rec.dates)) {
			var m = dateRangeRgx.exec(rec.dates);
			
			// 1970-01-01 00:00:01
			var start = (1 * m[3] + 2000) + '-' + m[1] + '-' + m[2] + ' ' + rec.starttime;
			var end = (1 * m[3] + 2000) + '-' + m[1] + '-' + m[2] + ' ' + rec.endtime;
			if (typeof m[4] !== 'undefined') {
				end = (1 * m[7] + 2000) + '-' + m[5] + '-' + m[6] + ' ' + rec.endtime;
			}
			
			rec.start = start;
			rec.end = end;
		}
		
		rec.name = entityHelper.replaceEntities(rec.name);
		rec.notes = entityHelper.replaceEntities(rec.notes);
		
		
		rec.user_id = 1;
		
		return rec;
	};
	
	var makeInsertable = function(rec) {
		var fields = [];
		for (var p in schema_columns) {
			var val = rec[schema_columns[p]];
			if (typeof val == 'undefined') {
				val = '';
			}
			
			val = '' + val;
			if (/^\d+$/.test(val)) {
				fields.push(val);
			} else {
				fields.push('"' + val.replace(/"/gm, '\\' +'"').replace(/(\r\n|\n|\r)/gm, " ") + '"');
			}
		}
		
		return '(' + fields.join(',') + '),';
	};
	
	
	setTimeout(function() {

		var $entries = $('.eventEntry');
		
		var sql = [];
		
		var records = [];
		for (var i = 0; i < $entries.length; i++) {
			var $e = $($entries.get(i));
			var $children = $e.find('div');
			var fields = {};
			for (var l = 0; l < $children.length; l++) {
				var $c = $($children.get(l));
				var val = $c.html();

				fields[$c.data('field-name')] = val;
			}
			
			var rec = cleanupRecord(fields)
			
			sql.push(makeInsertable(rec));
			records.push(rec);
		}
	
	
		var lead = 'INSERT INTO `Events`';
		var cols = [];
		for (var p in schema_columns) {
			cols.push('`' + p + '`');
		}
		sql.unshift(lead + ' (' + cols.join(',') + ') VALUES');
	
		console.log(sql.join("\n"));	
		//console.log(JSON.stringify(records));
		
	}, 3000);
});