var TREES = {};

$(function() {
	var MAX_X = document.width;
	var MAX_Y = document.height;
	var addMode = false;
	
	
	TREES.init = function(o) {
		var selector = o.selector || false;
		var $selector;
		var trees = o.trees || [];
		addMode = o.addMode === true ? true : false;
		
		TREES.treesRevealed = [];
		TREES.myTrees = [];
		TREES.placed = [];
		
		if (selector === false) {
			TREES.paper = Raphael(0, 0, MAX_X, MAX_Y);
		} else {
			$selector = $(selector);
			MAX_X = $selector.width();
			MAX_Y = $selector.height();
			TREES.paper = Raphael($selector.get(0), MAX_X, MAX_Y);
		}
		
		trees.sort(function(a, b) {
			return (a.y - b.y);
		});
						
		
		TREES.addTree(trees);
		
		Raphael.el.showGrow = function() {
			var bbox = this.getBBox();
			this.animate({'opacity': 1, 'transform': 't0 -30 s2 2 ' + (bbox.x + (bbox.width / 2)) + ' ' + (bbox.y + bbox.height)}, 500, '<>');
		};
		
		Raphael.el.hideWither = function() {
			var bbox = this.getBBox();
			this.animate({'opacity': 0, 'transform': 't0 30 s1 1 ' + (bbox.x - (bbox.width * 2)) + ' ' + (bbox.y - bbox.height)}, 500, '<>');
		};

		var revealTrees = function() {
			var y_pos = $(window).scrollTop();
			
			var top_y = y_pos;
			var bot_y = y_pos + screen.availHeight;
            
            if (!bfw.pauseAnimations) {
			
                for (var i = 0; i < TREES.myTrees.length; i++) {
                    var tree = TREES.myTrees[i];
                    var bbox = tree.getBBox();
                    
                    if (!TREES.treesRevealed[i] && bbox.y >= top_y + 100 && bbox.y + bbox.height <= bot_y - 300) {
                        tree.showGrow();
                        TREES.treesRevealed[i] = true;
                    } else if (!o.growOnly && TREES.treesRevealed[i] && (bbox.y < top_y || bbox.y + bbox.height > bot_y)) {
                        tree.hideWither();
                        TREES.treesRevealed[i] = false;
                    }
                }
            }
		};
		
		if (addMode) {
			
			
			$selector.click(function(e) {
				
				
				if (e.button === 0) {
					console.log(e);
					console.log($selector.css('margin-left'));
					
					var pos = $selector.position();
					
					var rel_x = e.offsetX; //e.pageX - pos.left;
					var rel_y = e.offsetY; //e.pageY - pos.top;
					
					var tree_params = {
						x: rel_x,
						y: rel_y,
						c: 2
					};
					
					tree_params = TREES.randomOptions(tree_params);
					
					var tree = TREES.addTree(tree_params);
					tree.showGrow();
					
					var tAr = TREES.placed;
					
					tAr.sort(function(a, b) {
						return (a.y - b.y);
					});
					
					var dbg = [];
					for (var i = 0, len = tAr.length; i < len; i++) {
						var t = tAr[i];
						dbg.push('	{"x": ' + t.x + ', "y": ' + t.y + ', "c": ' + t.c + ', "w": ' + t.w + ', "h": ' + t.h + ', "tw": ' + t.tw + ', "th": ' + t.th + '}');
					}
					//console.clear();
					console.log("var treeDefs = [\n" + dbg.join(",\n") + "\n];");
					
				}
				
				if (e.preventDefault) { e.preventDefault(); }
				return false;
			
				
			
			});
		}
		
		$(window).scroll(revealTrees);
		
		setTimeout(revealTrees, 1000);
	};
	
	TREES.params = {
		maxHeight: 100,
		minHeight: 30,
		maxWidth: 50,
		minWidth: 20,
		maxTrunkHeight: 15,
		minTrunkHeight: 10,
		maxTrunkWidth: 10,
		minTrunkWidth: 5,
		maxX: MAX_X,
		maxY: MAX_Y,
		pointSkew: 0.10,
		colors: ['85A56C','533E25','EA9D34','C9D9B9','EEE8D9', 'BBE3BF']
	}
	
	
	var mkint = function(f) {
		return Math.round(f);
	};
	
	TREES.addTree = function(o) {
		if (typeof o === 'object' && typeof o.length !== 'undefined') {
			for (var i = 0; i < o.length; i++) {
				var td = o[i];
				TREES.addTree(td);
			}
			return TREES.myTrees;
		}
		
		var x = o.x || mkint(Math.random() * TREES.params.maxX);
		var y = o.y || mkint(Math.random() * TREES.params.maxY);
		
		var w = o.w || mkint((Math.random() * (TREES.params.maxWidth - TREES.params.minWidth)) + TREES.params.minWidth);
		var h = o.h || mkint((Math.random() * (TREES.params.maxHeight - TREES.params.minHeight)) + TREES.params.minHeight);
		var th = o.th || mkint((Math.random() * (TREES.params.maxTrunkHeight - TREES.params.minTrunkHeight)) + TREES.params.minTrunkHeight);
		var tw = o.tw || mkint((Math.random() * (TREES.params.maxTrunkWidth - TREES.params.minTrunkWidth)) + TREES.params.minTrunkWidth);
		var c = typeof o.c === 'undefined' ? mkint(Math.random() * TREES.params.colors.length) : o.c;
		var mps = w * TREES.params.pointSkew;
		
		TREES.placed.push(o);
		
		y = y - (h/2) + 30;
		
		w /= 2;
		h /= 2;
		tw /= 2;
		th /= 2;
		
		c = TREES.params.colors[c];
		
		var ps = o.ps || Math.random() * mps;
		
		if (typeof o.ps === 'undefined' && Math.random() > 0.5) { ps = -ps; }

		
		// drawn from base of trunk
		var cmds = '';
		cmds += 'M' + mkint(x+(tw/2)) + ',' + mkint(y + h);        // right trunk base
		cmds += 'L' + mkint(x-(tw/2)) + ',' + mkint(y + h);        // left trunk base 
		cmds += 'L' + mkint(x-(tw/2)) + ',' + mkint(y + (h - th)); // left crown/trunk junction
		cmds += 'L' + mkint(x-(w/2)) + ',' + mkint(y + (h - th));  // left base of crown
		cmds += 'L' + mkint(x + ps) + ',' + (y);                   // top
		cmds += 'L' + mkint(x+(w/2)) + ',' + mkint(y + (h - th));  // right base of crown
		cmds += 'L' + mkint(x+(tw/2)) + ',' + mkint(y + (h - th)); // right crown/trunk junction
		cmds += 'Z';
		
		var tree = TREES.paper.path(cmds);
		tree.attr({
			'fill': '#' + c,
			'stroke': '#' + c,
			'opacity': 0
		});
		
		var tIdx = TREES.myTrees.length;

		if (addMode) {
			var hoverIn = function() {
				var s = '';
				s += tIdx + ') ' + o.x + ', ' + o.y + ' -- width: ' + o.w + ', height: ' + o.h + ', color: ' + o.c + ' (#' + TREES.params.colors[o.c] + ')';
				console.log(s);
			};
			
			var hoverOut = function() {
				
			};
			
			tree.hover(hoverIn, hoverOut, tree, tree);
		}
		
		TREES.myTrees.push(tree);
		TREES.treesRevealed.push(false);
		
		return tree;
	};
	
	TREES.randomOptions = function(o) {
		var x = o.x || mkint(Math.random() * TREES.params.maxX);
		var y = o.y || mkint(Math.random() * TREES.params.maxY);
		
		var w = o.w || mkint((Math.random() * (TREES.params.maxWidth - TREES.params.minWidth)) + TREES.params.minWidth);
		var h = o.h || mkint((Math.random() * (TREES.params.maxHeight - TREES.params.minHeight)) + TREES.params.minHeight);
		var th = o.th || mkint((Math.random() * (TREES.params.maxTrunkHeight - TREES.params.minTrunkHeight)) + TREES.params.minTrunkHeight);
		var tw = o.tw || mkint((Math.random() * (TREES.params.maxTrunkWidth - TREES.params.minTrunkWidth)) + TREES.params.minTrunkWidth);
		var c = typeof o.c === 'undefined' ? mkint(Math.random() * TREES.params.colors.length) : o.c;
		var mps = w * TREES.params.pointSkew;
		
		var opts = {
			x: x,
			y: y,
			w: w,
			h: h,
			th: th,
			tw: tw,
			c: c
		};
		
		return opts;
	}
	
	TREES.randTree = function() {
		var x = mkint(Math.random() * TREES.params.maxX);
		var y = mkint(Math.random() * TREES.params.maxY);
		
		var w = mkint((Math.random() * (TREES.params.maxWidth - TREES.params.minWidth)) + TREES.params.minWidth);
		var h = mkint((Math.random() * (TREES.params.maxHeight - TREES.params.minHeight)) + TREES.params.minHeight);
		var th = mkint((Math.random() * (TREES.params.maxTrunkHeight - TREES.params.minTrunkHeight)) + TREES.params.minTrunkHeight);
		var tw = mkint((Math.random() * (TREES.params.maxTrunkWidth - TREES.params.minTrunkWidth)) + TREES.params.minTrunkWidth);
		var c = mkint(Math.random() * (TREES.params.colors.length - 1));
		var mps = w * TREES.params.pointSkew;
		
		var ps = Math.random() * mps;
		
		if (Math.random() > 0.5) { ps = -ps; }
		
		var tr = TREES.addTree({
			x: x,
			y: y,
			w: w,
			h: h,
			th: th,
			tw: tw,
			c: c,
			ps: ps
		});
		
		return tr;
	};
	
	TREES.addRandom = function(cnt) {
		for (var i = 0; i < cnt; i++) {
			TREES.randTree();
		}
	}
});