$(function() {
	var $footer = $('div.footer');
	var $loggedInFooter = $footer.find('.loggedIn');
	var $loggedOutFooter = $footer.find('.loggedOut');
	var $centerWrapper = $footer.find('.centerWrapper');
	var polling = false;

	var showFooter = function() {
		if ($footer.data('displayed') != 'yes') {
			$footer.fadeIn(250);
			$footer.data('displayed', 'yes');
		}
	};

	var resizeAndPositionFooter = function(footer) {
		var $dispFooter = $(footer);
		$footer.show(10, function() {
			$dispFooter.show(10, function() {
				var vp = DLG.viewport();
				var tot_li_width = 0;
				var $LIs = $dispFooter.find('li');
				for (var i = 0; i < $LIs.length; i++) {
					var $li = $($LIs[i]);
					tot_li_width += $li.outerWidth();
				};
				
				
				
				$centerWrapper.css({
					'left': '' + ((vp.width / 2) - (tot_li_width / 2)) + 'px',
					'width': tot_li_width + 'px'
				});
				
			});
		});
	};
	
	var loggedOut = function() {
		$loggedInFooter.hide();
		resizeAndPositionFooter($loggedOutFooter);
	};

	var loggedIn = function(user) {

		$loggedInFooter.find('.myaccount a').data('dialog-action', '/users/view/' + user.id);
		$loggedInFooter.find('.welcome').html(Mustache.render($loggedInFooter.find('.welcome').data('message'), user));

		$loggedOutFooter.hide();
		resizeAndPositionFooter($loggedInFooter);
	};

	var updateStatus = function(error, data) {
		polling = false;
		
		
		
		if (typeof data !== 'undefined') {
			if (typeof data.loggedIn !== 'undefined' ) {
				if (data.loggedIn === true && typeof data.user !== 'undefined') {
				loggedIn(data.user);
				} else {
				loggedOut();
				}
			} else {
				loggedOut();
			}
		} else {
			loggedOut();
		}
	};

	var poll = function() {
		if (polling) { return; }

		polling = true;

		$.ajax({
		url: '/users/who',
		dataType: "json",
		error: function(data) {
			updateStatus(true);
		},
		success: function(data, status, xhr) {
			updateStatus(false, data);
		}
		});
	};

	poll();
	setInterval(poll, 1000 * 60);

	var $body = $('body');
	var lastScrollPos = $body.scrollTop();

	DLG.setup({
		onLoad: function(o) {
			poll();
		},
		onBeforeOpen: function(o) {
			lastScrollPos = $body.scrollTop();
			$body.css({'overflow':'hidden'});
		},
		onAfterClose: function(o) {
			$body.css({'overflow':'auto'});

			setTimeout(function() {
				$body.scrollTop(lastScrollPos);
			}, 10);
		}
	});

});