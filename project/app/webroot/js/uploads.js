$(function() {
	
	var fileSelected = function(e) {
		var $this = $(this);
		var $list = $this.closest('ul');
		var ul_cnt = $list.find('li').length;
		var autoAdd = $list.hasClass('autoAdd');
		var max = $list.data('max');
		
		if (autoAdd && ul_cnt < max) {
			
		
			var $newInput = $this.clone();
			var $li = $('<li></li>');
			$li.append($newInput);
			var id = $newInput.attr('id').toString().replace(/_\d+/, '_' + (ul_cnt + 1));;
			var name = $newInput.attr('name').toString().replace(/_\d+/, '_' + (ul_cnt + 1));;
			
			$newInput.attr('id', id).attr('name', name);
			
			
			$newInput.on('change', fileSelected);
			
			$list.append($li);
		}
	}
	
	$('.fileUploads	input[type="file"]').on('change', fileSelected);
});