<div class="dialog-wrap lrg">
  <div class="dl-header user-login">
	  <?php echo $this->element('dialog-header', array('title'=>'Not Found', 'icon'=>'x')); ?>
  </div><!-- /.dl-header -->
  <div class="dl-body">
	  <div class="section">
		  <p>Hmm, we goofed and somehow you wound up here.  Sorry about that.</p>
	  </div><!-- /.section -->
  </div><!-- /.dl-body -->
</div><!-- /.dialog-wrap-sm -->  