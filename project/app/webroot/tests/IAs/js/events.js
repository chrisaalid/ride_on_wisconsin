var stateList = ["AK","AL","AR","AS","AZ","CA","CO","CT","DC","DE","FL","GA","GU","HI","IA","ID","IL","IN","KS","KY","LA","MA","MD","ME","MH","MI","MN","MO","MS","MT","NC","ND","NE","NH","NJ","NM","NV","NY","OH","OK","OR","PA","PR","PW","RI","SC","SD","TN","TX","UT","VA","VI","VT","WA","WI","WV","WY"];
var cityList = ["Milwaukee","Madison","Green Bay","Kenosha","Racine","Appleton","Waukesha","Oshkosh","Eau Claire","Janesville","West Allis","La Crosse","Sheboygan","Wauwatosa","Fond du Lac","New Berlin","Wausau","Brookfield","Beloit","Franklin","Greenfield","Menomonee Falls","Oak Creek","Manitowoc","West Bend","Sun Prairie","Superior","Mount Pleasant","Stevens Point","Neenah","Fitchburg","Caledonia","Muskego","Watertown","De Pere","Mequon","South Milwaukee","Grand Chute","Pleasant Prairie","Germantown","Marshfield","Wisconsin Rapids","Cudahy","Onalaska","Middleton","Menasha","Ashwaubenon","Howard","Menomonie","Beaver Dam","Oconomowoc","Kaukauna","River Falls","Bellevue","Whitewater","Hartford","Whitefish Bay","Greendale","Allouez","Chippewa Falls","Weston","Pewaukee","Shorewood","Glendale","Hudson","Stoughton","Fort Atkinson","Plover","Waunakee","Baraboo","Brown Deer","Bellevue Town","Two Rivers","Grafton","Cedarburg","Salem","Suamico","Waupun","Richfield","Port Washington"];
var latlng = new google.maps.LatLng(-34.397, 150.644);

var geocoder;
var gmap;
var marker;
var zoom = 7;

var mapOptions = {
	mapTypeId: google.maps.MapTypeId.HYBRID,
	center: latlng,
	'zoom': zoom,
	disableDefaultUI: true,
	draggable: false,
	zoomControl: false,
	scrollwheel: false,
	disableDoubleClickZoom: true
};

var eventTags = ['Partner Events','Ride','Road Race','Off-Road Race','Road Time Trial','Off-Road Time Trial','Multi-Sport Event','Weekly Club Road Ride','Meeting','Class','Special Event','Trail Building','Cyclocross Race','Fun Ride',];

var regions = [
	{
		name: 'Wisconsin',
		points: [
			{x:-90.637207, y:47.338821},
			{x:-89.956055, y:47.279228},
			{x:-90.307617, y:46.649437},
			{x:-90.065918, y:46.346928},
			{x:-88.352051, y:46.057983},
			{x:-87.604980, y:45.644768},
			{x:-87.583008, y:45.151054},
			{x:-87.011719, y:45.537136},
			{x:-86.462402, y:45.367584},
			{x:-87.011719, y:43.929550},
			{x:-87.033691, y:42.488300},
			{x:-90.615234, y:42.536892},
			{x:-91.208496, y:42.698586},
			{x:-91.450195, y:43.786957},
			{x:-92.812500, y:44.527843},
			{x:-93.032227, y:45.537136},
			{x:-92.768555, y:46.088470},
			{x:-92.395020, y:46.164616},
			{x:-92.351074, y:46.785015},
			{x:-90.637207, y:47.338821}
		]
	},{
		name: 'Central Wisconsin',
		points: [
			{x:-91.384277,y:45.151054},
			{x:-90.681152,y:43.405048},
			{x:-88.659668,y:43.436966},
			{x:-88.725586,y:44.964798},
			{x:-91.384277,y:45.151054}
		]
	},{
		name: 'Western Wisconsin',
		points: [
			{x:-89.670410,y:46.271038},
			{x:-89.494629,y:42.504501},
			{x:-90.769043,y:42.536892},
			{x:-91.340332,y:42.714733},
			{x:-91.494141,y:43.834526},
			{x:-92.790527,y:44.527843},
			{x:-93.054199,y:45.521744},
			{x:-92.812500,y:46.073231},
			{x:-92.373047,y:46.164616},
			{x:-92.416992,y:46.860191},
			{x:-90.549316,y:47.353710},
			{x:-90.043945,y:47.234489},
			{x:-90.241699,y:46.604168},
			{x:-90.065918,y:46.346928},
			{x:-89.670410,y:46.271038}
		]
	},{
		name: 'Eastern Wisconsin',
		points: [
			{x:-89.406738,y:42.520699},
			{x:-89.604492,y:46.271038},
			{x:-88.308105,y:46.088470},
			{x:-87.583008,y:45.644768},
			{x:-87.561035,y:45.151054},
			{x:-86.901855,y:45.598667},
			{x:-86.374512,y:45.290348},
			{x:-86.967773,y:43.929550},
			{x:-87.011719,y:42.488300},
			{x:-89.406738,y:42.520699}
		]
	},{
		name: 'Southern Wisconsin',
		points: [
			{x:-92.153320,y:44.197960},
			{x:-86.901855,y:44.103367},
			{x:-87.033691,y:42.488300},
			{x:-90.703125,y:42.504501},
			{x:-91.318359,y:42.779274},
			{x:-91.560059,y:43.866219},
			{x:-92.153320,y:44.197960}	
		]
	},{
		name: 'Northern Wisconsin',
		points: [
			{x:-86.923828,y:44.134914},
			{x:-92.263184,y:44.213711},
			{x:-92.812500,y:44.559162},
			{x:-93.054199,y:45.506348},
			{x:-92.790527,y:46.134171},
			{x:-92.329102,y:46.210251},
			{x:-92.351074,y:46.830135},
			{x:-90.615234,y:47.338821},
			{x:-89.978027,y:47.219566},
			{x:-90.285645,y:46.664516},
			{x:-90.109863,y:46.377254},
			{x:-88.330078,y:46.057983},
			{x:-87.604980,y:45.644768},
			{x:-87.561035,y:45.213005},
			{x:-86.901855,y:45.583290},
			{x:-86.352539,y:45.274887},
			{x:-86.923828,y:44.134914}
		]
	},{
		name: 'The Driftless',
		points: [
			{x:-92.219238,y:44.166447},
			{x:-90.329590,y:43.945374},
			{x:-89.714355,y:43.405048},
			{x:-89.472656,y:42.617790},
			{x:-90.659180,y:42.472095},
			{x:-91.340332,y:42.666283},
			{x:-91.604004,y:43.802818},
			{x:-92.219238,y:44.166447}
		]
	}
];

function isPointInPoly(poly, pt){
	for (var c = false, i = -1, l = poly.length, j = l - 1; ++i < l; j = i)
		((poly[i].y <= pt.y && pt.y < poly[j].y) || (poly[j].y <= pt.y && pt.y < poly[i].y))
		&& (pt.x < (poly[j].x - poly[i].x) * (pt.y - poly[i].y) / (poly[j].y - poly[i].y) + poly[i].x)
		&& (c = !c);
	return c;
}

function initialize() {
	geocoder = new google.maps.Geocoder();
	//var latlng = new google.maps.LatLng(-34.397, 150.644);
	gmap = new google.maps.Map(document.getElementById("locationMap"), mapOptions);
	showAddress("Wisconsin");
}

function showAddress(address) {
	geocoder.geocode( { 'address': address}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			gmap.setCenter(results[0].geometry.location, zoom);
			
			var pt = {x: results[0].geometry.location.Za, y: results[0].geometry.location.Ya};
			var inRegions = [];
			for (var i = 0; i < regions.length; i++) {
				var r = regions[i];
				if (isPointInPoly(r.points, pt)) {
					inRegions.push(r.name);
				}
			}
			$('#eventRegionsDisp').html(inRegions.join(', '));
			$('#eventRegions').val(inRegions.join(', '));
			
			if (marker == null){
				marker = new google.maps.Marker({
					map: gmap,
					position: results[0].geometry.location,
					'zoom': zoom
				});
			} else {
				marker.setPosition(results[0].geometry.location);
				gmap.setZoom(zoom);
			}
			
			//marker = new google.maps.Marker({
			//	map: gmap,
			//	position: results[0].geometry.location
			//});
		} else {
			console.log("Geocode was not successful for the following reason: " + status);
		}
	});
}

initialize();


$(function() {
	function customRange(input) {
		if (input.id == 'eventEnd') {
			var minDate = new Date($('#eventStart').val());
			minDate.setDate(minDate.getDate() + 1)

			return {
				minDate: minDate
			};
		} else {
			var maxDate = new Date($('#eventEnd').val());
			maxDate.setDate(maxDate.getDate() - 1)

			return {
				maxDate: maxDate
			};
		}
	};
	
	
	$('.dateTimePicker').datetimepicker({
		changeMonth: true,
		changeYear: true,
		timeFormat: "hh:mm tt",
		minuteGrid: 15,
		stepMinute: 15,
		beforeShow: customRange
	});
	
	$('input.tagIt').tagit({
		availableTags: eventTags
	});
	
	$('input.city').autocomplete({
		source: cityList
	});
	
	$('input.state').autocomplete({
		source: stateList
	});
	
	$('.location input').change(function() {
		var $parent = $(this).closest('div.location');
		
		var $address = $parent.find('input.address');
		var $city = $parent.find('input.city');
		var $state = $parent.find('input.state');
		//var address = $address.val().trim() + ', ' + $city.val().trim() + ', ' + $state.val().trim();
		
		var parts = [];
		
		if ($address.length > 0 && $address.val().trim().length > 0) { parts.push($address.val().trim()); }
		if ($city.length > 0 && $city.val().trim().length > 0) { parts.push($city.val().trim()); }
		if ($state.length > 0 && $state.val().trim().length > 0) { parts.push($state.val().trim()); }
		
		var address = parts.join(', ');
		
		zoom = 7;
		if ($state.length > 0 && $state.val().trim().length > 0) {
			zoom = 7
			if ($city.length > 0 && $city.val().trim().length > 0) {
				zoom = 10;
				if ($address.length > 0 && $address.val().trim().length) {
					zoom = 16;
				}
			}
		}
		
		showAddress(address);
		
		//var url = 'http://maps.google.com/maps?q=' + encodeURIComponent($address.val().trim() + ', ' + $city.val().trim() + ', ' + $state.val().trim()) +  '&amp;ie=UTF8&amp;hq=&amp;hnear=Madison,+Dane,+Wisconsin&amp;gl=us&amp;t=h&amp;z=11&amp;ll=43.073052,-89.40123&amp;output=embed';
		
		//$('#locationFrame').attr('src', url);
	});
	
});