$(function() {
	
	var $form = $('form.mock');
	if ($form.length > 0) {
		var $fields = $form.find('input, select, textarea');
		var model = $form.data('model');
		var modelRgx = new RegExp("^" + model);
		
		var modelPC = model.substring(0, 1).toUpperCase() + model.substring(1) + 's';
		
		var sql = [];
		var indices = [];
		
		var finish = function() {
			sql.push("\t`approved` tinyint(1) NOT NULL DEFAULT '0',");
			sql.push("\t`created` DATETIME NOT NULL,");
			sql.push("\t`modified` DATETIME NOT NULL DEFAULT '1970-01-01 00:00:01',");
			for (var i = 0; i < indices.length; i++) {
				sql.push(indices[i]);
			}
			sql.push("\tPRIMARY KEY(`id`)");
			sql.push(") ENGINE=MyISAM DEFAULT CHARSET=utf8;");
			console.log(sql.join("\n"));
		}
		
		sql.push('DROP TABLE IF EXISTS ' + modelPC + ';');
		sql.push('CREATE TABLE ' + modelPC + ' (');
		sql.push("\t`id` int(10) unsigned NOT NULL AUTO_INCREMENT,");
		if (modelPC != 'Users') {
			sql.push("\t`user_id` int(10) unsigned NOT NULL,");
		}
		
		var fcnt = 0;
		
		$fields.each(function(idx, elem) {
			var $elem = $(elem);
			var elemType = typeof $elem.attr('type') === 'undefined' ? ($elem.is('textarea') ? 'textarea' : 'text') : $elem.attr('type').toLowerCase();
			var id = $elem.attr('id');
			if (typeof id !== 'undefined') {
				id = id.replace(modelRgx, "");
				if (/[a-z]/.test(id)) {
					id = id.substring(0, 1).toLowerCase() + id.substring(1);
				} else {
					id = id.toLowerCase();
				}
				
				if ($elem.hasClass('sqlIndex')) {
					if ($elem.hasClass('unique')) {
						indices.push("\tUNIQUE KEY " + id + "_idx (`" + id + "`),");
					} else {
						indices.push("\tKEY " + id + '_idx (`' + id + '`),');
					}
				}
				
				var constraints = [''];
				
				//if ($elem.hasClass('unique')) {
				//	constraints.push('UNIQUE');
				//}
				
				
				
				switch (elemType) {
					case 'text':
					case 'select-one':
					case 'file':
					case 'hidden':
						if ($elem.hasClass('stateCode')) {
							sql.push("\t`" + id + "` char(2) NOT NULL DEFAULT 'WI',");
						} else if ($elem.hasClass('currency')) {
							sql.push("\t`" + id + "` decimal(10,2) NOT NULL DEFAULT '0.00',");
						} else if ($elem.hasClass('integer')) {
							sql.push("\t`" + id + "` int(10) NOT NULL DEFAULT '0',");
						} else if ($elem.hasClass('text')) {
							sql.push("\t`" + id + "` TEXT NOT NULL DEFAULT '',");
						} else if ($elem.hasClass('mediumText')) {
							sql.push("\t`" + id + "` MEDIUMTEXT NOT NULL DEFAULT '',");
						} else {
							sql.push("\t`" + id + "` varchar(100)" + constraints.join(" ") + " NOT NULL DEFAULT '',");
						}
						break;
					case 'password':
						sql.push("\t`" + id + "` char(40) NOT NULL DEFAULT '',");
						break;
					case 'checkbox':
						sql.push("\t`" + id + "` tinyint(1) NOT NULL DEFAULT '0',");
						break;
					case 'textarea':
						sql.push("\t`" + id + "` TEXT NOT NULL DEFAULT '',");
						break;
				}
			}
			
			fcnt++;	
			if (fcnt == $fields.length) {
				finish();
			}
			
		});
	}
	
});