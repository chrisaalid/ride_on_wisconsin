var pw = 10 / 2;
var ph = 10 / 2;

var snapDistance = 2;

$(function() {
	var canvases = $(".canvas");
	canvases.each(function() {
		var canvas = new Canvas(this);
		$(this).data("obj", canvas);
	});
});

var Canvas = function(elem) {
	this.elem = $(elem);
	this.discard = $(".discard", this.elem);
	this.canvas = $("canvas", this.elem)[0];
	this.context = this.canvas.getContext("2d");
		
	this.points = [];
	this.elem.data("obj", this);
	
	$(this.canvas).click($.proxy(this.handleClick, this));
	
	this.modx = $("input[name=modx]", this.elem);
	this.mody = $("input[name=mody]", this.elem);
	
	this.curve = $("input[name=curve]", this.elem);
	
	this.curve.change($.proxy(this.redraw, this));
	this.modx.change($.proxy(this.redraw, this));
	this.mody.change($.proxy(this.redraw, this));
	
	$("button[name=reset]", this.elem).click($.proxy(this.reset, this));
	
	this.code = $(".code", this.elem);
};

Canvas.prototype.reset = function() {
	this.points = [];
	$(".point", this.elem).remove();
	this.curve.val(1);
	this.modx.val(0);
	this.mody.val(0);
	this.redraw();
}

Canvas.prototype.handleClick = function(e) {
	this.addPoint(new Point(e.offsetX - pw, e.offsetY - ph));
	this.redraw();
};

Canvas.prototype.addPoint = function(point) {
	this.points.push(point);
	this.elem.append(point.elem);
	
	point.elem.draggable({
		containment: this.canvas,
		start: null,
		drag: $.proxy(this.redraw, this),
		stop: $.proxy(this.handleDragStop, this)
	});
};

Canvas.prototype.removePoint = function(pointElem) {
	pointElem = $(pointElem);
	
	var index = this.points.indexOf(pointElem.data("obj"));
	if(index != -1) {
		this.points.splice(index, 1);
		pointElem.remove();
	}
};

Canvas.prototype.handleDragStop = function(e, ui) {
	var pos = ui.position;
	var minLeft = parseFloat(this.discard.width());
	
	if(pos.left < minLeft) {
		this.removePoint(e.target);
	}
	
	if(this.points.length > 1) {
		var first = this.points[0],
			last = this.points[this.points.length-1],
			firstO = first.offset(),
			lastO = last.offset();
		
		if(Math.abs(firstO.left - lastO.left) < snapDistance) {
			last.elem.css("left", firstO.left);
		}
		if(Math.abs(firstO.top - lastO.top) < snapDistance) {
			last.elem.css("top", firstO.top);
		}
	}
	
	this.redraw();
};

Canvas.prototype.redraw = function() {
	var context = this.context;
	
	var holdWidth = this.canvas.width;
	this.canvas.width = 1;
	this.canvas.width = holdWidth;
	
	if(this.points.length > 1) {
		var curviness = this.curve.val();
	
		var anchorArr = [];
		for(var i=0; i < this.points.length; i++) {
			var o = this.points[i].offset();
			anchorArr.push({
				x: o.left,
				y: o.top
			});
		}

		var beziers = BezierPlugin.bezierThrough(anchorArr, curviness, false);
		
		var len = beziers.x.length;
		
		context.strokeStyle = "blue";
		context.lineWidth = 3;

		context.beginPath();
		context.moveTo(beziers.x[0].a + pw, beziers.y[0].a + ph);
		
		for(var i=0; i < len; i++) {
			var xObj = beziers.x[i];
			var yObj = beziers.y[i];
			context.bezierCurveTo(xObj.b, yObj.b, xObj.c, yObj.c, xObj.d + pw, yObj.d + ph);
			
		}
		context.stroke();
	
		var modX = parseFloat(this.modx.val());
		var modY = parseFloat(this.mody.val());
		
		var pointArr = [];
		for(var i=0; i < this.points.length; i++) {
			var pointOffset = this.points[i].offset();
			pointArr.push("{x: " + (pointOffset.left + modX) + ", y: " + (pointOffset.top + modY) + "}");
		}
		var values = "[" + pointArr.join(", ") + "]";
		this.code.html("bezier: {curviness: " + curviness + ", values: " + values + "}");
	} else {
		this.code.empty();
	}
	
};

var Point = function(x, y) {
	if(typeof x == "undefined") x = 0;
	if(typeof y == "undefined") y = 0;
	this.elem = $("<div class='point'></div>");
	this.elem.css({
		top: y,
		left: x
	});
	this.elem.data("obj", this);
}

Point.prototype.offset = function() {
	var pos = this.elem.position();
	var offset = {
		left: parseFloat(pos.left),
		top: parseFloat(pos.top)
	};
	return offset;
}
