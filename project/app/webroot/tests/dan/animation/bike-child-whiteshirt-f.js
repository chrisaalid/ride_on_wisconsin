(function(window) {
mc-child-female-whiteshirt = function() {
	this.initialize();
}
mc-child-female-whiteshirt._SpriteSheet = new SpriteSheet({images: ["bike-child-whiteshirt-f.png"], frames: [[0,0,86,92,0,43,45.5],[86,0,86,92,0,43,45.5],[172,0,86,92,0,43,45.5],[258,0,86,92,0,43,45.5],[344,0,86,92,0,43,45.5],[430,0,86,92,0,43,45.5],[516,0,86,92,0,43,45.5],[602,0,86,92,0,43,45.5],[0,0,86,92,0,43,45.5]]});
var mc-child-female-whiteshirt_p = mc-child-female-whiteshirt.prototype = new BitmapAnimation();
mc-child-female-whiteshirt_p.BitmapAnimation_initialize = mc-child-female-whiteshirt_p.initialize;
mc-child-female-whiteshirt_p.initialize = function() {
	this.BitmapAnimation_initialize(mc-child-female-whiteshirt._SpriteSheet);
	this.paused = false;
}
window.mc-child-female-whiteshirt = mc-child-female-whiteshirt;
}(window));

