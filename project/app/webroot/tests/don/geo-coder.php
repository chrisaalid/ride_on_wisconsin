
<html>
  <head>
	<title>
	  GeoCoder
	</title>
	<style>
	  #main {
		width: 100%;
		height: 100%;
	  }
	</style>
	
	<script src="http://code.jquery.com/jquery-latest.min.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script src="js/csv.js"></script>
	<script src="js/geo-coder.js"></script>
	<script src="js/csvToSQL.js"></script>
	<script src="js/munger.js"></script>
  </head>
  <body>
	
	
	<!--Organization Name,Email,Phone,Address,City,State,Zip,Full Address,Lat Long,Members,Discount Provider
2 Rivers Bicycle &amp; Outdoor,info@2riversbicycle.com,920-563-2222,33 W. Sherman Ave,Fort Atkinson,WI,53538,"33 W. Sherman Ave Fort Atkinson, WI 53538","42.929916, -88.837564",WI Bike Fed Member,Discount Provider
3rd Coast Bicycles,3rdcoastbikes@gmail.com,262-634-0484,210 Third Street,Racine,WI,53403,"210 Third Street Racine, WI 53403","42.730625, -87.783707",WI Bike Fed Member,Discount Provider
Affordable Mobile Bike Repair,heyitstimmer@yahoocom,920-428-7824,2013 S Jefferson St,Appleton,WI,54915,"2013 S Jefferson St Appleton, WI 54915","44.243929, -88.399643",,
Allis Bike &amp; Fitness,allisbike@aol.com,414-327-1290,9622 W National Ave,West Allis,WI,53227,"9622 W National Ave West Allis, WI 53227","43.002048, -88.032478",WI Bike Fed Member,
Amery Pedal Paddle Ski,,715-268-BIKE,109 Center Street,Amery,WI,54001,"109 Center Street Amery, WI 54001","45.311472, -92.361798",,
Anybody's Bikeshop,eric@anybodysbikeshop.com,715-833-7100,411 Water St,Eau Claire,WI,54703,"411 Water St Eau Claire, WI 54703","44.802162, -91.508104",,
Appleton Bicycle Shop,appletonbike@tds.net,920-733-2595,121 S State St,Appleton,WI,54911,"121 S State St Appleton, WI 54911","44.261533, -88.413722",,Discount Provider
Art Doyle's Spokes and Pedals,art@spokesandpedals.com,715-386-8500,607 2nd St,Hudson,WI,54016,"607 2nd St Hudson, WI 54016","44.976078, -92.756995",WI Bike Fed Member,Discount Provider
Atkins Verona Bicycle Shoppe,atcycle@tds.net,608-845-6644,517 Half Mile Rd,Verona,WI,53593,"517 Half Mile Rd Verona, WI 53593","42.988023, -89.545834",,
Attitude Sports,dave@attitudesports.com,920-923-2323,209 A  North Macy St,Fond du Lac,WI,54935,"209 A  North Macy St Fond du Lac, WI 54935","43.77374, -88.420931",,
Attitude Sports,michael@attitudesports.com,262-695-7433,203 W.  Wisconsin Ave,Pewaukee,WI,53072,"203 W.  Wisconsin Ave Pewaukee, WI 53072","43.083912, -88.264116",,
B J's Sportshop Inc.,,715-356-3900,917 N US Highway 51,Minocqua,WI,54548,"917 N US Highway 51 Minocqua, WI 54548","45.874722, -89.706561",,
Back Door Bike Shop,josh@backdoorbikeshop.com,920-893-9786,828 Eastern Ave,Plymouth,WI,53073,"828 Eastern Ave Plymouth, WI 53073","43.746958, -87.970307",,
Backyard Bikes and Ski,info@backyardbikes.com,262-495-8600,W6098 Hwy 12,Whitewater,WI,53910,"W6098 Hwy 12 Whitewater, WI 53910","42.799364, -88.601743",,
Bay City Cycles,baycycle@ncis.net,715-682-2091,412 Main St West,Ashland,WI,54806,"412 Main St West Ashland, WI 54806","46.589732, -90.887194",,Discount Provider
Bayfield Bike Route,info@bayfieldbikeroute.com,715-209-6864,251 Manypenny Ave,Bayfield,WI,54814,"251 Manypenny Ave Bayfield, WI 54814","46.809698, -90.817965",,
Beloit Sport Center,,608-365-6952,557 E Grand Ave,Beloit,WI,53511,"557 E Grand Ave Beloit, WI 53511","42.499624, -89.032097",,
Bicycle Doctor Nordic Ski Shop,joe@bikedr.com,262-965-4144,105 N Main St,Dousman,WI,53118,"105 N Main St Dousman, WI 53118","43.013956, -88.472804",WI Bike Fed Member,
Bicycle Outlet,info@thebicycleoutlet.com,920-230-4668,2550 S Washburn St,Oshkosh,WI,54904,"2550 S Washburn St Oshkosh, WI 54904","43.988916, -88.58679",,
Bicycle Works LLC,,920-467-4549,1114 Plankview Green Blvd,Sheboygan Falls,WI,53085,"1114 Plankview Green Blvd Sheboygan Falls, WI 53085","43.732123, -87.832149",,
BicycleWise &amp; Sport Fitness,john.sotherland@bicyclewise.com,262-473-4730,1155 W Main St,Whitewater,WI,53190,"1155 W Main St Whitewater, WI 53190","42.834848, -88.751198",WI Bike Fed Member,Discount Provider
Big Bear Adventures,logcabin@nnex.net,715-479-8528,6063 Baker lake Rd.,Conover,WI,54519,"6063 Baker lake Rd. Conover, WI 54519","46.066571, -89.348099",,
Bigfoot Bike and Skate LLC,brian@bigfootbikeandskate.com,312-401-8491,2481 South Kinnickinnic Ave,Milwaukee,WI,53207,"2481 South Kinnickinnic Ave Milwaukee, WI 53207","42.999618, -87.901484",,
Bike Works,,608-758-8158,600 Center Ave,Janesville,WI,53548,"600 Center Ave Janesville, WI 53548","42.673083, -89.031702",,
Bikes Limited,benbikemtb@hotmail.com,608-785-2326,1001 La Crosse St.,La Crosse,WI,54603,"1001 La Crosse St. La Crosse, WI 54603","43.817309, -91.242321",,Discount Provider
Bikes-N- Boards,bikesnboards1@gmail.com,715-369-1999,1670 N Stevens St,Rhinelander,WI,54501,"1670 N Stevens St Rhinelander, WI 54501","45.655298, -89.396029",,
BikeSweets.com,dean@bikesweets.com,877-798-7933,6838 Maple Terrace,Wauwatosa,WI,53213,"6838 Maple Terrace Wauwatosa, WI 53213","43.042315, -87.998061",,
BJ's Sportsshop,,715-356-3900,917 Hwy 51 North,Minocqua,WI,54548,"917 Hwy 51 North Minocqua, WI 54548","45.874722, -89.706561",,
Blue Heron Bicycle Works LLC.,blueheronbikes@gmail.com,608-783-7433,213 Main St,Onalaska,WI,54650,"213 Main St Onalaska, WI 54650","43.882787, -91.234022",WI Bike Fed Member,Discount Provider
Bluedog Cycles Inc,brando45@gmail.com,608-637-6993,214 N State St,La Farge,WI,54639,"214 N State St La Farge, WI 54639","43.575468, -90.636339",,
Bob's Pedal Pusher,bobspdlpshr@tds.net,262-763-7794,466 S Pine St,Burlington,WI,53105,"466 S Pine St Burlington, WI 53105","42.669684, -88.268282",,
Bodins on the lake,,715-682-6441,2521 Lake Shore Dr. West,Ashland,WI,54806,"2521 Lake Shore Dr. West Ashland, WI 54806","46.582491, -90.915854",,
Bring's Cyclery &amp; Fitness Inc,jenny@bringscycling.com,715-423-5520,1710 8th St S,Wisconsin Rapids,WI,54494,"1710 8th St S Wisconsin Rapids, WI 54494","44.378824, -89.81722",,
Brone's Bike Shop,mbrone@bronesbikeshop.com,507-458-9861,S2842 Fern Circle,Fountain City,WI,54629,"S2842 Fern Circle Fountain City, WI 54629","44.140347, -91.673497",WI Bike Fed Member,Discount Provider
Brookfield Cycle &amp; Fitness,brookfieldcycle@att.net,262-784-3151,2205 N Calhoun Rd,Brookfield,WI,53005,"2205 N Calhoun Rd Brookfield, WI 53005","43.059303, -88.126255",,Discount Provider
Budget Bicycle Center,Adam@budgetbicyclectr.com,608-251-8413,930 Regent St,Madison,WI,53715,"930 Regent St Madison, WI 53715","43.067685, -89.40157",WI Bike Fed Member,Discount Provider
Budget Bicycle Center,darin@budgetbicylectr.com,608-251-8413,1230 Regent St.,Madison,WI,53715,"1230 Regent St. Madison, WI 53715","43.06775, -89.406634",WI Bike Fed Member,Discount Provider
Budget Bicycle Center,scott@bugetbicyclectr.com,608-251-8413,1201 Regent Street,Madison,WI,53715,"1201 Regent Street Madison, WI 53715","43.067732, -89.405807",WI Bike Fed Member,Discount Provider
Budget Bicycle Center,edwin@budgetbicycles.com,608-251-8413,1124 Regent St.,Madison,WI,53715,"1124 Regent St. Madison, WI 53715","43.067735, -89.404565",WI Bike Fed Member,Discount Provider
Builer's Cycle &amp; Fitness Center,builerscycle@aol.com,715-842-4185,215 S 3rd Ave,Wausau,WI,54401,"215 S 3rd Ave Wausau, WI 54401","44.961535, -89.639273",,
Buzz's Bikes &amp; Boats,buzzsbikes@hotmail.com,608-785-7233,800 Rose St,La Crosse,WI,54603,"800 Rose St La Crosse, WI 54603","43.835874, -91.248152",,Discount Provider
Campus Cycle &amp; Sport Shop,carlk@campus-cycle.com,715-341-2151,1732 4th Ave,Stevens Point,WI,54481,"1732 4th Ave Stevens Point, WI 54481","44.528852, -89.574323",,
Chain of Lakes Cyclery,,715-479-3920,PO Box 2428,Eagle River,WI,54521,"PO Box 2428 Eagle River, WI 54521","45.917374, -89.265932",,
Chain Reaction Cyclery,nikibikeshop@sbcglobal.net,920-733-1141,818 N. Superior Street,Appleton,WI,54911,"818 N. Superior Street Appleton, WI 54911","44.269024, -88.408706",,Discount Provider
Chain-Reaction Cycling &amp; Fitness,gary@chain-reactioncycling.com,920-468-4310,"821 S Huron Rd, Ste E",Green Bay,WI,54311,"821 S Huron Rd, Ste E Green Bay, WI 54311","44.486042, -87.917765",,
Chequamegon's Adventure Company,bikeshop@paddlerama.com,715 356-1618,8575 Hwy 51 North,Minocqua,WI,54548,"8575 Hwy 51 North Minocqua, WI 54548","45.879864, -89.703094",,Discount Provider
City Bike Works,,715-845-1656,908 N 3rd St,Wausau,WI,54403,"908 N 3rd St Wausau, WI 54403","44.964171, -89.627406",,
Classic Bike Shop,,(262) 251-2000,N90W16519 Roosevelt Drive,Menomonee Falls,WI,53051,"N90W16519 Roosevelt Drive Menomonee Falls, WI 53051","43.182098, -88.114835",,
Cool Bikes,bbikeman@hotmail.com,920-261-8688,N98467 Witte Ln,Watertown,WI,53094,"N98467 Witte Ln Watertown, WI 53094","43.155, -88.694909",,Discount Provider
Coontail Sports,sales@coontailsports.com,715-385-0250,5454 Park St,Boulder Junction,WI,54512,"5454 Park St Boulder Junction, WI 54512","46.110529, -89.641044",,
Cory The Bike Fixer,corythebikefixer@aol.com,414-967-9446,2410 N Murray Ave,Milwaukee,WI,53211,"2410 N Murray Ave Milwaukee, WI 53211","43.061646, -87.885538",,Discount Provider
Crankdaddy's Bicycle Works,andrew@crankdaddys.com,414-347-5511,2170 North Prospect Ave.,Milwaukee,WI,53202,"2170 North Prospect Ave. Milwaukee, WI 53202","43.058368, -87.885472",,
Cranked Bike Studio,,920-720-0800,200 Main Street,Neenah,WI,54956,"200 Main Street Neenah, WI 54956","44.187461, -88.465844",,Discount Provider
CrankWorx Bike Shop,crankworxbikeshop@gmail.com,715-629-7246,122 South Main St.,River Falls,WI,54022,"122 South Main St. River Falls, WI 54022","44.858241, -92.626132",,
Cronometro,colin@cronometro.com,608-243-7760,1402 Williamson St,Madison,WI,53703,"1402 Williamson St Madison, WI 53703","43.085909, -89.360589",,
Cyclesmith,todd@cyclesmith.net,262-544-5004,1314 S. West Avenue,Waukesha,WI,53186,"1314 S. West Avenue Waukesha, WI 53186","42.990225, -88.237102",WI Bike Fed Member,Discount Provider
Cyclova XC,cyclovaxcfrank@gmail.com,715-483-3278,125 N. Washington St.,St. Croix Falls,WI,54024,"125 N. Washington St. St. Croix Falls, WI 54024","45.410803, -92.644867",,
Dick's Sporting Goods,,920-954-9266,4350 Greenville Dr.,Appleton,WI,54913,"4350 Greenville Dr. Appleton, WI 54913","44.274081, -88.472346",,
DreamBikes-Madison,erik@dream-bikes.org,608-467-6315,4245 W. Beltline Hwy,Madison,WI ,53711,"4245 W. Beltline Hwy Madison, WI  53711","43.034416, -89.445549",WI Bike Fed Member,Discount Provider
DreamBikes-Milwaukee,aaron@dream-bikes.org,414-763-0909,2021A North Martin Luther King Dr.,Milwaukee,WI,53212,"2021A North Martin Luther King Dr. Milwaukee, WI 53212","43.056833, -87.914204",WI Bike Fed Member,Discount Provider
Eau Claire Bike and Sport,terry@bikeandsport.com,715-832-6149,403 Water St,Eau Claire,WI,54703,"403 Water St Eau Claire, WI 54703","44.802162, -91.507969",,
Ecology Outfitters,ecologyoutfitters@prodigy.com,920-452-9555,4501 Vanguard Drive,Sheboygan,WI,53083,"4501 Vanguard Drive Sheboygan, WI 53083","43.791666, -87.769337",,
Elroy Commons Trail Shop &amp; Info Center,elroy@comtennacom,608-462-2410,303 Railroad St,Elroy,WI,53929,"303 Railroad St Elroy, WI 53929","43.739634, -90.269826",,
Emery's Bicycle &amp; Super Fitness Store,ben1@emerys.com,262-255-0770,N88 W15036 Main St,Menomonee Falls,WI,53051,"N88 W15036 Main St Menomonee Falls, WI 53051","43.177617, -88.097165",WI Bike Fed Member,Discount Provider
Emery's Third Coast Tri Shops,Brent@emerys.com,414-463-0700,929 W. Lisbon Ave,Milwaukee,WI ,53222,"929 W. Lisbon Ave Milwaukee, WI  53222","43.08913, -88.038179",WI Bike Fed Member,Discount Provider
Erik's Bike and Board - Downtown,manager.madt@eriksbikeshop.com,608-250-2701,795 University Ave,Madison,WI,63715,"795 University Ave Madison, WI 63715","43.07315, -89.398871",WI Bike Fed Member,Discount Provider
Erik's Bike and Board - West,jtoombs@eriksbikeshop.com,608-278-9000,6610 Seybold Road,Madison,WI,53719,"6610 Seybold Road Madison, WI 53719","43.051778, -89.496108",WI Bike Fed Member,Discount Provider
Erik's Bike and Board -E.C.,manager.eclr@eriksbikeshop.com,715-835-6746,4130 Commonwealth Ave,Eau Claire,WI,,"4130 Commonwealth Ave Eau Claire, WI ","44.773723, -91.431222",WI Bike Fed Member,Discount Provider
Erik's Bike and Board -East,bsarnwick@eriksbikeshop.com,608-244-9825,1651 Thierer Road,Madison,WI,53704,"1651 Thierer Road Madison, WI 53704","43.121844, -89.311873",WI Bike Fed Member,Discount Provider
Erik's Bike and Board -Whitefish Bay,,,151 E. Silver Spring,Whitefish Bay,,,"151 E. Silver Spring Whitefish Bay,  ","43.118561, -87.909027",WI Bike Fed Member,Discount Provider
Expedition Supply,exporides@gmail.com,262-673-7303,20 W Sumner St,Hartford,WI,53027,"20 W Sumner St Hartford, WI 53027","43.317741, -88.379204",,Discount Provider
Expedition Supply,exporides@gmail.com,920-821-2000,108 S Center St,Beaver Dam,WI,53916,"108 S Center St Beaver Dam, WI 53916","43.456121, -88.839373",,Discount Provider
Extreme Ski &amp; Bike,extremebike@sbcglobal.net,262-242-1442,235 N Main St,Thiensville,WI,53092,"235 N Main St Thiensville, WI 53092","43.234349, -87.983876",WI Bike Fed Member,Discount Provider
Fish Nordic,,715-362-6224,5196 Spider Lake Rd,Rhinelander,WI,54501,"5196 Spider Lake Rd Rhinelander, WI 54501","45.723181, -89.40228",,
Fond du Lac Cyclery &amp; Fitness,fdlcyclery@yahoo.com,920 923 3211,209 S. Main St.,Fond du lac,WI,54935,"209 S. Main St. Fond du lac, WI 54935","43.773025, -88.447086",WI Bike Fed Member,Discount Provider
Fontana Outdoor Sports,,262-275-2220,543 Highway 67,Fontana,WI,53125,"543 Highway 67 Fontana, WI 53125","42.55644, -88.580159",,
"Fox River Sports &amp; Spas, Ltd",info@foxriversports.net,262-544-5557,143 E North St,Waukesha,WI,53188,"143 E North St Waukesha, WI 53188","43.014136, -88.234677",,Discount Provider
"Future Motion, Inc",stephen@lmobikes.com,262-618-4640,w62 n630 Washington ave,Cedarburg,WI,53012,"w62 n630 Washington ave Cedarburg, WI 53012","43.299242, -87.988598",WI Bike Fed Member,
Gear N Up Bicycle Shop,gearnup@gmail.com,920-722-2949,1276 W Winneconne Ave,Neenah,WI,54956,"1276 W Winneconne Ave Neenah, WI 54956","44.172854, -88.50034",,
Grafton Ski &amp; Cyclery,info@graftonskiandcyclery.com,262-377-5220,1275 Washington St,Grafton,WI,53024,"1275 Washington St Grafton, WI 53024","43.31996, -87.951247",,Discount Provider
Grinders Sports,race_w_me@hotmail.com,715 736-7858,816 Hammond Ave E,Rice Lake,WI,54868,"816 Hammond Ave E Rice Lake, WI 54868","45.509629, -91.7396",,
Heavy Pedal Bicycles,heavypedalbicycles@att.net,920-652-0888,826 South 8th Street,Manitowoc,WI,54220,"826 South 8th Street Manitowoc, WI 54220","44.090994, -87.65776",,
Hene Supply,,715-229-4530,N14704 French Town Ave.,Withee,WI,54498,"N14704 French Town Ave. Withee, WI 54498","44.951788, -90.628281",,
Hostel Shoppe,barbg@hostelshoppe.com,715-341-4340,3201 John Joanis Dr,Stevens Point,WI,54481,"3201 John Joanis Dr Stevens Point, WI 54481","44.503816, -89.513056",WI Bike Fed Member,
In Competition Sports,goincomp@att.net,920-465-1510,2439 University Ave,Green Bay,WI,54302,"2439 University Ave Green Bay, WI 54302","44.513194, -87.961517",,Discount Provider
JB Cycle &amp; Sport,jbcycle@new.rr.com,920-434-8338,2500 Glendale Ave,Green Bay,WI,54313,"2500 Glendale Ave Green Bay, WI 54313","44.56359, -88.079755",WI Bike Fed Member,Discount Provider
Joe BikeLer's Bicycle Store,,715-526-2216,620 S Main St,Shawano,WI,54166,"620 S Main St Shawano, WI 54166","44.774344, -88.609098",,Discount Provider
Johnnie's Bike Shop,,920-452-0934,1001 Michigan Ave,Sheboygan,WI,53081,"1001 Michigan Ave Sheboygan, WI 53081","43.758532, -87.716447",,
Johnson's Cycle &amp; Fitness,jjensen9@aol.com,414-476-2341,6916 W North Ave,Wauwatosa,WI,53213,"6916 W North Ave Wauwatosa, WI 53213","43.060768, -87.998886",WI Bike Fed Member,Discount Provider
La Crosse Bike Rentals,,608-782-7233,324 3rd Street South,La Crosse,WI,54601,"324 3rd Street South La Crosse, WI 54601","43.810707, -91.253447",,
Ladysmith Hardware,,715-532-3351,503 Lake Ave W,Ladysmith,WI,54848,"503 Lake Ave W Ladysmith, WI 54848","45.465124, -91.106505",,
Lakewood Ski and Sport,,715-276-3071,15684 State Highway 32,Lakewood,WI,54138,"15684 State Highway 32 Lakewood, WI 54138","45.30131, -88.541209",,Discount Provider
Lulloff and Sons Hardware,owner@lulloffhardware.com,920-894-3236,203 Fremont St.,Kiel,WI,53042,"203 Fremont St. Kiel, WI 53042","43.912394, -88.030994",,
M&amp;M Bike Repair,,414-542-5912,1309 Summit Ave,Waukesha,WI,53188,"1309 Summit Ave Waukesha, WI 53188","43.021448, -88.251066",,
Machinery Row Bicycles,luke@machineryrowbicycles.com,608-442-5974,601 Williamson St,Madison,WI,53703,"601 Williamson St Madison, WI 53703","43.076179, -89.37538",WI Bike Fed Member,Discount Provider
Marinette Cycle Center,marinettecycle@centurytel.net,715-735-5442,1555 Pierce Ave,Marinette,WI,54143,"1555 Pierce Ave Marinette, WI 54143","45.089432, -87.629501",,Discount Provider
Mel's Trading Post,jmmode@gmail.com,715-362-5800,105 South Brown st.,Rhinelander,WI,54501,"105 South Brown st. Rhinelander, WI 54501","45.636551, -89.412775",WI Bike Fed Member,
Michael's Cycles,bikedealer@michaelscycles.net,608-752-7676,2716 Pontiac Dr,Janesville,WI,53545,"2716 Pontiac Dr Janesville, WI 53545","42.714867, -88.989264",WI Bike Fed Member,Discount Provider
Mid-Life Cycle,neubauers3@sbcglobal.net,,221 N Main St,Waupaca,WI,54981,"221 N Main St Waupaca, WI 54981","44.359092, -89.084985",,
Middleton Cycle,midcycleoffice@charterinternet.com,608-831-7433,6641 University Ave,Middleton,WI,53562,"6641 University Ave Middleton, WI 53562","43.096396, -89.495668",,Discount Provider
Mike's Bike Shop,,920-361-3565,117 E Huron St,Berlin,WI,54923,"117 E Huron St Berlin, WI 54923","43.968352, -88.945956",,
Milwaukee Bicycle Collective,,414-431-0825,2910 W. Clyborn Street,Milwaukee,WI,53208,"2910 W. Clyborn Street Milwaukee, WI 53208","43.036158, -87.950646",,
Mokros Cycle,,262-521-1300,N6 W23757 Bluemound Rd,Waukesha,WI,53188,"N6 W23757 Bluemound Rd Waukesha, WI 53188","43.040013, -88.221351",,Discount Provider
Momentum Bicycle,momentum@momentumbikes.com,608-348-6888,25 West Main Street,Platteville,WI,53818,"25 West Main Street Platteville, WI 53818","42.734123, -90.478544",,Discount Provider
Mountain Outfitters,mntout@mountainout.com,262-335-0424,109 S Main St,West Bend,WI,53095,"109 S Main St West Bend, WI 53095","43.423251, -88.182329",,
Mr. Barts Bike Shop,,920-748-7801,316 Doty St,Ripon,WI,54971,"316 Doty St Ripon, WI 54971","43.848448, -88.833431",,
Nature Treks &amp; Nature's Niche,tlachac@dwave.net,,5223 Hwy 66,Stevens Point,WI,54481,"5223 Hwy 66 Stevens Point, WI 54481","44.555671, -89.52768",,Discount Provider
Neenah Bike &amp; Fitness,,920-722-5991,887 S Green Bay Rd,Neenah,WI,54956,"887 S Green Bay Rd Neenah, WI 54956","44.175294, -88.484974",,Discount Provider
New Moon Ski &amp; Bike Shop,joel@newmoonski.com,715-634-8685,15569 US Hwy 63 N,Hayward,WI,54843,"15569 US Hwy 63 N Hayward, WI 54843","46.02175, -91.470947",,
Nor Door Sport &amp; Cyclery,,920-868-2275,4007 Hwy 42,Fish Creek,WI,54212,"4007 Hwy 42 Fish Creek, WI 54212","45.125662, -87.235152",WI Bike Fed Member,Discount Provider
Nor Door Sport &amp; Cyclery,stretch@nordoorsports.com,920-818-0803,60 South Madison Ave,Sturgeon Bay,WI,54235,"60 South Madison Ave Sturgeon Bay, WI 54235","44.828355, -87.384416",WI Bike Fed Member,Discount Provider
O2 Gear Shop,tolsen@o2gearshop.com,920-731-3652,200 North Mall Drive,Appleton,WI,54913,"200 North Mall Drive Appleton, WI 54913","44.263582, -88.470533",,
Old Town Cycles,oldtownwrench@gmail.com,608-259-8696,920 East Johnson St,Madison,WI,53703,"920 East Johnson St Madison, WI 53703","43.085104, -89.375283",,
Olson Sporting Goods,,920-468-4321,1614 Cass St,Green Bay,WI,54302,"1614 Cass St Green Bay, WI 54302","44.495851, -87.990673",,
Oshkosh Cyclery &amp; Fitness,john@oshkoshcyclery.com,920-231-2211,1030-B  20th Ave,Oshkosh,WI,54902,"1030-B  20th Ave Oshkosh, WI 54902","43.995947, -88.563799",WI Bike Fed Member,Discount Provider
Patio Bike Shop,johnocag@yahoo.com,414-425-3535,9800 W Forest Home Ave,Hales Corners,WI,53130,"9800 W Forest Home Ave Hales Corners, WI 53130","42.94445, -88.036845",,
Pearsons Sport Shop,personssportshop@centurytel.net,715-284-9562,108 N. Water St.,Black River Falls,WI,54615,"108 N. Water St. Black River Falls, WI 54615","44.295624, -90.847381",,
Pedal  Mill,,715-247-5401,PO Box 58,Somerset,WI,54025,"PO Box 58 Somerset, WI 54025","45.133039, -92.696738",,
Pedal Moraine,pedalmoraine@onecommail.com,262-338-2453,1701 Evergreen St,West Bend,WI,53095,"1701 Evergreen St West Bend, WI 53095","43.413775, -88.199861",,Discount Provider
Perfect Circle Cycling,david@perfectcirclecycling.com,608-770-9916,5956 Executive Dr.,Fitchburg,WI,53719,"5956 Executive Dr. Fitchburg, WI 53719","43.013117, -89.457534",,
Perry's Bicycle Shop,,608-666-2927,N1377 Southern Rd.,Lyndon Station,WI,53944,"N1377 Southern Rd. Lyndon Station, WI 53944","43.685904, -89.868284",,
Quiet Hut Sports,dsaalsaa@sbcglobal.net,262-473-2950,186 W Main St,Whitewater,WI,53190,"186 W Main St Whitewater, WI 53190","42.834174, -88.73205",,
Racine Cyclery &amp; Fitness LLC.,info@racinecyclery.com,262-637-7241,4615 Washington Ave,Racine,WI,53405,"4615 Washington Ave Racine, WI 53405","42.7186, -87.835058",WI Bike Fed Member,
Rainbow Jersey Bicycle Shop,info@rainbowjersey.com,414-961-1110,4600 N Wilson Dr,Shorewood,WI,53211,"4600 N Wilson Dr Shorewood, WI 53211","43.099723, -87.902716",,Discount Provider
"Recyclist, Inc.",shop@recyclist.com,920-759-1200,631 Saunders Rd,Kaukauna,WI,54130,"631 Saunders Rd Kaukauna, WI 54130","44.258549, -88.285814",,Discount Provider
Red Cedar Outfitters - Roscoes,,715-235-3866,910 Hudson Rd,Menomonie,WI,,"910 Hudson Rd Menomonie, WI ","44.87768, -91.943244",,
Revel Sports,info@revelsports.com,866-502-4125,1903 Weston Ave,Schofield,WI,,"1903 Weston Ave Schofield, WI ","44.887354, -89.599792",,
Revolution Cycles,jeffitz007.revolution@gmail.com,608-244-0009,2330 Atwood Ave,Madison,WI,53704,"2330 Atwood Ave Madison, WI 53704","43.093522, -89.348027",,
"Rib Mountain Cycles, Inc.",rmcrandy@charter.net,715-359-3925,4001 D Rib Mountain Dr.,Wausau,WI,54401,"4001 D Rib Mountain Dr. Wausau, WI 54401","44.920705, -89.651996",,Discount Provider
Ride-A-Bike,,608-565-3417,W6406 20th St W,Necedah,WI,54646,"W6406 20th St W Necedah, WI 54646","44.028227, -90.089436",,
River Trail Cycles,info@rivertrailcycles.com,608-526-4678,500 Holmen Dr,Holmen,WI,54636,"500 Holmen Dr Holmen, WI 54636","43.967623, -91.263551",WI Bike Fed Member,
Riverbrook Bike &amp; Ski Shop,info@riverbrookbike.com,715-634-0437,15838 US Hwy 63,Hayward,WI,54843,"15838 US Hwy 63 Hayward, WI 54843","46.012423, -91.48557",,Discount Provider
Riverbrook Bike &amp; Ski Shop,tim@riverbrookbike.com,715-634-5600,13471 N U.S. Hwy 63,Seeley,WI,54843,"13471 N U.S. Hwy 63 Seeley, WI 54843","46.119817, -91.362746",,Discount Provider
Riverside Bike &amp; Skate,contact@riversidebikeskate.com,715-835-0088,937 Water St,Eau Claire,WI,54703,"937 Water St Eau Claire, WI 54703","44.802162, -91.518758",,Discount Provider
Rocket Bicycle Studio,peteroyen@rocketmail.com,608-239-3837,403 Venture Court #1,Madison,WI,53593,"403 Venture Court #1 Madison, WI 53593","42.982255, -89.539075",,Discount Provider
Ron's Lawn and Sport,,920-324-3181,650 W Main St,Waupun,WI,53963,"650 W Main St Waupun, WI 53963","43.633108, -88.745799",,
RRB Trek Cycles &amp; Fitness,bikeguydon@gmail.com,262-248-2588,629 Williams St,Lake Geneva,WI,53147,"629 Williams St Lake Geneva, WI 53147","42.597256, -88.435811",,
Russell's Sport N' Bike,sales@russellssport@bike.com,715-248-3644,703 Jewell St,Star Prairie,WI,54026,"703 Jewell St Star Prairie, WI 54026","45.200102, -92.529481",,
Scheels Sporting goods,gbbushbeck@sheelssports.com,715-833-1886,4710 Golf Rd,Eau Claire,WI,54701,"4710 Golf Rd Eau Claire, WI 54701","44.774069, -91.437762",,
Scheels Sporting goods,apredlin@scheelssports.com,920-830-2977,4301 W Wisconsin Ave,Appleton,WI,54913,"4301 W Wisconsin Ave Appleton, WI 54913","44.273028, -88.469748",,
Schwag LLC,info@schwagstores.com,920-733-0600,945 S Nicolet Rd,Appleton,WI,54914,"945 S Nicolet Rd Appleton, WI 54914","44.253712, -88.469186",,
"Sheboygan Bicycle Company, LLC",dan@sheboyganbike.com,920-208-8735,804 North 8th Street,Sheboygan,WI,53081,"804 North 8th Street Sheboygan, WI 53081","43.753234, -87.712957",,
Shepherd &amp; Schaller Sporting Goods,info@shepssportscom,715-845-5432,324 Scott St,Wausau,WI,54403,"324 Scott St Wausau, WI 54403","44.960621, -89.627464",,Discount Provider
Simple Sports,simplesports@hotmail.com,715-233-3493,326 Main St E,Menomonie,WI,54751,"326 Main St E Menomonie, WI 54751","44.876492, -91.92677",,Discount Provider
Smith's Cycling &amp; Fitness,smithsbikes@hotmail.com,608-784-1175,125  7th Street North,La Crosse,WI,54601,"125  7th Street North La Crosse, WI 54601","43.811964, -91.246771",WI Bike Fed Member,Discount Provider
"South Shore Cyclery, Inc.",leah.d@southshorecyclery.com,414-831-0211,4758 So. Packard Ave.,Cudahy,WI,53110,"4758 So. Packard Ave. Cudahy, WI 53110","42.957678, -87.860053",WI Bike Fed Member,
Speeds Bicycle &amp; Electric Motors,speeds@centurytel.net,608-269-2315,1126 John St.,Sparta,WI,53656,"1126 John St. Sparta, WI 53656","43.933076, -90.795503",,
Sports Authority - Brookfield,,262 860-0900,16220 W. Blue Mound Road,Brookfield,WI,53008,"16220 W. Blue Mound Road Brookfield, WI 53008","43.036516, -88.115349",,
Sports Authority-Delafield,,262-646-4110,Nagawaukee Center,Delafield,WI,53018,"Nagawaukee Center Delafield, WI 53018","43.057994, -88.404248",,
Sports Authority-Greenfield,,414-282-2300,5070 S 74th Street,Greenfield,WI,53220,"5070 S 74th Street Greenfield, WI 53220","42.952633, -88.006244",,
Sports Unlimited,,715-234-7273,2900 S Main St,Rice Lake,WI,54868,"2900 S Main St Rice Lake, WI 54868","45.472676, -91.735513",,Discount Provider
"Spring Street Sports, Inc.",davef@springstreetsports.com,715-723-6616,12 W Spring St,Chippewa Falls,WI,54729,"12 W Spring St Chippewa Falls, WI 54729","44.93556, -91.391791",WI Bike Fed Member,
Sprockets Bike &amp; Board,kenworthy549@centurytel.net,608-374-4296,14582 State Hwy 131,Tomah,WI,54660,"14582 State Hwy 131 Tomah, WI 54660","43.965947, -90.504811",,
Sprocketz Bike Shop,sprocketzbikes@charter.net,715-359-7600,2106 Schofield Ave Suite 3,Weston,WI,54476,"2106 Schofield Ave Suite 3 Weston, WI 54476","44.909153, -89.597406",WI Bike Fed Member,Discount Provider
Stadium Bike,info@stadiumbike.com,920-499-3400,2150 W Mason St,Green Bay,WI,54313,"2150 W Mason St Green Bay, WI 54313","44.524457, -88.085459",WI Bike Fed Member,Discount Provider
Stoton Cycle,stotoncycle@sbcglobal.net,608-877-1134,229 East Main Street,Stoughton,WI,53589,"229 East Main Street Stoughton, WI 53589","42.916759, -89.218701",WI Bike Fed Member,Discount Provider
Sun City Cyclery &amp; Skate,,608-837-2453,235 E Main St,Sun Prairie,WI,53590,"235 E Main St Sun Prairie, WI 53590","43.183145, -89.21208",,Discount Provider
Terry's Bike Shop,karen@terrysbikeshop.com,920-526-3478,N6695 W County Road A,Glenbeulah,WI,53023,"N6695 W County Road A Glenbeulah, WI 53023","43.77301, -88.081019",,
The Bike Guy,thebikeguy@sbcglobal.net,920-609-7500,107 Lebrun St,Green Bay,WI,54301,"107 Lebrun St Green Bay, WI 54301","44.453541, -88.040915",,Discount Provider
The Bike Hub,rebecca@thebikehubonline.com,920-339-0229,1025 N. Broadway,De Pere,WI,54115,"1025 N. Broadway De Pere, WI 54115","44.460656, -88.053496",,Discount Provider
The Bikeshop @ Southport Rigging,sales@southport-rigging.com,262-652-3126,2926 75th St,Kenosha,WI,53143,"2926 75th St Kenosha, WI 53143","42.566037, -87.84455",,
The Bikesmiths,bikesmiths@thebikesmiths.com,414-332-1330,2865 N Murray Ave,Milwaukee,WI,53211,"2865 N Murray Ave Milwaukee, WI 53211","43.070431, -87.885452",,Discount Provider
The Crazy Loon,Info@thecrazyloon.com,715-339-6254,125 N. Lake Ave (Hwy 13),Phillips,WI,54555,"125 N. Lake Ave (Hwy 13) Phillips, WI 54555","45.689666, -90.396653",,
The Fitness Store,shipping@thefitnessstore.com,920-684-8088,1410 Dewey St,Manitowoc,WI,54220,"1410 Dewey St Manitowoc, WI 54220","44.07374, -87.666507",,Discount Provider
The Prairie Peddler,theprairiepeddler@gmail.com,608-412-2786,200 W Blackhawk,Prairie du Chien,WI,53821,"200 W Blackhawk Prairie du Chien, WI 53821","43.051613, -91.14792",,
The Route Bike Shop,,715-425-8684,161 Bobwhite St.,River Falls,WI,54022,"161 Bobwhite St. River Falls, WI 54022","44.859792, -92.649013",,Discount Provider
The Sports Den,info@thesportsden.net,715-384-8313,1202 S Central Ave,Marshfield,WI,54449,"1202 S Central Ave Marshfield, WI 54449","44.656756, -90.180707",,
TJ's Bike Rental &amp; Gifts,,608-254-5466,W5559 Hwy 49,Waupun,WI,53963,"W5559 Hwy 49 Waupun, WI 53963","43.633158, -88.676897",,
Total Cyclery,,262-652-2222,2900 52nd Street,Kenosha,WI,53140,"2900 52nd Street Kenosha, WI 53140","42.588126, -87.844505",,
Tour Seven Sports,,715-424-2242,2030 Chestnut St,Wisconsin Rapids,WI,54494,"2030 Chestnut St Wisconsin Rapids, WI 54494","44.383822, -89.801861",,
Trailside Cycle,trailsidecycle@hotmail.com,414-422-9000,1849 S. Calhoun Rd,New Berlin,WI,53151,"1849 S. Calhoun Rd New Berlin, WI 53151","43.121429, -89.307276",,Discount Provider
Trek Bicycle Store - Madison East,Jake_Jones@trekbikes.com,608-442-8738,1706 Eagan Rd,Madison,WI,53704,"1706 Eagan Rd Madison, WI 53704","43.060488, -89.525164",,Discount Provider
Trek Bicycle Store - Madison West,Sara_Crass@trekbikes.com,(608) 833-8735,8108 Mineral Point Rd,Madison,WI,53719,"8108 Mineral Point Rd Madison, WI 53719","43.009927, -88.127255",,Discount Provider
Trek Bicycle Store - Wausau,info@stadiumbike.com,715-845-7433,2601 Stewart Street,Wausau,WI,54401,"2601 Stewart Street Wausau, WI 54401","44.957625, -89.668024",WI Bike Fed Member,Discount Provider
Trikes to Bikes &amp; More,melissa@trikes2bikes.com,920-866-2453,5286 Warehouse Dr.,Green Bay/New Frankin,WI,54229,"5286 Warehouse Dr. Green Bay/New Frankin, WI 54229","44.567522, -87.830509",,Discount Provider
Truly Spoken Cycles,centerstbikeshop@gmail.com,(414) 263-2453 ,600 E. Center St,Milwaukee,WI,53212,"600 E. Center St Milwaukee, WI 53212","43.067478, -87.903902",,
Uphill Grind Bicycle &amp; Coffee,lee.sorensen@uphillgrind.net,608-413-0068,1827 Bourbon Rd,Cross Plains,WI,53528,"1827 Bourbon Rd Cross Plains, WI 53528","43.111642, -89.650446",WI Bike Fed Member,Discount Provider
Village Pedaler,,608-221-0311,5511 Monona Dr,Monona,WI,53716,"5511 Monona Dr Monona, WI 53716","43.059584, -89.325946",,
Wandering Wheels Cyclery,,262-238-1194,11815 N. Solar Ave.,Mequon,WI,53097,"11815 N. Solar Ave. Mequon, WI 53097","43.23172, -88.00594",,
Wheel &amp; Sprocket - Appleton,peter.coons@wheelandsprocket.net,920-997-9300,3939 W College Ave,Appleton,WI,53914,"3939 W College Ave Appleton, WI 53914","44.261517, -88.462559",WI Bike Fed Member,Discount Provider
Wheel &amp; Sprocket - Brookfield,matt.laughlin@wheelandsprocket.net,262-783-0700,13925 W. Capitol Dr.,Brookfield,WI,53005,"13925 W. Capitol Dr. Brookfield, WI 53005","43.089546, -88.085271",WI Bike Fed Member,Discount Provider
Wheel &amp; Sprocket - Delafield,matt.geib@wheelandsprocket.net,262-646-6300,528 Wells St,Delafield,WI,53018,"528 Wells St Delafield, WI 53018","43.059922, -88.405198",WI Bike Fed Member,Discount Provider
Wheel &amp; Sprocket - Hales Corners,barb.barbian@wheelandsprocket.net,(414) 529-6600,5722 S 108th St,Hales Corners,WI,53130,"5722 S 108th St Hales Corners, WI 53130","42.940349, -88.048337",WI Bike Fed Member,Discount Provider
Wheel &amp; Sprocket - N.S.,jesse.kuester@wheelandsprocket.net,414 247-8100,6940 N Santa Monica Blvd,Milwaukee,WI,53217,"6940 N Santa Monica Blvd Milwaukee, WI 53217","43.142873, -87.902611",WI Bike Fed Member,Discount Provider
Wheel &amp; Sprocket - Oshkosh,dan.fedderly@wheelandsprocket.net,920-232-0900,1451 S. Washburn,Oshkosh,WI,54904,"1451 S. Washburn Oshkosh, WI 54904","44.004805, -88.582808",WI Bike Fed Member,Discount Provider
White Lake Market,,715-882-8419,633 Bissell St.,White Lake,WI,54491,"633 Bissell St. White Lake, WI 54491","45.155158, -88.76453",,
Wild Rose Bike Shop,,920-622-3638,840 Oakwood St.,Wild Rose,WI,54984,"840 Oakwood St. Wild Rose, WI 54984","44.175431, -89.249136",,
Wolf's Cycling &amp; Fitness,wolfcycle@hotmail.com,920-457-0664,1702 South 12th St,Sheboygan,WI,53081,"1702 South 12th St Sheboygan, WI 53081","43.735682, -87.720271",WI Bike Fed Member,Discount Provider
Won-A-Go Biking,,262-363-4770,106 Main St,Mukwonago,WI,53149,"106 Main St Mukwonago, WI 53149","42.862956, -88.332419",,Discount Provider
"Yellow Jersey, Ltd",am@yellowjersey.org,608-257-4737,419 State St,Madison,WI,53703,"419 State St Madison, WI 53703","43.074787, -89.392145",,Discount Provider
Z-Best Bikes,john@z-bestbikes.com,772-713-1416,329 Front St,Minocqua,WI,54548,"329 Front St Minocqua, WI 54548","45.871452, -89.709479",,
"ZuZu Pedals, LLC",tammy@zuzupedals.com,262-208-6571,228 North Franklin St,Port Washington,WI,53074,"228 North Franklin St Port Washington, WI 53074","43.388815, -87.869288",WI Bike Fed Member,-->
	
<!--"name","lat","lng"
"Alpha Mountain Bike Trail",42.919946,-88.015144
"Anvil Nation Recreation Area",45.945421,-89.059982
"Anvil Trails",46.096091,-89.01123
"Ash Creek",43.29495,-90.41851
"Baird Creek Parkway",44.516338,-87.949333
"Big Eau Pleine County Park",44.753438,-89.845419
"Black River State Forest Trails",44.220107,-90.619011
"Blue Mounds",43.034768,-89.840698
"Boulder Lake Trails",45.141973,-88.637695
"Brown County Reforestation Camp",44.674757,-88.090096
"Brush Creek Campground Trails",43.728189,-90.64991
"Calumet County Park Trails",44.114705,-88.323212
"Cam Rock 3",42.977902,-89.03492
"Camba - Cable Cluster",46.212625,-91.2957
"Camba - Delta Cluster",46.477827,-91.268921
"Camba - Drummond Cluster",46.336025,-91.257935
"Camba - Hayward Cluster",46.054173,-91.401443
"Camba - Namakagon Cluster",46.20027,-91.073227
"Camba - Namakagon Cluster (Rock Lake)",46.198874,-91.135197
"Copper Falls State Park Trails",46.378438,-90.64373
"Crystal Lake",45.715769,-89.420128
"Devil's Lake",43.436467,-89.709892
"Emma Carlin",42.876719,-88.542252
"Evergreen Park",43.788445,-87.748146
"Glacial Blue Hills",43.44943,-88.214207
"Governor Dodge",43.031004,-90.117073
"Greenbush",43.756465,-88.102112
"Hartman Creek State Park",44.323556,-89.219112
"Hickory Ridge",45.170738,-91.343207
"Hpt Lacrosse",43.82174,-91.191298
"John Muir",42.824617,-88.601303
"John Muir - Southern Unit of the Kettle Moraine State Forest",42.821027,-88.601625
"Kiekhaefer",43.849879,-88.36853
"Lakewood Trails",45.300973,-88.475304
"Lauterman/Perch Lake Trails",45.930856,-88.498306
"Levis Mounds",44.478074,-90.720549
"Levis-Trow Mound - Clark County Parks",44.466315,-90.723832
"Log Jam",45.715289,-90.389328
"Lowes Creek",44.761362,-91.470451
"Morgan Falls",46.376544,-90.922165
"New Fane",43.549295,-88.174553
"Nicolet North Trail",45.916766,-89.042816
"Nicolet Roche",45.136887,-88.650227
"Nine Mile",44.865116,-89.709892
"Peninsula State Park",45.132407,-87.238655
"Pines And Mines Trails",46.436437,-90.246849
"Quarry Park",43.076412,-89.440899
"Quarry Ridge",43.006844,-89.480124
"Raven Trails",45.873278,-89.651184
"Razorback Ridge Trails",46.00197,-89.568443
"Riverview Park Trails",44.480616,-87.573094
"Standing Rocks",44.437457,-89.401588
"Stoney Hill",46.542805,-91.594048
"The Underdown",45.335013,-89.585266
"Washburn Lake Trail",45.634837,-89.576093
"Wauwatosa Trails",43.055829,-88.025744
"Whitetail Ridge",44.887628,-92.635206
"Winter Park",44.469071,-87.559662-->

<pre><?php

$input = '';

if (array_key_exists('fileUpload', $_FILES)) {
  $path = $_FILES['fileUpload']['tmp_name'];
  if (file_exists($path)) {
	$input = file_get_contents($path);
  }
}

?></pre>
	
	<table id="main">
	  <tr>
		<td>
		  <label for="inputField">Input:</label><br/>
		  <textarea id="inputField" style="width: 100%; height: 450px; font-family: Consolas, Courier New, Courier, monospace;"><?php echo $input; ?></textarea>	  
		</td>
	  </tr><tr>
		<td>
		  
		  <form method="POST" enctype="multipart/form-data">
			<label for="filePicker">Load File: <input type="file" id="filePicker" name="fileUpload"></label>
			<input type="submit" value="Upload">
		  </form>
		  
		  <!--<input type="submit" id="actionCSVtoJSON" value="CSV -> JSON">  -->
		  <input type="submit" id="actionCSVtoSQL" value="CSV -> SQL">
		  <input type="submit" id="actionRevGeoCodeCSV" value="Reverse Geocode CSV (lat,lng -> address)">
			
		</td>
	  </tr><tr>
		<td>
		  <label for="outputField">Output:</label><br/>
		  <textarea id="outputField" style="width: 100%; height: 450px; font-family: Consolas, Courier New, Courier, monospace;"></textarea>
		</td>
	  </tr>
	</table>
  </body>
</html>