var CSV = {};

$(function() {
	CSV.parse = function(input) {
		var lines = input.split("\n");
		var cols = [];
		var fields = [];
		var len = lines.length;
		var parsed = [];
		
		for (var i = 0; i < len; i++) {
			var l = lines[i];
			
			
			var all_fields = [];
			var cfv = '';
			var inquote = false;
			var next_is_literal = false;
			for (var q = 0; q < l.length; q++) {
				var chr = l[q];
				if (inquote) {
					if (chr == '\\') {
						next_is_literal = true;
					} else if (chr == '"' && !next_is_literal) {
						inquote = false;
					} else {
						cfv += chr;
						next_is_literal = false;
					}
				} else {
					if (chr == '"') {
						inquote = true;
					} else if (chr == ',') {
						all_fields.push(cfv);
						cfv = '';
					} else {
						cfv += chr;
					}
				}
			}
			all_fields.push(cfv);
			
			//console.log(all_fields);
			
			
			if (i == 0) {
				cols = all_fields; //l.split(',');
			} else {
				fields = all_fields; //l.split(',');
				var nrec = {};
				for (var x = 0; x < cols.length; x++) {
					nrec[cols[x]] = fields[x];
				}
				
				if (typeof nrec['Lat Long'] !== 'undefined') {
					var ll = nrec['Lat Long'].split(/,\s+/);
					delete nrec['Lat Long'];
					nrec['lat'] = ll[0];
					nrec['lng'] = ll[1];		
				}
				
				
				parsed.push(nrec);
				
			}
		}
		
		return parsed;
	};
	
	CSV.qin = function(v) {
		if (typeof v === 'undefined') {
			return '""';
		} else if (v === '' || /[^0-9\.-]/.test(v)) {
			return '"' + v.replace(/"/g, '\\"') + '"';
		} else {
			return v;
		}
	};
	
	CSV.stringify = function(data) {
		if (data.length < 1) {
			return '';
		}
		
		var records = [];
		var columns = [];
		
		var col_rec = data[0];
		for (var col in col_rec) {
			if (col_rec.hasOwnProperty(col)) {
				columns.push(col);
			}
		}
		
		records.push(columns.join(','));
		
		for (var i = 0; i < data.length; i++) {
			var rec = data[i];
			var rec_ar = [];
			for (var l = 0; l < columns.length; l++) {
				var val = typeof rec[columns[l]] !== 'undefined' ? rec[columns[l]] : '';
				rec_ar.push(CSV.qin(val));
			}
			records.push(rec_ar.join(','));
		}
		
		
		return records.join("\n");
	};
	
});