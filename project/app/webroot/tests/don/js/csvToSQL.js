var CSV_TO_SQL = {};

(function(args) {
	CSV_TO_SQL.process = function(csv, table, columns, template) {
		
		var parsed = CSV.parse(csv);
		
		var sql = [];
		
		//columns = `user_id`, `name`, `type`, `address`, `city`, `state`, `lat`, `lng`, `member`, `discount`, `approved`, `created`, `modified`
		
		sql.push("INSERT INTO `" + table + "` (`" + columns.join('`,`') + "`)");
		sql.push("VALUES");
		var vals = [];
		
		var oito = template;
		
		//var qin = function(v) {
		//	if (/[^0-9\.-]/.test(v)) {
		//		return '"' + v + '"';
		//	} else {
		//		return v;
		//	}
		//};
		
		for (var idx in parsed) {
			var rec = parsed[idx];
			
			var nrec = [];
			for (var i = 0; i < oito.length; i++) {
				var of = oito[i];
				switch (of.t) {
					case 'l':
						nrec.push(CSV.qin(of.v));
						break;
					case 'f':
						nrec.push(CSV.qin(rec[of.v]));
						break;
					case 'c':
						nrec.push(CSV.qin(of.v(rec)));
						break;
				}
				
			}
			vals.push('(' + nrec.join(',') + ')');
		}
		
		sql.push(vals.join(",\n"));
		sql.push(';');
		
		//$output.val(sql.join("\n"));
		return sql.join("\n");
	};
})();