var DLG = {};

$(function() {
	var $dlg = $('#dialogWrapper');
	var $iframe = $('#dialogIframe');
	var $dlgTrig = $('.dlgTrig');
	
	$dlgTrig.click(function(e) {
		var $this = $(this);
		var action = $this.data('dialog-action');
		DLG.open(action);
	});
	
	DLG = {
		noOpen: false,
		
		close: function() {
			this.noOpen = true;
			$.magnificPopup.close();
		},
		
		navTo: function(url) {
			document.location = url;	
		},
		
		ready: function(f) {
			if (typeof f === 'function') {
				f(this);
			}
		},
		
		popup: function() {
			if (this.noOpen) {
				return;
			}
			$.magnificPopup.open({
				items: [{
					type: 'inline',
					src: '#dialogWrapper',
				}],
			
				fixedContentPos: false,
				fixedBgPos: true,
			
				overflowY: 'auto',
			
				closeBtnInside: true,
				preloader: false,
				
				midClick: true,
				removalDelay: 300,
				mainClass: 'my-mfp-slide-bottom',
			});
		},
		
		open: function(action) {
			this.noOpen = false;
			var ctxt = this;
			var doIt = true;
			var locCnt = 0;
			
			$iframe.get(0).onload = function() {
				if (doIt) {
					doIt = false;
					ctxt.popup();
				}
			};
			$iframe.attr('src', action);
			
			
		},
		
		resizeDialog: function(w, h) {
			var vp = this.viewport();
			
			var dw = vp.width;
			var dh = vp.height;
			
			if (typeof w === 'undefined') {
				w = 860;
			}
			
			if (typeof h === 'undefined') {
				h = vp.height - 100;
			}/* else {
				h += 30
			}*/
			
			if (h > dh) {
				h = dh - 100;
			}
			
			if (h < 200) {
				h = 200
			}
			
			
			var cx = Math.floor(dw / 2);
			var cy = Math.floor(dh / 2);
			var l = Math.floor(cx - (w / 2));
			var t = Math.floor(cy - (h / 2));
			
			$dlg.css({
				position: 'fixed',
				left: l,
				top: t,
				width: w,
				height: h
			});
		},
		
		viewport: function() {
			var e = window, a = 'inner';
			if (!( 'innerWidth' in window ) ) {
				a = 'client';
				e = document.documentElement || document.body;
			}
			return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
		},	
	};
	
	
});