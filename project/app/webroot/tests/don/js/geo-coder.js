var CSV_GEOCODE = {};

$(function() {
	

	var geocoder = new google.maps.Geocoder();

	
	CSV_GEOCODE.next = function(list, idx) {
		var ctxt = this;
		if (idx + 1 < list.length) {
			setTimeout(function() {
				ctxt.revGeoCode(list, idx + 1);
			}, 5000);
		} else {
			ctxt.completed(ctxt.results);
		}
	};
	
  CSV_GEOCODE.revGeoCode = function(list, idx) {
		console.log("Coding " + (idx + 1) + ' of ' + list.length);
		var ctxt = this;
		var rec = list[idx]
		
		var lat_fld = typeof this.opts.lat === 'undefined' ? 'lat' : this.opts.lat;
		var lng_fld = typeof this.opts.lng === 'undefined' ? 'lng' : this.opts.lng;
		
		var lat = rec[lat_fld];
		var lng = rec[lng_fld];
		
		var point = new google.maps.LatLng(rec[lat_fld], rec[lng_fld]);
		
		geocoder.geocode({latLng: point}, function(results, status) {
		  if (status == google.maps.GeocoderStatus.OK) {
				if (results[0]) {
					var r = results[0].address_components;
					
					var number = '';
					var street = '';
					var city = '';
					var state = '';
					var zip = '';
					
					
					
					for (var i in r) {
						var ac = r[i];
						switch (ac.types[0]) {
							case 'street_number': number = ac.short_name; break;
							case 'route': street = ac.short_name; break;
							case 'locality': city = ac.short_name; break;
							case 'administrative_area_level_1': state = ac.short_name; break;
							case 'postal_code': zip = ac.short_name; break;
						}
					}
					
					var addr_str = number.trim().length > 0 ? (number + ' ') : '';
					
					var address = {
						'address': addr_str + street,
						'city': city,
						'state': state,
						'zip': zip,
						'lat': lat,
						'lng': lng
					};
					
					
					var nrec = {};
					$.extend(nrec, list[idx], address);
					console.log(nrec);
					
					ctxt.results.push(nrec);
					
					ctxt.next(list, idx);
				
				} else {
					console.log("Unable to to code " + lat + ', '  + lng);
					ctxt.next(list, idx);
				}
			} else {
				console.log("Failed to code " + lat + ', '  + lng + ': ' + status);
				ctxt.next(list, idx);
			}
		});
  };
	
	CSV_GEOCODE.revGeoCodeAll = function(list, opts, complete) {
		this.results = [];
		this.opts = opts;
		console.log('Reverse GeoCoding ' + list.length + ' coordinate' + (list.length == 1 ? '' : 's'));
		this.revGeoCode(list, 0);
		this.completed = complete;
	};
	
	
});