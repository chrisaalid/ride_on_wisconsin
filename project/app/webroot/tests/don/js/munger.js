$(function() {
	
	var $input = $('#inputField');
	var $output = $('#outputField');
	
	var $csvToJson = $('#actionCSVtoJSON');
	var $csvToSQL = $('#actionCSVtoSQL');
	var $revGeoCodeCSV = $('#actionRevGeoCodeCSV');
	
	
	var location_columns = ['user_id', 'name', 'type', 'address', 'city', 'state', 'lat', 'lng', 'member', 'discount', 'approved', 'created', 'modified'];
	var locations_template = [
		{t:'l',v:1},
		{t:'f',v:'Organization Name'},
		{t:'l',v:'bike-shop'},
		{t:'f',v:'Address'},
		{t:'f',v:'City'},
		{t:'l',v:'WI'},
		{t:'f',v:'lat'},
		{t:'f',v:'lng'},
		{t:'c',v: function(r) {return (r['Members'].trim().length > 0 ? 1 : 0);}},
		{t:'c',v: function(r) {return (r['Discount Provider'].trim().length > 0 ? 1 : 0);}},
		{t:'l',v:1},
		{t:'l',v:'1970-01-01 00:00:01'},
		{t:'l',v:'1970-01-01 00:00:01'},
	];
	
	var mtb_template = [
		{t:'l',v:1},
		{t:'f',v:'name'},
		{t:'l',v:'mtb-trail-head'},
		{t:'f',v:'address'},
		{t:'f',v:'city'},
		{t:'f',v:'state'},
		{t:'f',v:'lat'},
		{t:'f',v:'lng'},
		{t:'l',v:0},
		{t:'l',v:0},
		{t:'l',v:1},
		{t:'l',v:'1970-01-01 00:00:01'},
		{t:'l',v:'1970-01-01 00:00:01'},
	];
	
	var event_columns = ['id','user_id','name','start','end','venue','address','state','city','regions','tags','price','free','sponsor','email','phone','url','details','approved','created','modified'];
	var event_template = [
		{t:'l',v:1},
		{t:'l',v:1},
		{t:'l',v:1},
		{t:'l',v:1},
		{t:'l',v:1},
	];
	
	$csvToSQL.click(function(e) {
		//var sql = CSV_TO_SQL.process($input.val(), 'Locations', location_columns, mtb_template);
		var sql = CSV_TO_SQL.process($input.val(), 'Events', ['user_id', 'name', 'type', 'address', 'city', 'state', 'lat', 'lng', 'member', 'discount', 'approved', 'created', 'modified'], mtb_template);
		$output.val(sql);
		
		e.preventDefault();
	});
	
	
	$revGeoCodeCSV.click(function(e) {
		var list = CSV.parse($input.val());
		
		CSV_GEOCODE.revGeoCodeAll(list, {
			
		}, function(data){
			console.log("DONE!");
			console.log(data);
			$output.val(CSV.stringify(data));
		});
		
		e.preventDefault();
	});
	
});