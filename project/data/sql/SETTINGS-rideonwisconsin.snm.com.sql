-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 04, 2013 at 03:05 PM
-- Server version: 5.1.51
-- PHP Version: 5.3.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rideonwisconsin`
--

-- --------------------------------------------------------

--
-- Table structure for table `Settings`
--

DROP TABLE IF EXISTS `Settings`;
CREATE TABLE IF NOT EXISTS `Settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(200) NOT NULL DEFAULT '',
  `name` varchar(200) NOT NULL DEFAULT '',
  `data_type` varchar(30) NOT NULL DEFAULT 'string',
  `description` text NOT NULL,
  `content` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL DEFAULT '1970-01-01 00:00:01',
  PRIMARY KEY (`id`),
  KEY `key_idx` (`key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `Settings`
--

INSERT INTO `Settings` (`id`, `key`, `name`, `data_type`, `description`, `content`, `created`, `modified`) VALUES
(1, 'google-analytics', 'Google Analytics ID', 'string', '', 'UA-123', '2013-10-11 13:55:48', '2013-10-22 15:27:12'),
(2, 'sample-json', 'Some JSON', 'JSON', '', '{\r\n  "x": "something here" \r\n}', '2013-10-11 14:06:30', '1970-01-01 00:00:01'),
(4, 'regions', 'Regions', 'JSON', 'Coordinates for various geographical regions', '[\r\n	{\r\n		"name": "Wisconsin",\r\n		"points": [\r\n			{"x":-90.637207, "y":47.338821},\r\n			{"x":-89.956055, "y":47.279228},\r\n			{"x":-90.307617, "y":46.649437},\r\n			{"x":-90.065918, "y":46.346928},\r\n			{"x":-88.352051, "y":46.057983},\r\n			{"x":-87.604980, "y":45.644768},\r\n			{"x":-87.583008, "y":45.151054},\r\n			{"x":-87.011719, "y":45.537136},\r\n			{"x":-86.462402, "y":45.367584},\r\n			{"x":-87.011719, "y":43.929550},\r\n			{"x":-87.033691, "y":42.488300},\r\n			{"x":-90.615234, "y":42.536892},\r\n			{"x":-91.208496, "y":42.698586},\r\n			{"x":-91.450195, "y":43.786957},\r\n			{"x":-92.812500, "y":44.527843},\r\n			{"x":-93.032227, "y":45.537136},\r\n			{"x":-92.768555, "y":46.088470},\r\n			{"x":-92.395020, "y":46.164616},\r\n			{"x":-92.351074, "y":46.785015},\r\n			{"x":-90.637207, "y":47.338821}\r\n		]\r\n	},{\r\n		"name": "Central Wisconsin",\r\n		"points": [\r\n			{"x":-91.384277,"y":45.151054},\r\n			{"x":-90.681152,"y":43.405048},\r\n			{"x":-88.659668,"y":43.436966},\r\n			{"x":-88.725586,"y":44.964798},\r\n			{"x":-91.384277,"y":45.151054}\r\n		]\r\n	},{\r\n		"name": "Western Wisconsin",\r\n		"points": [\r\n			{"x":-89.670410,"y":46.271038},\r\n			{"x":-89.494629,"y":42.504501},\r\n			{"x":-90.769043,"y":42.536892},\r\n			{"x":-91.340332,"y":42.714733},\r\n			{"x":-91.494141,"y":43.834526},\r\n			{"x":-92.790527,"y":44.527843},\r\n			{"x":-93.054199,"y":45.521744},\r\n			{"x":-92.812500,"y":46.073231},\r\n			{"x":-92.373047,"y":46.164616},\r\n			{"x":-92.416992,"y":46.860191},\r\n			{"x":-90.549316,"y":47.353710},\r\n			{"x":-90.043945,"y":47.234489},\r\n			{"x":-90.241699,"y":46.604168},\r\n			{"x":-90.065918,"y":46.346928},\r\n			{"x":-89.670410,"y":46.271038}\r\n		]\r\n	},{\r\n		"name": "Eastern Wisconsin",\r\n		"points": [\r\n			{"x":-89.406738,"y":42.520699},\r\n			{"x":-89.604492,"y":46.271038},\r\n			{"x":-88.308105,"y":46.088470},\r\n			{"x":-87.583008,"y":45.644768},\r\n			{"x":-87.561035,"y":45.151054},\r\n			{"x":-86.901855,"y":45.598667},\r\n			{"x":-86.374512,"y":45.290348},\r\n			{"x":-86.967773,"y":43.929550},\r\n			{"x":-87.011719,"y":42.488300},\r\n			{"x":-89.406738,"y":42.520699}\r\n		]\r\n	},{\r\n		"name": "Southern Wisconsin",\r\n		"points": [\r\n			{"x":-92.153320,"y":44.197960},\r\n			{"x":-86.901855,"y":44.103367},\r\n			{"x":-87.033691,"y":42.488300},\r\n			{"x":-90.703125,"y":42.504501},\r\n			{"x":-91.318359,"y":42.779274},\r\n			{"x":-91.560059,"y":43.866219},\r\n			{"x":-92.153320,"y":44.197960}	\r\n		]\r\n	},{\r\n		"name": "Northern Wisconsin",\r\n		"points": [\r\n			{"x":-86.923828,"y":44.134914},\r\n			{"x":-92.263184,"y":44.213711},\r\n			{"x":-92.812500,"y":44.559162},\r\n			{"x":-93.054199,"y":45.506348},\r\n			{"x":-92.790527,"y":46.134171},\r\n			{"x":-92.329102,"y":46.210251},\r\n			{"x":-92.351074,"y":46.830135},\r\n			{"x":-90.615234,"y":47.338821},\r\n			{"x":-89.978027,"y":47.219566},\r\n			{"x":-90.285645,"y":46.664516},\r\n			{"x":-90.109863,"y":46.377254},\r\n			{"x":-88.330078,"y":46.057983},\r\n			{"x":-87.604980,"y":45.644768},\r\n			{"x":-87.561035,"y":45.213005},\r\n			{"x":-86.901855,"y":45.583290},\r\n			{"x":-86.352539,"y":45.274887},\r\n			{"x":-86.923828,"y":44.134914}\r\n		]\r\n	},{\r\n		"name": "The Driftless",\r\n		"points": [\r\n			{"x":-92.219238,"y":44.166447},\r\n			{"x":-90.329590,"y":43.945374},\r\n			{"x":-89.714355,"y":43.405048},\r\n			{"x":-89.472656,"y":42.617790},\r\n			{"x":-90.659180,"y":42.472095},\r\n			{"x":-91.340332,"y":42.666283},\r\n			{"x":-91.604004,"y":43.802818},\r\n			{"x":-92.219238,"y":44.166447}\r\n		]\r\n	}\r\n]', '2013-10-11 15:45:52', '2013-10-22 15:38:18');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
