-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 04, 2013 at 03:06 PM
-- Server version: 5.1.51
-- PHP Version: 5.3.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rideonwisconsin`
--

-- --------------------------------------------------------

--
-- Table structure for table `Templates`
--

DROP TABLE IF EXISTS `Templates`;
CREATE TABLE IF NOT EXISTS `Templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `tmpl` text NOT NULL,
  `model` varchar(100) NOT NULL DEFAULT '',
  `single` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `Templates`
--

INSERT INTO `Templates` (`id`, `name`, `description`, `tmpl`, `model`, `single`) VALUES
(4, 'slideshow.Route.slide', '', '<img src="/uploads/fetch/{{firstImage}}/450x250" class="slide{{bonusClass}}" data-route-id="{{id}}" alt="Slide" >', '', 0),
(5, 'slideshow.Route.nav', '', '<a class="index{{bonusClass}}" data-route-id="{{id}}"></a>', '', 0),
(6, 'search.map.Route.list', '', '<tr class="rtSearchResult route {{oddEven}}" data-type="{{dataType}}" data-id="{{id}}">\r\n	<td class="icon"><span aria-hidden="true" class="icon-path"></span></td>\r\n	<td class="search-info">\r\n      <a href="#!/routes/view/{{id}}">\r\n        <h4>{{name}} </h4>\r\n		<span class="result-type">{{difficultyLabel}} Ride/Trail {{distance}} miles</span> <br>\r\n		{{city}}, {{state}}{{#howFar}} - {{dispHowFar}} to route start{{/howFar}}\r\n      </a>\r\n	</td>\r\n</tr>', '', 0),
(3, 'slideshow.Route.detail', 'Template for the main content portion of the featured routes slideshow', '<div class="routeDetail{{bonusClass}}" data-route-id="{{id}}">\r\n  <h3>{{name}}</h3>\r\n  <span class="location">{{city}}, {{state}}</span>\r\n</div>', '', 0),
(7, 'search.map.Route.info', '', '<div class="content{{dataType}}">\r\n  <strong>{{name}}</strong><br>\r\n  {{city}}, {{state}} - {{distance}} miles<br>\r\n  <a href="#!/{{ctrl}}/view/{{id}}" class="row-dialog-trigger" data-dialog-action="/{{ctrl}}/view/{{id}}">View Details</a>\r\n</div>', '', 0),
(8, 'search.map.Location.list', '', '<tr class="rtSearchResult location {{oddEven}}" data-type="{{dataType}}" data-id="{{id}}">\r\n  <td class="icon"><span aria-hidden="true" class="icon-location icon-{{subType}}"></span></td>\r\n  <td class="search-info">\r\n    <a href="#!/locations/view/{{id}}">\r\n      <h4>{{name}} </h4>\r\n      <span class="result-type">{{subTypeName}}</span> <br>\r\n      {{city}}, {{state}}{{#howFar}} - {{dispHowFar}} miles away{{/howFar}}\r\n    </a>\r\n  </td>\r\n</tr>', '', 0),
(9, 'search.map.Location.info', '', '<div class="content{{dataType}}">\r\n  <strong>{{name}}</strong><br>\r\n  <span>{{subTypeName}}</span><br>\r\n  <div class="address">\r\n    {{address}}<br>\r\n    {{city}}, {{state}}<br>\r\n  </div>\r\n  <a href="#!/{{ctrl}}/view/{{id}}" class="row-dialog-trigger" data-dialog-action="/{{ctrl}}/view/{{id}}">View Details</a>\r\n</div>', '', 0),
(10, 'search.calendar.Event.list', '', '<tr class="evSearchResult {{oddEven}}" data-type="{{dataType}}" data-id="{{id}}">\n  <td class="date">{{startDate}}</td>\n  <td class="search-info">\n    <a href="#!/events/view/{{id}}">\n      <h4>{{name}}</h4>\n      <span class="location">{{venue}}, {{city}}, {{state}}</span>\n    </a>\n  </td>\n</tr>', '', 0),
(11, 'slideshow.Route.wrapper', '', '<div class="ss-featured-rides-container">\r\n	<a href="#" class="btn-prev">&lt;</a><a href="#" class="btn-next">&gt;</a>\r\n	<img src="/images/bg-featured-rides.svgz" alt="">\r\n	<div class="slide-container">\r\n		<div class="slides">\r\n          {{{slides}}}\r\n		</div><!-- /.slides -->\r\n	</div><!-- /.slide-container -->\r\n	<div class="texts">\r\n		{{{details}}}\r\n	</div><!-- .texts -->\r\n  <a href="#" class="details row-dialog-trigger" data-dialog-action="/routes/view/{{firstID}}">View Details</a>\r\n	<div class="indexes">\r\n      {{{nav}}}\r\n	</div><!-- /.indexes -->\r\n	<div class="shadow-featured-rides">\r\n		<img src="/images/shadow-featured-rides.png">\r\n	</div>                  \r\n</div>', '', 0),
(12, 'search.classifieds.Classified.list', 'Template for classifieds results list', '<tr class="{{oddEven}}">\r\n  <td class="thumb"><img src="/uploads/fetch/{{firstImage}}/80x80" alt="image"></td>\r\n  <td>\r\n    <a href="/classifieds/view/{{id}}/from-search" class="title"><h2>{{title}}</h2></a>\r\n    <span class="details">\r\n      {{#isBike}}\r\n	  	Complete Bike: {{bikeMake}} / {{bikeModel}}\r\n      {{/isBike}}\r\n      {{#isPart}}\r\n	    Bike Part: {{partMake}} / {{partModel}} / {{partType}}\r\n      {{/isPart}}\r\n    </span><br>\r\n    <span class="price">${{formattedPrice}}</span><br>\r\n    <span class="city">{{city}}</span>, <span class="state">{{state}}</span> / <span class="distance">{{howFar}} miles away</span>\r\n  </td>\r\n</tr>', '', 0),
(13, 'featured.Event', '', '<div class="featured-event">\r\n  <div class="sec03-pic">\r\n    <img src="/uploads/fetch/{{firstImage}}/300x180" alt="{{name}}">               \r\n  </div>\r\n  <div class="info">\r\n    <div class="date">{{featuredTime}}</div>\r\n    <div class="details"><a href="#" class="row-dialog-trigger" data-dialog-action="/events/view/{{id}}">{{name}}</a></div>\r\n    <div class="location">{{venue}}, {{city}}, {{state}}</div>\r\n  </div>\r\n  <div style="clear:both;"></div>               \r\n</div><!-- /.featured-event -->', '', 0),
(14, 'search.groups.Group.list', '', '<tr class="{{oddEven}}">\r\n  <td><h2><a href="/groups/view/{{id}}/from-search">{{name}}</a></h2></td>\r\n  <td>{{city}}, {{state}}</td>\r\n  <td class="ta-center">\r\n    {{howFar}} Miles\r\n  </td>\r\n</tr>', '', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
