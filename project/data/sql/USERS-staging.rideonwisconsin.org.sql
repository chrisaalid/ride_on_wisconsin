-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: internal-db.s132128.gridserver.com
-- Generation Time: Nov 04, 2013 at 12:49 PM
-- Server version: 5.1.55-rel12.6
-- PHP Version: 5.3.27

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db132128_rowstgdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

--
-- Dumping data for table `Users`
--

INSERT INTO `Users` (`id`, `username`, `firstname`, `lastname`, `email`, `password`, `resetkey`, `admin`, `developer`, `canedit`, `canapprove`, `state`, `city`, `profile`, `approved`, `created`, `modified`) VALUES
(8, 'swelter', 'Scott', 'Welter', 'welterdesign@gmail.com', '8177f9ecf14a68bb29a851b54600d5dfda996197', '5bc6cb57', 0, 0, 0, 0, 'WI', 'Madison', '', 1, '2013-08-01 12:30:00', '2013-08-01 12:30:47'),
(2, 'don', 'Don', 'Smith', 'dsmith+don@shineunited.com', 'e527c8fd8c769d6bb4d93b021a86ded2568d889d', 'ee7f5b73', 1, 1, 0, 0, 'WI', 'Madison', 'Shine developer.  Bike commuter.', 1, '2013-05-17 09:07:39', '2013-05-17 09:08:39'),
(7, 'nicknewlin', 'Nick', 'Newlin', 'nnewlin@shineunited.com', '7206c02da65f60737520e6c0033df54291945d69', '76d94c0e', 0, 0, 0, 0, 'WI', 'madison', '', 1, '2013-08-01 12:24:11', '2013-08-01 12:35:31'),
(4, 'bfwadmin', 'Bike', 'Fed', 'dsmith+bfwadmin@shineunited.com', 'c9a3d166fe94fa41e2d9b87736c7663ead3ed05c', 'fb26f481', 1, 0, 0, 0, 'WI', 'Madison', 'Ride On!', 1, '2013-05-17 09:16:30', '2013-05-17 09:16:59'),
(10, 'etk6v4', 'Emily', 'Steele', 'esteele@shineunited.com', 'b65bb83f5da90d1f73d714adcc2c802c0cb41100', 'df1418ad', 0, 0, 0, 0, 'WI', 'Madison', '', 1, '2013-08-01 13:01:15', '2013-08-01 13:11:22'),
(16, 'jakenewborn', 'Jake', 'Newborn', 'jake.newborn@wisconsinbikefed.org', 'c40cf0b0cd7fbb04880f8ff85fb86a1db689b7e4', '9a97b5c7', 0, 0, 0, 0, 'WI', 'Milwaukee', 'Youth Education Program Manager at Bike Fed', 1, '2013-08-05 10:33:51', '2013-08-05 10:34:19'),
(11, 'chrisaalid', 'Chris', 'Aalid', 'chris.aalid@wisconsinbikefed.org', 'a96eef7904fd2094ad7ab06fd1e2bbeacfe9a9f4', '03188148', 1, 0, 0, 0, 'WI', 'Madison', '', 1, '2013-08-01 13:43:51', '2013-08-02 08:30:35'),
(12, 'DaveSchlabowske', 'Dave', 'Schlabowske', 'dave.schlabowske@wisconsinbikefed.org', 'c91a645d1e22871706b03f4e908c8196c85bfbb8', 'be6b59bd', 1, 0, 0, 0, 'WI', 'Milwaukee', 'Rides 365', 1, '2013-08-01 13:54:16', '2013-08-02 12:38:54'),
(13, 'Bjmainwood', 'Barry', 'Mainwood', 'bjmainwood@att.net', 'c3d47665a81868ce2b95d42a507181b154bfb57d', '6cde7b0f', 0, 0, 0, 0, 'WI', 'New Berlin', '', 1, '2013-08-02 09:42:36', '2013-08-02 09:43:48'),
(14, 'mmaffitt', 'Mark', 'Maffitt', 'Mark.Maffitt@gmail.com', 'e6ac3e95375e073298a0dc45d93c4b6911e2e153', '4bb09ada', 0, 0, 0, 0, 'WI', 'Madison', '', 1, '2013-08-02 10:50:56', '2013-08-02 10:51:35'),
(15, 'Barneszj', 'Zachary', 'Barnes', 'central@wisconsinbikefed.org', '885af8ea3811b6cd3b3b23a496fd7f15629ebc4d', '4b973fe2', 0, 0, 0, 0, 'WI', 'Madison', 'Zachary Barnes, The Wisconsin Bike Federation''s newest hire and Central Region Director (covering Dane, Columbia, Dodge, Jefferson, Rock, Green, Lafayette, Iowa, and Sauk counties), is striving for a diverse network of bicycle and pedestrian advocates and ambassadors. \r\n\r\nWe often find Zac (or Zachary/Zacarías) wearing a Share & Be Aware shirt and hat, speaking in several tongues (Zacarías, his Spanish name, can also be used), informing the State of a need to see bicyclists and pedestrians as traffic. The increase in numbers of bicyclists and pedestrians fuels the need for this education work and will continue to tout these modes of transportation (especially partnered with public transit!) as the most efficient and enjoyable way to move around our communities.\r\n\r\nWe look forward to seeing Zacarías doing public and private partnerships to spread this multicultural education! If you or an organization in your community is interested in more information about brown bag almuerzos, and other ways to increase safe and assertive pedestrian and bicycle activity in your hometown, please be in touch!\r\n\r\nGracias y mucho gusto, el placer es mio. (Thank you, nice to meet you, the pleasure is mine)\r\n\r\n\r\nZachary Barnes\r\nWisconsin Bike Fed \r\nCentral Region Director\r\ncentral@wisconsinbikefed.org\r\n608-807-1180\r\n\r\n409 E. Main Street, Suite 203 Madison, WI 53703 \r\nPO Box 1224 Madison, WI 53701-1224\r\n\r\n www.wisconsinbikefed.org\r\n', 1, '2013-08-05 09:03:44', '2013-08-05 09:04:22'),
(17, 'amyklein', 'Amy', 'Klein', 'amy@redamte.com', 'e2b97b13da42113b445152d1845af85979c2d908', 'e5450760', 0, 0, 0, 0, 'WI', 'Madison', '', 1, '2013-08-07 07:37:32', '2013-08-07 07:56:21'),
(18, 'carolyndvorak', 'Carolyn', 'Dvorak', 'carolyn.dvorak@wisconsinbikefed.org', '2a6b6a1a84ff3b331ca0df0b1ff487f0a0bc7ba0', '553c1002', 0, 0, 0, 0, 'WI', 'Holmen', '', 1, '2013-08-08 08:26:49', '2013-08-08 08:29:58'),
(19, 'BetsyPopMass', 'Betsy', 'Massnick', 'betsy.massnick@wisconsinbikefed.org', '5a26577c72f8f21d171d6e9d07359a5e0db9ad45', 'fa49d267', 0, 0, 0, 0, 'WI', 'Madison', '', 1, '2013-08-09 09:55:22', '2013-08-09 09:56:12'),
(20, 'MDr3ws', 'Matt', '', 'matt.andrews@wisconsinbikefed.org', '209ee1480b7b71df2667fd2bf42a0b6c06b59ab1', '8b3f5a44', 0, 0, 0, 0, 'WI', 'Eau Claire', '', 1, '2013-08-09 13:11:19', '2013-08-09 13:12:01'),
(21, 'SpinCycleRiders', 'Brian', 'Fried', 'brianf531@gmail.com', 'b4460120472c3d36a2971bb96856f9b9d7bf17e3', 'd5872a0c', 0, 0, 0, 0, 'WI', 'Minocqua', 'A rider''s group based in Minocqua, Wisconsin', 1, '2013-08-13 10:48:34', '2013-08-13 10:48:55'),
(22, 'Pgallagher', 'Pat', 'Gallagher', 'pgallagher@entercom.com', 'b7ea5a64d930119e453036eb0aa94852420354f1', 'b0a3e90c', 0, 0, 0, 0, 'WI', 'Madison', '', 1, '2013-08-13 12:46:03', '2013-08-13 12:46:30'),
(23, 'BikeCzar', 'Dave', 'Schlabowske', 'daveschlabowske@yahoo.com', 'c91a645d1e22871706b03f4e908c8196c85bfbb8', 'c44e24b5', 0, 0, 0, 0, 'WI', 'Milwaukee', '', 0, '2013-08-13 13:41:09', '1970-01-01 00:00:01'),
(24, 'sarahgaskell', 'Sarah', 'Gaskell', 'sarah.gaskell@wisconsinbikefed.org', '141783773374377662f3f0fcc6e97eef037845d7', 'e6fabeab', 0, 0, 0, 0, 'WI', 'Verona', '', 1, '2013-08-13 14:25:48', '2013-08-13 14:26:08'),
(25, 'Olmo', 'Jessica ', 'Ginster', 'jessica.ginster@wisconsinbikefed.org', '817fb8a3d771c6da03f28b3d8bc699248e8c677f', 'db0616ff', 0, 0, 0, 0, 'WI', 'Milwaukee', '', 1, '2013-08-13 17:20:46', '2013-08-13 17:21:04'),
(26, 'Daveharney', 'David', 'Harney', 'daveharney@wi.rr.com', '1f3f8b22e6935ff82f9f020bbdec9ea88b6da46c', '9a31b4d5', 0, 0, 0, 0, 'WI', 'Grafton', 'I''m a long time bicycle advocate who lives in Ozaukee County.  I manage the Ozaukee Bike Routes website (bikex.net).  The mission of this website is to encourage residents and visitors to explore Ozaukee County by bicycle.  The website partners with the Ozaukee County Inter Urban Trail and the Treasurers of OZ organization that promotes the recreational and environmental assets of the county.', 1, '2013-08-13 18:00:48', '2013-08-13 18:02:19'),
(27, 'bkw', 'Brian', 'Werner', 'wernerjunk42@gmail.com', 'f7351c461ec7f79b9262c5cffe1209d0876d9855', '6ed3b8aa', 0, 0, 0, 0, 'WI', 'Edgar', '', 1, '2013-08-13 19:17:16', '2013-08-13 19:19:46'),
(28, 'raz', 'Randy', 'Zarecki', 'raz@WisconsinBikeFed.org', '80fe5277882a8b3ab8b1b7aa8c760ffc2d48adc0', '26121977', 0, 0, 0, 0, 'WI', 'Barnes', '', 1, '2013-08-14 05:13:44', '2013-08-14 05:15:06'),
(29, 'tomklein', 'Tom', 'Klein', 'tom.klein@wisconsinbikefed.org', '1e19a85c03c157547ba6a88bffc7cf20e83343b9', '93b596a2', 0, 0, 0, 0, 'WI', 'Madison', 'Dane County Director for the Wisconsin Bike Fed', 1, '2013-08-14 07:28:27', '2013-08-14 07:29:22'),
(30, 'scottreilly', 'Scott', 'Reilly', 'scott.reilly@wisconsinbikefed.org', '9f601e044be1a71a8c6d4c1da6d76416969c73b8', '64243ac6', 0, 0, 0, 0, 'WI', 'Stevens Point', '', 1, '2013-08-14 10:51:52', '2013-08-14 10:54:13'),
(31, 'MJS', 'Mike', 'Stafford', 'gbpackr@aol.com', 'c24a1df56055956c584dcc431392e5888d1da82a', '6031cfd4', 0, 0, 0, 0, 'WI', 'Milwaukee', 'Casual rider', 1, '2013-08-14 11:35:28', '2013-08-14 11:37:05'),
(32, 'shage249', 'sarah', 'hagedon', 'sarah.hagedon@wisconsinbikefed.org', '292d77cd1656f654f8c9a50e4f0d63a8b45956c4', 'd0558f7f', 0, 0, 0, 0, 'WI', 'madison', 'Bike Parking!', 1, '2013-08-14 14:45:56', '2013-08-14 14:46:18'),
(33, 'Robbie', 'Robbie', 'Webber', 'robbie@robbiewebber.org', '7eb73c4fcc14ff555e9950e3baaf343afb6c8072', 'adf37137', 0, 0, 0, 0, 'WI', 'Madison', '', 1, '2013-08-16 11:59:02', '2013-08-16 12:02:15'),
(34, 'chill', 'Chuck', 'Hill', 'chill@newnorth.net', '348c8cce69aa01a284046ecc567c5404216c3d3e', '844b681d', 0, 0, 0, 0, 'WI', 'Eagle River', '', 1, '2013-08-20 07:41:00', '2013-08-20 07:54:27'),
(35, 'kauerback', 'Kathryne', 'Auerback', 'kauerback@gmail.com', '86c08330294091da0f27783a945c1386ea20dd71', '7eeb6c15', 0, 0, 0, 0, 'WI', 'Madison', '', 1, '2013-08-25 06:25:41', '2013-08-25 06:26:03'),
(36, 'melissakvernon', 'melissa', 'vernon', 'melissakvernon@yahoo.com', 'b8c444761ac36c0ded44fff2cd14c145ef7a9c72', '84583d07', 0, 0, 0, 0, 'WI', 'oconomowoc', '', 1, '2013-08-28 08:48:30', '2013-08-28 08:49:05');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
