-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 10, 2013 at 11:00 AM
-- Server version: 5.1.51
-- PHP Version: 5.3.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rideonwisconsin`
--

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE IF NOT EXISTS `Users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL DEFAULT '',
  `firstname` varchar(100) NOT NULL DEFAULT '',
  `lastname` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `password` char(40) NOT NULL DEFAULT '',
  `resetkey` char(64) NOT NULL DEFAULT '',
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `canedit` tinyint(1) NOT NULL DEFAULT '0',
  `canapprove` tinyint(1) NOT NULL DEFAULT '0',
  `state` char(2) NOT NULL DEFAULT 'WI',
  `city` varchar(100) NOT NULL DEFAULT '',
  `profile` text NOT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL DEFAULT '1970-01-01 00:00:01',
  `developer` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_idx` (`username`),
  UNIQUE KEY `email_idx` (`email`),
  KEY `resetkey_idx` (`resetkey`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `Users`
--

INSERT INTO `Users` (`id`, `username`, `firstname`, `lastname`, `email`, `password`, `resetkey`, `admin`, `developer`, `canedit`, `canapprove`, `state`, `city`, `profile`, `approved`, `created`, `modified`)
VALUES (1, 'admin', 'Admin', 'User', 'contact@shineunited.com', 'd229546a171a3cbe80029e06f10eb88ff73b1be3', '7d4413cab2f3d0fd6f3c7a06549a5fe80ff7bba83fbe203104a86d725389ea02', 1, 1, 1, 1, 'WI', 'Madison', 'Shine On!', 1, '2013-03-12 18:30:24', '1970-01-01 00:00:01');

INSERT INTO `Users` (`id`, `username`, `firstname`, `lastname`, `email`, `password`, `resetkey`, `admin`, `canedit`, `canapprove`, `state`, `city`, `profile`, `approved`, `created`, `modified`, `developer`) VALUES
(9, 'testuser', 'test', 'user', 'dsmith+testuser@shineunited.com', '47b37c675bb4de7e5c01b7c2a637f4da8fbbcf00', '88a492ea', 0, 0, 0, 'WI', 'Madison', 'Meh!', 1, '2013-05-29 14:33:27', '2013-05-29 16:55:21', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

