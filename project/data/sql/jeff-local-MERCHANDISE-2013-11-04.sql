-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 04, 2013 at 01:45 PM
-- Server version: 5.5.29
-- PHP Version: 5.3.20

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rideonwisconsin`
--

-- --------------------------------------------------------

--
-- Table structure for table `Merchandise`
--

DROP TABLE IF EXISTS `Merchandise`;
CREATE TABLE `Merchandise` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `details` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL DEFAULT '1970-01-01 00:00:01',
  `url` varchar(255) NOT NULL DEFAULT '',
  `position` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `position_idx` (`position`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `Merchandise`
--

INSERT INTO `Merchandise` (`id`, `name`, `price`, `details`, `created`, `modified`, `url`, `position`) VALUES
(1, 'Forward T-Shirt', 25.00, 'The Forward shirt symbolizes our momentum as a bicycling community. We are united, strong, and moving â€œforwardâ€ together to make Wisconsin an even better place to live and bicycle. T-shirts come in mens and womens sizes and have the Bike Fed logo on the back.', '2013-10-24 13:28:35', '2013-10-31 11:37:26', '', 3),
(2, 'Wisconsin Bike Fed Magazine (003 - October 2013)', 8.00, '<strong><em>Our brand new magazine! </em></strong> The October 2013 issue features the Door County Century, a report on why Americans are driving less and a piece by <a href="http://theactivepursuit.com/">Tom Held</a> on Richard Schwinn and the talented builders at Waterford Precision Cycles ', '2013-10-25 15:36:43', '2013-10-31 15:37:48', '', 0),
(3, 'Endure: Ballet in the Mud', 35.00, '<em>Authored by Gary Boulanger, photographs by Peter DiAntoni, Darren Hauck, Jeffrey Phelps and Dave Schlabowske</em>\r\n\r\n<p>Endure: Ballet in the Mud, features selected photos from a new book to be published in November 2013 by Wisconsin Bike Fed. The book features more than 300 photographs taken by four Wisconsin-based photographers â€” Peter DiAntoni, Darren Hauck, Jeffrey Phelps and Dave Schlabowske â€“ at the 2013 USA Cycling Cyclo-cross National Championships held in January in Verona, Wis. Nationally-known cycling writer and Green Bay native, Gary Boulanger, served as the bookâ€™s author.</p>', '2013-10-25 15:38:14', '2013-10-31 11:37:28', '', 2),
(4, 'Wheel Fever', 24.95, '<em>by <a href="http://wheelfeverbook.com/">Jesse J. Gant &amp; Nicholas J. Hoffman</a></em>\r\n\r\n<p>\r\nWheel Fever is about the origins of bicycling in Wisconsin and why those origins still matter.  From the velocipede era to the crashing end of first bicycle boom, the authors analyze the impassioned debate over who should be allowed to ride, where they could ride, and even what they could wear.</p>\r\n\r\n<em>â€œWheel Fever gives an excellent overview of bicycle development during a crucial period when the curious two-wheeler was still vying to prove its lasting value.â€ â€” David Herlihy, From the foreword</em>', '2013-10-25 15:39:28', '2013-10-31 15:38:06', '', 1),
(5, 'Bike Fed Jersey', 85.00, 'Bike Fed jerseys are custom made after you place your order. Allow four to eight weeks for delivery. Jerseys come in menâ€™s and womenâ€™s sizes.', '2013-10-31 10:18:59', '1970-01-01 00:00:01', 'https://bfw.memberclicks.net/online-store', 4),
(6, 'Bike Fed Bib Shorts', 100.00, 'Bike Fed bib shorts are custom made after you place your order. Allow 4-8 weeks for delivery. Bib shorts have the message â€œRide on Wisconsinâ€ featured on the lower back and come in menâ€™s S-XL.', '2013-10-31 10:20:32', '1970-01-01 00:00:01', 'https://bfw.memberclicks.net/online-store', 5),
(7, 'Bike Fed Team Shorts', 85.00, 'Bike Fed Team Shorts are custom made after you place your order. Allow 4-8 weeks for delivery. Bib shorts have the message â€œRide on Wisconsinâ€ featured on the lower back and come in womenâ€™s S-XL.', '2013-10-31 10:21:15', '1970-01-01 00:00:01', 'https://bfw.memberclicks.net/online-store', 6),
(8, '3 Ft Law Yard Sign', 10.00, 'These signs are available to order online or can be purchased at the following stores: <strong>Sprocketz</strong> in Wausau, <strong>Bikes N Boards</strong> in Rhinelander, <strong>Cyclesmith</strong> in Waukesha, <strong>Appleton Bicycle Shop</strong> and <strong>Wheel &amp; Sprocket</strong> (all locations). You can also purchase and pick up yard signs from our Madison and Milwaukee offices. Please call ahead if you are interested in picking them up. <br>', '2013-10-31 10:52:41', '2013-10-31 13:30:55', 'https://bfw.memberclicks.net/online-store', 7),
(9, 'Wisconsin State Trail Pass', 20.00, 'Provides access to non-motorized state trails managed by the Wisconsin Department of Natural Resources through the end of the calendar year. ', '2013-10-31 10:53:29', '1970-01-01 00:00:01', 'https://bfw.memberclicks.net/online-store', 8),
(10, 'Bicycle Maps of Wisconsin', 6.25, '<strong>$6.25 per map for non-members, $4.25 for members</strong><br>\r\n\r\nFour maps â€“ North, South, East and West Save $8 on the set of four maps by joining the Bike Fed today! <br>\r\n', '2013-10-31 10:54:49', '2013-10-31 13:31:24', 'https://bfw.memberclicks.net/online-store', 9),
(11, 'I Bike Wisconsin T-Shirt', 25.00, '50% organic cotton, 50% recycled plastic bottles T-shirts come in mens and womens sizes and have the Wisconsin Bike Fed logo centered on the back. <br>', '2013-10-31 10:59:54', '2013-10-31 13:31:39', 'https://bfw.memberclicks.net/online-store', 10),
(12, 'DeFeet cycling socks with Bike Fed logo', 11.00, '60 % Nylon 39% Coolmax eco made, 1% Lycra Socks have 2â€³ cuffs and come in sizes M-XL. <strong>M</strong> = Womenâ€™s sizes 8.5-10 & Menâ€™s sizes 7-9. <strong>L</strong> = Womenâ€™s sizes 11-13 & Menâ€™s sizes 9.5-11.5. <strong>XL</strong> = Menâ€™s sizes 12+ ', '2013-10-31 11:02:20', '1970-01-01 00:00:01', '', 11),
(13, 'â€œBiking on Bike Trails Between Chicago & Milwaukeeâ€ book', 14.95, '<p>Peter Plommerâ€™s book details a 100-mile Route between Chicagoâ€™s Loop and Downtown Milwaukee â€“ 80% on Off-Road Bike Paths. Book Provides:</p>\r\n<ul>\r\n<li>Maps &amp; Route Descriptions going both North &amp; South</li>\r\n<li>Easy to Follow Best Connector Routes between Bike Paths</li>\r\n<li>Suggestions on Day &amp; Side Trips</li>\r\n</ul>', '2013-10-31 12:12:41', '1970-01-01 00:00:01', 'https://bfw.memberclicks.net/online-store', 12),
(14, '*NEW* Ceramic Mugs', 25.00, '<p>Available in three styles! Each item is handcrafted so no two will look exactly alike. It will resemble what is shown here, but will not be an exact match. Pottery is <em>handmade</em> by an artist in Milwaukee.</p>\r\n<ul>\r\n<li><span style="line-height: 19px;">Water/leak tested</span></li>\r\n<li><span style="line-height: 19px;">Dishwasher safe</span></li>\r\n<li><span style="line-height: 19px;">Usable Art</span></li>\r\n</ul>', '2013-10-31 12:15:09', '1970-01-01 00:00:01', 'https://bfw.memberclicks.net/online-store', 13),
(15, '*NEW* Ceramic Platters', 55.00, '<p>Available in two styles! Each item is handcrafted so no two will look exactly alike. It will resemble what is shown here, but will not be an exact match. Pottery is <em>handmade</em> by an artist in Milwaukee.</p>\r\n\r\n<ul>\r\n<li><span style="line-height: 19px;">Water/leak tested</span></li>\r\n<li><span style="line-height: 19px;">Dishwasher safe</span></li>\r\n<li><span style="line-height: 19px;">Usable Art</span></li>\r\n</ul>\r\n', '2013-10-31 12:16:59', '1970-01-01 00:00:01', 'https://bfw.memberclicks.net/online-store', 14),
(16, '*NEW* Wisconsin Bike Fed Poster', 100.00, '<p>Beautifully designed posters by artist <a href="http://michaelvalenti.com/michael/" target="_blank">Michael Valenti</a>. Each poster is printed as a GiclÃ©e print on heavyweight stock with archival inks. The poster is then rolled and shipped in a tube. Each is made to order.</p>', '2013-10-31 13:26:14', '1970-01-01 00:00:01', 'https://bfw.memberclicks.net/online-store', 15),
(17, '*NEW* Ceramic Beer Steins', 30.00, '<p>Each item is handcrafted so no two will look exactly alike. It will resemble what is shown here, but will not be an exact match. Pottery is <em>handmade</em> by an artist in Milwaukee.</p>\r\n<p>Stein size is H 6&#8243;, W 2.3/4&#8243; and each stein can hold 14oz. of liquid</p>\r\n\r\n<ul>\r\n<li><span style="line-height: 19px;">Water/leak tested</span></li>\r\n<li><span style="line-height: 19px;">Dishwasher safe</span></li>\r\n<li><span style="line-height: 19px;">Usable Art</span></li>\r\n</ul>', '2013-10-31 13:27:24', '2013-10-31 13:29:35', 'https://bfw.memberclicks.net/online-store', 16);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
