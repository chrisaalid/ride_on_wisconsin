-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 04, 2013 at 01:51 PM
-- Server version: 5.5.29
-- PHP Version: 5.3.20

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rideonwisconsin`
--

-- --------------------------------------------------------

--
-- Table structure for table `Uploads`
--

CREATE TABLE `Uploads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `filename` varchar(200) NOT NULL DEFAULT '',
  `md5` char(32) NOT NULL DEFAULT '',
  `mime_type` varchar(50) NOT NULL DEFAULT 'application/octet-stream',
  `size` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `event_id` int(10) unsigned NOT NULL DEFAULT '0',
  `classified_id` int(10) unsigned NOT NULL DEFAULT '0',
  `route_id` int(10) unsigned NOT NULL DEFAULT '0',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `location_id` int(10) unsigned NOT NULL DEFAULT '0',
  `merchandise_id` int(10) unsigned NOT NULL DEFAULT '0',
  `order` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id_idx` (`user_id`),
  KEY `event_id_idx` (`event_id`),
  KEY `classified_id_idx` (`classified_id`),
  KEY `route_id_idx` (`route_id`),
  KEY `group_id_idx` (`group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=102 ;

--
-- Dumping data for table `Uploads`
--

INSERT INTO `Uploads` (`id`, `filename`, `md5`, `mime_type`, `size`, `user_id`, `event_id`, `classified_id`, `route_id`, `group_id`, `location_id`, `merchandise_id`, `order`) VALUES
(63, 'Merch-tShirt-Forward-Womens-200x200.jpg', '85fd903811f7188ad5e77f20431ea974', 'image/jpeg', 11385, 1, 0, 0, 0, 0, 0, 1, 2),
(62, 'Merch-tShirt-Forward-Mens-200x200.jpg', '7950a2758a1a8181f373a81982abfce4', 'image/jpeg', 9870, 1, 0, 0, 0, 0, 0, 1, 1),
(64, '03_WIBikeFedMagazine_Page_01-780x1024.jpg', '9485ad7aed57bf98d9b502acc5c2fef3', 'image/jpeg', 288057, 1, 0, 0, 0, 0, 0, 2, 1),
(65, 'Endure-PreOrder-Small.jpg', '6f21f80507452f65f017cf045decd79e', 'image/jpeg', 80139, 1, 0, 0, 0, 0, 0, 3, 1),
(66, 'wheel-fever-cover.jpg', 'd21d00fcc6577e0298801c26f6c9999c', 'image/jpeg', 81681, 1, 0, 0, 0, 0, 0, 4, 1),
(67, 'merch-fwi-tshirt.jpg', '6684706fc3168c4229c25015c0a9bb60', 'image/jpeg', 13026, 1, 0, 0, 0, 0, 0, 1, 3),
(70, 'merch-fwi-tshirt-womens.jpg', 'c3ac192e09b451ae94c2512b05ba00ac', 'image/jpeg', 9107, 1, 0, 0, 0, 0, 0, 1, 4),
(71, 'merch-bikefed-mag.jpg', 'e56112577c02c2d70c25279a1e3dc30e', 'image/jpeg', 17650, 1, 0, 0, 0, 0, 0, 2, 2),
(72, 'merch-endure-preorder.jpg', '95d54314d3bfd164b17c7db1f0d7c390', 'image/jpeg', 14088, 1, 0, 0, 0, 0, 0, 3, 2),
(73, 'merch-wheel-fever.jpg', 'a44fe3deb6a393f1c3449dfc5c419926', 'image/jpeg', 19585, 1, 0, 0, 0, 0, 0, 4, 2),
(74, 'merch-jersey-mens.jpg', '3358e7945480154ea380dfa4a5651d93', 'image/jpeg', 8699, 1, 0, 0, 0, 0, 0, 5, 1),
(75, 'merch-jersey-womens.jpg', '86aef2cbb6282f30cda3bd02cc0861a2', 'image/jpeg', 9945, 1, 0, 0, 0, 0, 0, 5, 2),
(76, 'merch-bib-shorts.jpg', '2ca3cfb50ec22cecd7cb806513836c32', 'image/jpeg', 7216, 1, 0, 0, 0, 0, 0, 6, 1),
(77, 'merch-shorts-women.jpg', '6758bdd1fe79eb1079b0a03ab92ac3ca', 'image/jpeg', 8040, 1, 0, 0, 0, 0, 0, 7, 1),
(78, 'merch-yard-sign.jpg', '4344cfcda442dbbecdb25416d7e4e074', 'image/jpeg', 12147, 1, 0, 0, 0, 0, 0, 8, 1),
(79, 'merch-trail-pass.jpg', '63c076e465a5fb7e471fc84d3a78971d', 'image/jpeg', 13311, 1, 0, 0, 0, 0, 0, 9, 1),
(80, 'merch-bike-maps.jpg', 'ade959f2e9a16f3602b9f9cbed3a7192', 'image/jpeg', 15372, 1, 0, 0, 0, 0, 0, 10, 1),
(81, 'merch-bike-wi-tshirt-mens.jpg', '8cdc9ea62698b7f6625586b0f3f51b76', 'image/jpeg', 9288, 1, 0, 0, 0, 0, 0, 11, 1),
(82, 'merch-bike-wi-tshirt-womens.jpg', 'cd24ef7d5c7356079552372c59b42776', 'image/jpeg', 8002, 1, 0, 0, 0, 0, 0, 11, 2),
(83, 'merch-socks-short.jpg', '67a450616d986618afb4244954c1cf63', 'image/jpeg', 12795, 1, 0, 0, 0, 0, 0, 12, 1),
(84, 'merch-book-bikingtrails.jpg', '6e52c143ac4bb5db3a800c16fb3029b5', 'image/jpeg', 15942, 1, 0, 0, 0, 0, 0, 13, 1),
(85, 'merch-mugs01.jpg', '7a2ad9853a8920a3f8d05b8405e96693', 'image/jpeg', 6016, 1, 0, 0, 0, 0, 0, 14, 1),
(86, 'merch-mugs02.jpg', 'd9a6005a6fc8fa55015f0caa42052c21', 'image/jpeg', 6541, 1, 0, 0, 0, 0, 0, 14, 2),
(87, 'merch-mugs03.jpg', '9bdc7a70523f393a7363882eb7fdb019', 'image/jpeg', 8703, 1, 0, 0, 0, 0, 0, 14, 3),
(88, 'merch-platter01.jpg', '3d287273cb41c58374bf0332d7643758', 'image/jpeg', 5308, 1, 0, 0, 0, 0, 0, 15, 1),
(89, 'merch-platter02.jpg', 'c1cfd1afe18c0f075743bd98757716a5', 'image/jpeg', 11751, 1, 0, 0, 0, 0, 0, 15, 2),
(90, 'merch-bfw-poster.jpg', '1861220910e796261b3e152c79c56e82', 'image/jpeg', 14745, 1, 0, 0, 0, 0, 0, 16, 1),
(91, 'merch-beer-steins.jpg', '1c711727103d7e5e0a2344bfd9b15d5b', 'image/jpeg', 9633, 1, 0, 0, 0, 0, 0, 17, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
