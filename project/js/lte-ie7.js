/* Use this script if you need to support IE 7 and IE 6. */

window.onload = function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'BFW-Icons\'">' + entity + '</span>' + html;
	}
	var icons = {
			'icon-icon-wisconsin' : '&#xe000;',
			'icon-icon-volunteer' : '&#xe001;',
			'icon-icon-trees' : '&#xe002;',
			'icon-icon-trail-sign' : '&#xe003;',
			'icon-icon-star' : '&#xe004;',
			'icon-icon-search' : '&#xe005;',
			'icon-icon-road-rides' : '&#xe006;',
			'icon-icon-road-bike' : '&#xe007;',
			'icon-icon-restaurants' : '&#xe008;',
			'icon-icon-print' : '&#xe009;',
			'icon-icon-phone' : '&#xe00a;',
			'icon-icon-path' : '&#xe00b;',
			'icon-icon-mt-bike-rides' : '&#xe00c;',
			'icon-icon-milwaukee' : '&#xe00d;',
			'icon-icon-merch' : '&#xe00e;',
			'icon-icon-map-pin' : '&#xe00f;',
			'icon-icon-madison' : '&#xe010;',
			'icon-icon-lodging' : '&#xe011;',
			'icon-icon-local-chapters' : '&#xe012;',
			'icon-icon-event' : '&#xe013;',
			'icon-icon-email' : '&#xe014;',
			'icon-icon-dutch-bike' : '&#xe015;',
			'icon-icon-discount-providers' : '&#xe016;',
			'icon-icon-calendar' : '&#xe017;',
			'icon-icon-bike-wheel' : '&#xe018;',
			'icon-icon-bike-shops' : '&#xe019;',
			'icon-icon-bike-gear' : '&#xe01a;',
			'icon-icon-add-ride-trail' : '&#xe01b;',
			'icon-icon-add-event' : '&#xe01c;',
			'icon-bikefed-logo' : '&#xe01d;',
			'icon-bikefed-logo-rev' : '&#xe01e;'
		},
		els = document.getElementsByTagName('*'),
		i, attr, html, c, el;
	for (i = 0; i < els.length; i += 1) {
		el = els[i];
		attr = el.getAttribute('data-icon');
		if (attr) {
			addIcon(el, attr);
		}
		c = el.className;
		c = c.match(/icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
};