-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 25, 2013 at 02:48 PM
-- Server version: 5.1.51
-- PHP Version: 5.3.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rideonwisconsin`
--

-- --------------------------------------------------------

--
-- Table structure for table `Classifieds`
--

CREATE TABLE IF NOT EXISTS `Classifieds` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `type` varchar(100) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `bikeMake` varchar(100) NOT NULL DEFAULT '',
  `bikeModel` varchar(100) NOT NULL DEFAULT '',
  `partMake` varchar(100) NOT NULL DEFAULT '',
  `partModel` varchar(100) NOT NULL DEFAULT '',
  `condition` varchar(100) NOT NULL DEFAULT '',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `offers` tinyint(1) NOT NULL DEFAULT '0',
  `photo` varchar(255) NOT NULL DEFAULT '',
  `details` text NOT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL DEFAULT '1970-01-01 00:00:01',
  `city` varchar(100) NOT NULL DEFAULT '',
  `state` char(2) NOT NULL DEFAULT 'WI',
  `partType` varchar(100) NOT NULL DEFAULT '',
  `lat` decimal(12,6) NOT NULL DEFAULT '0.000000',
  `lng` decimal(12,6) NOT NULL DEFAULT '0.000000',
  PRIMARY KEY (`id`),
  KEY `type_idx` (`type`),
  KEY `bikeMake_idx` (`bikeMake`),
  KEY `partMake_idx` (`partMake`),
  KEY `condition_idx` (`condition`),
  KEY `price_idx` (`price`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `Classifieds`
--

INSERT INTO `Classifieds` (`id`, `user_id`, `type`, `title`, `bikeMake`, `bikeModel`, `partMake`, `partModel`, `condition`, `price`, `offers`, `photo`, `details`, `approved`, `created`, `modified`, `city`, `state`, `partType`, `lat`, `lng`) VALUES
(1, 9, 'part', 'Sweet Salsa wheelset', 'X', 'X', 'Salsa', 'Gordo', 'gently-used', '70.00', 0, '', 'A newish wheel (act', 1, '2013-05-23 13:41:45', '2013-10-22 15:29:50', 'La Crosse', 'WI', 'wheel', '43.801356', '-91.239581'),
(3, 9, 'bike', 'Green Disc Trucker', 'Surly', 'Disc Trucker', 'XX', 'XX', 'gently-used', '1200.00', 0, '', 'Get it!', 1, '2013-05-23 14:27:50', '2013-08-09 10:23:45', 'Madison', 'WI', '', '43.073052', '-89.401230'),
(4, 1, 'part', 'Phil Wood Hub', 'X', 'X', 'Phil Wood', 'HU308B03', 'new', '130.00', 1, '', 'You need this', 1, '2013-08-06 14:47:56', '2013-08-12 13:36:10', 'Milwaukee', 'WI', 'Hub', '43.038903', '-87.906474'),
(5, 1, 'part', 'Chris King Headset', 'X', 'X', 'Chris King', 'CKG0037', 'new', '100.00', 0, '', 'Your fancy new carbon frame has a massive headtube to quell chatter so you better make sure you installed a Chris King Inset 7 Headset to ensure your steering is as smooth as your freshly-shaved legs. Designed to provide seamless integration between your stem, headtube and fork, the Inset 7 isn''t just an addition to any high-end machineâ€”it''s an upgrade.', 1, '2013-08-06 14:49:19', '2013-08-12 15:59:16', 'Cable', 'WI', 'Headset', '46.207944', '-91.293257'),
(6, 1, 'bike', 'Waterford', 'Waterford', '22-Series', '', '', 'new', '1600.00', 0, '', 'This is a super bike', 1, '2013-08-09 10:00:39', '2013-08-12 14:01:14', 'Madison', 'WI', '', '43.073052', '-89.401230'),
(7, 1, 'bike', 'No Image', 'Fuji', 'Absolute', '', '', 'heavily-used', '150.00', 1, '', 'I rode this a lot', 1, '2013-08-12 14:00:51', '2013-08-12 14:01:18', 'Madison', 'WI', '', '43.073052', '-89.401230');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
