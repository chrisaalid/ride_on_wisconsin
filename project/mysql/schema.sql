-- Schema for Ride On Wisconsin

DROP TABLE IF EXISTS Users;
CREATE TABLE Users (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL DEFAULT '',
  `firstname` varchar(100) NOT NULL DEFAULT '',
  `lastname` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `password` char(40) NOT NULL DEFAULT '',
  `resetkey` char(64) NOT NULL DEFAULT '',
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `developer` tinyint(1) NOT NULL DEFAULT '0',
  `canedit` tinyint(1) NOT NULL DEFAULT '0',
  `canapprove` tinyint(1) NOT NULL DEFAULT '0',
  `state` char(2) NOT NULL DEFAULT 'WI',
  `city` varchar(100) NOT NULL DEFAULT '',
  `profile` TEXT NOT NULL DEFAULT '',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `created` DATETIME NOT NULL,
  `modified` DATETIME NOT NULL DEFAULT '1970-01-01 00:00:01',
  UNIQUE KEY username_idx (`username`),
  UNIQUE KEY email_idx (`email`),
  KEY resetkey_idx (`resetkey`),
  PRIMARY KEY(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `Users` (`id`, `username`, `firstname`, `lastname`, `email`, `password`, `resetkey`, `admin`, `developer`, `canedit`, `canapprove`, `state`, `city`, `profile`, `approved`, `created`, `modified`)
VALUES (1, 'admin', 'Admin', 'User', 'contact@shineunited.com', 'd229546a171a3cbe80029e06f10eb88ff73b1be3', '7d4413cab2f3d0fd6f3c7a06549a5fe80ff7bba83fbe203104a86d725389ea02', 1, 1, 1, 1, 'WI', 'Madison', 'Shine On!', 1, '2013-03-12 18:30:24', '1970-01-01 00:00:01');
INSERT INTO `Users` (`id`, `username`, `firstname`, `lastname`, `email`, `password`, `resetkey`, `admin`, `canedit`, `canapprove`, `state`, `city`, `profile`, `approved`, `created`, `modified`, `developer`) VALUES
(9, 'testuser', 'test', 'user', 'dsmith+testuser@shineunited.com', '47b37c675bb4de7e5c01b7c2a637f4da8fbbcf00', '88a492ea', 0, 0, 0, 'WI', 'Madison', 'Meh!', 1, '2013-05-29 14:33:27', '2013-05-29 16:55:21', 0);

DROP TABLE IF EXISTS Routes;
CREATE TABLE Routes (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `offroad` tinyint(1) NOT NULL DEFAULT '0',
  `surAsphalt` tinyint(1) NOT NULL DEFAULT '0',
  `surLimestone` tinyint(1) NOT NULL DEFAULT '0',
  `surGravel` tinyint(1) NOT NULL DEFAULT '0',
  `surWoodchips` tinyint(1) NOT NULL DEFAULT '0',
  `surDirt` tinyint(1) NOT NULL DEFAULT '0',
  `surSand` tinyint(1) NOT NULL DEFAULT '0',
  `topFlat` tinyint(1) NOT NULL DEFAULT '0',
  `topRolling` tinyint(1) NOT NULL DEFAULT '0',
  `topClimbs` tinyint(1) NOT NULL DEFAULT '0',
  `surUrban` tinyint(1) NOT NULL DEFAULT '0',
  `surRural` tinyint(1) NOT NULL DEFAULT '0',
  `surResidential` tinyint(1) NOT NULL DEFAULT '0',
  `surWooded` tinyint(1) NOT NULL DEFAULT '0',
  `surStream` tinyint(1) NOT NULL DEFAULT '0',
  `surLake` tinyint(1) NOT NULL DEFAULT '0',
  `surPrairie` tinyint(1) NOT NULL DEFAULT '0',
  `difficulty` int(10) NOT NULL DEFAULT '0',
  `comment` TEXT NOT NULL DEFAULT '',
  `points` MEDIUMTEXT NOT NULL DEFAULT '',
  `bounds` varchar(100) NOT NULL DEFAULT '0,0,0,0',
  `distance` DECIMAL(12,6) NOT NULL DEFAULT '0.0',
  `start_lat` DECIMAL(12,6) NOT NULL DEFAULT '0.0',
  `start_lng` DECIMAL(12,6) NOT NULL DEFAULT '0.0',
  `end_lat` DECIMAL(12,6) NOT NULL DEFAULT '0.0',
  `end_lng` DECIMAL(12,6) NOT NULL DEFAULT '0.0',
  `city` varchar(100) NOT NULL DEFAULT '',
  `state` char(2) NOT NULL DEFAULT 'WI',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `created` DATETIME NOT NULL,
  `modified` DATETIME NOT NULL DEFAULT '1970-01-01 00:00:01',
  PRIMARY KEY(`id`),
  KEY user_id_idx (`user_id`),
  KEY start_lat_idx (`start_lat`),
  KEY start_lng_idx (`start_lng`),
  KEY end_lat_idx (`end_lat`),
  KEY end_lng_idx (`end_lng`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS Classifieds;
CREATE TABLE Classifieds (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `type` varchar(100) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `bikeMake` varchar(100) NOT NULL DEFAULT '',
  `bikeModel` varchar(100) NOT NULL DEFAULT '',
  `partMake` varchar(100) NOT NULL DEFAULT '',
  `partModel` varchar(100) NOT NULL DEFAULT '',
  `partType` varchar(100) NOT NULL DEFAULT '',
  `condition` varchar(100) NOT NULL DEFAULT '',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `city` varchar(100) NOT NULL DEFAULT '',
  `state` char(2) NOT NULL DEFAULT 'WI',
  `lat` DECIMAL(12,6) NOT NULL DEFAULT '0.0',
  `lng` DECIMAL(12,6) NOT NULL DEFAULT '0.0',
  `offers` tinyint(1) NOT NULL DEFAULT '0',
  `details` TEXT NOT NULL DEFAULT '',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `created` DATETIME NOT NULL,
  `modified` DATETIME NOT NULL DEFAULT '1970-01-01 00:00:01',
  KEY type_idx (`type`),
  KEY bikeMake_idx (`bikeMake`),
  KEY partMake_idx (`partMake`),
  KEY condition_idx (`condition`),
  KEY price_idx (`price`),
  PRIMARY KEY(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8; 

DROP TABLE IF EXISTS Events;
CREATE TABLE Events (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `start` DATETIME NOT NULL DEFAULT '1970-01-01 00:00:01',
  `end` DATETIME NOT NULL DEFAULT '1970-01-01 00:00:01',
  `venue` varchar(100) NOT NULL DEFAULT '',
  `address` varchar(100) NOT NULL DEFAULT '',
  `state` varchar(100) NOT NULL DEFAULT '',
  `city` varchar(100) NOT NULL DEFAULT '',
  `lat` DECIMAL(12,6) NOT NULL DEFAULT '0.0',
  `lng` DECIMAL(12,6) NOT NULL DEFAULT '0.0',
  `regions` varchar(100) NOT NULL DEFAULT '',
  `tags` varchar(100) NOT NULL DEFAULT '',
  `free` tinyint(1) NOT NULL DEFAULT '0',
  `sponsor` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `phone` varchar(100) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `details` TEXT NOT NULL DEFAULT '',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `created` DATETIME NOT NULL,
  `modified` DATETIME NOT NULL DEFAULT '1970-01-01 00:00:01',
  PRIMARY KEY(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS Locations;
CREATE TABLE Locations (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `type` varchar(100) NOT NULL DEFAULT '',
  `address` varchar(100) NOT NULL DEFAULT '',
  `city` varchar(100) NOT NULL DEFAULT '',
  `state` varchar(100) NOT NULL DEFAULT '',
  `description` TEXT NOT NULL DEFAULT '',
  `member` tinyint(1) NOT NULL DEFAULT '0',
  `lat` DECIMAL(12,6) NOT NULL DEFAULT '0.0',
  `lng` DECIMAL(12,6) NOT NULL DEFAULT '0.0',
  `discount` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `created` DATETIME NOT NULL,
  `modified` DATETIME NOT NULL DEFAULT '1970-01-01 00:00:01',
  PRIMARY KEY(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS Merchandise;
CREATE TABLE Merchandise (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,

  `name` varchar(100) NOT NULL DEFAULT '',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `details` TEXT NOT NULL DEFAULT '',
  `url` VARCHAR(255) NOT NULL DEFAULT '',
  `created` DATETIME NOT NULL,
  `position` int(10) NOT NULL DEFAULT '0',
  `modified` DATETIME NOT NULL DEFAULT '1970-01-01 00:00:01',
  
  PRIMARY KEY(`id`),
  KEY position_idx (`position`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS Groups;
CREATE TABLE Groups (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `contact` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `phone` varchar(100) NOT NULL DEFAULT '',
  `venue` varchar(100) NOT NULL DEFAULT '',
  `address` varchar(100) NOT NULL DEFAULT '',
  `state` varchar(100) NOT NULL DEFAULT '',
  `city` varchar(100) NOT NULL DEFAULT '',
  `lat` DECIMAL(12,6) NOT NULL DEFAULT '0.0',
  `lng` DECIMAL(12,6) NOT NULL DEFAULT '0.0',
  `details` TEXT NOT NULL DEFAULT '',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `created` DATETIME NOT NULL,
  `modified` DATETIME NOT NULL DEFAULT '1970-01-01 00:00:01',
  PRIMARY KEY(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS Billboards;
CREATE TABLE Billboards (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `key` varchar(100) NOT NULL DEFAULT '',
  `notes` TEXT NOT NULL DEFAULT '',
  `markup` TEXT NOT NULL DEFAULT '',
  `created` DATETIME NOT NULL,
  `modified` DATETIME NOT NULL DEFAULT '1970-01-01 00:00:01',
  PRIMARY KEY(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS Templates;
CREATE TABLE Templates (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `model` varchar(100) NOT NULL DEFAULT '',
  `single` tinyint(1) NOT NULL DEFAULT '0',
  `description` TEXT NOT NULL DEFAULT '',
  `tmpl` TEXT NOT NULL DEFAULT '',
  
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS Contents;
CREATE TABLE Contents (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `content` TEXT NOT NULL DEFAULT '',
  
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS Uploads;
CREATE TABLE Uploads (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `filename` varchar(200) NOT NULL DEFAULT '',
  `order` int(10) unsigned NOT NULL DEFAULT '0',
  `md5` char(32) NOT NULL DEFAULT '',
  `mime_type` varchar(50) NOT NULL DEFAULT 'application/octet-stream',
  `size` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `location_id` int(10) unsigned NOT NULL DEFAULT '0',
  `event_id` int(10) unsigned NOT NULL DEFAULT '0',
  `classified_id` int(10) unsigned NOT NULL DEFAULT '0',
  `route_id` int(10) unsigned NOT NULL DEFAULT '0',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `merchandise_id` int(10) unsigned NOT NULL DEFAULT '0',
	`billboard_id` int(10) unsigned NOT NULL DEFAULT '0',
  
  PRIMARY KEY (`id`),
  KEY user_id_idx (`user_id`),
  KEY location_id_idx (`location_id`),
  KEY event_id_idx (`event_id`),
  KEY classified_id_idx (`classified_id`),
  KEY route_id_idx (`route_id`),
  KEY group_id_idx (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS Resources;
CREATE TABLE Resources (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `filename` varchar(200) NOT NULL DEFAULT '',
  `md5` char(32) NOT NULL DEFAULT '',
  `mime_type` varchar(50) NOT NULL DEFAULT 'application/octet-stream',
  `size` int(10) unsigned NOT NULL DEFAULT '0',
  
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS Settings;
CREATE TABLE Settings (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(200) NOT NULL DEFAULT '',
  `name` varchar(200) NOT NULL DEFAULT '',
  `data_type` varchar(30) NOT NULL DEFAULT 'string',
  `description` TEXT NOT NULL DEFAULT '',
  `content` TEXT NOT NULL DEFAULT '',
  `created` DATETIME NOT NULL,
  `modified` DATETIME NOT NULL DEFAULT '1970-01-01 00:00:01',
  
  PRIMARY KEY (`id`),
  KEY key_idx (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `cake_sessions`;
CREATE TABLE IF NOT EXISTS `cake_sessions` (
  `id` varchar(255) NOT NULL DEFAULT '',
  `data` text,
  `expires` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
